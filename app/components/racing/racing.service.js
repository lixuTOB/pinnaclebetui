(function(){
    'use strict';

    angular
        .module('pinnacleBetApp')
        .factory('racingService', racingService);

    racingService.$inject = ['$q', '$http', '$timeout', 'EVN', 'util', 'mainService'];

    function racingService($q, $http, $timeout, EVN, util, mainService){
    	var self = this;

	    this.nameMap = {
	        "TobRacing": {
	            /* "W": "STAB", */
	            "W": "Win",
	            /* "P": "STAB", */
	            "P": "Place",
	            "W_TIPS": "Tote Win",
	            "P_TIPS": "Tote Place"
	        },
	        "TopSport": {
	            "W": "Win",
	            "P": "Place",
	            /* "BT3W": "Best of 3 Totes", */
	            "BT3W": "Win",
	            /* "BT3SP": "Best of 3 Totes + SP", */
	            "BT3SP": "Win",
	            "WT5": "Win",
	            "PT5": "Place",
	            "BT1W": "Win",
	            "BT1P": "Place",
	            "TF": "TF",
	            "W_TIPS": "Fixed Win",
	            "P_TIPS": "Fixed Place",
	            "BT3W_TIPS": "This product is the best of the 3 Australian TAB prices. For Each-Way bets, the place portion is paid at best tote. For any Place Only bet, all bets are paid at Middle Tote, or the middle dividend as declared by the 3 Australia TAB's",
	            "BT3SP_TIPS": "This product is the best of either the 'Best of the 3 Australian TAB Prices' or the 'Official On-Course Starting Price (SP)'. SP only applies to the Win Portion of the bet. For any Place Only bet, all bets are paid at Middle Tote, or the middle dividend as declared by the 3 Australia TAB's.",
	            "TF_TIPS": "This product is the Best Price declared by the official On-Course Bookmakers Fluctuations. Each Way Betting is only available on certain races; determined at TopSport management discretion, based upon individual market percentages. This option is available until the horse shortens on selected race meetings"
	        },
	        "V": {
	            "W": "STAB",
	            "P": "STAB"
	        },
	        "N": {
	            "W": "NSW",
	            "P": "NSW"
	        },
	        "U": {
	            "W": "UNITAB",
	            "P": "UNITAB"
	        }
	    };

		function modal (ignore, isOn) {
			if (!ignore) {
				if (isOn == 'on') {
					mainService.modalOn();
				} else {
					mainService.modalOff();
				}
			};
		};

		/*  Get race meeting list with date and type filters.
		 *  Used by meetingsModule
		 *	@param mettingType: string - type filter, options: [ALL, R(Thoroughred), G(Greyhounds), T(Harness)]
		 *	@param meetingDate: string - date filter, options: [TODAY, TOMORROW, ...]
		*/
		this.getMeetingList = function(mettingType, meetingDate, isSilence, timeoutMilli){
			var url = '';
			modal(isSilence, 'on');
			if(meetingDate == 'TODAY' || meetingDate == 'TOMORROW')
				url = EVN.apiDataUrl + '/data/racing/meetingList/' + mettingType + '/' + meetingDate;
			else
				url = EVN.apiDataUrl + '/data//racing/meetingList/' + mettingType + '/date/' + meetingDate;
			var deferred = $q.defer();
			var tempID = undefined;
			var returnVal = $http.get(url, {timeout: deferred.promise}).then(function (res) {
				modal(isSilence, 'off');
				if(tempID) {
					$timeout.cancel(tempID);
				}
				if (mainService.handleResponse(res.data)) {
					return res.data;
				}
				else {
					return undefined;
				}
			}, function(reject) {
				if(tempID) {
					$timeout.cancel(tempID);
				}
				return undefined;
			});
			if(timeoutMilli) {
				tempID = $timeout(function() {
				    deferred.resolve();
				}, timeoutMilli);
			}
			return returnVal;
		};

		this.getMeetingDetails = function(meetingID, isSilence){
			modal(isSilence, 'on');
			return $http.get(EVN.apiDataUrl + "/data/racing/meeting/" + meetingID).then(function (res) {
				modal(isSilence, 'off');
				if (mainService.handleResponse(res.data) && res.data.meeting) {
					return res.data.meeting;
				}
				else {
					return undefined;
				}
			});
		};

		this.getMeetingNotice = function(meetingID, isSilence){
			modal(isSilence, 'on');
			return $http.get(EVN.apiDataUrl + "/data/racing/meeting/" + meetingID).then(function (res) {
				modal(isSilence, 'off');
				return res.data;
			});
        };

		/**
		 *  meetingID:
		 */
        this.getRaceList = function(meetingID, isSilence){
			modal(isSilence, 'on');
			return $http.get(EVN.apiDataUrl + "/data/racing/meeting/" + meetingID).then(function (res) {
				modal(isSilence, 'off');
				if (res.data && res.data.meeting) {
					return res.data.meeting.races;
				}
				else {
					return undefined;
				}
			});
		};

		/**
		 *  meetingID:
		 *	raceNum:
		 */
		this.getRaceDetails = function(meetingID, raceNum, isSilence){
			modal(isSilence, 'on');
			return $http.get(EVN.apiDataUrl + '/data/racing/race/' + raceNum + '/WP').then(function (res) {
				modal(isSilence, 'off');

				if (mainService.handleResponse(res.data) && res.data.race) {
					return res.data.race;
				}
				else {
					return undefined;
				}
			});
		};

		/**
		 *
		 *	meetingID:
		 *	raceNum:
		 *	betMethod: string
		 */
		this.getRaceOdds = function (meetingID, raceNum, betMethod, isResultRequest, isSilence){
			if (isResultRequest != false && !isResultRequest) {
				isResultRequest = false;
			};

			modal(isSilence, 'on');
			if (!betMethod) {
				betMethod = "WinPlace"
			};
			switch(betMethod) {
				case "WinPlace":
					betMethod = "WP";
				break;
				case "Quinella":
					betMethod = isResultRequest ? "Q" : "WP";
					// betMethod = "Q";
				break;
				case "Exacta":
					betMethod = isResultRequest ? "E" : "WP";
					// betMethod = "E";
				break;
				case "Trifecta":
					betMethod = isResultRequest ? "T" : "WP";
					// betMethod = "T";
				break;
				case "FirstFour":
					betMethod = isResultRequest ? "FF" : "WP";
					// betMethod = "FF";
				break;
				case "Quadrella":
					betMethod = isResultRequest ? "QD" : "WP";
					// betMethod = "QD";
				break;
			}
			return $http.get(EVN.apiDataUrl + '/data/racing/race/' + raceNum + '/' + betMethod).then(function (res) {
				modal(isSilence, 'off');
				if (mainService.handleResponse(res.data)) {
					return res.data.race;
				}
				else {
					return undefined;
				}
			});
		};

		this.getRaceWagersByRaceId = function (raceId, isSilence) {
			modal(isSilence, 'on');
			return $http.get(EVN.apiDataUrl + '/data/wager/racewagers/' + raceId).then(function (res) {
				modal(isSilence, 'off');
				if (mainService.handleResponse(res.data)) {
					return res.data.wagersForRaceForLogin;
				}
				else {
					return undefined;
				}
			});
		};

		this.getRaceWagerStatus = function (wagerId, isSilence) {
			modal(isSilence, 'on');
			return $http.get(EVN.apiDataUrl + '/data//wager/race/' + wagerId).then(function (res) {
				modal(isSilence, 'off');
				if (mainService.handleResponse(res.data)) {
					return res.data.wager;
				}
				else {
					return undefined;
				}
			});
		};

		this.submitRaceWagers = function (requests, isSilence){
			modal(isSilence, 'on');
			return $http({
				method: "POST",
				url: EVN.apiDataUrl + '/data/wager/race',
				header: {
					"content-type": "application/json; charset=utf-8"
				},
				data: {
                    toteWagerRequests: requests.toteWagerRequests,
                    fixedOddsWagerRequests: requests.fixedOddsWagerRequests
				}
			}).then(function (res) {
                return res.data;
			});
		};

		this.temporalPriceUpdate = function (raceId){
			return $http.put(app_url2 + '/data/race/'+raceId).then(function (res) {

                return res.data;
			});
        };

        this.resubmitRaceBet = function (wagerId, fixedOdds, isSilence){
			modal(isSilence, 'on');
			return $http.post(EVN.apiDataUrl + '/data/wager/resubmitRaceWager', {
				wagerId: wagerId,
				fixedOdds: fixedOdds
			}).then(function (res) {
				modal(isSilence, 'off')
				if (mainService.handleResponse(res.data)) {
					return res.data;
				}
				else {
					return undefined;
				}
			});
		};

		this.getMultiLegsByMeetingID = function (meetingId, isSilence){
			modal(isSilence, 'on');
			return $http.get(EVN.apiDataUrl + '/data/racing/meeting/multilegs/' + meetingId).then(function (res) {
				modal(isSilence, 'off');
				if (mainService.handleResponse(res.data)) {
					return res.data.meeting;
				};
			});
		};

		/**
		 *  Get next to jump races
		 */
		this.getNextRaces = function (timeoutMilli){
			var deferred = $q.defer();
			var tempID = undefined;
			var returnVal = $http.get(EVN.apiDataUrl + '/data/racing/nextraces', {timeout: deferred.promise}).then(function (res) {
				if(tempID) {
					$timeout.cancel(tempID);
				}
				if (mainService.handleResponse(res.data)) {
					return res.data;
				}
				else {
					return undefined;
				}
			}, function(reject) {
				if(tempID) {
					$timeout.cancel(tempID);
				}
				return undefined;
			});
			if(timeoutMilli) {
				tempID = $timeout(function() {
				    deferred.resolve();
				}, timeoutMilli);
			}
			return returnVal;
		};

		/**
		 *  Get next to jump races
		 */
		this.getSidebarRaces = function (timeoutMilli){
			var deferred = $q.defer();
			var tempID = undefined;
			var returnVal = $http.get(EVN.apiDataUrl + '/data/racing/allraces', {timeout: deferred.promise}).then(function (res) {
				if(tempID) {
					$timeout.cancel(tempID);
				}
				if (mainService.handleResponse(res.data)) {
					return res.data;
				}
				else {
					return undefined;
				}
			}, function(reject) {
				if(tempID) {
					$timeout.cancel(tempID);
				}
				return undefined;
			});
			if(timeoutMilli) {
				tempID = $timeout(function() {
				    deferred.resolve();
				}, timeoutMilli);
			}
			return returnVal;
		};

		this.getServerTime = function(){
			return $http.get(EVN.apiDataUrl + '/data/racing/servertime').then(function (res) {
				if (mainService.handleResponse(res.data, true)) {
					return res.data.time;
				}
				else {
					return undefined;
				}
			});
		}

	    this.getOdds = function(entrant, poolObj) {
	        var isResultPool = (poolObj && (poolObj.bookmaker.id == EVN.NSW_TOTE ||
        		poolObj.bookmaker.id == EVN.UNITAB_TOTE || poolObj.bookmaker.id == EVN.STAB_TOTE));
	        if (isResultPool) {
	            var isRunnerWithPosition = (entrant.placing == 3 ||
            		entrant.placing == 2 || entrant.placing == 1);
	            if (isRunnerWithPosition) {
	                var outcomeExisted = self.isOutcomeExisted(entrant, poolObj);
	                if (!outcomeExisted) {
	                    if (entrant.placing == 3) {
	                        return EVN.NO_THIRD_DIVIDEND;
	                    }
	                    if (entrant.placing == 2) {
	                        return EVN.NO_SECOND_DIVIDEND;
	                    }
	                    if (entrant.placing == 1) {
	                        return EVN.NO_DIVIDEND;
	                    }
	                }
	            }
	        }

	        if (!poolObj) {
	            return '-';
	        };
	        var pool = poolObj;
	        if (pool.entrantOdds.length > 0) {
	            for (var i = 0; pool && i < pool.entrantOdds.length; i++) {
	                var odd = pool.entrantOdds[i];
	                if (odd.firstEntrant.id == entrant.id) {
	                    if (self.isNumber(odd.odds) && odd.odds == 100) {
	                        //|| poolObj.poolTypeId == "BT3SP" || poolObj.poolTypeId == "BT3W" removed it as it is not needed
	                        // only for TF/BT3SP pooltype we need to check whether it is 1.00 and display TF
	                        if(poolObj.poolTypeId == 'TF'){
	                            return "TF";
	                        }
	                    }
	                    if (!self.isNumber(odd.odds)){
	                        return odd.odds;
	                    }
	                    return util.oddsFromPrice(odd.odds, EVN.RACE_ODDS_PRECISION, 2);
	                }
	            }
	        } else {
	            for (var i = 0; i < pool.outcomes.length; i++) {
	                var o = pool.outcomes[i];
	                if (entrant.rugNumber == o.rugNumbers[0]) {
	                    return util.oddsFromPrice(o.dividend, EVN.RACE_ODDS_PRECISION, 2);
	                };
	            };
	        }
	        return "-";
	    };

        this.getRaceOddsObj = function(entrant, pool){
            return util.getRaceOddsObj(entrant, pool);
        }

	    /**
	     * Check if the outcome of a specific runner in a specific pool exists or not
	     */
	    this.isOutcomeExisted = function(entrant, poolObj) {
	        var isExisted = false;

	        for (var i = 0; i < poolObj.outcomes.length; i++) {
	            var outcome = poolObj.outcomes[i];
	            if (outcome && outcome.dividend && (outcome.dividend > 100) && (entrant.rugNumber == outcome.rugNumbers[0])) {
	                isExisted = true;
	                break;
	            }
	        }

	        return isExisted;
	    }

	    /**
	     * Separate the outcome getter from the above odds getter
	     */
	    this.getOutcome = function(entrant, poolObj) {
	        if (!poolObj) {
	            return '-';
	        };

	        var pool = poolObj;
	        for (var i = 0; i < pool.outcomes.length; i++) {
	            var o = pool.outcomes[i];
	            if (entrant.rugNumber == o.rugNumbers[0]) {
	                return util.oddsFromPrice(o.dividend, EVN.RACE_ODDS_PRECISION, 2);
	            }
	        }

	        return "-";
	    }

	    this.getFlucts = function(entrant, fluctuations) {
	    	if (fluctuations || typeof fluctuations != undefined) {
		        for (var i = 0; i < fluctuations.length; i++) {
		            if (fluctuations[i].entrantId == entrant.id) {
		                return {
		                    top_fluct: fluctuations[i].fluc_top,
		                    flucs: fluctuations[i].flucs
		                };
		            }
		        }
	    	}
	        return {
	            top_fluct: -1,
	            flucs: []
	        };
	    }

	    this.getColumnName = function(poolObj, nameMap, isWinPlace) {
	        if (!poolObj) {
	            return;
	        };
	        var returnJson = {
	            bookmakerId: poolObj.bookmaker.id,
	            poolTypeId: poolObj.poolTypeId,
	            displayName: poolObj.poolTypeId
	        };
	        if (nameMap[poolObj.bookmaker.id] && nameMap[poolObj.bookmaker.id][poolObj.poolTypeId]) {
	            returnJson.displayName = nameMap[poolObj.bookmaker.id][poolObj.poolTypeId];
	        }
	        if (nameMap[poolObj.bookmaker.id] && nameMap[poolObj.bookmaker.id][poolObj.poolTypeId + "_TIPS"])
	            returnJson.tooltip = nameMap[poolObj.bookmaker.id][poolObj.poolTypeId + "_TIPS"];
	        return returnJson;
	    }

	    this.isNumber = function(s) {
	        return !isNaN(parseInt(s));
	    }

	    this.isHeader = function(header) {
	        return header == 'true';
	    }

	    this.isScratched = function(header, entrant) {
	        if (!entrant) {
	            return;
	        };
	        return (entrant.scratched || entrant.lateScratching);
	    }

	    this.silkImage = function(url) {
	        if(url){
	            return {
	                "background-repeat": "no-repeat",
	                "background-size": "24px 24px",
	                "background-image": 'url('+url+')',
	                "margin": "0 auto",
	                "height": "30px",
	                "width": "30px"
	            }
	        }else{
	            return {

	            }
	        }
	    }

	    this.colorUpdate = function(entrant, pool) {
	        for (var i = 0; i < pool.entrantOdds.length; i++) {
	            var o = pool.entrantOdds[i];
	            if (o.firstEntrant.id == entrant.id) {
	                if (!o.__lastUpdate) {
	                    break;
	                };
                    entrant[pool.bookmaker.id+pool.poolTypeId] = o.__lastUpdate;
	                if (o.__lastUpdate === 'higher') {
	                    return {
                            'lower': false,
            	            'status_quo': false,
            	            'higher': true
	                    };
	                };
	                if (o.__lastUpdate === 'lower') {
	                    return {
                            'lower': true,
            	            'status_quo': false,
            	            'higher': false
	                    }
	                }
	            }
	        }
	        return {
	            'lower': false,
	            'status_quo': true,
	            'higher': false
	        }
	    }
        this.exoticsSelection = function exoticsSelection(arr){
            var res = [];
            for(var i = 0; i < arr.length; i++){
                switch (i) {
                    case 0:
                        res.push('1st: ' + arr[i].toString().replace(/,/g ,'+'));
                        break;
                    case 1:
                        res.push('2nd: ' + arr[i].toString().replace(/,/g ,'+'));
                        break;
                    case 2:
                        res.push('3rd: ' + arr[i].toString().replace(/,/g ,'+'));
                        break;
                    default:
                        res.push((i + 1) + 'th: ' + arr[i].toString().replace(/,/g ,'+'));
                        break;
                }

            }

            return res;
        }

        /* Translate the 2D boolean table to index table for combination calculation.
         * @Param [[bool]] 2D boolean table. eg.[[true, false, true],[false, true, false], [true, false, false]]
         * @Return [[number]] indexes for true values. eg.[[0,2],[1], [0]]
         */
        this.tableTranslation = function tableTranslation(bool_table_2D, isBox) {
            var resultTable = [];
            for (var i = 0; i < bool_table_2D.length; i++) {
                var tempRow = [];
                for (var j = 0; j < bool_table_2D[i].length; j++) {
                    if (bool_table_2D[i][j])
                        tempRow.push(j+1);
                }
                resultTable.push(tempRow);
            }
            if(isBox){
                for(var i = 1 ; i< resultTable.length; i++){
                    resultTable[i] = resultTable[0];
                }
            }
            return resultTable;

        }

        /* Recursivly get all valid combinations with no duplicates from two dimentional list of checked boxes.
         * @param [[Object]]  - Two dimentional array. eg.[[1,3,5],[8],[1,4,6]]
         * @return [[Object]]] - All combination string. eg.[[1,8,4],[1,8,6],[3,8,1],[3,8,4],[3,8,6],[5,8,1],[5,8,4],[5,8,6]]
         */
        this.getAllCombinations = function getAllCombinations(arr) {
            if (!arr[0] || arr[0] == null)
                return [];
            if (arr.length == 1) {
                var array = [];
                for (var i = 0; i < arr[0].length; i++) {
                    array.push([arr[0][i]]);
                }
                return array;
            } else {
                var result = [];
                // recursive call with the rest of array
                var restArray = getAllCombinations(arr.slice(1));
                for (var i = 0; i < restArray.length; i++) {
                    for (var j = 0; j < arr[0].length; j++) {
                        var newArray = restArray[i].slice(0);
                        var valid = true;
                        for (var k = 0; k < newArray.length; k++) {
                            if (newArray[k] == arr[0][j]) {
                                valid = false;
                                break;
                            }
                        }
                        if (valid) {
                            newArray.splice(0, 0, (arr[0][j]));
                            result.push(newArray);
                        }
                    }
                }
                return result;
            }
        }

        this.getAllCombinationsForMultiLegs = function getAllCombinationsForMultiLegs(arr) {
            if (arr.length == 1) {
                var array = [];
                for (var i = 0; i < arr[0].length; i++) {
                    array.push([arr[0][i]]);
                }
                return array;
            } else {
                var result = [];
                // recursive call with the rest of array
                var restArray = getAllCombinationsForMultiLegs(arr.slice(1));
                for (var i = 0; i < restArray.length; i++) {
                    for (var j = 0; j < arr[0].length; j++) {
                        var newArray = restArray[i].slice(0);
                        newArray.splice(0, 0, (arr[0][j]));
                        result.push(newArray);
                    }
                }
                return result;
            }
        }
		return this;
	}

})();
