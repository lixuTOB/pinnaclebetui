(function(){
    'use strict';

    angular
        .module('pinnacleBetApp')
//        .config(function (datepickerConfig) {
//    	  datepickerConfig.showWeeks = false;
//    	})
        .controller('racingMeetingsCtrl', racingMeetingsCtrl);

    racingMeetingsCtrl.$inject = ['$scope', '$rootScope','$stateParams', '$location', '$interval', 'racingService', 'EVN', 'util'];

    function racingMeetingsCtrl($scope,$rootScope, $stateParams, $location, $interval, racingService, EVN, util){
		$scope.raceTypeMap = {
			'R'	: 'Thoroughbred',
			'T'	: 'Harness',
			'G'	: 'Greyhounds'
		};
        util.sidebarView('RACING',$rootScope);


		/* Set update meeting list data interval as 60 seconds */
		var meetingListIntervalID, meetingListInterval = EVN.racing_meetingListIntervalID;

		$scope.switcher = {
			R: {
				isDisplay: $scope.meetingType == 'ALL' || $scope.meetingType == 'R',
				isOpen: $scope.meetingType == 'ALL' || $scope.meetingType == 'R'
			},
			G: {
				isDisplay: $scope.meetingType == 'ALL' || $scope.meetingType == 'G',
				isOpen: $scope.meetingType == 'ALL' || $scope.meetingType == 'G'
			},
			T: {
				isDisplay: $scope.meetingType == 'ALL' || $scope.meetingType == 'T',
				isOpen: $scope.meetingType == 'ALL' || $scope.meetingType == 'T'
			}
		};

		/* Clear all the interval when scope is destroyed */
		$scope.$on('$destroy', function () {
			if (meetingListIntervalID) {
				$interval.cancel(meetingListIntervalID);
			};
			update_event_canceller();
		});

		/* Set default fields in case the params are not set */
		$scope.meetingType = 'ALL';
		$scope.meetingDate = 'TODAY';

		/* Date options contains all of the possible candidates for meetingDate.
		 * Set as static for now and to be changed when more date options are introduced.
		 */
		$scope.meetingDateOptions = ['TODAY', 'TOMORROW'];

		/* Set meetingType from path parameter */
		if ($stateParams.meetingType) {
			$scope.meetingType = $stateParams.meetingType;
		};

		/* Set meetingDate from path parameter */
		if ($stateParams.meetingDate) {
			$scope.meetingDate = $stateParams.meetingDate;
		};

		/* Initialize the meeting table data */
		$scope.meetingList = {
			R: 	{
					meetingTypeClass: 'T',
					meetingDetails: undefined,
					mobileProperties:  {
						dayFilterOptions: [{text: 'TODAY'}, {text: 'TOMORROW'}],
						dayFilterSelected: undefined,
						venueFilterOptions: [{text: 'All Venues', meeting_races: undefined}],
						venueFilterSelected: {text: 'All Venues', meeting_races: undefined}
					}
				},
			G:  {
					meetingTypeClass: 'G',
					meetingDetails: undefined,
					mobileProperties:  {
						dayFilterOptions: [{text: 'TODAY'}, {text: 'TOMORROW'}],
						dayFilterSelected: undefined,
						venueFilterOptions: [{text: 'All Venues', meeting_races: undefined}],
						venueFilterSelected: {text: 'All Venues', meeting_races: undefined}
					}
			    },
			T:  {
					meetingTypeClass: 'H',
					meetingDetails: undefined,
					mobileProperties:  {
						dayFilterOptions: [{text: 'TODAY'}, {text: 'TOMORROW'}],
						dayFilterSelected: undefined,
						venueFilterOptions: [{text: 'All Venues', meeting_races: undefined}],
						venueFilterSelected: {text: 'All Venues', meeting_races: undefined}
					}
			}};

		/* The number of columns for the meeting list depending on the max(raceNumber). */
		$scope.range = 12;

		/* Get range as an array for angular ng-repeat */
		$scope.getRange = function() {
			return new Array($scope.range);
		}

		/* Initialize the angular bootstrap calendar data
		 * Refer to: http://angular-ui.github.io/bootstrap/#/datepicker
		 */
		if($scope.meetingDate != 'TODAY' && $scope.meetingDate != 'TOMORROW'){
			// var date = new Date($scope.meetingDate);
			$scope.calendar = {
				dateSelected: undefined,
				dateDisplay: parseDateParamToDisplay($scope.meetingDate),
				isOpen: false,
				minDate: '2010-01-01',
				maxDate: '2020-12-31',
				showWeeks: false
			};
		}
		else {
			$scope.calendar = {
				dateSelected: undefined,
				dateDisplay: 'Select by Date',
				isOpen: false,
				minDate: '2010-01-01',
				maxDate: '2020-12-31',
				showWeeks: false
			};
		}

		/* Show calendar */
		$scope.popupCalendar = function() {
			$scope.calendar.isOpen = true;
		}

		/* Redirect to the new page when clicking the date in calendar */
		$scope.redirectPath = function() {
			var date = $scope.calendar.dateSelected;
	   		var path = '#/racing?meetingDate=' + parseDateToParam(date);
			window.location.href = path;
		}

        /* Transform date to parameter format (YYYYMMDD)
         * eg. Date(20140101) to "20140101"
         */
        function parseDateToParam(date) {
            var yyyy = date.getFullYear().toString();
            var mm = (date.getMonth() + 1).toString();
            var dd  = date.getDate().toString();
            return yyyy + (mm[1] ? mm : "0" + mm[0]) + (dd[1] ? dd : "0" + dd[0]);
        }



		//  //Listen to the window size change event and dynamically change the template.
		window.onresize = function () {
			$scope.$apply();
		}

		/* listen meeting messaging */
		var eventsLastUpdate = util.getTime().msec();
		var update_event_canceller = $scope.$on(EVN.meeting_update_event_server_addr, function (e, data) {
			data = angular.copy(data);
			eventsLastUpdate = util.getTime().msec();
			var meetingList = angular.copy($scope.meetingList);
			for(var type in meetingList) {
				for(var nation in meetingList[type].meetingDetails) {
					for (var i = 0; i < meetingList[type].meetingDetails[nation].length; i++) {
						var m = meetingList[type].meetingDetails[nation][i];
						if (m.id == data.id) {
							var __races = buildRaces(data);
							for (var n = 0; n < __races.length; n++) {
								var dr = __races[n];
								for (var p = 0; dr.statusClass != "NoRace" && p < m.races.length; p++) {
									var r = m.races[p];
									if (r.id == dr.id) {
										m.races[p] = dr;
									};
								};
							};
						};
					};
				}
			}
			$scope.meetingList = meetingList;
		});

		/* Get meetingList from service
		 */
		$scope.getMeetingList = function(meetingType, meetingDate, isSilence) {
			racingService.getMeetingList(meetingType, meetingDate, isSilence, EVN.AJAX_TIMEOUT).then(function (data) {
				if (data){
					$scope.range = data.raceMaximumNumber;
					// $scope.maxTdWidth = 84 / $scope.range + '%';
					//Put data into the meetingList to display as per order of Thoroughbred, Greyhounds and Harness.
					if(meetingType == 'ALL') {
						buildMeetings(data, 'R', meetingDate);
						buildMeetings(data, 'T', meetingDate);
						buildMeetings(data, 'G', meetingDate);
					}
					else
						buildMeetings(data, meetingType, meetingDate);
				}
			});
		};

		/* Get meeting list data */
		function loadMeetingList(meetingType, meetingDate) {
			var dt = util.getTime().msec() - eventsLastUpdate;
			if (dt > meetingListInterval) {
				racingService.getMeetingList(meetingType, meetingDate, true, EVN.AJAX_TIMEOUT).then(function (data) {
					if (data){
						// Put data into the meetingList to display as per order of Thoroughbred, Greyhounds and Harness.
						if(meetingType == 'ALL') {
							buildMeetings(data, 'R', meetingDate);
							buildMeetings(data, 'T', meetingDate);
							buildMeetings(data, 'G', meetingDate);
						}
						else
							buildMeetings(data, meetingType, meetingDate);
					}
					if(meetingListIntervalID)
						$interval.cancel(meetingListIntervalID);
					meetingListIntervalID = $interval(function () {
						loadMeetingList(meetingType, meetingDate);
					}, meetingListInterval);
				});
			} else {
				if(meetingListIntervalID)
					$interval.cancel(meetingListIntervalID);
				meetingListIntervalID = $interval(function () {
					loadMeetingList(meetingType, meetingDate);
				}, meetingListInterval);
			}
		};

		$scope.getMeetingList($scope.meetingType, $scope.meetingDate, false);

        meetingListIntervalID = $interval(function () {
			loadMeetingList($scope.meetingType, $scope.meetingDate);
		}, meetingListInterval);


		function buildMeetings(data, type, date) {
			if(data.meetings[type]){
				//venue list for mobile filters
				var venueList = [{text: 'All Venues', meeting_races: undefined}];
				var meetingDetails = data.meetings[type];
				if(meetingDetails.AUS){
					for(var i = 0; i < meetingDetails.AUS.length; i ++) {
						var tempRaces = buildRaces(meetingDetails.AUS[i]);
						//Add venue state to the meeting.
						if(meetingDetails.AUS[i].races[0] && meetingDetails.AUS[i].races[0].venueState != '')
							meetingDetails.AUS[i].venueState = meetingDetails.AUS[i].races[0].venueState.trim().toUpperCase();
						meetingDetails.AUS[i].races = tempRaces;
						meetingDetails.AUS[i].tooltip = buildMeetingToolTip(meetingDetails.AUS[i]);
						venueList.push({text: meetingDetails.AUS[i].venue, meeting_races: meetingDetails.AUS[i].races});
					}
				}
				if(meetingDetails.INT) {
					for(var i = 0; i < meetingDetails.INT.length; i ++) {
						var tempRaces = buildRaces(meetingDetails.INT[i]);
						var venue_country = meetingDetails.INT[i].venue.split(' - ');
						if(venue_country.length == 2){
							meetingDetails.INT[i].venue = venue_country[0];
							meetingDetails.INT[i].country = venue_country[1];
						}
						meetingDetails.INT[i].country = meetingDetails.INT[i].country.toUpperCase();
						meetingDetails.INT[i].races = tempRaces;
						meetingDetails.INT[i].tooltip = buildMeetingToolTip(meetingDetails.INT[i]);
						venueList.push({text: meetingDetails.INT[i].venue + ' - ' + meetingDetails.INT[i].country, meeting_races: meetingDetails.INT[i].races});
					}
				}
				$scope.meetingList[type].meetingDetails = meetingDetails;
				$scope.meetingList[type].mobileProperties.venueFilterOptions = venueList;
				$scope.meetingList[type].mobileProperties.dayFilterSelected = {text: date}
			}
			else{
				$scope.meetingList[type].meetingDetails = undefined;
				$scope.meetingList[type].mobileProperties.venueFilterOptions = [{text: 'All Venues', meeting_races: undefined}];
				$scope.meetingList[type].mobileProperties.dayFilterSelected = {text: date}
			}
			$scope.meetingList[type].mobileProperties.venueFilterSelected = {text: 'All Venues', meeting_races: undefined};
		};


		$scope.buildMeetingTip = buildMeetingTip;
		function buildMeetingTip(meeting){
			function changeDateFormat(date){
				var year = date.split("")[0] + date.split("")[1] + date.split("")[2] + date.split("")[3];
				var month = date.split("")[4] + date.split("")[5];
				var day = date.split("")[6] + date.split("")[7];
				var result = day + '-' + month + '-' + year;
				return result;
			}
			var date = changeDateFormat(meeting.date);
			var venue = meeting.venue;
			var type = $scope.raceTypeMap[meeting.type];
			var country = meeting.country;
			var html = 'Date: ' + date + '\n' + 'Venue: ' + venue + '\n'  + 'Type: ' + type + '\n'  + 'Country: ' + country;
			return html;
		}

		$scope.buildRaceTip = buildRaceTip;
		function buildRaceTip(race){
			var raceName = race.raceNumber ? 'Race '  + race.raceNumber + ' - ' + race.title  : 'Race: Not Available';
			var distance = race.distanceInMeters ? 'Distance: ' + race.distanceInMeters + 'm' : 'Distance: Not Available';
			var track = (race.trackCondition && !race.trackCondition.match(/^\s*$/)) ? 'Track: ' + race.trackCondition : 'Track: Not Available';
			var weather = (race.weatherCondition && !race.weatherCondition.match(/^\s*$/)) ? 'Weather: ' + race.weatherCondition : 'Weather : Not Available';
			var starttime = race.startTime ? 'StartTime: ' + util.getTime(race.startTime).yymmdd + ' ' + util.getTime(race.startTime).mmss : 'StartTime: Not Available';
			var result = raceName + '\n' + distance + '\n' + track + '\n' + weather + '\n' + starttime;
			return result;
		}

		$scope.lessFiveMins = lessFiveMins;
		function lessFiveMins(time){
			var timestamp=new Date().getTime();
			if(time - timestamp < 300000){
				return true;
			}else{
				return false;
			}
		}

		$scope.lessThirtyMins = lessThirtyMins;
		function lessThirtyMins(time){
			var timestamp = new Date().getTime();
			if(time - timestamp < 1800000 && time - timestamp > 300000){
				return true;
			}else{
				return false;
			}
		}
		function minDisplay(time){
			var display = '';
			var timestamp = new Date().getTime();
			if(time - timestamp >= 0 && time - timestamp <60000){
				display = '< 1 m';
			}else if(time - timestamp > 60000){
				display = ((time - timestamp)/1000/60).toFixed(0) + ' m';
			}else if(time - timestamp < 0){
				display = ((time - timestamp)/1000/60).toFixed(0) + ' m';
			}
			return display;
		}



		function buildRaces(meeting) {
			var tempRaces = [];
			var diff = 1;
			//Feed the races into the list as per raceNumber and fill the empty race with {status: "None"}
			for(var j = 0; j < $scope.range; j ++) {
				var tempRace = meeting.races[j];
				if(tempRace) {
					var index = j;
					while(tempRace.raceNumber - index > diff) {
						tempRaces.push({statusClass: "NoRace", url: "", textDisplay: "-"});
						index ++;
					}
					//Build tooltip for the race.
					var dateString = util.formatDateString(tempRace.startTime);
					dateString = dateString.substring(dateString.length - 11);
					var weather = "Not available";
					if(tempRace.weatherCondition != null && tempRace.weatherCondition != undefined && tempRace.weatherCondition.trim() != "") {
						weather = tempRace.weatherCondition;
					}
					var track = "Not available";
					if(tempRace.trackCondition != null && tempRace.trackCondition != undefined && tempRace.trackCondition.trim() != "") {
						track = tempRace.trackCondition;
					}
					tempRace.tooltip =
						'<div>Race ' + tempRace.raceNumber + ' - ' + tempRace.title + '</div>' +
						'<div>Distance: ' + tempRace.distanceInMeters + 'm</div>' +
						'<div>Track: ' + track + '</div>' +
						'<div>Weather: ' + weather + '</div>' +
						'<div>Start Time: ' + util.parseStartTime(tempRace.startTime) + dateString + '</div>';
					tempRace.statusClass = tempRace.status;
					tempRace.url = '#/racing/meeting/' + tempRace.meetingId + '/' + tempRace.id;
					tempRace.textDisplay = getTextDisplay(tempRace);
					tempRace.fixedOdds = tempRace.hasFixedOddsPool;
					tempRaces.push(tempRace);
					diff += index - j;
				}
				else {
					if(tempRaces.length < $scope.range)
						tempRaces.push({statusClass: "NoRace", url: "", textDisplay: "-"});
				}
			}
			return tempRaces;
		};

		/* Get display text for a race in meeting table cells */
		function getTextDisplay(tempRace) {
			var textDisplay = '';
			/* For Open races
			 * If jump in more than an hour, diplay 'Open'.
			 * If jump in an hour, diplay the remaining time to jump.
			 * If jump in 30 mins, diplay the remaining time to jump with class 'Closing30'.
			 * If jump in 5 mins, diplay the remaining time to jump with class 'Closing5'.
			 */
			if(tempRace.status == 'Open') {
				var startTime = util.timeToStartText(tempRace.startTime);
				if(startTime.match(/Min/g)) {
					var startValue = startTime.replace('Min', '');
					if(startValue <= 5 || startValue.match(/</g)) {
						tempRace.statusClass = "Closing5";
					}
					else if(startValue > 5 && startValue <= 30) {
						tempRace.statusClass = "Closing30";
					}
					startTime = startValue + ' m';
				}
				if(lessFiveMins(tempRace.startTime)){
					var mins = minDisplay(tempRace.startTime);
					textDisplay = mins;
				}else if (lessThirtyMins(tempRace.startTime)){
					var mins = minDisplay(tempRace.startTime);
					textDisplay = mins;
				}else{
					textDisplay = startTime;
				}


			}
			/* For Paying races, try to display the first four result. */
			else if(tempRace.status == 'Paying') {
				if(tempRace.results == "")
					textDisplay = "Paying";
				else
					textDisplay = tempRace.results;
			}
			/* For Abandoned races, display 'Aband.' */
			else if(tempRace.status == 'Abandoned') {
				textDisplay = 'Aband.';
			}
			/* For other cases, display the race status */
			else {
				textDisplay = tempRace.status;
			}
			return textDisplay;
		};

		/* Build tooltip html for a meeting */
		function buildMeetingToolTip(meeting) {
			var meetingType = 'Unknown';
			switch (meeting.type) {
				case 'R':
					meetingType = 'Thoroughbred';
					break;
				case 'G':
					meetingType = 'Greyhounds';
					break;
				case 'T':
					meetingType = 'Harness';
					break;
				default:
					break;
			}
			return '<div>Date: ' + parseDateParamToDisplay(meeting.date) + '</div>' +
				'<div>Venue: ' + meeting.venue + '</div>' +
				'<div>Type: ' + meetingType + '</div>' +
				'<div>Country: ' + meeting.country + '</div>';
		};

		/* Transform date display from parameter (YYYYMMDD) to required format (DD-MM-YYYY)
		 * eg. "20140101" to "01-01-2014"
		 */
		function parseDateParamToDisplay(dateString) {
			var day = dateString[6] + dateString[7];
			var month = dateString[4] + dateString[5];
			var year = dateString.substring(0, 4);
			return day + '-' + month + '-' + year;
		};

		/* Transform date to parameter format (YYYYMMDD)
		 * eg. Date(20140101) to "20140101"
		 */
		function parseDateToPara(date) {
			var yyyy = date.getFullYear().toString();
	   		var mm = (date.getMonth() + 1).toString();
	   		var dd  = date.getDate().toString();
	   		return yyyy + (mm[1] ? mm : "0" + mm[0]) + (dd[1] ? dd : "0" + dd[0]);
		};
	}

})();
