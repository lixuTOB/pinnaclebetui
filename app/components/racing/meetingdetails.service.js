(function () {
	'use strict';

	angular
		.module('pinnacleBetApp')
		.factory('meetingDetailsService', meetingDetailsService);

	meetingDetailsService.$inject = ['messagingService', 'EVN', 'util'];

	function meetingDetailsService(messagingService, EVN, util) {
        var self = this;

        this.raceUpdateEventLateScratchingHandler = function (
    		scratchingStatus,
    		newStatus,
    		oldStatus,
    		entrants,
    		extraInfoOfEntrant
    	) {
            var entrants = angular.copy(entrants);

            for (var id in scratchingStatus) {
                for (var i = 0; i < entrants.length; i++) {
                    var e = entrants[i];
                    if (e.id == id) {
                        e.scratched = scratchingStatus[id];
                    };
                };
            }
            for (var i = 0; i < entrants.length; i++) {
                var e = entrants[i];
                if (extraInfoOfEntrant[e.id]) {
                    var flucs = extraInfoOfEntrant[e.id].flucs.split(" ");
                    if (flucs[flucs.length - 1] == "0") {
                        e.lateScratching = true;
                    } else {
                        e.lateScratching = false;
                    }
                } else {
                    e.lateScratching = false;
                }
            };

            return entrants;
        };

        this.raceUpdateEventDeletePools = function (pools, deletedIDs) {
            pools = angular.copy(pools);
            for (var i = 0; i < deletedIDs.length; i++) {
                var id = deletedIDs[i];
                for (var n = 0; n < pools.length; n++) {
                    var p = pools[n];
                    if (p.id == id) {
                        pools.splice(n, 1);
                    };
                };
            };
            return pools;
        };

        this.subscribeRaceUpdateEvent = function (raceNum, callback) {
            messagingService.subscribe('RaceUpdateEvent', raceNum, callback);
        };

        this.unsubscribeRaceUpdateEvent = function (raceNum, callback) {
            messagingService.unsubscribe('RaceUpdateEvent', raceNum, callback);
        };

        this.changePoolSubscription = function (new_pools, old_pools) {
            if (!new_pools) {
                return;
            };
            var subscribe = [];
            var unsubscribe = [];
            if (!old_pools) {
                for (var i = 0; i < new_pools.length; i++) {
                    var p = new_pools[i];
                    subscribe.push(p);
                };
            } else {
                /* look for new pools to subscribe */
                for (var i = 0; i < new_pools.length; i++) {
                    var np = new_pools[i];
                    var found = false;
                    for (var n = 0; n < old_pools.length; n++) {
                        var op = old_pools[n];
                        if (np.id == op.id) {
                            found = true;
                            break;
                        };
                    };
                    if (!found) {
                        subscribe.push(np);
                    };
                };

                /* look for pools to unsubscribe */
                for (var i = 0; i < old_pools.length; i++) {
                    var op = old_pools[i];
                    var found = false;
                    for (var n = 0; n < new_pools.length; n++) {
                        var np = new_pools[n];
                        if (np.id == op.id) {
                            found = true;
                            break;
                        };
                    };
                    if (!found) {
                        unsubscribe.push(op);
                    };
                };
            }

            self.subscribePools(subscribe);
            self.unsubscribePools(unsubscribe);
        };

        this.subscribePools = function (pools) {
            for (var i = 0; pools && i < pools.length; i++) {
                var p = pools[i];
                var addr = messagingService.getAddrFromPool(p);
                if (addr) {
                    messagingService.subscribe(addr, p.id);
                };
            };
        };

        this.unsubscribePools = function (pools) {
            for (var i = 0; pools && i < pools.length; i++) {
                var p = pools[i];
                var addr = messagingService.getAddrFromPool(p);
                if (addr) {
                    messagingService.unsubscribe(addr, p.id);
                };
            };
        };

        return this;
	}

})();
