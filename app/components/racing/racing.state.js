(function(){
    'use strict';

    angular
        .module('pinnacleBetApp')
        .config(racingStateConfig);

    racingStateConfig.$inject = ['$stateProvider'];

    function racingStateConfig($stateProvider) {
        $stateProvider
	        .state('home.racing', {
	        	url : "racing?meetingDate",
	        	views : {
	        		'content@': {
	                    templateUrl: "components/racing/racingMeetings.html",
	                    controller: 'racingMeetingsCtrl'
	                }
	        	},
	    	})
	       	.state('home.racing.meeting',  {
	            url : "/meeting/:meetingID/:raceNum",
	            views : {
	                'content@' : {
	                    templateUrl: "components/racing/racingDetails.html",
	                    controller: 'racingDetailsCtrl'
	                }
	            }
	        })
    }

})();
