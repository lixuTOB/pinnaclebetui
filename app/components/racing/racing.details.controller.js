(function(){
    'use strict';

    angular
        .module('pinnacleBetApp')
        .controller('racingDetailsCtrl', racingDetailsCtrl);

    racingDetailsCtrl.$inject = [
        '$scope',
        '$rootScope',
        '$stateParams',
        '$interval',
        '$location',
        'racingService',
        'betslipService',
        'mainService',
        'messagingService',
        'meetingDetailsService',
        'accountService',
        'util',
        'EVN'
    ];

    function racingDetailsCtrl(
    	$scope,
        $rootScope,
    	$stateParams,
    	$interval,
    	$location,
    	racingService,
        betslipService,
    	mainService,
    	messagingService,
    	meetingDetailsService,
    	accountService,
    	util,
    	EVN
    ) {
        util.scrollTop();
        util.sidebarView('RACING',$rootScope);
        $scope.origin = EVN.apiDataUrl;
        $scope.meetingID = $stateParams.meetingID;
        $scope.raceNum = $stateParams.raceNum;
        $scope.betMethod = "WinPlace";
        $scope.availablePool = {
    		WinPlace : true,
    		Exacta: true,
    		Quinella: true ,
    		Trifecta : true,
    		FirstFour : true,
    		Quadrella: true
		}
        $scope.jumpinTime = "";
        $scope.detailsLastUpdate = util.getTime().msec();
        $scope.poolsLastUpdate = util.getTime().msec();
        $scope.raceListLastUpdate = util.getTime().msec();
        $scope.isUserLogged = mainService.isUserLogged;
        $scope.details = {};
        $scope.raceList = [];
        $scope.pools = [];
        $scope.lastUpdate = 0;

        $scope.optionDetails = {
            WinPlace: true,
            Quinella: false,
            Exacta: false,
            Trifecta: false,
            FirstFour: false,
            Quadrella: true
        };

        var detailsUpdateIntervalID, detailsUpdateInterval = 60000;
        var raceListUpdateIntervalID, raceListUpdateInterval = 60000;
        var poolsUpdateIntervalID, poolsUpdateInterval = 60000;
        var jumpInUpldateIntervalID, jumpInUpldateInterval = 1000;
        var lessThenThreeMinutesID, lessThenThreeMinutesInterval = 5000;
        var ieAjaxCallTimeoutID = undefined;
        var ieAjaxCallIntervalID, ieAjaxCallInterval = 8000;
        var oddsStatusIntervalID, oddsStatusIntervalID = 5000;

        var eventListeners = {};

        setInterval(function(){
            if((util.getTime().msec() - $scope.lastUpdate) > 5000){
                $scope.pools.forEach(function(pool){
                    pool.entrantOdds.forEach(function(odds){
                        odds.__lastUpdate = 'status_quo';
                    });
                });
            }
        },oddsStatusIntervalID);

        $scope.$watchCollection('pools', function (nv, ov) {
            optionAvailbility();
        });

        $scope.$on('$destroy', function () {
            if (jumpInUpldateIntervalID) {
                $interval.cancel(jumpInUpldateIntervalID);
            };
            if (detailsUpdateIntervalID) {
                $interval.cancel(detailsUpdateIntervalID);
            };
            if (poolsUpdateIntervalID) {
                $interval.cancel(poolsUpdateIntervalID);
            };
            if (raceListUpdateIntervalID) {
                $interval.cancel(raceListUpdateIntervalID);
            };
            if (ieAjaxCallIntervalID) {
                $interval.cancel(ieAjaxCallIntervalID);
            };
            for (var k in eventListeners) {
                if (eventListeners[k]) {
                    eventListeners[k]();
                };
            }

            msgUnsubrRaceEvent();
            msgUnsubrRacePoolEvent();
            msgUnsubrRaceEntrantEvent();
        });

        racingService.getRaceList($scope.meetingID).then(function (data) {
             if (!data) {
                return;
            };
            $scope.raceList = data;

            /* update race list every [raceListUpdateInterval] sec */
            var _flag = false;
            if(raceListUpdateIntervalID)
				$interval.cancel(raceListUpdateIntervalID);
            raceListUpdateIntervalID = $interval(function (data) {
                var t = util.getTime().msec() - $scope.raceListLastUpdate;
                if (t > raceListUpdateInterval) {
                    if (_flag) {
                        return;
                    };
                    _flag = true;
                    $scope.raceListLastUpdate = util.getTime().msec();
                    racingService.getRaceList($scope.meetingID, true).then(function (data) {
                        _flag = false;
                        if (!data) {
                            return;
                        };
                        $scope.raceList = data;
                    }, function () {
                        _flag = false;
                    });
                }
            }, raceListUpdateInterval);
        });

        racingService.getRaceDetails($scope.meetingID, $scope.raceNum).then(function (data) {
            if (!data) {
                return;
            };
            meetingFilter(data);
            $scope.details = data;

            /* subscribe to race update queue */

            /* update jump in time every 1 sec */
            jumpInUpldateIntervalID = $interval(function () {
                $scope.jumpinTime = parseBetDeadline($scope.getTimeDiff());
            }, jumpInUpldateInterval);

            /* update details every [detailsUpdateInterval] sec */
            (function () {
                var _flag = false;
                detailsUpdateIntervalID = $interval(function (data) {
                    racingService.getRaceDetails($scope.meetingID, $scope.raceNum, true)
                        .then(function (data) {
                            if (!data) {
                                return;
                            };
                            meetingFilter(data);
                            for (var k in data) {
                                if (!data[k] && $scope.details[k]) {
                                    data[k] = $scope.details[k];
                                };
                            }
                            $scope.details = data;
                            if ($scope.details.status && $scope.details.status != "Open") {
                                $scope.betMethod = "WinPlace";
                            };
                        }, function (data) {
                        });

                }, detailsUpdateInterval);
            })();
        });

        racingService.getMeetingNotice($scope.meetingID).then(function (data) {

            if (data.notice && data.notice !== "") {
            	$scope.PRACopyright = data.notice;
            } else {
            	$scope.PRACopyright = "2015 NZ or others working with it. " +
            	"These racing materials are reproduced under a copyright licence.  " +
            	"Any unauthorised reproduction, adaptation or communication is strictly prohibited.";
            }
        });

        racingService.getRaceOdds(
    		$scope.meetingID,
    		$scope.raceNum,
    		"WP"
    	).then(function (data) {
            if (!data) {
                return;
            };
            meetingFilter(data);
            $scope.entrants = meetingDetailsService.raceUpdateEventLateScratchingHandler(
        		data.scratchingStatus,
        		data.status,
        		undefined,
        		data.entrants,
        		data.extraInfoOfEntrant

            );
            $scope.pools = data.pools;
            $scope.showEmpty = checkAnyPoolAvailable($scope.pools);
            $scope.fluctuations = getFluctuations(data.extraInfoOfEntrant);

            meetingDetailsService.changePoolSubscription($scope.pools);
            listenAllPools($scope.pools);

            /* update entrants/pools every [poolsUpdateInterval] sec */
            (function () {
                var _flag = false;
                poolsUpdateIntervalID = $interval(function (data) {
                    // if ($scope.details.status == "Open") {return;};
                    var dt = $scope.getTimeDiff();
                    if (dt > 60000) {
                        return;
                    };
                    if ($scope.details.status == "Paying") {
                        return;
                    };
                    var t = util.getTime().msec() - $scope.poolsLastUpdate;
                    if (t > $scope.poolsLastUpdate) {
                        if (_flag) {
                            return;
                        };
                        _flag = true;
                        $scope.poolsLastUpdate = util.getTime().msec();
                        racingService.getRaceOdds(
                    		$scope.meetingID,
                    		$scope.raceNum,
                    		"WP",
                    		false,
                    		true
                    	).then(function (data) {
                            _flag = false;
                            if (!data) {
                                return;
                            };
                            meetingFilter(data);
                            var old_pools = $scope.pools;
                            $scope.entrants = meetingDetailsService.raceUpdateEventLateScratchingHandler(
                            	data.scratchingStatus,
                            	data.status,
                            	undefined,
                            	data.entrants,
                            	data.extraInfoOfEntrant
                            );
                            $scope.pools = data.pools;
                            $scope.showEmpty = checkAnyPoolAvailable($scope.pools);
                            $scope.fluctuations = getFluctuations(data.extraInfoOfEntrant);
                            meetingDetailsService.changePoolSubscription($scope.pools, old_pools);
                            listenAllPools($scope.pools);

                            for (var i = 0; i < old_pools.length; i++) {
                                var op = old_pools[i];
                                for (var n = 0; n < $scope.pools.length; n++) {
                                    var np = $scope.pools[n];
                                    if (op.id == np.id) {
                                        if (op.bookmaker.id == "TopSport") {
                                            if (op.poolTypeId == "P" || op.poolTypeId == "W") {
                                                addTimeStampToOdds(np, op);
                                            }
                                        }
                                    }
                                }
                            }
                        }, function (data) {
                            _flag = false;
                        });
                    }
                }, poolsUpdateInterval);
            })();
        });

        $scope.getTimeDiff = function () {
            if (!$scope.details) {
                return 0;
            };
            var timeDiff = $scope.details.startTime - util.getTime().msec();
            return timeDiff;
        };

        $scope.parseStartTime = function (st) {
            var dateString = util.formatDateString($scope.details.startTime);
            dateString = dateString.substring(dateString.length - 11);
            return util.parseStartTime(st) + dateString;
        };

        $scope.changeBetMethod = function (b) {
            if($scope.optionDetails[b]){
                $scope.betMethod = b;
            }
        };

        $scope.getBetMethodBtnCss = function (s) {
            return {
                'disabled': !$scope.optionDetails[s],
                'selected': $scope.betMethod == s
            }
        };

        $scope.getRaceNumBtnCss = function (n) {
            return {
                'selected': $scope.raceNum == n
            }
        };

        $scope.meetingPaneCss = function () {
            return {
                'deactivate': $scope.slips.length > 0
            }
        };

        $scope.slipsPaneCss = function () {
            return {
                'activate': $scope.slips.length > 0
            }
        };

        $scope.addSlip = function (raceInfo, pool, entrant) {
            var slip = {
                data : {
                    raceInfo : raceInfo,
                    pool : pool,
                    entrant : entrant
                },
                type : 'RACING',
                odds :  racingService.getRaceOddsObj(entrant, pool).odds,
                oddsId : racingService.getRaceOddsObj(entrant, pool).id,
                isMulti: true
            };
            betslipService.addSlip(slip);
        };

        $scope.poolsFilter = function () {
            return function (item) {
                var pid = item.poolTypeId;
                var bmid = item.bookmaker.id;

                var isNotExoticPool = (pid != "Q" && pid != "E" && pid != "T" && pid != "FF");
                var isNotTote = (bmid != "U" && bmid != "N" && bmid != "V");
                var isPoolAvailable =   item.entrantOdds.length > 0  ;

                /* now, every pool coming from TopSport should be checked if it is available, not only TF and SP
                var isTF = (pid == 'TF' ? (item.poolAvailable && item.entrantOdds.length > 0) : true);
                var isSP = (pid == 'BT3SP' ? (item.poolAvailable && item.entrantOdds.length > 0) : true);
                */
                return isNotExoticPool && isNotTote && isPoolAvailable;
            };
        };

        $scope.getFixedPools = function (item) {
            var pid = item.poolTypeId;
            var bookmakerId = item.bookmaker.id;
            return  (pid == 'W' || pid == 'P') && bookmakerId == "TopSport"  &&
            	(item.poolAvailable || $scope.details.status != 'Open');
        };

        $scope.getTotePools = function (item) {
            var pid = item.poolTypeId;
            var bookmakerId = item.bookmaker.id;
            return ( (pid == 'W' || pid == 'P')  &&
        		bookmakerId == "TobRacing" && item.poolAvailable );
        };

        $scope.getBestTotePools = function (item) {
            var pid = item.poolTypeId;
            var bookmakerId = item.bookmaker.id;
            return (pid == 'BT3W' && bookmakerId == "TopSport") &&
            	(item.poolAvailable || $scope.details.status != 'Open');
        }

        $scope.getBestToteSPPools = function (item) {
            var pid = item.poolTypeId;
            var bookmakerId = item.bookmaker.id;
            return ( pid == 'BT3SP' && item.entrantOdds.length > 0 &&
        		bookmakerId == "TopSport" ) &&  (item.poolAvailable || $scope.details.status != 'Open');
        }

        $scope.getMiddleToteSPPools = function (item) {
            var pid = item.poolTypeId;
            var bookmakerId = item.bookmaker.id;
            return  (pid == 'BT1W' ||pid == 'BT1P')  &&
            	(item.poolAvailable || $scope.details.status != 'Open') &&
            		item.entrantOdds.length > 0 && bookmakerId == "TopSport";
        }

        $scope.geteToteFivePercentPPools = function (item) {
            var pid = item.poolTypeId;
            var bookmakerId = item.bookmaker.id;
            return  (pid == 'PT5' ||pid == 'WT5') &&
            	(item.poolAvailable || $scope.details.status != 'Open') &&
            		item.entrantOdds.length > 0 && bookmakerId == "TopSport";
        }

        $scope.getTopFlucPools = function (item) {
            var pid = item.poolTypeId;
            return pid == 'TF' && item.poolAvailable  && item.entrantOdds.length > 0;
        }

        $scope.getOtherPoolsV2 = function (item) {
            return !$scope.getFixedPools(item) && !$scope.getTotePools(item) &&
            	!$scope.getBestTotePools(item) && !$scope.getBestToteSPPools(item) &&
            		!$scope.getTopFlucPools(item) && !$scope.getMiddleToteSPPools(item) &&
            			!$scope.geteToteFivePercentPPools(item);
        };

        $scope.userAccountToteRestricted = function(data){
            var user = mainService.getUserAccountDto();
            if(!$scope.details && data) {
                var startTime = data.startTime;
                var time = startTime - util.getTime().msec();

                var isLess = lessThanThreeMinutesRemain(time,data);
                if(user != null && user.restrictions.indexOf('TOTE') >= 0 && isLess)
                {
                    return true;
                }
                return false;
            } else {
                var time = 0;
                if($scope.details) {
                     time = $scope.getTimeDiff();
                }
                else
                {
                    var startTime = data.startTime;
                    time = startTime - util.getTime().msec();
                }
                var isLess = lessThanThreeMinutesRemain(time,data);
                if(user != null && user.restrictions.indexOf('TOTE') >= 0 && isLess)
                {
                    return true;
                }
               return false;
            }
        }

        $scope.hiddenPools = function (item) {
            var pid = item.poolTypeId;

            // when it is 'STAB' under 'WIN', we need to hide it as needed
            if (pid == "W" && item.bookmaker.id == "TobRacing") {
                for (var i = 0; i < $scope.pools.length; i++) {
                    var pool = $scope.pools[i];
                    // hide it when 'BT3W' or 'BT3SP' is available
                    if (pool.poolTypeId == "BT3W" ||
                        (pool.poolTypeId == "BT3SP" && pool.poolAvailable &&
                    		pool.entrantOdds.length > 0)) {
                        return false;
                    }
                }
            }
            // when there are 'BT3W' and 'BT3SP' at the same time, only display 'BT3SP'
            if (pid == "BT3W") {
                for (var i = 0; i < $scope.pools.length; i++) {
                    var pool = $scope.pools[i];
                    if (pool.poolTypeId == "BT3SP" && pool.poolAvailable &&
                		pool.entrantOdds.length > 0)
                    {
                        return false;
                    }
                }
            }
            return true;
        };

        $scope.scrollTo = function (hash) {
            $location.hash(hash);
        };

        function checkAnyPoolAvailable(pools) {
            if (!pools || pools.length == 0) {
                return true;
            } else {
                return false;
            }
        }

        function getFluctuations(extraInfoOfEntrant) {
            var fluctuations = [];
            for (var i in extraInfoOfEntrant) {
                var extraInfo = extraInfoOfEntrant[i];
                var flucs = extraInfo.flucs.split(' ');
                var topFluct = 0;
                for (var j = 0; j < flucs.length; j++) {
                    var tempFluct = parseFloat(flucs[j]);
                    if (tempFluct > topFluct)
                        topFluct = tempFluct;
                }
                extraInfo.flucs = flucs;
                extraInfo.fluc_top = topFluct;
                fluctuations.push(extraInfo);
            }
            if (fluctuations.length == 0)
                return $scope.fluctuations ? $scope.fluctuations : undefined;
            return fluctuations;
        }

        function parseBetDeadline(mm) {
            if ($scope.details.status != "Open") {
                return $scope.details.status;
            };
            var _mm = mm < 0 ? -1 * mm : mm;
            var days = Math.floor(_mm / (24 * 3600 * 1000));
            var hrs = Math.floor((_mm % (24 * 3600 * 1000)) / 1000 / 3600);
            var mins = Math.floor((_mm % (3600 * 1000)) / 1000 / 60);
            var secs = Math.round((_mm % (60 * 1000)) / 1000);
            if (hrs > 0 && mm > 0) {
                return "OPEN";
            };
            var ret = (mm < 0 ? "-" : "") + (days != 0 ? days + " days, " : "") +
            	(hrs != 0 ? hrs + " hrs, " : "") + mins + " min, " + secs + "s";
            return ret;
        };

        function lessThanThreeMinutesRemain (_mm,data) {
    		if(_mm < 0) {
    		    _mm = 0;
    		}
    		var days = Math.floor(_mm / (24 * 3600 * 1000));
    		var hrs = Math.floor((_mm % (24 * 3600 * 1000)) / 1000 / 3600);
    		var mins = Math.floor((_mm % (3600 * 1000)) / 1000 / 60);
    		var secs = Math.round((_mm %(60 * 1000)) / 1000);
    		return mins < 3? true : false;
        };

        function optionAvailbility() {
            if (!$scope.pools) {
                return;
            };
            $scope.optionDetails.WinPlace = $scope.pools.length > 0;
            var found = false;
            for (var i = 0; i < $scope.pools.length; i++) {
                var p = $scope.pools[i];
                if (p.bookmaker.id == "TobRacing" && p.poolTypeId == "W") {
                    found = true;
                    break;
                };
            };

            for (var i = 0; found && i < $scope.pools.length; i++) {
                var p = $scope.pools[i];
                switch (p.poolTypeId) {
                    case "Q":
                        $scope.optionDetails.Quinella = true;
                        break;
                    case "E":
                        $scope.optionDetails.Exacta = true;
                        break;
                    case "T":
                        $scope.optionDetails.Trifecta = true;
                        break;
                    case "FF":
                        $scope.optionDetails.FirstFour = true;
                        break;
                }
            };
        };

        /* attach listeners to messaging server addresses */
        function listenAllPools(pools) {
            var listenedAddr = [];
            for (var i = 0; i < pools.length; i++) {
                var p = pools[i];
                var addr = messagingService.getAddrFromPool(p);
                if ($.inArray(addr, listenedAddr) != -1) {
                    continue;
                };
                if (addr) {
                    listenedAddr.push(addr);
                    if (!eventListeners[addr]) {
                        var cancel = $scope.$on(addr, function (e, data) {
                            for (var i = 0; i < $scope.pools.length; i++) {
                                var p = $scope.pools[i];
                                if (p.id == data.id) {
                                    // console.log("pool is changed.", p.bookmaker.id, p.poolTypeId);
                                    if (p.bookmaker.id == "TopSport") {
                                        if (p.poolTypeId == "W" || p.poolTypeId == "P") {
                                            $scope.poolsLastUpdate = util.getTime().msec();
                                            addTimeStampToOdds($scope.pools[i], data);
                                        };
                                    };
                                    $scope.pools[i] = data;
                                    break;
                                };
                            };
                        });
                        eventListeners[addr] = cancel;
                    };
                };
            };
        };

        /* attach timestamp to new pool, inherient timestamp if odd value
         * is not changed (meaning timestamp may be undefined)
         */
        function addTimeStampToOdds(oldP, newP) {
            for (var n = 0; n < newP.entrantOdds.length; n++) {
                var o = newP.entrantOdds[n];
                var po;
                for (var m = 0; m < oldP.entrantOdds.length; m++) {
                    var _po = oldP.entrantOdds[m];
                    if (_po.id == o.id) {
                        po = _po;
                        break;
                    };
                };
                if (po) {
                    if (o.odds != po.odds) {
                        o.__lastUpdate = o.odds > po.odds ? 'higher' : 'lower';
                    }
                };
            };
            return newP;
        };

        function findPoolIndex(type, pool) {
            for(var i = 0; i < pool.length; i++)
            {
                 if(pool[i].poolTypeId === type)
                 {
                     return i;
                 }
            }
            return -1;
        }

        /* filter */
        function meetingFilter(data){
            if (data) {
            	data.pools.forEach(function(pool) {
                    if(pool.poolTypeId == "BT1W"){
                        var pools = data.pools;
                        for(var i = 0; i < pools.length; i++)
                        {
                            var pool = pools[i];
                            if(pool.poolTypeId === "BT3SP" || pool.poolTypeId === "BT3W")
                            {
                                    var index = findPoolIndex('BT1W', data.pools);
                                    if(index != -1){
                                        data.pools.splice(index, 1);
                                    }
                            }
                        }
                    }
            	});
            }

	        /*
	        * replece WT5 PT5 and BT1W , BT1P with TOTE win place
	        */
	        if(data.pools) {
	            var tote = { win: {} , place: {} , winIdx: -1, placeIdx: -1};
	            var BT1 = { win: {} , place: {}, winIdx: -1, placeIdx: -1};     // Middle TOTE
	            var T5 = { win: {} , place: {}, winIdx: -1, placeIdx: -1};      // TOTE + 5%
	            var BT3 = { win: {} , place: {}, winIdx: -1, placeIdx: -1};     // Best TOTE
	            var BT3SP = { win: {} , winIdx: -1};                            // Best TOTE SP
	            for(var idx= 0; idx < data.pools.length ; idx++ ){
	                var pool = data.pools[idx];
	                if(pool.bookmaker.id == "TobRacing" ){
	                    if(pool.poolTypeId == "W" ){
	                        tote.win = pool;
	                        tote.winIdx = idx;
	                        tote.win.poolAvailable = false;
	                    }
	                    if(pool.poolTypeId == "P" ){
	                        tote.place = pool;
	                        tote.placeIdx = idx;
	                        tote.place.poolAvailable = false;
	                    }

	                    swopValid();
	                }
	                if(pool.bookmaker.id == "TopSport" ){
	                    if(pool.poolTypeId == "WT5" ){
	                        T5.win = pool;
	                        T5.winIdx = idx;
	                    }
	                    if(pool.poolTypeId == "PT5" ){
	                        T5.place = pool;
	                        T5.placeIdx = idx;
	                    }

	                    swopValid();
	                }
	                if(pool.bookmaker.id == "TopSport"  ){
	                    if(pool.poolTypeId == "BT1W" ){
	                        BT1.win = pool;
	                        BT1.winIdx = idx;
	                    }
	                    if(pool.poolTypeId == "BT1P" ){
	                        BT1.place = pool;
	                        BT1.placeIdx = idx;
	                    }

	                    swopValid();
	                }
	                if(pool.bookmaker.id == "TopSport" ){
	                    if(pool.poolTypeId == "BT3W" ){
	                        BT3.win = pool;
	                        BT3.winIdx = idx;
	                    }
	                    if(pool.poolTypeId == "BT3P" ){
	                        BT3.place = pool;
	                        BT3.placeIdx = idx;
	                    }

	                    swopValid();
	                }
	                if(pool.bookmaker.id == "TopSport" ){
	                    if(pool.poolTypeId == "BT3SP" ){
	                        BT3SP.win = pool;
	                        BT3SP.winIdx = idx;
	                    }
	                    swopValid();
	                }


	            };

	            if(tote.winIdx >= 0){ data.pools[tote.winIdx] = tote.win};
	            if(tote.placeIdx >= 0){ data.pools[tote.placeIdx] = tote.place};
	            if(BT1.winIdx >= 0){ data.pools[BT1.winIdx] = BT1.win};
	            if(BT1.placeIdx >= 0){ data.pools[BT1.placeIdx] = BT1.place};
	            if(T5.winIdx >= 0){ data.pools[T5.winIdx] = T5.win};
	            if(T5.placeIdx >= 0){ data.pools[T5.placeIdx] = T5.place};
	            if(BT3.winIdx >= 0){ data.pools[BT3.winIdx] = BT3.win};
	            if(BT3.placeIdx >= 0){ data.pools[BT3.placeIdx] = BT3.place};
	            if(BT3SP.winIdx >= 0){ data.pools[BT3SP.winIdx] = BT3SP.win};
	        }

	        function swopValid(){
	            if(!util.isEmpty(tote.win) && !util.isEmpty(BT1.win)){
	                swopFun(tote.win, BT1.win);
	            }
	            if(!util.isEmpty(tote.win) && !util.isEmpty(T5.win)){
	                swopFun(tote.win, T5.win);
	            }
	            if(!util.isEmpty(tote.place) && !util.isEmpty(BT1.place)){
	                swopFun(tote.place, BT1.place);
	            }
	            if(!util.isEmpty(tote.place) && !util.isEmpty(T5.place)){
	                swopFun(tote.place, T5.place);
	            }
	           if(!util.isEmpty(tote.win) && !util.isEmpty(BT3.win)){
	                swopFun(tote.win, BT3.win);
	            }
	            if(!util.isEmpty(tote.place) && !util.isEmpty(BT3.place)){
	                swopFun(tote.place, BT3.place);
	            }
	            if(!util.isEmpty(tote.win) && !util.isEmpty(BT3SP.win)){
	                swopFun(tote.win, BT3SP.win);
	            }
	        }

	        function swopFun(A, B){
	            A.entrantOdds.forEach(function(poolA){
	                B.entrantOdds.forEach(function(poolB){
	                    if(poolB.firstEntrant.rugNumber == poolA.firstEntrant.rugNumber ){
	                        poolB.odds = poolA.odds;
	                    }
	                });
	            });
	        }
        }

        function msgSwopPool(orgPools, newPools){
            newPools.forEach(function(newPool){
                if(newPool.entrantOdds.length > 0){
                    for(var i = 0; i < orgPools.length; i++){
                        if(orgPools[i].bookmaker.id == newPool.bookmaker.id &&
                        		orgPools[i].poolTypeId == newPool.poolTypeId)
                        {
                            orgPools[i] = newPool;
                        }
                    };
                }
            });
        }

        /*
        *  Message section
        */
        msgSubrRaceEvent();
        msgSubrRacePoolEvent();
        msgSubrRaceEntrantEvent();

        function msgSubrRaceEvent(){
            messagingService.subscribe(EVN.RaceUpdateEvent , $scope.raceNum, function (data) {
                if(!data){return;}
                util.log("racing msgSubrRaceEvent work ");
                util.log(data);
                if(!$scope.details){return;}
                for(var key in data){
                    if(key == 'startTime'){
                        $scope.details[key] = new Date(data[key]).getTime();
                    }else{
                        $scope.details[key] = data[key];
                    }
                }

            });
        }

        function msgUnsubrRaceEvent(){
            messagingService.unsubscribe(EVN.RaceUpdateEvent , $scope.raceNum);
        }

        function msgSubrRacePoolEvent(){
            messagingService.subscribe(EVN.RacePoolUpdateEvent, $scope.raceNum, function (data) {
                if(!data){return;}
                util.log("racing msgSubrRacePoolEvent work ");
                util.log(data);
                $scope.lastUpdate = util.getTime().msec();
                var tmp_pools = angular.copy($scope.pools);
                msgSwopPool(tmp_pools, data);
                meetingFilter({pools: tmp_pools});
                var old_pools = $scope.pools;
                $scope.pools = tmp_pools;

                for (var i = 0; i < old_pools.length; i++) {
                    var op = old_pools[i];
                    for (var n = 0; n < $scope.pools.length; n++) {
                        var np = $scope.pools[n];
                        if (op.id == np.id) {
                            if (op.bookmaker.id == "TopSport") {
                                if (op.poolTypeId == "P" || op.poolTypeId == "W") {
                                    addTimeStampToOdds(op, np);
                                }
                            }
                        }
                    }
                }
            });
        }

        function msgUnsubrRacePoolEvent(){
            messagingService.unsubscribe(EVN.RacePoolUpdateEvent, $scope.raceNum);
        }

        function msgSubrRaceEntrantEvent(){
            messagingService.subscribe(EVN.RaceEntrantUpdateEvent, $scope.raceNum, function (data) {
                if(!data){return;}
                util.log("racing msgSubrRaceEntrantEvent work ");
                util.log(data);
                $scope.entrants = data;
            });
        }

        function msgUnsubrRaceEntrantEvent(){
            messagingService.unsubscribe(EVN.RaceEntrantUpdateEvent, $scope.raceNum);
        }
    }

})();
