(function() {
    'use strict';

    angular
        .module('pinnacleBetApp')
        .directive('raceTableFirstFour', raceTableFirstFour);

    raceTableFirstFour.$inject = ['racingService','util'];

    function raceTableFirstFour (racingService, util) {
        var directive = {
            restrict: 'A',
            replace: true,
            templateUrl: 'components/racing/details/racingFirstfour.html',
            controller: raceTableFirstFourCtl,
            link: linkFunc
        };

        return directive;

        function raceTableFirstFourCtl($scope, $element, $attrs,
            $transclude) {
            $scope.nameMap = racingService.nameMap;
            $scope.getColumnName = racingService.getColumnName;
            $scope.getOdds = racingService.getOdds;
            $scope.isHeader = racingService.isHeader;
            $scope.isScratched = racingService.isScratched;
            $scope.isNumber = util.isNumber;

            $scope.silkImage = racingService.silkImage;
            $scope.colorUpdate = racingService.colorUpdate;
        }

        function linkFunc(scope, iElement) {
        }
    }
})();
