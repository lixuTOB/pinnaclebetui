(function() {
    'use strict';

    angular
        .module('pinnacleBetApp')
        .directive('raceResultTable', raceResultTable);

    raceResultTable.$inject = ['racingService','util'];

    function raceResultTable (racingService, util) {
        var directive = {
            restrict: 'A',
            replace: true,
            templateUrl: 'components/racing/details/racingResultTable.html',
            controller: raceResultTableCtl,
        	link: linkFunc
        };

        return directive;

        function raceResultTableCtl($scope, $element, $attrs, $transclude, racingService){
            $scope.nameMap = racingService.nameMap;
            $scope.getColumnName = getColumnName;
            $scope.getOdds = racingService.getOdds;
            $scope.getFlucts = racingService.getFlucts;
            $scope.getOutcome = racingService.getOutcome;
            $scope.oddsFromPrice = racingService.oddsFromPrice;
            $scope.getRaceOdds = util.getOdds;
            $scope.silkImage = racingService.silkImage;
            $scope.parseExoticsResult = parseExoticsResult;
            $scope.getNumOfResult = getNumOfResult;
            $scope.getColspan = getColspan;
            $scope.exotics = [];
            $scope.place = [];

            $scope.$watch('typeId', function(newValue, oldValue) {
                      if (newValue !== oldValue) {
                        // You actions here
                      }
             }, true);
            /*
             * PROBLEM: entrants is refreshed with status
             * change of race, however status change message
             * is sent once per change, as result entrants
             * may not have placing when it gets the Interim
             * status
             */
            $scope.$watch('entrants', function (nv, ov) {
                var place = [
                    [],
                    [],
                    [],
                    []
                ];
                for (var i = 1; nv && i <= 3; i++) {
                    for (var n = 0; n < nv.length; n++) {
                        var e = nv[n];
                        if (e.placing && e.placing == i) {
                            place[i - 1].push(e);
                        };
                    };
                };

                for (var i = 1; i < place.length; i++) {
                    var p = place[i];
                    if (p.length == 0) {
                        place.splice(i, 1);
                    };
                };
                $scope.place = place;
            },true);
            /*
             * pools is replaced by reference, so do not
             * need to watch collection
             */

            $scope.$watchCollection('pools', function (nv, ov) {
                if (!nv) {
                    return;
                }
                if( angular.isArray(nv) && nv.length === 0){
                    return;
                }

                $scope.resultPools = {};
                $scope.otherResults = {};
                // $scope.exotics = [];
                var exotics = [];
                for (var i = 0; i < nv.length; i++) {
                    var p = nv[i];
                    if (p.bookmaker.id != "TobRacing" && p.bookmaker.id != "TopSport") {
                        if (!$scope.resultPools[p.bookmaker.id]) {
                            $scope.resultPools[p.bookmaker.id] = {};
                        };
                        $scope.resultPools[p.bookmaker.id][p.poolTypeId] = p;
                    };
                    if (p.poolTypeId == "TF") {
                        $scope.otherResults.TF = p;
                    };
                    if (p.poolTypeId == "BT3SP") {
                        $scope.otherResults.BT3SP = p;
                    };
                };

                for (var i = 0; i < nv.length; i++) {
                    var p = nv[i];
                    switch (p.poolTypeId) {
                        case "E":
                            if (p.outcomes.length > 0) {
                                exotics.push(p.outcomes[0]);
                            }
                            break;
                        case "Q":
                            if (p.outcomes.length > 0) {
                                exotics.push(p.outcomes[0]);
                            }
                            break;
                        case "FF":
                            if (p.outcomes.length > 0) {
                                exotics.push(p.outcomes[0]);
                            }
                            break;
                        case "T":
                            if (p.outcomes.length > 0) {
                                exotics.push(p.outcomes[0]);
                            }
                            break;
                        case "QD":
                            if (p.outcomes.length > 0) {
                                exotics.push(p.outcomes[0]);
                            }
                            break;
                        case "W":
                        case "P":
                            if (p.bookmaker.id == "TobRacing" || p.bookmaker.id == "TopSport") {
                                $scope.place = getPlacingFromPool(p);
                            }
                    };
                };


                /*
                 * query Quadrella outcome
                 */
                racingService.getMultiLegsByMeetingID($scope.meetingID, true)
                    .then(function (data){
                        if (!data){
                            return ;
                        }else if(data.pools.length === 0){
                            return;
                        }else if(data.pools[0].outcomes.length === 0) {
                            return;
                        }
                        data.pools[0].outcomes[0].__races = "("+data.pools[0].multiLegRaceNumbers.toString()+")";
                        $scope.exotics.push(data.pools[0].outcomes[0]);
                    });

                $scope.exotics = exotics;

            },true);

            function getNumOfResult() {
                var c = 0;
                for (var k in $scope.resultPools) {
                    c++;
                }
                for (var k in $scope.otherResults) {
                    c++;
                }
                return c;
            };
            function getColspan() {
                return Math.max(2, $scope.getNumOfResult());
            };
            function parseExoticsResult(item) {
                var ret = "(";
                for (var i = 0; i < item.rugNumbers.length; i++) {
                    var r = item.rugNumbers[i];
                    if (i != 0) {
                        if (item.poolType == "Q") {
                            ret += "/";
                        } else {
                            ret += ","
                        }
                    };
                    ret += r;
                };
                ret += ")";
                return ret;
            };
            function getPlacingFromPool(pool) {
                if (!pool.entrantOdds || pool.entrantOdds.length == 0) {
                    return []
                };
                var place = [
                    [],
                    [],
                    [],
                    []
                ];
                for (var i = 0; i < pool.entrantOdds.length; i++) {
                    var o = pool.entrantOdds[i];
                    if (o.firstEntrant.placing && o.firstEntrant.placing >= 1 && o.firstEntrant.placing <= 3) {
                        place[o.firstEntrant.placing - 1]
                            .push(o.firstEntrant);
                    };
                };
                for (var i = 0; i < place.length; i++) {
                    var p = place[i];
                    if (p.length == 0) {
                        place.splice(i, 1);
                    };
                };
                return place;
            }
            function getColumnName(poolObj,nameMap, isWinPlace) {
                if (poolObj && poolObj.poolTypeId == "BT3SP") {
                    return {
                        displayName: "SP"
                    };
                } else {
                    return racingService.getColumnName(poolObj, nameMap, isWinPlace);
                };
            };

    	}

        function linkFunc($scope, iElm, iAttrs, controller) {}
    }

})();
