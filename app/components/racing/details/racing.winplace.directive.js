(function() {
    'use strict';

    angular
        .module('pinnacleBetApp')
        .directive('raceTableWinPlace', raceTableWinPlace)
        .directive('raceTableWinPlaceColumns', raceTableWinPlaceColumns);

    raceTableWinPlace.$inject = ['racingService','util'];
    raceTableWinPlaceColumns.$inject = ['racingService','util'];


    function raceTableWinPlace (racingService, util) {
        var directive = {
            restrict: 'A', // E = Element, A = Attribute, C =
            replace: true,
            templateUrl: 'components/racing/details/racingWinplace.html',
            controller: raceTableWinPlaceCtrl,
            link: linkFunc
        };

        return directive;

        function raceTableWinPlaceCtrl($scope, $element, $attrs, $transclude){
            $scope.nameMap = racingService.nameMap;
            $scope.getColumnName = racingService.getColumnName;
            $scope.getOdds = racingService.getOdds;
            $scope.getOddsOrig = racingService.getOddsOrig;
            $scope.isHeader = racingService.isHeader;
            $scope.isScratched = racingService.isScratched;
            $scope.getFlucts = racingService.getFlucts;
            $scope.getColumnTooltips = getColumnTooltips;
            $scope.isNumber = isNumber;
            $scope.silkImage = racingService.silkImage;
            $scope.colorUpdate = racingService.colorUpdate;

            function getColumnTooltips(c) {
                switch (c) {
                    case "FO":
                        return "Fixed Win & Place odds are available on the Pinnaclebet website and once confirmed, are not subject to fluctuation.";
                        break;
                    case "BT":
                        return "This product is the best of the 3 Australian TAB prices.";
                        break;
                    case "BTP":
                        return "This product is the best of either the \"Best of the 3 Australian TAB Prices\" or the \"Official On-Course Starting Price (SP)\". SP only applies to the Win Portion of the bet.";
                        break;
                    case "TOT":
                        return "Supertab Dividend";
                        break;
                    case "TF":
                        return "This product is the Best Price declared by the official On-Course Bookmakers Fluctuations. This option is available until the horse shortens on selected race meetings.";
                        break;
                    case "BT3SP":
                        return "This product is the Best of either, \"Best of 3 Australian TAB prices\" or the \"Official On-Course Best Fluctuation\". <b>Bets must be placed 30 minutes prior to the advertised starting time on selected meetings as displayed on the Pinnaclebet website.</b> This Product is subject to (f) and (m) below.";
                        break;
                    case "T5":
                        return "Supertab";
                        break;
                    case "BT1":
                        return "This product is the middle dividend of the 3 Australian TAB prices.";
                        break;
                    default:
                        return c;
                }
            };
            function isNumber(s) {
                return util.isNumber(s) || s == "TF" || s == "BT3SP" || s == "BT3W";
            };
        }

        function linkFunc($scope, iElm, iAttrs, controller) {}

    }



	function raceTableWinPlaceColumns (racingService, util) {
	    var directive = {
	        restrict: 'A',
	        replace: false,
	        templateUrl: 'components/racing/details/racingWinplaceColumns.html',
	        controller: raceTableWinPlaceColumnsCtrl,
	        link: linkFunc
	    };

	    return directive;

		function raceTableWinPlaceColumnsCtrl($scope, $element, $attrs, $transclude) {
	        $scope.nameMap = racingService.nameMap;
	        $scope.getColumnName = racingService.getColumnName;
	        $scope.getOdds = racingService.getOdds;
            $scope.getOddsOrig = racingService.getOddsOrig;
	        $scope.isHeader = racingService.isHeader;
	        $scope.isScratched = racingService.isScratched;
	        $scope.isNumber = util.isNumber;

	        $scope.silkImage = racingService.silkImage;
	        $scope.colorUpdate = racingService.colorUpdate;
		}

		function linkFunc($scope, iElm, iAttrs, controller) {}
    }

})();
