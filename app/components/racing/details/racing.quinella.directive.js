(function() {
    'use strict';

    angular
        .module('pinnacleBetApp')
        .directive('raceTableQuinella', raceTableQuinella);

    raceTableQuinella.$inject = ['racingService','util'];

    function raceTableQuinella (racingService, util) {
        var directive = {
            restrict: 'A',
            replace: true,
            templateUrl: 'components/racing/details/racingQuinella.html',
            controller: raceTableQuinellaCtrl,
            link: linkFunc
        };

        return directive;

        function raceTableQuinellaCtrl($scope, $element, $attrs, $transclude) {
            $scope.nameMap = racingService.nameMap;
            $scope.getColumnName = racingService.getColumnName;
            $scope.getOdds = racingService.getOdds;
            $scope.isHeader = racingService.isHeader;
            $scope.isScratched = racingService.isScratched;
            $scope.isNumber = util.isNumber;

            $scope.silkImage = racingService.silkImage;
            $scope.colorUpdate = racingService.colorUpdate;
        }

        function linkFunc(scope, iElement) {
        }
    }

})();
