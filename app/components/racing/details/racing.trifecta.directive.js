(function() {
    'use strict';

    angular
        .module('pinnacleBetApp')
        .directive('raceTableTrifecta', raceTableTrifecta);

    raceTableTrifecta.$inject = ['racingService','util'];

    function raceTableTrifecta (racingService, util) {
        var directive = {
            restrict: 'A',
            replace: true,
            templateUrl: 'components/racing/details/racingTrifecta.html',
            controller: raceTableTrifectaCtl,
        	link: linkFunc
        };

        return directive;

        function raceTableTrifectaCtl($scope, $element, $attrs, $transclude) {
            $scope.nameMap = racingService.nameMap;
            $scope.getColumnName = racingService.getColumnName;
            $scope.getOdds = racingService.getOdds;
            $scope.isHeader = racingService.isHeader;
            $scope.isScratched = racingService.isScratched;
            $scope.isNumber = util.isNumber;

            $scope.silkImage = racingService.silkImage;
            $scope.colorUpdate = racingService.colorUpdate;
        }

        function linkFunc(scope, iElement) {}

    }

})();
