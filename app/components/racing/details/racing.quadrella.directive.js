(function() {
    'use strict';

    angular
        .module('pinnacleBetApp')
        .directive('raceTableQuadrella', raceTableQuadrella);

    raceTableQuadrella.$inject = ['racingService', 'mainService', 'betslipService','EVN', 'util'];

    function raceTableQuadrella (racingService, mainService, betslipService ,EVN, util) {
        var directive = {
            restrict: 'A',
            replace: true,
            templateUrl: 'components/racing/details/racingQuadrella.html',
            controller: raceTableQuadrellaCtl,
            link: linkFunc
        };

        return directive;

        function raceTableQuadrellaCtl(
        	$scope,
        	$element,
        	$attrs,
        	$transclude,
        	mainService,
        	racingService,
        	meetingDetailsService,
        	util
        ) {
            $scope.nameMap = racingService.nameMap;
            $scope.getColumnName = racingService.getColumnName;
            $scope.getOdds = racingService.getOdds;
            $scope.isScratched = racingService.isScratched;
            $scope.silkImage = racingService.silkImage;
            $scope.colorUpdate = racingService.colorUpdate;
            $scope.makeMultilegs = makeMultilegs;
            $scope.clearMultilegs = clearMultilegs;
            $scope.closed = false;
            $scope.isConfirm = false;

            $scope.legs = [{}, {}, {}, {}];
            $scope.leg_details = {
        		multiLegRaceIds: []
            };

            var eventListeners = {};
            var listenedPools = [];
            var multiLegRaceNums = [4];



            $scope.$on('$destroy', function () {
                for (var i = 0; $scope.leg_details && i < $scope.leg_details.multiLegRaceIds.length; i++) {
                    meetingDetailsService
                        .unsubscribeRaceUpdateEvent($scope.leg_details.multiLegRaceIds[i]);
                };
                meetingDetailsService
                    .unsubscribePools(listenedPools);

                for (var k in eventListeners) {
                    eventListeners[k]();
                }
            });

            $scope.getSTABPool = function (race) {
                if (!race) {
                    return;
                }
                for (var i = 0; i < race.pools.length; i++) {
                    var p = race.pools[i];
                    if (p.poolTypeId == "W" && p.bookmaker.id == "TobRacing") {
                        return p;
                    }
                }
                // safelog("no stab");
            };

            $scope.toogleSelection = function (race) {
                if ($scope.isClosed) {
                    return;
                }

                var ns = false;
                for (var i = 0; i < race.selectedEntrants.length; i++) {
                    var s = race.selectedEntrants[i];
                    if (!s && !(race.entrants[i].scratched || race.entrants[i].lateScratching ) ) {
                        ns = true;
                        break;
                    }
                    ;
                }

                for (var i = 0; i < race.selectedEntrants.length; i++) {
                    if ( !(race.entrants[i].scratched || race.entrants[i].lateScratching) ) {
                        race.selectedEntrants[i] = ns;
                    }
                }
            };

            $scope.checkBox = function (legIndex, entrantIndex) {
                $scope.legs[legIndex].selectedEntrants[entrantIndex] =
                	!$scope.legs[legIndex].selectedEntrants[entrantIndex];
            }

            racingService.getMultiLegsByMeetingID($scope.meetingID, true).then(function (data){
                // safelog("legs: ", data);
                if (!data) {
                    return;
                }

                if (!data.pools || data.pools.length == 0) {
                    $scope.leg_details = undefined;
                    return;
                }

                $scope.leg_details = data.pools[0];
                if ($scope.leg_details.multiLegRaceIds.length < 4) {
                    $scope.closed = true;
                }

                for (var i = 0; data.pools.length > 0 && i < $scope.leg_details.multiLegRaceIds.length; i++)
                {
                    var r = $scope.leg_details.multiLegRaceIds[i];
                    (function (index, race) {
                        racingService.getRaceOdds($scope.meetingID, r, "WP", false, true)
                        .then(function (data) {
                            if (!data) {
                                return;
                            };
//                            safelog("multi: ", data);
                            multiLegRaceNums[index] = data.raceNumber;
                            $scope.legs[index] = {
                                pools: data.pools,
                                entrants: meetingDetailsService
                                    .raceUpdateEventLateScratchingHandler(
                                        data.scratchingStatus,
                                        data.status,
                                        undefined,
                                        data.entrants,
                                        data.extraInfoOfEntrant),
                                race: data,
                                silkRunnersUrl: data.silkRunnersUrl,
                                type: data.type
                            };
                            $scope.legs[index].selectedEntrants = [];
                            for (var n = 0; n < $scope.legs[index].entrants.length; n++) {
                                $scope.legs[index].selectedEntrants.push(false);
                            }

                            /* decide if QD is closed
                             */
                            $scope.closed = isClosed($scope.legs);
                            if ($scope.legs.length == 4 && $scope.closed) {
                                popClosedMessage($scope.legs);
                            }

                            /*watch selection change
                             */
                            $scope.$watchCollection('legs[' + index + '].selectedEntrants', function (nv) {

                                if($scope.legs.length === 4){
                                    var table2D = getCombinationstable($scope.legs);
                                    if(table2D.length === 4){
                                        var comb = racingService.tableTranslation(table2D,$scope.box_selection);
                                        var combArr = racingService.getAllCombinationsForMultiLegs(comb);
                                        combArr.length > 0
                                        ? $scope.isConfirm = true
                                        : $scope.isConfirm = false;
                                    }

                                }


                            });

                            /* subscribe for stab pool
                             */
                            var stab_pool = $scope.getSTABPool(data);
                            if (stab_pool) {
                                meetingDetailsService.changePoolSubscription([stab_pool]);
                                listenedPools.push(stab_pool);
                            }


                            /* subscribe for race
                             */
                            meetingDetailsService.subscribeRaceUpdateEvent(r);

                            /* listen for race
                             */
                            if (!eventListeners[EVN.race_update_event_server_addr]){
                                eventListeners[EVN.race_update_event_server_addr] = $scope.$on(
                                    EVN.race_update_event_server_addr,
                                    function (e, data){
                                        for (var n = 0; n < $scope.legs.length; n++) {
                                            var l = $scope.legs[n];
                                            if (l.race.id == data.id) {
                                                l.race = data;
                                                l.entrants = meetingDetailsService
                                                    .raceUpdateEventLateScratchingHandler(
                                                        data.scratchingStatus,
                                                        data.status,
                                                        undefined,
                                                        data.entrants,
                                                        data.extraInfoOfEntrant);
                                                l.pools = data.pools;
                                                /*do not resubscribe pool, assuming pool id will not change
                                                 */
                                            };
                                        };
                                        $scope.closed = isClosed($scope.legs);
                                    });
                            }

                            /* listen for stab pools
                             */
                            if (stab_pool) {
                                var addr = null;
                                if (addr && !eventListeners[addr])
                                {
                                    eventListeners[addr] = $scope.$on(addr, function (e, data) {
                                        for (var n = 0; n < $scope.legs.length; n++) {
                                            var p = $scope.legs[n].pools;
                                            for (var m = 0; m < p.length; m++) {
                                                if (p[m].id == data.id) {
                                                    p[m] = data;
                                                };
                                            };
                                        };
                                    });
                                };
                            }

                        });
                    })(i, r);
                }
            });

            function getCombinationstable(legs){
                var table2D =[];
                legs.forEach(function(elem){
                    if(angular.isArray(elem.selectedEntrants) && elem.selectedEntrants.length > 0){
                        table2D.push(elem.selectedEntrants);
                    }
                })
                return table2D;
            }


            function makeMultilegs(){
                if($scope.isConfirm){
                    var slip = {
                        data : {
                            selections : racingService.tableTranslation(getCombinationstable($scope.legs), $scope.box_selection),
                            selectionArr : racingService.exoticsSelection(racingService.tableTranslation(getCombinationstable($scope.legs), $scope.box_selection)),
                            combinations : racingService.getAllCombinationsForMultiLegs(racingService.tableTranslation(getCombinationstable($scope.legs), $scope.box_selection)).length,
                            pool : $scope.leg_details,
                            raceInfo : $scope.details
                        },
                        type : 'EXOTICS',
                        odds :  1,
                        isMulti: false
                    };
                    betslipService.addSlip(slip);
                    clearMultilegs();
                }
            }
            function clearMultilegs(){
                reset();
            }
            function isClosed(legs) {
                for (var i = 0; i < 4; i++) {
                    if (!legs[i].race) {
                        return false;
                    };
                    if (legs[i].race.status != "Open")
                        return true;
                }
                return false;
            }

            function popClosedMessage(legs) {
                var raceNum = "";
                for (var i = 0; i < legs.length; i++) {
                    var l = legs[i];
                    if (!l.race)
                        return;
                    if (l.race.status != "Open") {
                        if (raceNum.length != 0) {
                            raceNum += ", ";
                        }

                        raceNum += l.race.raceNumber;
                    }
                }

            }

            function assembleSlip() {
                var hasSelected = false;

                for (var i = 0; i < $scope.legs.length; i++) {
                    var l = $scope.legs[i];
                    for (var n = 0; l.selectedEntrants && n < l.selectedEntrants.length; n++) {
                        var s = l.selectedEntrants[n];
                        if (s) {
                            hasSelected = true;
                            break;
                        }

                    }

                    if (hasSelected)
                        break;
                }

                if (!hasSelected) {
                    return;
                }

                /*
                 * reach here only if every race has at
                 * least one entrant selected
                 */
                var slip = {
                    type: "Racing",
                    content: {
                        raceInfo: $scope.details,
                        bettype: 'Multileg',
                        product: $scope.betMethod,
                        pool: {
                            id: $scope.leg_details.id,
                            poolTypeId: "QD",
                            bookmaker: {
                                id: 'TobRacing'
                            }
                        },
                        checked_boxes: [
                            $scope.legs[0].selectedEntrants,
                            $scope.legs[1].selectedEntrants,
                            $scope.legs[2].selectedEntrants,
                            $scope.legs[3].selectedEntrants
                        ],
                        box_selection: false,
                        isFlexi: false,
                        exotics_selection_confirmed: false,
                        entrants: [
                            $scope.legs[0].entrants,
                            $scope.legs[1].entrants,
                            $scope.legs[2].entrants,
                            $scope.legs[3].entrants
                        ],
                        amount: 0,
                        meetingId: $scope.meetingID,
                        multiLegRaceIds: angular.copy($scope.leg_details.multiLegRaceIds),
                        multiLegRaceNums: multiLegRaceNums,
                        runnerSelectionString: [],
                        combinations: 0
                    }
                };
                return slip;
            }

            function reset() {
                if ($scope.leg_details.multiLegRaceIds) {
                    for (var i = 0; i < $scope.leg_details.multiLegRaceIds.length; i++) {
                        if ($scope.legs[i].entrants) {
                            for (var n = 0; n < $scope.legs[i].entrants.length; n++) {
                                $scope.legs[i].selectedEntrants[n] = false;
                            }
                        }
                    }

                }
            }
        }

        function linkFunc(scope, el) {
        }

    }

})();
