(function() {
    'use strict';

    angular
	    .module('pinnacleBetApp')
	    .directive('raceTable', raceTable);
    raceTable.$inject = ['racingService', 'betslipService', 'util', 'EVN'];

	function raceTable (racingService, betslipService, util, EVN) {
	    var directive = {
	        restrict: 'A',
	        templateUrl: 'components/racing/racingRaceTable.html',
	        controller: raceTableCtrl,
	        link: linkFunc
	    };

	    return directive;

	    function raceTableCtrl($scope, $element){
	        $scope.box_selection = false;
	        $scope.checked_boxes = [];
            $scope.toggleColumn = toggleColumn;
	        $scope.changeBoxSelection = changeBoxSelection;
	        $scope.checkBox = checkBox;
            $scope.makeSelection = makeSelection;
            $scope.clearSelection = clearSelection;
            $scope.isConfirm = false;
            $scope.slip = {};

	        var subset_size = 0;
	        var product = undefined;
	        var poolTypeId = undefined;



	        $scope.$watch('betMethod', function (nv, ov) {
	            // delete $scope.checked_boxes;
	            // delete $scope.box_selection;
	            product = nv;
	            if (nv == 'Quinella') {
	                subset_size = 2;
	                poolTypeId = "Q";
	            } else if (nv == 'Exacta') {
	                subset_size = 2;
	                poolTypeId = "E";
	            } else if (nv == 'Trifecta') {
	                subset_size = 3;
	                poolTypeId = "T";
	            } else if (nv == 'FirstFour') {
	                subset_size = 4;
	                poolTypeId = "FF";
	            }

	            reset();
	        });

	        $scope.$watch('[checked_boxes, box_selection]', function (oldV, newV) {
	            if ($scope.betMethod == 'Quinella') {
	                var count = 0;
	                for (var i = 0; i < $scope.checked_boxes[0].length; i++) {
	                    if ($scope.checked_boxes[0][i]) {
	                        count++;
	                        if (count > 1) {
	                            $scope.box_selection = true;
	                            break;
	                        }
	                    }
	                }

                    if($scope.box_selection){
                        getQuinellaCombinations() > 0
                            ? $scope.isConfirm = true
                            : $scope.isConfirm = false;
                    }else{
                        getCombinations().length > 0
                            ? $scope.isConfirm = true
                            : $scope.isConfirm = false;
                    }

	            }else{
                    var combArr = getCombinations();
                    combArr.length > 0
                        ? $scope.isConfirm = true
                        : $scope.isConfirm = false;
                }
	        }, true);

            function getCombinations (){
                var table2D = racingService.tableTranslation($scope.checked_boxes, $scope.box_selection);
                var combArr = racingService.getAllCombinations(table2D);
                return combArr;
            }

            function getQuinellaCombinations (){
                var combArr = racingService.tableTranslation($scope.checked_boxes, $scope.box_selection);
                return (combArr[0].length * (combArr[0].length - 1) / 2);
            }





            function checkBox(column, row) {
	            $scope.checked_boxes[column][row] = !$scope.checked_boxes[column][row];
	        }
            function changeBoxSelection($event) {
	            var checkbox = $event.target;
	            if (!checkbox.checked && $scope.slip.content.product == 'Quinella') {
	                for (var i = 0; i < $scope.checked_boxes[0].length; i++) {
	                    $scope.checked_boxes[0][i] = false;
	                }
	            }
	        }
            function makeSelection(){
                if($scope.isConfirm){
                    var slip = {
                        data : {
                            selections : racingService.tableTranslation($scope.checked_boxes,$scope.box_selection),
                            selectionArr : racingService.exoticsSelection(racingService.tableTranslation($scope.checked_boxes,$scope.box_selection)),
                            combinations : $scope.betMethod === 'Quinella' && $scope.box_selection ? getQuinellaCombinations() : getCombinations().length,
                            pool : $scope.details.pools.find(findFun),
                            raceInfo : $scope.details
                        },
                        type : 'EXOTICS',
                        odds :  1,
                        oddsId : 0,
                        isMulti: false
                    };
                    betslipService.addSlip(slip);
                    clearSelection();
                }


                function findFun(pool){
                    return pool.poolTypeId == poolTypeId;
                }
            }
            function clearSelection(){
                reset();
            }

            function toggleColumn(d) {
	            var cp = angular.copy($scope.checked_boxes[d]);
	            for (var i = cp.length - 1; i >= 0; i--) {
	                if ($scope.entrants[i].scratched || $scope.entrants[i].lateScratching) {
	                    cp.splice(i, 1);
	                };
	            };
	            var index = $.inArray(false, cp);
	            if (index >= 0) {
	                for (var i = 0; i < $scope.checked_boxes[d].length; i++) {
	                    if (!($scope.entrants[i].scratched || $scope.entrants[i].lateScratching) ) {
	                        $scope.checked_boxes[d][i] = true;
	                    };
	                };
	            } else {
	                for (var i = 0; i < $scope.checked_boxes[d].length; i++) {
	                    $scope.checked_boxes[d][i] = false;
	                };
	            }
	        }
	        function isSelectionExist() {
	            for (var i = 0; i < $scope.checked_boxes.length; i++) {
	                for (var j = 0; j < $scope.checked_boxes[i].length; j++) {
	                    if ($scope.checked_boxes[i][j])
	                        return true;
	                }
	            }
	            return false;
	        }
	        function reset() {
	            if ($scope.betMethod != "WinPlace") {
	                var poolId = -1;
	                $scope.checked_boxes = [];
	                $scope.box_selection = false;
	                $scope.isConfirm = false;
	                if ($scope.entrants) {
	                    for (var i = 0; i < subset_size; i++) {
	                        var temp_column = [];
	                        for (var j = 0; j < $scope.entrants.length; j++) {
	                            temp_column.push(false);
	                        }
	                        $scope.checked_boxes.push(temp_column);
	                    }
	                };
	                if ($scope.details.pools instanceof Array && poolTypeId) {
	                    $scope.details.pools.forEach(function (pool) {
	                        if (pool.poolTypeId == poolTypeId) {
	                            poolId = pool.id;
	                        }
	                    });
	                }

                }
	        }
	    }

	    function linkFunc($scope, iElm, iAttrs, controller) {
        }
    }

})();
