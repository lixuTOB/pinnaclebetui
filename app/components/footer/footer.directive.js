/**
 * Created by tob on 19/05/2016.
 */
 (function(){
     'use strict';

     angular
         .module('pinnacleBetApp')
         .directive('footerDir', footer)


     footer.$inject = ['EVN'];
     function footer(EVN){
         var directive = {
             restrict: 'A',
             templateUrl: 'components/footer/footer.html',
             link: linkFunc
         };

         return directive;

         function linkFunc(scope, element) {
            scope.mobileUrl = EVN.mobileUrl;
         }
     }

 })();
