(function() {
     'use strict';

     angular
         .module('pinnacleBetApp')
         .config(footConfig);

     footConfig.$inject = ['$stateProvider'];

     function footConfig($stateProvider) {
         $stateProvider
             .state('aboutus',{
                 url : "/aboutus",
                 views : {
                     'content': {
                         templateUrl: "components/footer/aboutus.html",
                         controller: 'footerCtrl'
                     }
                 },
             })

             .state('faqs',{
                 url : "/faqs",
                 views : {
                     'content': {
                         templateUrl: "components/footer/faqs.html",
                         controller: 'footerCtrl'
                     }
                 },
             })

             .state('policy',{
                 url : "/policy",
                 views : {
                     'content': {
                         templateUrl: "components/footer/privacy-policy.html",
                         controller: 'footerCtrl'
                     }
                 },
             })

             .state('terms',{
                 url : "/terms",
                 views : {
                     'content': {
                         templateUrl: "components/footer/terms-conditions.html",
                         controller: 'footerCtrl'
                     }
                 },
             })

             .state('responsible',{
                 url : "/responsible",
                 views : {
                     'content': {
                         templateUrl: "components/footer/responsible-gambling.html",
                         controller: 'footerCtrl'
                     }
                 },
             });
     }


 })();
