(function() {
    'use strict';

    angular
        .module('pinnacleBetApp')
        .config(homeStateConfig);

    homeStateConfig.$inject = ['$stateProvider'];

    function homeStateConfig($stateProvider) {
        $stateProvider
            .state('home', {
                url : "/",
                views : {
                    'content' : {
                        templateUrl: "components/home/home.html",
                        controller: "homeCtrl"
                    }
                }
            });
    }

})();
