(function(){
    'use strict';

    angular
        .module('pinnacleBetApp')
        .controller('homeCtrl', homeCtrl);

    homeCtrl.$inject = ['$scope','$rootScope','homeService','util','betslipService'];

    function homeCtrl($scope,$rootScope,homeService,util,betslipService){
        $scope.hashCode = util.hashCode;
        $scope.getTime = util.getTime;
        $scope.getSportPriceDSP = util.getSportPriceDSP;
        $scope.nameMap = nameMap();
        $scope.findImage = findImage;
        $scope.startTimeDisplay = startTimeDisplay;
        $scope.addSlip = addSlip;
        $scope.featureEvents = [];
        $scope.moreFeatureEvents = [];
        $scope.topLeagueEvents = {};
        $scope.australianEvents = [];
        $scope.popularEvents = [];
        $scope.nextTWRaces = [];

        util.sidebarView('HOME',$rootScope);

        homeService.featureEvents().then(function(res){
            if(res && res.data){
                $scope.featureEvents = res.data.events.slice(0,5);
                $scope.moreFeatureEvents = res.data.events.slice(5,10);
            }
        });
        homeService.topLeagueEvents().then(function(res){
            if(res && res.data && res.data.sportLeagueEvents){
                $scope.topLeagueEvents.LeagueNames =[];
                $scope.topLeagueEvents.events ={};
                res.data.sportLeagueEvents.forEach(function(event){
                    for(var key in event){
                        event[key].forEach(function(evnt){
                            evnt.sportPriceDtos.sort(function(a,b){
                                return util.sportPricePrio(a) - util.sportPricePrio(b);
                            });
                        });
                        $scope.topLeagueEvents.LeagueNames.push(key);
                        $scope.topLeagueEvents.events[key] = event[key];
                        $scope.topLeagueEvents.activeLeague = 'Racing'; // Default
                    }
                });

            }
        });
        homeService.australianEvents().then(function(res){
            if(res && res.data){
                $scope.australianEvents = res.data.events;
            }
        });
        homeService.popularEvents().then(function(res){
            if(res && res.data){
                for(var i=0; i< 2; i++){
                    $scope.popularEvents.push(res.data.sportEventAndMarketDto.splice(0,2));
                }
            }
        });
        homeService.nextTWRaces().then(function(res){
            if(res && res.data){
                for(var i = 0; i < 3 ; i++){
                    $scope.nextTWRaces.push(res.data.races.splice(0,4));
                }
            }
        });

        function startTimeDisplay(starttime){
            var timestamp = new Date().getTime();
            var display = '';
            if(starttime - timestamp > 0 && starttime - timestamp <= 300000){
                var sec = Math.floor((starttime - timestamp)/1000%60);
                var min = Math.floor((starttime - timestamp)/1000/60%60);
                display = min + ' m ' + sec + ' s';
            }else if(starttime - timestamp > 300000 && starttime - timestamp <= 3600000){
                display = ((starttime - timestamp)/1000/60).toFixed(0) + ' m';
            }else{
                display = ((starttime - timestamp)/1000/60).toFixed(0) + ' m';
            }
            return display;
        }

        function nameMap(){
            return {
                'National Rugby League'         : 'NRL',
                'Aussie Rules Football (AFL)'   : 'AFL',
                'England - Premier League'      : 'EPL',
                'Super 18 Rugby'                : 'Super 18',
                'NBA'                           : 'NBA',
                'NCAA'                          : 'NCAA',
                'UEFA - Champions League'       : 'UEFA',
                'Australia - A League'          : 'A-League',
                'NFL'                           : 'NFL',
                'NHL OT Included Alternates'    : 'NHL'
            }
        }
        function backgroundImagesMap(){
            return{
                'RUGBY_LEAGUE'                  : 'rugby league 1',
                'AUSSIE_RULES'                  : 'Aussie Rules 1',
                'SOCCER'                        : 'Soccer 1',
                'RUGBY_UNION'                   : 'Rugby Union 1',
                'BASKETBALL'                    : 'Basketball 1',
                'GRID_IRON'                     : 'Gridiron 1',
                'BASEBALL'                      : 'Baseball 1',
                'TENNIS'                        : 'ATP_Tennis_1'
            }
        }
        function findImage(sportType){
            var imageNamae = backgroundImagesMap()[sportType];
            if(!imageNamae){
                return imageNamae = 'defaultImage';
            }
            return imageNamae;
        }
        function addSlip(price){
            var slip = {
                data : price,
                type : 'SPORT',
                odds : price.price,
                oddsId : price.id,
                isMulti: false
            };
            betslipService.addSlip(slip);
        }


    }

})();
