(function(){
    'use strict';

    angular
        .module('pinnacleBetApp')
        .factory('homeService', homeService);

    homeService.$inject = ['$http', 'EVN'];

    function homeService($http, EVN){
        var service =  {
            featureEvents   : featureEvents,
            topLeagueEvents : topLeagueEvents,
            australianEvents: australianEvents,
            popularEvents   : popularEvents,
            nextTWRaces     : nextTWRaces

        }

        return service;

        function featureEvents(){
            return $http.get(EVN.apiDataUrl + '/data/pntsport/featureevents');
        }
        function topLeagueEvents(){
            return $http.get(EVN.apiDataUrl + '/data/pntsport/topleagueevents');
        }
        function australianEvents(){
            return $http.get(EVN.apiDataUrl + '/data/pntsport/australianEvents');
        }
        function popularEvents(){
            return $http.get(EVN.apiDataUrl + '/data/pntsport/popularevents');
        }
        function nextTWRaces(){
            return $http.get(EVN.apiDataUrl + '/data/racing/nexttwraces');
        }


    }

})();
