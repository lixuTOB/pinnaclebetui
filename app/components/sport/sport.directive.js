(function(){
    'use strict';

    angular
        .module('pinnacleBetApp')
        .directive('sportWinMarketRow', sportWinMarketRow);

    sportWinMarketRow.$inject = ['$compile'];

    function sportWinMarketRow($compile){
        var directive = {
            restrict: 'E',
            templateUrl: 'components/sport/markets/winRow.html',
            scope: {
              prices: '=prices'
            },
            link: linkFunc
        };

        return directive;

        /* private helper methods*/

        function linkFunc(scope, element) {
        }
    }
})();
