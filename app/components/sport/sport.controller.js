(function(){
    'use strict';

    angular
        .module('pinnacleBetApp')
        .controller('sportCtrl', sportCtrl);

    sportCtrl.$inject = ['$scope','$rootScope','$stateParams','sportService','util','$http','betslipService','messagingService'];

    function sportCtrl($scope,$rootScope, $stateParams, sportService, util, $http, betslipService, messagingService){
        $scope.hashCode = util.hashCode;
        $scope.leagueId = $stateParams.leagueId;
        $scope.eventId = $stateParams.eventId;
        $scope.duration = $stateParams.duration;
        $scope.getMarket = getMarket;
        $scope.sportDurt = [];
        $scope.sportAll = {};
        $scope.markets = {};
        $scope.getSportPriceDSP = util.getSportPriceDSP;
        $scope.addSlip = addSlip;
        $scope.getAddlMakt = sportAdditionalMarketHandler;
        $scope.getDurt = sportDurtHandler;
        $scope.getTime = util.getTime;
        $scope.getColorStatus = getColorStatus;

        util.sidebarView('SPORT',$rootScope);
        mainHandler();

        function mainHandler(){
            util.scrollTop();
            if($scope.leagueId || $scope.eventId){
                sportDetailHandler();
            }
            if($scope.duration){
                sportDurtHandler($scope.duration);
            }

        }

        function sportDetailHandler(){
            if($scope.leagueId){
                sportService.getSportAllLeagues($scope.leagueId)
                    .then(sportDetailRes, sportDetailRej);
            }
            if($scope.eventId){
                sportService.getSportEvent($scope.eventId)
                    .then(sportDetailRes, sportDetailRej);
            }
            /*
            *   @resolve   sportDetailRes
            *   @reject    sportDetailRej
            */
            function sportDetailRes(res){
                if(!res.data.eventsAndPrices){
                    return;
                }
                $scope.sportAll = new sportAllInit();
                for(var idx = 0 ; idx < res.data.eventsAndPrices.length ; idx++ ){
                    var prices      = res.data.eventsAndPrices[idx].sportPriceDtos;
                    var sportEvent  = res.data.eventsAndPrices[idx].sportEventDto;
                    var avlMarket   = res.data.eventsAndPrices[idx].numberOfAvailableMarkets;
                    var homeTeam    = res.data.eventsAndPrices[idx].homeTeam;
                    var awayTeam    = res.data.eventsAndPrices[idx].awayTeam;
                    var sportDTO    = new sportDTOInit();

                    if(!$scope.sportAll.sportType){
                        getSportInfo($scope.sportAll,sportEvent);
                    }
                    getLeagueInfo(prices,sportEvent,homeTeam,awayTeam,avlMarket,sportDTO);
                    getPrices(prices,sportDTO);
                    $scope.sportAll.leagues.push(sportDTO);

                    /* fire messaging*/
                    subWagerEvent(sportDTO.sportInfo.leagueId);
                }
            }
            function sportDetailRej(sportAll){}

            function getSportInfo(sportAll, sportEvent){
                sportAll.sportType = sportEvent.sportType;
            }
            function getLeagueInfo(prices, sportEvent, homeTeam, awayTeam, avlMarket, sportDTO){
                sportDTO.sportInfo.leagueName    = sportEvent.parentEvent.value;
                sportDTO.sportInfo.leagueId      = sportEvent.parentEvent.key;
                sportDTO.sportInfo.eventName     = sportEvent.id.value;
                sportDTO.sportInfo.eventId       = sportEvent.id.key;
                sportDTO.sportInfo.awayTeam      = awayTeam;
                sportDTO.sportInfo.homeTeam      = homeTeam;
                sportDTO.sportInfo.avlMarket     = avlMarket;
                sportDTO.sportInfo.isLiveEvents  = sportEvent.isLiveEvents;
                sportDTO.sportInfo.startTime     = sportEvent.startTime;
                sportDTO.sportInfo.colNum        = prices.propDrawWin ? 3 : 2;
            }
            function getPrices(prices, sportDTO){
                sportDTO.prices.propAwayWin = prices.propAwayWin|| {};
                sportDTO.prices.propDrawWin = prices.propDrawWin|| {};
                sportDTO.prices.propHomeWin = prices.propHomeWin|| {};
                sportDTO.prices.propHomeHDP = prices.propHomeHDP|| {};
                sportDTO.prices.propAwayHDP = prices.propAwayHDP|| {};
                sportDTO.prices.propHomeOU  = prices.propHomeOU || {};
                sportDTO.prices.propAwayOU  = prices.propAwayOU || {};
            }


            function sportAllInit(sportType, leagues){
                this.sportType = '';
                this.leagues = [];
            }
            function sportDTOInit(){
                this.sportInfo = {
                    leagueName: '',
                    leagueId  : -1,
                    eventName : '',
                    eventId   : -1,
                    startTime : -1,
                    homeTeam  : '',
                    awayTeam  : '',
                    avlMarket   : -1
                }
                this.prices  = {
                    propAwayHDP : {},
                    propAwayOU  : {},
                    propAwayWin : {},
                    propDrawWin : {},
                    propHomeHDP : {},
                    propHomeOU  : {},
                    propHomeWin : {}
                }
            }

        }
        function sportAdditionalMarketHandler(eventId){
            if(angular.isNumber(parseInt(eventId))){
                if(!$scope.markets[eventId]){
                    sportService.getSportAllMarkets(eventId)
                        .then(sportEventRes, sportEventRej);
                }
            }

            /*
            *   @resolve   sportEventRes
            *   @reject    sportEventRej
            */
            function sportEventRes(res){
                if(!res.data.markets || !res.data.event){
                    return;
                }
                for(var i =0 ; i < res.data.markets.length; i++){
                    getGroupPrice(res.data.markets[i]);
                }
                $scope.markets[res.data.event.id.key] = {
                    markets : res.data.markets,
                    event   : res.data.event,
                    isReady : true
                };

            }
            function sportEventRej(res){

            }

            function getMarketName(){

            }
            function getGroupPrice(market){
                /**
                *   Main markets( win, handicap, over_under) have particular
                *   to display, so it need to group up beforehand.
                */
                if(market.type.toLowerCase() === 'win'){
                    var groupHash = {};
                    var sortedRes = [];
                    market.prices.forEach(function(price){
                        if(!groupHash[price.priceGroupId]){
                            groupHash[price.priceGroupId] = {
                                home : {},
                                away : {},
                                draw : {}
                             }
                        }
                        if(price.home){
                            groupHash[price.priceGroupId].home = price;
                        }else if(price.proposition.key.toLowerCase() === 'draw'){
                            groupHash[price.priceGroupId].draw = price;
                        }else {
                            groupHash[price.priceGroupId].away = price;
                        }
                    });
                    for(var key in groupHash){
                        sortedRes.push(groupHash[key]);
                    }
                    market.prices = sortedRes;
                }
                if(market.type.toLowerCase() === 'handicap'  ){
                    var groupHash = {};
                    var sortedRes = [];
                    market.prices.forEach(function(price){
                        if(!groupHash[price.priceGroupId]){
                            groupHash[price.priceGroupId] = {
                                home : {},
                                away : {}
                             }
                        }
                        price.home ? groupHash[price.priceGroupId].home = price : groupHash[price.priceGroupId].away = price;
                    });
                    for(var key in groupHash){
                        sortedRes.push(groupHash[key]);
                    }
                    market.prices = sortedRes;
                }
                if(market.type.toLowerCase() === 'over_under'){
                    var groupHash = {};
                    var sortedRes = [];
                    market.prices.forEach(function(price){
                        if(!groupHash[price.priceGroupId]){
                            groupHash[price.priceGroupId] = {
                                home : {},
                                away : {}
                             }
                        }
                        price.proposition.key.toLowerCase().indexOf('over') !==-1 ? groupHash[price.priceGroupId].home = price : groupHash[price.priceGroupId].away = price;
                    });
                    for(var key in groupHash){
                        sortedRes.push(groupHash[key]);
                    }
                    market.prices = sortedRes;
                }
            }

        }
        function sportDurtHandler(duration){
            if(duration.toLowerCase() === 'live'){
                sportService.sportDurationLive()
                    .then(sportDurtLiveRes, sportDurtLiveRej);
            }else{
                sportService.sportDuration('ALL', duration)
                    .then(sportDurtRes, sportDurtRej);
            }

            /*
            *   @resolve   sportDurtRes
            *   @reject    sportDurtRej
            */
            function sportDurtRes(res){
                if(!res.data.sportEvents){
                    return ;
                }
                $scope.sportDurt = res.data.sportEvents;
            }
            function sportDurtRej(res){}

            /*
            *   @resolve   sportDurtLiveRes
            *   @reject    sportDurtLiveRej
            */
            function sportDurtLiveRes(res){
                if(!res.data.sportEvents){
                    return ;
                }
                $scope.sportDurt = res.data.sportEvents;
            }
            function sportDurtLiveRej(res){}



        }
        function getMarket(str){
            if(str){
                if(str.toLowerCase() === 'win'){
                    return 'win';
                }
                if(str.toLowerCase() === 'handicap'){
                    return 'handicap';
                }
                if(str.toLowerCase() === 'over_under'){
                    return 'over_under';
                }
            }

            return 'others';

        }
        function addSlip(price){
            if(price && price.id){
                var slip = {
                    data : price,
                    type : 'SPORT',
                    odds : price.price,
                    oddsId : price.id,
                    isMulti: false
                };
                betslipService.addSlip(slip);
            }
        }
        function getColorStatus(priceObj){
            if(priceObj){
                if(priceObj.colorStatus === 'higher'){
                    return {
                        'higher' : true
                    };
                }else if(priceObj.colorStatus === 'lower'){
                    return {
                        'lower' : true
                    };
                }else{
                    return {
                        'status_quo' : true
                    };
                }
            }
        }

        function updateMarket(data){
            $scope.sportAll.leagues.forEach(function(league){
                for(var key in league.prices){
                    var priceObj = league.prices[key];
                    if(priceObj.id && data.prices[priceObj.id]){
                        if(priceObj.price > data.prices[priceObj.id].price){
                            priceObj.colorStatus = 'lower';
                        }else if (priceObj.price < data.prices[priceObj.id].price){
                            priceObj.colorStatus = 'higher';
                        }else{
                            priceObj.colorStatus = 'status_quo';
                        }
                        priceObj.maximumRisk = data.prices[priceObj.id].maximumRisk;
                        priceObj.minimumRisk = data.prices[priceObj.id].minimumRisk;
                        priceObj.price = data.prices[priceObj.id].price;
                    }
                }
            });
        }

        $scope.$on('$destroy',function(){
            unsubSportEvent();
        });

        /*
        *  Message section
        */
        function subWagerEvent(leagueId) {
            if(leagueId){
                messagingService.subscribe('SportEventUpdateEvent', leagueId, function (data) {
                    util.log("channel: "+$scope.leagueId+" , sport message work:");
                    util.log(data);
                    if(!data){return;}
                    updateMarket(data);
                });
            }
        }
        function unsubSportEvent() {
            messagingService.unsubscribe('SportEventUpdateEvent');
        }

    }

})();
