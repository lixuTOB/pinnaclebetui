(function() {
    'use strict';

    angular
        .module('pinnacleBetApp')
        .config(sportConfig);

    sportConfig.$inject = ['$stateProvider'];

    function sportConfig($stateProvider) {
        $stateProvider
            .state('sports',{
                url : "/sports?duration",
                views : {
                    'content' : {
                        templateUrl: "components/sport/sports.html",
                        controller: "sportCtrl"
                    }
                }
            })
            .state('sportDetail', {
                url : "/sportDetail?leagueId",
                views : {
                    'content' : {
                        templateUrl: "components/sport/sportDetail.html",
                        controller: "sportCtrl"
                    }
                }
            })
            .state('sportEvent', {
                url : "/event?eventId",
                views : {
                    'content' : {
                        templateUrl: "components/sport/sportDetail.html",
                        controller: "sportCtrl"
                    }
                }
            });
    }

})();
