(function(){
    'use strict';

    angular
        .module('pinnacleBetApp')
        .factory('sportService', sportService);

    sportService.$inject = ['$http', 'EVN'];

    function sportService($http, EVN){
        var service = {
            getSportAllLeagues: getSportAllLeagues,
            getSportEvent: getSportEvent,
            getSportAllMarkets: getSportAllMarkets,
            sportDuration: sportDuration,
            sportDurationLive: sportDurationLive
        }

        return service;

        function getSportAllLeagues(leagueId){
            return $http.get(EVN.apiDataUrl + '/data/pntsport/league/'+leagueId);
        }
        function getSportEvent(eventId){
            return $http.get(EVN.apiDataUrl + '/data/pntsport/event/'+eventId);
        }
        function getSportAllMarkets(eventId){
            return $http.get(EVN.apiDataUrl + '/data/pntsport/markets/'+eventId);
        }
        function sportDuration(sportType, duration){
            return $http.get(EVN.apiDataUrl + '/data/pntsport/sportTypes/'+sportType+'/sportEvents/duration/'+duration);
        }
        function sportDurationLive(){
            return $http.get(EVN.apiDataUrl + '/data/pntsport/allLiveEvents');
        }

    }


})();
