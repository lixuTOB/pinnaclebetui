(function() {
    'use strict';

    angular
        .module('pinnacleBetApp')
        .config(accountStateConfig);

    accountStateConfig.$inject = ['$stateProvider'];

    function accountStateConfig($stateProvider) {
        $stateProvider
            .state('account', {
                url : "/account?parentCategory",
                views : {
                    'content' : {
                        templateUrl: "components/account/accountPage.html",
                        controller: "accountCtrl"
                    }
                }
            })

            .state('forgotPassword', {
                url : "/account/forgotPassword",
                views : {
                    'content' : {
                        templateUrl: "components/account/forgotPassword.html",
                        controller: "forgotPasswordCtrl"
                    }
                }
            })

            .state('resetPassword', {
                url : "/account/resetPassword?t&d",
                views : {
                    'content' : {
                        templateUrl: "components/account/resetPassword.html",
                        controller: "forgotPasswordCtrl"
                    }
                }
            })

            .state('changePassword', {
                url: '/account/changePassword',
                views : {
                    'content' : {
                        templateUrl: "components/account/changePassword.html",
                        controller: "profileCtrl"
                    }
                }
            })

            .state('idSubmission', {
                url: '/account/idSubmission',
                views : {
                    'content' : {
                        templateUrl: "components/account/idSubmission.html",
                        controller: "bankCtrl"
                    }
                }
            })

            .state('viewProfile', {
                url: '/account/viewProfile',
                views : {
                    'content' : {
                        templateUrl: "components/account/viewProfile.html",
                        controller: "profileCtrl"
                    }
                }
            })

            .state('editProfile', {
                url: '/account/editProfile',
                views : {
                    'content' : {
                        templateUrl: "components/account/editProfile.html",
                        controller: "profileCtrl"
                    }
                }
            })

            .state('depositinstr', {
                url: '/account/depositinstr',
                views : {
                    'content' : {
                        templateUrl: "components/account/depositinstr.html",
                        controller: "bankCtrl"
                    }
                }
            })

            .state('withdraw', {
                url: '/account/withdraw',
                views : {
                    'content' : {
                        templateUrl: "components/account/withdraw.html",
                        controller: "bankCtrl"
                    }
                }
            })

            .state('creditcarddeposit', {
                url: '/account/creditcarddeposit',
                views : {
                    'content' : {
                        templateUrl: "components/account/creditcarddeposit.html",
                        controller: "bankCtrl"
                    }
                }
            })

            .state('polideposit', {
                url: '/account/polideposit',
                views : {
                    'content' : {
                        templateUrl: "components/account/polideposit.html",
                        controller: "bankCtrl"
                    }
                }
            })

            .state('withdrawSuccess', {
                url: '/account/withdrawSuccess',
                views : {
                    'content' : {
                        templateUrl: "components/account/withdrawSuccess.html",
                        controller: "bankCtrl"
                    }
                }
            })

            .state('creditConfirm', {
                url: '/account/creditConfirm?gateway&token',
                views : {
                    'content' : {
                        templateUrl: "components/account/creditConfirm.html",
                        controller: "bankCtrl"
                    }
                }
            })

            .state('poliDepositeConfirmation', {
                url: '/account/poliDepositeConfirmation/?gateway&token',
                views : {
                    'content' : {
                        templateUrl: "components/account/poliDepositeConfirmation.html",
                        controller: "bankCtrl"
                    }
                }
            })

            .state('registration_success', {
                url: '/account/registration_success',
                views : {
                    'content' : {
                        templateUrl: "components/account/registration_success.html",
                        controller: "accountCtrl"
                    }
                }
            })

            .state('signup',{
                url : "/account/signup",
                views : {
                    'content' : {
                        templateUrl: "components/account/signup.html",
                        controller: "accountCtrl"
                    }
                }
            });

    }

})();
