(function(){
    'use strict';

    angular
        .module('pinnacleBetApp')
        .factory('accountService', accountService);

    accountService.$inject = ['$http','EVN'];

    function accountService($http,EVN){
        var service ={
            accountOpenbet          : accountOpenbet,
            accountBetHistory       : accountBetHistory,
            accountBanking          : accountBanking,
            accountAllTrans         : accountAllTrans,
            accountBonusBet         : accountBonusBet,
            accountMultisDetail     : accountMultisDetail,
            changePassword          : changePassword,
            isCorrectPassword       : isCorrectPassword,
            updateAccount           : updateAccount,
            isEmailExist            : isEmailExist,
            isMobileExist           : isMobileExist,
            isUserExist             : isUserExist,
            signUp                  : signUp,
            deposit                 : deposit,
            getAccount              : getAccount,
            accountLogin            : accountLogin,
            isLogin                 : isLogin,
            login                   : login,
            logout                  : logout,
            withdraw                : withdraw,
            ewayDepositConfirmation : ewayDepositConfirmation,
            poliDepositConfirmation : poliDepositConfirmation,
            getPasswordByEmail      : getPasswordByEmail,
            resetPassword           : resetPassword,
            accountBonusBetHistory  : accountBonusBetHistory

        }
        return service;

        function accountOpenbet(type, limitFrom, limitTo, date){
            return $http.get(EVN.apiDataUrl + '/data/wager/wagers/'+type+'/'+limitFrom+'/'+limitTo+'/'+date);
        }
        function accountBetHistory(type, limitFrom, limitTo, date){
            return $http.get(EVN.apiDataUrl + '/data/wager/wagers/'+type+'/'+limitFrom+'/'+limitTo+'/'+date);
        }
        function accountBanking(type, limitFrom, limitTo, date){
            return $http.get(EVN.apiDataUrl + '/data/banking/transfers/'+type+'/'+limitFrom+'/'+limitTo+'/'+date);
        }
        function accountAllTrans(type, limitFrom, limitTo, date){
            return $http.get(EVN.apiDataUrl + '/data/banking/transactions/'+type+'/'+limitFrom+'/'+limitTo+'/'+date);
        }
        function accountBonusBet(type, limitFrom, limitTo, date){
            return $http.get(EVN.apiDataUrl + '/data/wager/wagers/'+type+'/'+limitFrom+'/'+limitTo+'/'+date);
        }
        function accountBonusBetHistory(){
            return $http.get(EVN.apiDataUrl + '/data/account/bonusbet');
        }
        function accountMultisDetail(id){
            return $http.get(EVN.apiDataUrl + '/data/wager/multis/'+id);
        }
        function changePassword(username,password,changePasswordDto){
            return $http.post(EVN.apiDataUrl + '/data/account/changePassword',{
                username : username,
                password : password,
                changePasswordDto : changePasswordDto
            });
        }
        function isCorrectPassword(password){
            return $http.post(EVN.apiDataUrl + '/data/account/isCorrectPassword',{
                password : password
            });
        }
        function isEmailExist(email){
            return $http.post(EVN.apiDataUrl + '/data/security/isEmailExists',{
                emailAddress : email
            });
        }
        function isMobileExist(mobile){
            return $http.post(EVN.apiDataUrl + '/data/security/isMobileExists',{
                mobileNumber : mobile
            });
        }
        function isUserExist(username){
            return $http.post(EVN.apiDataUrl +'/data/security/isUsernameExists',{
                username : username
            });
        }
        function deposit(amount,gateway){
            return $http.post(EVN.apiDataUrl + '/data/account/depositMoneyForAccountHolder',{
                amount : amount,
                gateway : gateway
            });
        }

        function getAccount(){
            return $http.post(EVN.apiDataUrl + '/data/account/getAccount');
        }
        function signUp(profile){
            return $http.post(EVN.apiDataUrl + '/data/security/signUp',{
                addressCountry : profile.address.addressCountry.selected.text,
                addressFlatNumber : profile.address.unitNo,
                addressPostcode : profile.address.postcode,
                addressState : profile.address.state.selected.text,
                addressStreet : profile.address.street,
                addressStreetNumber : profile.address.streetNo,
                addressSuburb : profile.address.suburb,
                areaCode : profile.phone.areaCode,
                countryCode : profile.phone.countryCode.selected.text,
                cpassword : profile.account.reTypePassword,
                dob : profile.person.dob.daySelected.value + '/' + profile.person.dob.monthSelected.value + '/' + profile.person.dob.yearSelected.value,
                emailAddress : profile.person.email,
                firstName : profile.person.firstname,
                lastName : profile.person.lastname,
                mobileNumber : profile.phone.mobile,
                password : profile.account.password,
                phoneNumber : profile.phone.landLine,
                username : profile.account.username
            });
        }
        function updateAccount(info){
            return $http.post(EVN.apiDataUrl + '/data/account/editProfile',{
                addressCountry : info.address.addressCountry.selected.text,
                addressFlatNumber : info.address.unitNo,
                addressPostcode : info.address.postcode,
                addressState : info.address.state.selected.text,
                addressStreet : info.address.street,
                addressStreetNumber : info.address.streetNo,
                addressSuburb : info.address.suburb,
                areaCode : info.phone.areaCode,
                countryCode : info.phone.countryCode.selected.text,
                emailAddress : info.person.email,
                mobileNumber : info.phone.mobile,
                phoneNumber : info.phone.landLine
            })
        }
        function accountLogin(username,password){
            return $http.post(EVN.apiDataUrl + '/data/security/signIn',{
                username : username,
                password : password
            });
        }
        function isLogin(rootScope){
            return rootScope.accountInfo.isLogin;
        }
        function login(username, password, rootScope){
            if(username && password){
                return accountLogin(username,password)
                    .then(accountLoginRes,accountLoginRej);
            }

            function accountLoginRes(res){
                //login rejected
                if(!!res.data.errorMessage){
                    rootScope.accountInfo.isLogin = false;
                    rootScope.accountInfo.errorMessage = res.data.errorMessage;
                }

                //login successfully
                if(!!res.data.accountHolderDto){
                    rootScope.accountInfo.isLogin = true;
                    rootScope.accountInfo.errorMessage = '';
                    rootScope.accountInfo.accountHolderDto = res.data.accountHolderDto;
                    rootScope.accountInfo.username = username;
                    rootScope.accountInfo.password = password;
                    window.localStorage.setItem("username",rootScope.accountInfo.username);
                    window.localStorage.setItem("password",rootScope.accountInfo.password);
                    rootScope.navbarStatus.myAccountTab = true;

                }

                return res;
            }
            function accountLoginRej(res){}
        }
        function logout(rootScope){
            rootScope.accountInfo.isLogin = false;
            rootScope.accountInfo.username = '';
            rootScope.accountInfo.password = '';
            rootScope.accountInfo.accountHolderDto = {};
            window.localStorage.setItem("username","undefined");
            window.localStorage.setItem("password","undefined");
            rootScope.navbarStatus.myAccountTab = false;
            rootScope.sidebarStatus.myAccount = false;
            rootScope.sidebarStatus.sportBar = true;
            window.location.href = '#/';
        }
        function withdraw(withdrawInfo){
            return $http.post(EVN.apiDataUrl + '/data/account/withdrawal',{
                accountName : withdrawInfo.accountHolderName,
                accountNumber : withdrawInfo.accountNumber,
                amount : withdrawInfo.withdrawAmount,
                areaCode : withdrawInfo.areaCode,
                bankName : withdrawInfo.bankName,
                bsbNumber : withdrawInfo.bsb,
                countryCode : withdrawInfo.countryCode,
                homePhone : withdrawInfo.homePhone,
                mobileNumber : withdrawInfo.mobile,
                password : withdrawInfo.password

            });
        }
        function ewayDepositConfirmation(token){
            return $http.get(EVN.apiDataUrl + '/data/account/getEwayDepositResult/' + token);
        }
        function poliDepositConfirmation(token){
            return $http.get(EVN.apiDataUrl +'/data/account/getPoliDepositResult/' + token);
        }
        function getPasswordByEmail(username,email){
            return $http.post(EVN.apiDataUrl + '/data/account/sendForgetPasswordEmail',{
                username : username,
                email    : email
            });
        }

        function resetPassword(username,password,token){
            return $http.post(EVN.apiDataUrl + '/data/account/resetPassword',{
                username    :   username,
                password    :   password,
                token       :   token
            });
        }

    }

})();
