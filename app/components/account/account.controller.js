(function(){
    'use strict';

    angular
        .module('pinnacleBetApp')
        .controller('accountCtrl', accountCtrl)
        .controller('profileCtrl',profileCtrl)
        .controller('forgotPasswordCtrl',forgotPasswordCtrl)
        .controller('bankCtrl', bankCtrl);

    accountCtrl.$inject = ['$rootScope','$scope','accountService','util','$sce','$stateParams'];
    bankCtrl.$inject = ['$rootScope','$scope','accountService','$stateParams','util'];
    profileCtrl.$inject = ['$rootScope','$scope','accountService'];
    forgotPasswordCtrl.$inject = ['$rootScope','$scope','accountService','$stateParams'];

    function accountCtrl($rootScope,$scope,accountService,util,$sce,$stateParams){
        $scope.accountInfo = $rootScope.accountInfo;
        $scope.editAccountInfo = $rootScope.accountInfo;
        $scope.yearThreshold = new Date().getFullYear();
        $scope.initProfile = initProfile();
        $scope.setAmount = util.setAmount;
        $scope.setOdds = util.setOdds;
        $scope.getTime = util.getTime;
        $scope.param = param();             //transaction types
        $scope.disablePwdStatus = false;
        $scope.emailExist = false;
        $scope.mobileExist = false;
        $scope.mobilePrefix = false;
        $scope.userExist = false;
        $scope.getParentCategory = getParentCategory;
        $scope.getType= getType;
        $scope.dateStart =  getDateBefore7DaysFromNow();
        $scope.dateTo =  new Date();
        $scope.pageChanged = pageChanged;
        $scope.pagination = {
            currentPage: 0,
            totalPages: 0,
            maxSize : 6
        };

        $scope.calendarStart = {
            dateSelected: $scope.dateStart,
            dateDisplay: parseDateToParamD($scope.dateStart),
            isOpen: false,
            minDate: '2010-01-01',
            maxDate: '2030-12-31',
            showWeeks: false,
            format: "yyyy-MM-dd"
        };
        $scope.calendarEnd = {
            dateSelected: $scope.dateTo,
            dateDisplay: parseDateToParamD($scope.dateTo),
            isOpen: false,
            minDate: '2010-01-01',
            maxDate: '2030-12-31',
            showWeeks: false,
            format: "yyyy-MM-dd"
        };
        $scope.popupCalendarStart = popupCalendarStart;
        $scope.popupCalendarEnd = popupCalendarEnd;
        $scope.redirectPathStart = redirectPathStart;
        $scope.redirectPathEnd = redirectPathEnd;
        $scope.multiDetail = multiDetail;
        $scope.userInfo = $rootScope.accountInfo;
        $scope.emailVerify = emailVerify;       //email format is correct or not
        $scope.isEmailExist = isEmailExist;     //send request to judge email exit
        $scope.mobileJudge = mobileJudge;       //mobile prefix should be 04 and send request to judge mobile number exist
        $scope.isUserExist = isUserExist;
        $scope.resetSignUpForm = resetSignUpForm;
        $scope.submitSignUpFormHandler = submitSignUpFormHandler;
        //$scope.inValidText = '';
        $scope.getAccountSuccessful = true;
        $scope.depositError = '';
        $scope.stringMatch = stringMatch;
        $scope.getDayOptions = getDayOptions;
        $scope.getMonthOptions = getMonthOptions;
        $scope.getYearOptions = getYearOptions;
        $scope.getStateList = getStateList;
        $scope.getCountryList = getCountryList;
        $scope.getCountryPhoneCodeList = getCountryPhoneCodeList;
        $scope.setSelectedDay = setSelectedDay;
        $scope.setSelectedMonth = setSelectedMonth;
        $scope.setSelectedYear = setSelectedYear;
        $scope.setSelectedcountryCode = setSelectedcountryCode;
        $scope.setSelectedCountry = setSelectedCountry;
        $scope.setSelectedState = setSelectedState;
        $scope.raceMap = {
            'Duet' : 'exotic',
            'Exacta' : 'exotic',
            'Quinella' : 'exotic',
            'Trifecta' : 'exotic',
            'First Four' : 'exotic',
            'Quadrella' : 'multileg',
            'Big 6' : 'multileg',
            'Treble' : 'multileg',
            'Running Double' : 'multileg'
        };

        util.sidebarView('ACCOUNT',$rootScope);
        // isLogin();
        if($stateParams.parentCategory){
            getParentCategory($stateParams.parentCategory);
        }else{
            transactionHandler();
        }

        function transactionHandler() {
            if ($scope.param.parentCategory == 'OPENBET') {
                accountOpenbetHandler($scope.param);
            } else if ($scope.param.parentCategory == 'HISTORY') {
                accountHistorybetHandler($scope.param);
            } else if ($scope.param.parentCategory == 'BANKING') {
                accountBankingHandler($scope.param);
            } else if ($scope.param.parentCategory == 'ALLTRANSACTION') {
                accountAllTransactionHandler($scope.param);
            } else if ($scope.param.parentCategory == 'BONUS') {
                accountBonuebetHandler($scope.param);
            }
        }
        function accountOpenbetHandler(param){

            accountService.accountOpenbet(param.type,param.limitFrom, param.limitTo, param.dateStr)
                .then(accountOpenbetRes,accountOpenbetRej);

            /*
             *   @resolve   accountOpenbetRes
             *   @reject    accountOpenbetRej
             */
            function accountOpenbetRes(res){
                $scope.transactionList = [];
                $scope.pagination.totalPages = res.data.totalLength ? res.data.totalLength : 0;
                if(!res.data.wagers){
                    return;
                 }
                for(var idx = 0 ; idx < res.data.wagers.length ; idx ++){
                    var transaction = initTransaction();
                    var transactionType = res.data.wagers[idx].selection.type;
                    judgRaceOrSport(transaction,transactionType);
                    getMultiLeg(transaction,res.data.wagers[idx]);
                    var selection;
                    if(transaction.wagerType == 'RACE'){
                        if($scope.raceMap[res.data.wagers[idx].selection.betType]=='multileg'){
                            selection = matchMultileg(res.data.wagers[idx].selection.selection,res.data.wagers[idx].selection.event);
                        }else if($scope.raceMap[res.data.wagers[idx].selection.betType]=='exotic'){
                            selection = getRaceSelection(res.data.wagers[idx].selection.selection);
                        }else{
                            selection = res.data.wagers[idx].selection.selection;
                        }
                    }else if (transaction.wagerType == 'SPORTS'){
                        selection = getSportSelection(res.data.wagers[idx].selection,transaction.wagerType);
                    }else{
                        selection = getMultisSelection(res.data.wagers[idx].selection);
                    }

                    getTransactionInfo(transaction,res.data.wagers[idx]);
                    getSelectOrder(transaction,selection);
                    $scope.transactionList.push(transaction);
                }

            }
            function accountOpenbetRej(res){

            }

        }
        function accountHistorybetHandler(param){
            accountService.accountOpenbet(param.type,param.limitFrom, param.limitTo, param.dateStr)
                .then(accountHistorybetRes,accountHistorybetRej);

            function accountHistorybetRes(res){
                $scope.transactionList = [];
                $scope.pagination.totalPages = res.data.totalLength ? res.data.totalLength : 0;
                if(!res.data.wagers){
                    return;
                }
                for(var idx = 0 ; idx < res.data.wagers.length ; idx++){
                    var transaction = initTransaction();
                    var type = res.data.wagers[idx].selection.type;
                    judgRaceOrSport(transaction,type);
                    getMultiLeg(transaction,res.data.wagers[idx]);
                    getTransactionInfo(transaction,res.data.wagers[idx]);
                    var selection;
                    if(transaction.wagerType == 'RACE'){
                        if($scope.raceMap[res.data.wagers[idx].selection.betType]=='multileg'){
                            selection = matchMultileg(res.data.wagers[idx].selection.selection,res.data.wagers[idx].selection.event);
                        }else if($scope.raceMap[res.data.wagers[idx].selection.betType]=='exotic'){
                            selection = getRaceSelection(res.data.wagers[idx].selection.selection);
                        }else{
                            selection = res.data.wagers[idx].selection.selection;
                        }
                    }else if (transaction.wagerType == 'SPORTS'){
                        selection = getSportSelection(res.data.wagers[idx].selection,type);
                    }else{
                        selection = getMultisSelection(res.data.wagers[idx].selection,type);
                    }
                    getSelectOrder(transaction,selection);
                    $scope.transactionList.push(transaction);
                }

            }
            function accountHistorybetRej(res){

            }
        }
        function accountBonuebetHandler(param){
            if(param.type == 'BONUSBET_HISTORY'){
                accountService.accountBonusBetHistory()
                    .then(accountBonusbetHistoryRes,accountBonusbetHistoryRej);
            }else{
                accountService.accountBonusBet(param.type,param.limitFrom, param.limitTo, param.dateStr)
                    .then(accountBonusbetRes,accountBonusbetRej);
            }
            function accountBonusbetHistoryRes(res){
                $scope.historyTransactionList = [];
                if(!res.data.bonusBets){
                    return;
                }
                for(var idx = 0; idx < res.data.bonusBets.length; idx++){
                    var transaction = initBonusHistoryTransaction();
                    getBonusHistory(transaction,res.data.bonusBets[idx]);
                    $scope.historyTransactionList.push(transaction);
                }
            }
            function accountBonusbetHistoryRej(res){

            }
            function accountBonusbetRes(res){
                $scope.transactionList = [];
                $scope.pagination.totalPages = res.data.totalLength ? res.data.totalLength : 0;
                if(!res.data.wagers){
                    return;
                }
                for(var idx =0 ; idx < res.data.wagers.length ; idx++){
                    var transaction = initBonusTransaction();
                    getBonusTransaction(transaction,res.data.wagers[idx]);

                    if(transaction.wagerType == 'RACE'){
                        transaction.selection = getBonusRaceSelection(res.data.wagers[idx]);
                    }else if(transaction.wagerType == 'SPORTS'){
                        transaction.selection = getBonusSportSelection(res.data.wagers[idx]);
                    }else{
                        transaction.selection = getBonusMultisSelection(res.data.wagers[idx]);
                    }
                    getMultiLeg(transaction,res.data.wagers[idx]);
                    $scope.transactionList.push(transaction);

                }
            }

            function accountBonusbetRej(res){

            }


        }
        function accountBankingHandler(param){
            accountService.accountBanking(param.type,param.limitFrom, param.limitTo, param.dateStr)
                .then(accountBankingRes,accountBankingRej);

            function accountBankingRes(res){
                $scope.transactionList = [];
                $scope.pagination.totalPages = res.data.totalLength ? res.data.totalLength : 0;
                if(!res.data.transactions){
                    return;
                }
                $scope.pagination.totalPages = res.data.totalLength ? res.data.totalLength : 0;
                for(var idx = 0; idx < res.data.transactions.length; idx++){
                    var transaction = initBankingTransaction();
                    getBankingTransactionInfo(transaction,res.data.transactions[idx]);
                    $scope.transactionList.push(transaction);
                }

            }
            function accountBankingRej(res){

            }

        }
        function accountAllTransactionHandler(param){
            accountService.accountAllTrans(param.type,param.limitFrom, param.limitTo, param.dateStr)
                .then(accountAllTransRes,accountAllTransRej);

            function accountAllTransRes(res) {
                $scope.transactionList = [];
                $scope.pagination.totalPages = res.data.totalLength ? res.data.totalLength : 0;
                if(!res.data.transactions){
                    return;
                }
                for(var idx = 0; idx < res.data.transactions.length; idx++){
                    var transaction = initAllTransaction();
                    var desc = getTransactionDesc(res.data.transactions[idx]);
                    getAllTransaction(transaction,res.data.transactions[idx]);
                    transaction.description = desc;
                    $scope.transactionList.push(transaction);

                }
            }
            function accountAllTransRej(res) {

            }
        }
        function accountMultibetDetailHandler(id){
            accountService.accountMultisDetail(id)
                .then(accountMultibetDetailRes,accountMultibetDetailRej);

            function accountMultibetDetailRes(res){
                $scope.multisDetailList = {};
                if(!res.data.subWagers){
                    return;
                }
                for(var idx = 0; idx<res.data.subWagers.length; idx++){
                    var multisDetail  = initMultisDetail();
                    getWagerType(multisDetail,res.data.subWagers[idx]);
                    getMultisDetailInfo(multisDetail,res.data.subWagers[idx]);
                    if(!$scope.multisDetailList[res.data.id]){
                        $scope.multisDetailList[res.data.id] = [];
                    }
                    $scope.multisDetailList[res.data.id].push(multisDetail);

                }
            }
            function accountMultibetDetailRej(res){

            }
        }

        /*
        * general functions
        *
        *
        * */
        /*BETING RECORD METHOD*/

        function getMultiLeg(transaction,info) {
            if(info.selection.betType == 'First Four' || info.selection.betType == 'Trifecta'){
                transaction.multiLeg = true;
            }else{
                transaction.multiLeg = false;
            }

        }
        function getMultisSelection(selection){
            return {
                select : {
                    choose : selection.subwagers,
                    odds : util.setOdds(selection.odds,'MULTIS')
                }
            }
        }
        function getSportSelection(selection,type){
            return {
                select : {
                        parentEvent : selection.parentEvent,
                        startTime : util.getTime(selection.startTime).yymmdd + ' ' + util.getTime(selection.startTime).hhmmss,
                        event : selection.event,
                        choose : selection.selection,
                        amount : util.setAmount(selection.amount).setSportAmountWithDollarSign(),
                        odds : util.setOdds(selection.odds,type)
                }
            }
        }
        function getRaceSelection(selection){
            var html = '';
            var arr = selection.split('/');
            for(var i = 0 ; i < arr.length ; i++){
                if(i==0){
                    html += '1st: ';
                    var subArr = arr[i].split('+');
                    for(var j = 0 ; j < subArr.length ; j++){
                        if(j==subArr.length-1){
                            html += subArr[j] + '<br>';
                        }else{
                            html += subArr[j]+',';
                        }

                    }
                }
                if(i==1){
                    html += '2nd: ';
                    var subArr = arr[i].split('+');
                    for(var j = 0 ; j < subArr.length ; j++){
                        if(j==subArr.length-1){
                            html += subArr[j] + '<br>';
                        }else{
                            html += subArr[j]+',';
                        }

                    }
                }
                if(i==2){
                    html += '3rd: ';
                    var subArr = arr[i].split('+');
                    for(var j = 0 ; j < subArr.length ; j++){
                        if(j==subArr.length-1){
                            html += subArr[j] + '<br>';
                        }else{
                            html += subArr[j]+',';
                        }

                    }
                }
                if(i==3){
                    html += '4th: ';
                    var subArr = arr[i].split('+');
                    for(var j = 0 ; j < subArr.length ; j++){
                        if(j==subArr.length-1){
                            html += subArr[j] + '<br>';
                        }else{
                            html += subArr[j]+',';
                        }

                    }
                }
                if(i==4){
                    html += '5th: ';
                    var subArr = arr[i].split('+');
                    for(var j = 0 ; j < subArr.length ; j++){
                        if(j==subArr.length-1){
                            html += subArr[j] + '<br>';
                        }else{
                            html += subArr[j]+',';
                        }

                    }
                }
                if(i==5){
                    html += '6th: ';
                    var subArr = arr[i].split('+');
                    for(var j = 0 ; j < subArr.length ; j++){
                        if(j==subArr.length-1){
                            html += subArr[j] + '<br>';
                        }else{
                            html += subArr[j]+',';
                        }

                    }
                }
            }
            return $sce.trustAsHtml(html);
        }
        function initTransaction() {
            return{
                transactionInfo : {
                    ticketId : -1,
                    creationTime : '',
                    startTime : '',
                    raceType : '',
                    parentEvent : '',
                    event : '',
                    betType : '',
                    selection : '',
                    amount : -1,
                    amountWon : -1,
                    odds    : -1,
                    status : '',
                    wagerType : '',
                    multiLeg : '',
                    flexiPercent : '',
                    amountPerPermutation : ''
                }
            }
        }
        function getTransactionInfo(transaction,wagerInfo) {
            transaction.ticketId = wagerInfo.id;
            transaction.creationTime = $sce.trustAsHtml(util.getTime(wagerInfo.creationTime).ddmmyy +'<br> '+util.getTime(wagerInfo.creationTime).hhmmss);
            transaction.startTime = util.getTime(wagerInfo.startTime).yymmdd +'  '+util.getTime(wagerInfo.startTime).hhmmss;
            transaction.raceType = wagerInfo.selection.type;
            transaction.parentEvent = wagerInfo.selection.parentEvent;
            transaction.event = wagerInfo.selection.event;
            transaction.betType = wagerInfo.selection.betType;
            transaction.status = wagerInfo.status.value;
            transaction.amount = util.setAmount(wagerInfo.amount).setRaceAmountWithDollarSign();
            transaction.amountWon = util.setAmount(wagerInfo.amountWon).setRaceAmountWithDollarSign();
            if(transaction.wagerType == 'RACE'){
                transaction.odds = util.setOdds(wagerInfo.selection.odds,transaction.wagerType);
                if(wagerInfo.selection.flexiPercent){
                    transaction.flexiPercent = 'Flexi Returning: ' + wagerInfo.selection.flexiPercent + '%';
                }
                if(wagerInfo.selection.amountPerPermutation){
                    transaction.amountPerPermutation = util.setAmount(wagerInfo.selection.amountPerPermutation).setRaceAmountWithDollarSign();
                }
            }else if(transaction.wagerType == 'SPORTS'){
                transaction.odds = util.setOdds(wagerInfo.selection.odds,transaction.wagerType);
            }
        }
        function getSelectOrder(transaction,selection) {
            transaction.selection = selection;
        }
        function judgRaceOrSport(transaction,type) {
            if(type == 'Thoroughbred' || type == 'Greyhound' || type == 'Harness' || type == 'Jockey Challenge' || type == 'Specials' || type == 'Feature Races'){
                transaction.wagerType = 'RACE';
            }else if(type == 'Multis'){
                transaction.wagerType = 'MULTIS'
            }else{
                transaction.wagerType = 'SPORTS';
            }
        }
        function matchMultileg(rug,race){
            var html = '';
            var arrRug = rug.split('/');
            var arrRace = race.split('/');
            for(var i = 0; i < arrRace.length; i++){
                if (i == 0){
                    html += arrRace[i]+': ';
                    var subArr = arrRug[0].split('+');
                    for(var k = 0; k<subArr.length; k++){
                        if(k==subArr.length - 1){
                            html += subArr[k] + '<br>';
                        }else{
                            html += subArr[k] + ',';
                        }
                    }
                }
                if (i == 1){
                    html += arrRace[i]+': ';
                    var subArr = arrRug[1].split('+');
                    for(var k = 0; k<subArr.length; k++){
                        if(k==subArr.length - 1){
                            html += subArr[k] + '<br>';
                        }else{
                            html += subArr[k] + ',';
                        }
                    }
                }
                if (i == 2){
                    html += arrRace[i]+': ';
                    var subArr = arrRug[2].split('+');
                    for(var k = 0; k<subArr.length; k++){
                        if(k==subArr.length - 1){
                            html += subArr[k] + '<br>';
                        }else{
                            html += subArr[k] + ',';
                        }
                    }
                }
                if (i == 3){
                    html += arrRace[i]+': ';
                    var subArr = arrRug[3].split('+');
                    for(var k = 0; k<subArr.length; k++){
                        if(k==subArr.length - 1){
                            html += subArr[k] + '<br>';
                        }else{
                            html += subArr[k] + ',';
                        }
                    }
                }
                if (i == 4){
                    html += arrRace[i]+': ';
                    var subArr = arrRug[4].split('+');
                    for(var k = 0; k<subArr.length; k++){
                        if(k==subArr.length - 1){
                            html += subArr[k] + '<br>';
                        }else{
                            html += subArr[k] + ',';
                        }
                    }
                }
                if (i == 5){
                    html += arrRace[i]+': ';
                    var subArr = arrRug[5].split('+');
                    for(var k = 0; k<subArr.length; k++){
                        if(k==subArr.length - 1){
                            html += subArr[k] + '<br>';
                        }else{
                            html += subArr[k] + ',';
                        }
                    }
                }


            }
            return $sce.trustAsHtml(html);
        }


        /*BONUS METHOD*/
        function initBonusTransaction(){
            return{
                transactionInfo : {
                    ticketId : -1,
                    creationTime : '',
                    eventType : '',
                    selection : '',
                    stake : '',
                    status : '',
                    wagerType : '',
                    multiLeg : ''

                }
            }
        }
        function initBonusHistoryTransaction(){
            return{
                transactionInfo : {
                    id : -1,
                    status : '',
                    change_time : '',
                    load_time : '',
                    load_amount : '',
                    active_time : '',
                    init_balance : ''
                }
            }
        }
        function getBonusTransaction(transaction,info) {
            transaction.ticketId = info.id;
            transaction.creationTime = util.getTime(info.creationTime).yymmdd +' '+ util.getTime(info.creationTime).mmss;
            transaction.eventType = info.selection.type;
            transaction.stake = util.setAmount(info.amount).setRaceAmountWithDollarSign();
            transaction.status = info.status.value;
            if(info.multibet == false){
                if(info.selection.type == 'Thoroughbred' || info.selection.type == 'Greyhound' || info.selection.type == 'Harness'){
                    transaction.wagerType = 'RACE'
                }else{
                    transaction.wagerType = 'SPORTS';
                }
            }else{
                transaction.wagerType = 'MULTIS';
            }

        }
        function getBonusHistory(transaction,info){
            transaction.id = info.id;
            transaction.status = info.statusName;
            transaction.change_time = util.getTime(info.statusChangeTime).yymmdd + ' ' + util.getTime(info.statusChangeTime).mmss;
            transaction.load_time = util.getTime(info.loadTime).yymmdd + ' ' + util.getTime(info.loadTime).mmss;
            transaction.load_amount = util.setAmount(info.loadAmount).setRaceAmountWithDollarSign();
            transaction.active_time = util.getTime(info.walletLoadTime).yymmdd + ' ' + util.getTime(info.walletLoadTime).mmss;
            transaction.init_balance = util.setAmount(info.walletLoadAmount).setRaceAmountWithDollarSign();
        }
        function getBonusRaceSelection(info){
            return{
                select :{
                    parentEvent : info.selection.parentEvent,
                    event : info.selection.event,
                    betType : info.selection.betType,
                    choosed : info.selection.selection,
                    amount : util.setAmount(info.selection.amount).setRaceAmountWithDollarSign(),
                    odds : util.setOdds(info.selection.odds,'RACE')
                }
            }

        }
        function getBonusSportSelection(info){
            return {
                select : {
                    parentEvent : info.selection.parentEvent,
                    startTime : util.getTime(info.selection.startTime).yymmdd + ' ' + util.getTime(info.selection.startTime).mmss,
                    event : info.selection.event,
                    choosed : info.selection.selection,
                    amount :  util.setAmount(info.selection.amount).setRaceAmountWithDollarSign(),
                    odds : util.setOdds(info.selection.odds,'SPORTS')
                }
            }

        }


        /*BANKING METHOD*/
        function initBankingTransaction(){
            return{
                transaction : {
                    ticketId : -1,
                    creationTime : '',
                    transactionType : '',
                    description : '',
                    amount : '',
                    status : ''

                }
            }
        }
        function getBankingTransactionInfo(transaction,info){
            transaction.ticketId = info.id;
            transaction.creationTime = $sce.trustAsHtml(util.getTime(info.creationTime).yymmdd +'<br>'+util.getTime(info.creationTime).mmss);
            transaction.transactionType = info.type;
            transaction.description = info.description;
            transaction.amount = util.setAmount(info.amount).setRaceAmountWithDollarSign();
            transaction.status = info.status.value;

        }

        /*ALL TRANSACTION METHOD*/
        function initAllTransaction(){
            return{
                transaction : {
                    ticketId : -1,
                    creationTime : '',
                    transactionType : '',
                    description : '',
                    debit : '',
                    credit : '',
                    balance : '',
                    eventType : ''

                }

            }
        }
        function getAllTransaction(transaction,info){
            transaction.ticketId = info.ticketNumber;
            transaction.creationTime = $sce.trustAsHtml(util.getTime(info.creationTime).yymmdd +'<br>'+util.getTime(info.creationTime).mmss);
            if(info.wagerDesc){
                transaction.transactionType = info.wagerDesc.type;
            }else{
                transaction.transactionType = info.eventType;
            }
            if(info.balanceEffect<0){
                transaction.debit = util.setAmount(Math.abs(info.balanceEffect)).setRaceAmountWithDollarSign();
                transaction.credit = '-';
            }else if(info.balanceEffect>0){
                transaction.debit = '-';
                transaction.credit = util.setAmount(info.balanceEffect).setRaceAmountWithDollarSign();
            }
            transaction.balance = util.setAmount(info.balance).setRaceAmountWithDollarSign();
            if(info.eventType == 'EWay'){
                transaction.eventType ='EWAY';
            }else if(info.eventType == 'G' || info.eventType == 'T' || info.eventType == 'R'){
                transaction.eventType ='RACE';
            }else if(info.eventType == 'Multis'){
                transaction.eventType ='MULTIS';
            }else if(info.eventType == 'Manual'){
                transaction.eventType ='MANUAL';
            }else{
                transaction.eventType ='SPORT';
            }


        }
        function getTransactionDesc(info){
            var desc = '';
            if(info.eventType == 'EWay' || info.eventType == 'Manual'){
                desc = info;
            }else{
                desc = info.wagerDesc;
            }
            return desc;
        }

        /*MULTIS DETAIL*/
        function initMultisDetail(){
            return{
                multisDetail : {
                    type : '',
                    parentEvent : '',
                    event : '',
                    startTime : '',
                    betType : '',
                    odds : '',
                    selection : '',
                    wagerType : ''
                }
            }
        }
        function getMultisDetailInfo(multisDetail,info){
            multisDetail.type = info.type;
            multisDetail.parentEvent = info.parentEvent;
            multisDetail.event = info.event;
            multisDetail.startTime = util.getTime(info.startTime).yymmdd +' '+util.getTime(info.startTime).mmss;
            multisDetail.betType = info.betType;
            if(multisDetail.wagerType == 'RACE'){
                multisDetail.odds = util.setOdds(info.odds,multisDetail.wagerType);
            }else{
                multisDetail.odds = util.setOdds(info.odds,multisDetail.wagerType);
            }
            multisDetail.selection = info.selection;

        }
        function getWagerType(multisDetail,info){
            if(info.type == 'Thoroughbred' || info.type == 'Greyhound' || info.type == 'Harness'){
                multisDetail.wagerType = 'RACE';
            }else{
                multisDetail.wagerType = 'SPORTS';
            }
        }

        /*OTHER FUNCTION*/
        function param(){
            return {
                parentCategory : 'OPENBET',
                type: 'OPENBET_RACING' ,
                limitFrom : 0,
                limitTo : 10,
                dateStr: getDateRangeStringForQuery(getDateBefore7DaysFromNow(),new Date())
            };
        }
        function getParentCategory(parentCategory){
            $scope.param.parentCategory = parentCategory;
            if($scope.param.parentCategory == 'OPENBET'){
                $scope.param.type = 'OPENBET_RACING';
            }else if($scope.param.parentCategory == 'HISTORY'){
                $scope.param.type = 'RACING';
            }else if($scope.param.parentCategory == 'BANKING'){
                $scope.param.type = 'ALL';
            }else if($scope.param.parentCategory == 'ALLTRANSACTION'){
                $scope.param.type = 'ALL';
            }else if($scope.param.parentCategory == 'BONUS'){
                $scope.param.type = 'BONUSBET_RACING';
            }
            transactionHandler();
        }
        function getType(type){
            $scope.param.type = type;
            transactionHandler();
        }
        function getDateRangeStringForQuery(dateStart,dateEnd){
            return parseDateToParam(dateStart) + "-" +  parseDateToParam(dateEnd);
        }
        function parseDateToParam(date) {
            if (date != null && date != undefined && date !="") {
                var yyyy = date.getFullYear().toString();
                var mm = (date.getMonth() + 1).toString();
                var dd  = date.getDate().toString();
                return yyyy + (mm[1] ? mm : "0" + mm[0]) + (dd[1] ? dd : "0" + dd[0]);
            } else {
                // console.log("ERROR :parseDateToParam is called with date " + date);
                return "";
            }
        }
        function getDateBefore7DaysFromNow(){
            var sevenDaysInMilliSec = 7* 24* 60*60*1000;
            var dateBeforeSevenDay = new Date().getTime() - sevenDaysInMilliSec;
            return  new Date(dateBeforeSevenDay);
        }
        function parseDateToParamD(date) {
            if (date) {
                var yyyy = date.getFullYear().toString();
                var mm = (date.getMonth() + 1).toString();
                var dd  = date.getDate().toString();
                return yyyy + "-" +(mm[1] ? mm : "0" + mm[0]) +"-" + (dd[1] ? dd : "0" + dd[0]);
            } else {
                // console.log("ERROR :parseDateToParamD is called with date " + date);
                return "";
            }
        }
        function multiDetail(id){
            $('#transaction-summary #'+id).toggle();
            $scope.transactionId = id;
            accountMultibetDetailHandler($scope.transactionId);
        }
        function popupCalendarStart(){
            $scope.calendarStart.isOpen = true;
        }
        function popupCalendarEnd(){
            $scope.calendarEnd.isOpen = true;
        }
        function redirectPathStart() {
            $scope.calendarStart.dateDisplay = parseDateToParamD($scope.calendarStart.dateSelected);
            $scope.param.dateStr = getDateRangeStringForQuery($scope.calendarStart.dateSelected,$scope.calendarEnd.dateSelected);
            transactionHandler();
        };
        function redirectPathEnd() {
            $scope.calendarEnd.dateDisplay = parseDateToParamD($scope.calendarEnd.dateSelected);
            $scope.param.dateStr = getDateRangeStringForQuery($scope.calendarStart.dateSelected,$scope.calendarEnd.dateSelected);
            transactionHandler();
        };
        /*COMMON*/
        function getCountryPhoneCodeList() {
            return [
                {text: "Algeria (+213)", countryCode: "DZ", phoneCode: "213"},
                {text: "Andorra (+376)", countryCode: "AD", phoneCode: "376"},
                {text: "Angola (+244)", countryCode: "AO", phoneCode: "244"},
                {text: "Anguilla (+1264)", countryCode: "AI", phoneCode: "1264"},
                {text: "Antigua & Barbuda (+1268)", countryCode: "AG", phoneCode: "1268"},
                {text: "Argentina (+54)", countryCode: "AR", phoneCode: "54"},
                {text: "Armenia (+374)", countryCode: "AM", phoneCode: "374"},
                {text: "Aruba (+297)", countryCode: "AW", phoneCode: "297"},
                {text: "Australia (+61)", countryCode: "AU", phoneCode: "61"},
                {text: "Austria (+43)", countryCode: "AT", phoneCode: "43"},
                {text: "Azerbaijan (+994)", countryCode: "AZ", phoneCode: "994"},
                {text: "Bahamas (+1242)", countryCode: "BS", phoneCode: "1242"},
                {text: "Bahrain (+973)", countryCode: "BH", phoneCode: "973"},
                {text: "Bangladesh (+880)", countryCode: "BD", phoneCode: "880"},
                {text: "Barbados (+1246)", countryCode: "BB", phoneCode: "1246"},
                {text: "Belarus (+375)", countryCode: "BY", phoneCode: "375"},
                {text: "Belgium (+32)", countryCode: "BE", phoneCode: "32"},
                {text: "Belize (+501)", countryCode: "BZ", phoneCode: "501"},
                {text: "Benin (+229)", countryCode: "BJ", phoneCode: "229"},
                {text: "Bermuda (+1441)", countryCode: "BM", phoneCode: "1441"},
                {text: "Bhutan (+975)", countryCode: "BT", phoneCode: "975"},
                {text: "Bolivia (+591)", countryCode: "BO", phoneCode: "591"},
                {text: "Bosnia Herzegovina (+387)", countryCode: "BA", phoneCode: "387"},
                {text: "Botswana (+267)", countryCode: "BW", phoneCode: "267"},
                {text: "Brazil (+55)", countryCode: "BR", phoneCode: "55"},
                {text: "Brunei (+673)", countryCode: "BN", phoneCode: "673"},
                {text: "Bulgaria (+359)", countryCode: "BG", phoneCode: "359"},
                {text: "Burkina Faso (+226)", countryCode: "BF", phoneCode: "226"},
                {text: "Burundi (+257)", countryCode: "BI", phoneCode: "257"},
                {text: "Cambodia (+855)", countryCode: "KH", phoneCode: "855"},
                {text: "Cameroon (+237)", countryCode: "CM", phoneCode: "237"},
                {text: "Canada (+1)", countryCode: "CA", phoneCode: "1"},
                {text: "Cape Verde Islands (+238)", countryCode: "CV", phoneCode: "238"},
                {text: "Cayman Islands (+1345)", countryCode: "KY", phoneCode: "1345"},
                {text: "Central African Republic (+236)", countryCode: "CF", phoneCode: "236"},
                {text: "Chile (+56)", countryCode: "CL", phoneCode: "56"},
                {text: "China (+86)", countryCode: "CN", phoneCode: "86"},
                {text: "Colombia (+57)", countryCode: "CO", phoneCode: "57"},
                {text: "Comoros (+269)", countryCode: "KM", phoneCode: "269"},
                {text: "Congo (+242)", countryCode: "CG", phoneCode: "242"},
                {text: "Cook Islands (+682)", countryCode: "CK", phoneCode: "682"},
                {text: "Costa Rica (+506)", countryCode: "CR", phoneCode: "506"},
                {text: "Croatia (+385)", countryCode: "HR", phoneCode: "385"},
                {text: "Cuba (+53)", countryCode: "CU", phoneCode: "53"},
                {text: "Cyprus North (+90392)", countryCode: "CY", phoneCode: "90392"},
                {text: "Cyprus South (+357)", countryCode: "CY", phoneCode: "357"},
                {text: "Czech Republic (+42)", countryCode: "CZ", phoneCode: "42"},
                {text: "Denmark (+45)", countryCode: "DK", phoneCode: "45"},
                {text: "Djibouti (+253)", countryCode: "DJ", phoneCode: "253"},
                {text: "Dominica (+1809)", countryCode: "DM", phoneCode: "1809"},
                {text: "Dominican Republic (+1809)", countryCode: "DO", phoneCode: "1809"},
                {text: "Ecuador (+593)", countryCode: "EC", phoneCode: "593"},
                {text: "Egypt (+20)", countryCode: "EG", phoneCode: "20"},
                {text: "El Salvador (+503)", countryCode: "SV", phoneCode: "503"},
                {text: "Equatorial Guinea (+240)", countryCode: "GQ", phoneCode: "240"},
                {text: "Eritrea (+291)", countryCode: "ER", phoneCode: "291"},
                {text: "Estonia (+372)", countryCode: "EE", phoneCode: "372"},
                {text: "Ethiopia (+251)", countryCode: "ET", phoneCode: "251"},
                {text: "Falkland Islands (+500)", countryCode: "FK", phoneCode: "500"},
                {text: "Faroe Islands (+298)", countryCode: "FO", phoneCode: "298"},
                {text: "Fiji (+679)", countryCode: "FJ", phoneCode: "679"},
                {text: "Finland (+358)", countryCode: "FI", phoneCode: "358"},
                {text: "France (+33)", countryCode: "FR", phoneCode: "33"},
                {text: "French Guiana (+594)", countryCode: "GF", phoneCode: "594"},
                {text: "French Polynesia (+689)", countryCode: "PF", phoneCode: "689"},
                {text: "Gabon (+241)", countryCode: "GA", phoneCode: "241"},
                {text: "Gambia (+220)", countryCode: "GM", phoneCode: "220"},
                {text: "Georgia (+7880)", countryCode: "GE", phoneCode: "7880"},
                {text: "Germany (+49)", countryCode: "DE", phoneCode: "49"},
                {text: "Ghana (+233)", countryCode: "GH", phoneCode: "233"},
                {text: "Gibraltar (+350)", countryCode: "GI", phoneCode: "350"},
                {text: "Greece (+30)", countryCode: "GR", phoneCode: "30"},
                {text: "Greenland (+299)", countryCode: "GL", phoneCode: "299"},
                {text: "Grenada (+1473)", countryCode: "GD", phoneCode: "1473"},
                {text: "Guadeloupe (+590)", countryCode: "GP", phoneCode: "590"},
                {text: "Guam (+671)", countryCode: "GU", phoneCode: "671"},
                {text: "Guatemala (+502)", countryCode: "GT", phoneCode: "502"},
                {text: "Guinea (+224)", countryCode: "GN", phoneCode: "224"},
                {text: "Guinea - Bissau (+245)", countryCode: "GW", phoneCode: "245"},
                {text: "Guyana (+592)", countryCode: "GY", phoneCode: "592"},
                {text: "Haiti (+509)", countryCode: "HT", phoneCode: "509"},
                {text: "Honduras (+504)", countryCode: "HN", phoneCode: "504"},
                {text: "Hong Kong (+852)", countryCode: "HK", phoneCode: "852"},
                {text: "Hungary (+36)", countryCode: "HU", phoneCode: "36"},
                {text: "Iceland (+354)", countryCode: "IS", phoneCode: "354"},
                {text: "India (+91)", countryCode: "IN", phoneCode: "91"},
                {text: "Indonesia (+62)", countryCode: "ID", phoneCode: "62"},
                {text: "Iran (+98)", countryCode: "IR", phoneCode: "98"},
                {text: "Iraq (+964)", countryCode: "IQ", phoneCode: "964"},
                {text: "Ireland (+353)", countryCode: "IE", phoneCode: "353"},
                {text: "Israel (+972)", countryCode: "IL", phoneCode: "972"},
                {text: "Italy (+39)", countryCode: "IT", phoneCode: "39"},
                {text: "Jamaica (+1876)", countryCode: "JM", phoneCode: "1876"},
                {text: "Japan (+81)", countryCode: "JP", phoneCode: "81"},
                {text: "Jordan (+962)", countryCode: "JO", phoneCode: "962"},
                {text: "Kazakhstan (+7)", countryCode: "KZ", phoneCode: "7"},
                {text: "Kenya (+254)", countryCode: "KE", phoneCode: "254"},
                {text: "Kiribati (+686)", countryCode: "KI", phoneCode: "686"},
                {text: "Korea North (+850)", countryCode: "KP", phoneCode: "850"},
                {text: "Korea South (+82)", countryCode: "KR", phoneCode: "82"},
                {text: "Kuwait (+965)", countryCode: "KW", phoneCode: "965"},
                {text: "Kyrgyzstan (+996)", countryCode: "KG", phoneCode: "996"},
                {text: "Laos (+856)", countryCode: "LA", phoneCode: "856"},
                {text: "Latvia (+371)", countryCode: "LV", phoneCode: "371"},
                {text: "Lebanon (+961)", countryCode: "LB", phoneCode: "961"},
                {text: "Lesotho (+266)", countryCode: "LS", phoneCode: "266"},
                {text: "Liberia (+231)", countryCode: "LR", phoneCode: "231"},
                {text: "Libya (+218)", countryCode: "LY", phoneCode: "218"},
                {text: "Liechtenstein (+417)", countryCode: "LI", phoneCode: "417"},
                {text: "Lithuania (+370)", countryCode: "LT", phoneCode: "370"},
                {text: "Luxembourg (+352)", countryCode: "LU", phoneCode: "352"},
                {text: "Macao (+853)", countryCode: "MO", phoneCode: "853"},
                {text: "Macedonia (+389)", countryCode: "MK", phoneCode: "389"},
                {text: "Madagascar (+261)", countryCode: "MG", phoneCode: "261"},
                {text: "Malawi (+265)", countryCode: "MW", phoneCode: "265"},
                {text: "Malaysia (+60)", countryCode: "MY", phoneCode: "60"},
                {text: "Maldives (+960)", countryCode: "MV", phoneCode: "960"},
                {text: "Mali (+223)", countryCode: "ML", phoneCode: "223"},
                {text: "Malta (+356)", countryCode: "MT", phoneCode: "356"},
                {text: "Marshall Islands (+692)", countryCode: "MH", phoneCode: "692"},
                {text: "Martinique (+596)", countryCode: "MQ", phoneCode: "596"},
                {text: "Mauritania (+222)", countryCode: "MR", phoneCode: "222"},
                {text: "Mayotte (+269)", countryCode: "YT", phoneCode: "269"},
                {text: "Mexico (+52)", countryCode: "MX", phoneCode: "52"},
                {text: "Micronesia (+691)", countryCode: "FM", phoneCode: "691"},
                {text: "Moldova (+373)", countryCode: "MD", phoneCode: "373"},
                {text: "Monaco (+377)", countryCode: "MC", phoneCode: "377"},
                {text: "Mongolia (+976)", countryCode: "MN", phoneCode: "976"},
                {text: "Montserrat (+1664)", countryCode: "MS", phoneCode: "1664"},
                {text: "Morocco (+212)", countryCode: "MA", phoneCode: "212"},
                {text: "Mozambique (+258)", countryCode: "MZ", phoneCode: "258"},
                {text: "Myanmar (+95)", countryCode: "MN", phoneCode: "95"},
                {text: "Namibia (+264)", countryCode: "NA", phoneCode: "264"},
                {text: "Nauru (+674)", countryCode: "NR", phoneCode: "674"},
                {text: "Nepal (+977)", countryCode: "NP", phoneCode: "977"},
                {text: "Netherlands (+31)", countryCode: "NL", phoneCode: "31"},
                {text: "New Caledonia (+687)", countryCode: "NC", phoneCode: "687"},
                {text: "New Zealand (+64)", countryCode: "NZ", phoneCode: "64"},
                {text: "Nicaragua (+505)", countryCode: "NI", phoneCode: "505"},
                {text: "Niger (+227)", countryCode: "NE", phoneCode: "227"},
                {text: "Nigeria (+234)", countryCode: "NG", phoneCode: "234"},
                {text: "Niue (+683)", countryCode: "NU", phoneCode: "683"},
                {text: "Norfolk Islands (+672)", countryCode: "NF", phoneCode: "672"},
                {text: "Northern Marianas (+670)", countryCode: "NP", phoneCode: "670"},
                {text: "Norway (+47)", countryCode: "NO", phoneCode: "47"},
                {text: "Oman (+968)", countryCode: "OM", phoneCode: "968"},
                {text: "Palau (+680)", countryCode: "PW", phoneCode: "680"},
                {text: "Panama (+507)", countryCode: "PA", phoneCode: "507"},
                {text: "Papua New Guinea (+675)", countryCode: "PG", phoneCode: "675"},
                {text: "Paraguay (+595)", countryCode: "PY", phoneCode: "595"},
                {text: "Peru (+51)", countryCode: "PE", phoneCode: "51"},
                {text: "Philippines (+63)", countryCode: "PH", phoneCode: "63"},
                {text: "Poland (+48)", countryCode: "PL", phoneCode: "48"},
                {text: "Portugal (+351)", countryCode: "PT", phoneCode: "351"},
                {text: "Puerto Rico (+1787)", countryCode: "PR", phoneCode: "1787"},
                {text: "Qatar (+974)", countryCode: "QA", phoneCode: "974"},
                {text: "Reunion (+262)", countryCode: "RE", phoneCode: "262"},
                {text: "Romania (+40)", countryCode: "RO", phoneCode: "40"},
                {text: "Russia (+7)", countryCode: "RU", phoneCode: "7"},
                {text: "Rwanda (+250)", countryCode: "RW", phoneCode: "250"},
                {text: "San Marino (+378)", countryCode: "SM", phoneCode: "378"},
                {text: "Sao Tome &amp; Principe (+239)", countryCode: "ST", phoneCode: "239"},
                {text: "Saudi Arabia (+966)", countryCode: "SA", phoneCode: "966"},
                {text: "Senegal (+221)", countryCode: "SN", phoneCode: "221"},
                {text: "Serbia (+381)", countryCode: "CS", phoneCode: "381"},
                {text: "Seychelles (+248)", countryCode: "SC", phoneCode: "248"},
                {text: "Sierra Leone (+232)", countryCode: "SL", phoneCode: "232"},
                {text: "Singapore (+65)", countryCode: "SG", phoneCode: "65"},
                {text: "Slovak Republic (+421)", countryCode: "SK", phoneCode: "421"},
                {text: "Slovenia (+386)", countryCode: "SI", phoneCode: "386"},
                {text: "Solomon Islands (+677)", countryCode: "SB", phoneCode: "677"},
                {text: "Somalia (+252)", countryCode: "SO", phoneCode: "252"},
                {text: "South Africa (+27)", countryCode: "ZA", phoneCode: "27"},
                {text: "Spain (+34)", countryCode: "ES", phoneCode: "34"},
                {text: "Sri Lanka (+94)", countryCode: "LK", phoneCode: "94"},
                {text: "St. Helena (+290)", countryCode: "SH", phoneCode: "290"},
                {text: "St. Kitts (+1869)", countryCode: "KN", phoneCode: "1869"},
                {text: "St. Lucia (+1758)", countryCode: "SC", phoneCode: "1758"},
                {text: "Sudan (+249)", countryCode: "SD", phoneCode: "249"},
                {text: "Suriname (+597)", countryCode: "SR", phoneCode: "597"},
                {text: "Swaziland (+268)", countryCode: "SZ", phoneCode: "268"},
                {text: "Sweden (+46)", countryCode: "SE", phoneCode: "46"},
                {text: "Switzerland (+41)", countryCode: "CH", phoneCode: "41"},
                {text: "Syria (+963)", countryCode: "SI", phoneCode: "963"},
                {text: "Taiwan (+886)", countryCode: "TW", phoneCode: "886"},
                {text: "Tajikstan (+7)", countryCode: "TJ", phoneCode: "7"},
                {text: "Thailand (+66)", countryCode: "TH", phoneCode: "66"},
                {text: "Togo (+228)", countryCode: "TG", phoneCode: "228"},
                {text: "Tonga (+676)", countryCode: "TO", phoneCode: "676"},
                {text: "Trinidad &amp; Tobago (+1863)", countryCode: "TT", phoneCode: "1868"},
                {text: "Tunisia (+216)", countryCode: "TN", phoneCode: "216"},
                {text: "Turkey (+90)", countryCode: "TR", phoneCode: "90"},
                {text: "Turkmenistan (+7)", countryCode: "TM", phoneCode: "7"},
                {text: "Turkmenistan (+993)", countryCode: "TM", phoneCode: "993"},
                {text: "Turks & Caicos Islands (+1649)", countryCode: "TC", phoneCode: "1649"},
                {text: "Tuvalu (+688)", countryCode: "TV", phoneCode: "688"},
                {text: "Uganda (+256)", countryCode: "UG", phoneCode: "256"},
                {text: "UK (+44)", countryCode: "GB", phoneCode: "44"},
                {text: "Ukraine (+380)", countryCode: "UA", phoneCode: "380"},
                {text: "United Arab Emirates (+971)", countryCode: "AE", phoneCode: "971"},
                {text: "Uruguay (+598)", countryCode: "UY", phoneCode: "598"},
                {text: "USA (+1)", countryCode: "US", phoneCode: "1"},
                {text: "Uzbekistan (+7)", countryCode: "UZ", phoneCode: "7"},
                {text: "Vanuatu (+678)", countryCode: "VU", phoneCode: "678"},
                {text: "Vatican City (+379)", countryCode: "VA", phoneCode: "379"},
                {text: "Venezuela (+58)", countryCode: "VE", phoneCode: "58"},
                {text: "Vietnam (+84)", countryCode: "VN", phoneCode: "84"},
                {text: "Virgin Islands - British (+84)", countryCode: "VG", phoneCode: "84"},
                {text: "Virgin Islands - US (+84)", countryCode: "VI", phoneCode: "84"},
                {text: "Wallis &amp; Futuna (+681)", countryCode: "WF", phoneCode: "681"},
                {text: "Yemen (North) (+969)", countryCode: "YE", phoneCode: "969"},
                {text: "Yemen (South) (+967)", countryCode: "YE", phoneCode: "967"},
                {text: "Zambia (+260)", countryCode: "ZM", phoneCode: "260"},
                {text: "Zimbabwe (+263)", countryCode: "ZW", phoneCode: "263"}
            ];
        }
        function getCountryList() {
            return [
                {text: "Afghanistan"},
                {text: "Akrotiri"},
                {text: "Albania"},
                {text: "Algeria"},
                {text: "American Samoa"},
                {text: "Andorra"},
                {text: "Angola"},
                {text: "Anguilla"},
                {text: "Antarctica"},
                {text: "Antigua and Barbuda"},
                {text: "Argentina"},
                {text: "Armenia"},
                {text: "Aruba"},
                {text: "Ashmore and Cartier Islands"},
                {text: "Australia"},
                {text: "Austria"},
                {text: "Azerbaijan"},
                {text: "The Bahamas"},
                {text: "Bahrain"},
                {text: "Bangladesh"},
                {text: "Barbados"},
                {text: "Bassas da India"},
                {text: "Belarus"},
                {text: "Belgium"},
                {text: "Belize"},
                {text: "Benin"},
                {text: "Bermuda"},
                {text: "Bhutan"},
                {text: "Bolivia"},
                {text: "Bosnia and Herzegovina"},
                {text: "Botswana"},
                {text: "Bouvet Island"},
                {text: "Brazil"},
                {text: "British Indian Ocean Territory"},
                {text: "British Virgin Islands"},
                {text: "Brunei"},
                {text: "Bulgaria"},
                {text: "Burkina Faso"},
                {text: "Burma"},
                {text: "Burundi"},
                {text: "Cambodia"},
                {text: "Cameroon"},
                {text: "Canada"},
                {text: "Cape Verde"},
                {text: "Cayman Islands"},
                {text: "Central African Republic"},
                {text: "Chad"},
                {text: "Chile"},
                {text: "Christmas Island"},
                {text: "Clipperton Island"},
                {text: "Cocos (Keeling) Islands"},
                {text: "Colombia"},
                {text: "Comoros"},
                {text: "Congo"},
                {text: "Congo"},
                {text: "Cook Islands"},
                {text: "Coral Sea Islands"},
                {text: "Costa Rica"},
                {text: "Cote d'Ivoire"},
                {text: "Croatia"},
                {text: "Cuba"},
                {text: "Cyprus"},
                {text: "Czech Republic"},
                {text: "Denmark"},
                {text: "Dhekelia"},
                {text: "Djibouti"},
                {text: "Dominica"},
                {text: "Dominican Republic"},
                {text: "Ecuador"},
                {text: "Egypt"},
                {text: "El Salvador"},
                {text: "Equatorial Guinea"},
                {text: "Eritrea"},
                {text: "Estonia"},
                {text: "Ethiopia"},
                {text: "Europa Island"},
                {text: "Falkland Islands (Islas Malvinas)"},
                {text: "Faroe Islands"},
                {text: "Fiji"},
                {text: "Finland"},
                {text: "France"},
                {text: "French Guiana"},
                {text: "French Polynesia"},
                {text: "French Southern and Antarctic Lands"},
                {text: "Gabon"},
                {text: "Gambia"},
                {text: "Gaza Strip"},
                {text: "Georgia"},
                {text: "Ghana"},
                {text: "Gibraltar"},
                {text: "Glorioso Islands"},
                {text: "Greece"},
                {text: "Greenland"},
                {text: "Grenada"},
                {text: "Guadeloupe"},
                {text: "Guam"},
                {text: "Guatemala"},
                {text: "Guernsey"},
                {text: "Guinea"},
                {text: "Guinea-Bissau"},
                {text: "Guyana"},
                {text: "Haiti"},
                {text: "Heard Island and McDonald Islands"},
                {text: "Holy See (Vatican City)"},
                {text: "Honduras"},
                {text: "Hong Kong"},
                {text: "Hungary"},
                {text: "Iceland"},
                {text: "India"},
                {text: "Indonesia"},
                {text: "Iran"},
                {text: "Iraq"},
                {text: "Ireland"},
                {text: "Isle of Man"},
                {text: "Israel"},
                {text: "Italy"},
                {text: "Jamaica"},
                {text: "Jan Mayen"},
                {text: "Japan"},
                {text: "Jersey"},
                {text: "Jordan"},
                {text: "Juan de Nova Island"},
                {text: "Kazakhstan"},
                {text: "Kenya"},
                {text: "Kiribati"},
                {text: "North Korea"},
                {text: "South Korea"},
                {text: "Kuwait"},
                {text: "Kyrgyzstan"},
                {text: "Laos"},
                {text: "Latvia"},
                {text: "Lebanon"},
                {text: "Lesotho"},
                {text: "Liberia"},
                {text: "Libya"},
                {text: "Liechtenstein"},
                {text: "Lithuania"},
                {text: "Luxembourg"},
                {text: "Macau"},
                {text: "Macedonia"},
                {text: "Madagascar"},
                {text: "Malawi"},
                {text: "Malaysia"},
                {text: "Maldives"},
                {text: "Mali"},
                {text: "Malta"},
                {text: "Marshall Islands"},
                {text: "Martinique"},
                {text: "Mauritania"},
                {text: "Mauritius"},
                {text: "Mayotte"},
                {text: "Mexico"},
                {text: "Micronesia"},
                {text: "Federated States of"},
                {text: "Moldova"},
                {text: "Monaco"},
                {text: "Mongolia"},
                {text: "Montserrat"},
                {text: "Morocco"},
                {text: "Mozambique"},
                {text: "Namibia"},
                {text: "Nauru"},
                {text: "Navassa Island"},
                {text: "Nepal"},
                {text: "Netherlands"},
                {text: "Netherlands Antilles"},
                {text: "New Caledonia"},
                {text: "New Zealand"},
                {text: "Nicaragua"},
                {text: "Niger"},
                {text: "Nigeria"},
                {text: "Niue"},
                {text: "Norfolk Island"},
                {text: "Northern Mariana Islands"},
                {text: "Norway"},
                {text: "Oman"},
                {text: "Pakistan"},
                {text: "Palau"},
                {text: "Panama"},
                {text: "Papua New Guinea"},
                {text: "Paracel Islands"},
                {text: "Paraguay"},
                {text: "Peru"},
                {text: "Philippines"},
                {text: "Pitcairn Islands"},
                {text: "Poland"},
                {text: "Portugal"},
                {text: "Puerto Rico"},
                {text: "Qatar"},
                {text: "Reunion"},
                {text: "Romania"},
                {text: "Rwanda"},
                {text: "Saint Helena"},
                {text: "Saint Kitts and Nevis"},
                {text: "Saint Lucia"},
                {text: "Saint Pierre and Miquelon"},
                {text: "Saint Vincent and the Grenadines"},
                {text: "Samoa"},
                {text: "San Marino"},
                {text: "Sao Tome and Principe"},
                {text: "Saudi Arabia"},
                {text: "Senegal"},
                {text: "Serbia and Montenegro"},
                {text: "Seychelles"},
                {text: "Sierra Leone"},
                {text: "Singapore"},
                {text: "Slovakia"},
                {text: "Slovenia"},
                {text: "Solomon Islands"},
                {text: "Somalia"},
                {text: "South Africa"},
                {text: "South Georgia and the South Sandwich Islands"},
                {text: "Spain"},
                {text: "Spratly Islands"},
                {text: "Sri Lanka"},
                {text: "Sudan"},
                {text: "Suriname"},
                {text: "Svalbard"},
                {text: "Swaziland"},
                {text: "Sweden"},
                {text: "Switzerland"},
                {text: "Syria"},
                {text: "Taiwan"},
                {text: "Tajikistan"},
                {text: "Tanzania"},
                {text: "Thailand"},
                {text: "Timor-Leste"},
                {text: "Togo"},
                {text: "Tokelau"},
                {text: "Tonga"},
                {text: "Trinidad and Tobago"},
                {text: "Tromelin Island"},
                {text: "Tunisia"},
                {text: "Turkey"},
                {text: "Turkmenistan"},
                {text: "Turks and Caicos Islands"},
                {text: "Tuvalu"},
                {text: "Uganda"},
                {text: "Ukraine"},
                {text: "United Arab Emirates"},
                {text: "United Kingdom"},
                {text: "Uruguay"},
                {text: "Uzbekistan"},
                {text: "Vanuatu"},
                {text: "Venezuela"},
                {text: "Vietnam"},
                {text: "Virgin Islands"},
                {text: "Wake Island"},
                {text: "Wallis and Futuna"},
                {text: "West Bank"},
                {text: "Western Sahara"},
                {text: "Yemen"},
                {text: "Zambia"},
                {text: "Zimbabwe"}
            ];
        }
        function getStateList(){
            return [
                {text: "New South Wales"},
                {text: "Victoria"},
                {text: "Queensland"},
                {text: "Western Australia"},
                {text: "South Australia"},
                {text: "Tasmania"},
                {text: "Australian Capital Territory"},
                {text: "Northern Territory"},
                {text: "Norfolk Island"},
                {text: "Christmas Island"},
                {text: "Australian Antarctic Territory"},
                {text: "Jervis Bay Territory"},
                {text: "Cocos (Keeling) Islands"},
                {text: "Coral Sea Islands"},
                {text: "Ashmore and Cartier Islands"},
                {text: "Heard Island and McDonald Islands"}
            ];

        }
        function getYearOptions() {
            var yearOptions = [];
            for(var i  = 1900; i < $scope.yearThreshold; i ++){
                yearOptions.push({text: i + "", value: i});
            }
            return yearOptions;
        }
        function getMonthOptions() {
            return [
                {text: "January", value: 0},
                {text: "February", value: 1},
                {text: "March", value: 2},
                {text: "April", value: 3},
                {text: "May", value: 4},
                {text: "June", value: 5},
                {text: "July", value: 6},
                {text: "August", value: 7},
                {text: "September", value: 8},
                {text: "October", value: 9},
                {text: "November", value: 10},
                {text: "December", value: 11}
            ];
        }
        function getDayOptions() {
            var dayOptions = [];
            for(var i  = 1; i < 32; i ++){
                dayOptions.push({text: i + "", value: i});
            }
            return dayOptions;
        }

        /*Login*/
        function accountLoginHandler(username,password){
            accountService.accountLogin(username,password)
                .then(accountLoginRes,accountLoginRej);

            function accountLoginRes(res){
                //login rejected
                if(!!res.data.errorMessage){
                    $rootScope.accountInfo.isLogin = false;
                    $rootScope.accountInfo.errorMessage = res.data.errorMessage;
                }

                //login successfully
                if(!!res.data.accountHolderDto){
                    $rootScope.accountInfo.isLogin = true;
                    $rootScope.accountInfo.accountHolderDto = res.data.accountHolderDto;
                }

            }
            function accountLoginRej(res){

            }
        }
        function isLogin(){
            if(!$rootScope.accountInfo.isLogin){
                window.location.href = '#/';
            }
        }

        /*PROFILE FUNCTIONS*/
        function initProfile(){
            var profile = {
                person : {
                    firstname : '',
                    lastname : '',
                    dob : {
                        daySelected : {text:'Day',value:-1},
                        dayOptions : getDayOptions(),
                        monthSelected : {text:'Month',value:-1},
                        monthOptions : getMonthOptions(),
                        yearSelected : {text:'Year',value:-1},
                        yearOptions : getYearOptions(),
                        value: ''
                    },
                    email : '',
                    reTypeEmail : ''
                },
                phone : {
                    mobile : '',
                    countryCode : {
                        selected : {text:"Australia (+61)",countryCode:"AU",phoneCode:"61"},
                        options: getCountryPhoneCodeList()
                    },
                    areaCode: '',
                    landLine:''
                },
                address : {
                    addressCountry : {
                        selected : {text:"Australia"},
                        options : getCountryList()
                    },
                    unitNo: '',
                    streetNo: '',
                    street: '',
                    suburb: '',
                    postcode: '',
                    state:{
                        selected : {text:"New South Wales"},
                        options : getStateList()
                    }
                },
                account : {
                    accountNo : '',
                    username : '',
                    password : '',
                    reTypePassword : ''
                },
                err: ''
            };
            return profile;
        }
        function resetSignUpForm(){
            $scope.initProfile = initProfile();
        }
        function setSelectedDay(option){
            $scope.initProfile.person.dob.daySelected.text = option.text;
            $scope.initProfile.person.dob.daySelected.value = option.value;
        }
        function setSelectedMonth(option){
            $scope.initProfile.person.dob.monthSelected.text = option.text;
            $scope.initProfile.person.dob.monthSelected.value = option.value;
        }
        function setSelectedYear(option){
            $scope.initProfile.person.dob.yearSelected.text = option.text;
            $scope.initProfile.person.dob.yearSelected.value = option.value;
        }
        function setSelectedcountryCode(option){
            $scope.initProfile.phone.countryCode.selected.text = option.text;
        }
        function setSelectedCountry(option){
            $scope.initProfile.address.addressCountry.selected.text = option.text;
        }
        function setSelectedState(option){
            $scope.initProfile.address.state.selected.text = option.text;
        }
        function emailVerify(email){
            var isMatch = false;
            if(!email){
                return isMatch;
            }
            var EMAIL_REGX = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i;
            if(email.match(EMAIL_REGX)){
                isMatch = true;
            }
            return isMatch;
        }
        function isEmailExist(email){
            accountService.isEmailExist(email)
                .then(isEmailExistRes,isEmailExistRej);

            function isEmailExistRes(res){
                $scope.emailExist = res.data.success;
            }
            function isEmailExistRej(res){

            }
        }
        function isMobileExist(mobile){
            accountService.isMobileExist(mobile)
                .then(isMobileExistRes,isMobileExistRej);
            function isMobileExistRes(res){
                $scope.mobileExist = res.data.success;
            }
            function isMobileExistRej(res){

            }
        }
        function mobilePrefix(mobile){
            var prefix = '04';
            if(mobile.startsWith(prefix)){
                $scope.mobilePrefix = true;
            }

        }
        function mobileJudge(mobile){
            mobilePrefix(mobile);
            isMobileExist(mobile);
        }
        function isUserExist(username){
            accountService.isUserExist(username)
                .then(isUserExistRes,isUserExistRej);
            function isUserExistRes(res){
                $scope.userExist = res.data.success;
            }
            function isUserExistRej(res){

            }
        }
        function dobValidation(dob){
            var dobValid = false;
            var mydate = new Date;
            var nowYear = mydate.getFullYear();
            var nowMonth = mydate.getMonth();
            var nowDate = mydate.getDate();
            if(nowYear - dob.yearSelected.text > 18){
                dobValid = true;
            }else if(nowYear - dob.yearSelected.text < 18){
                dobValid = false;
            }else if(nowYear - dob.yearSelected.text == 18){
                if(nowMonth - dob.monthSelected.value > 0){
                    dobValid = true;
                }else if (nowMonth - dob.monthSelected.value < 0){
                    dobValid = false;
                }else if (nowMonth - dob.monthSelected.value == 0){
                    if(nowDate - dob.daySelected.text > 0){
                        dobValid = true;
                    }else{
                        dobValid = false;
                    }
                }
            }
            return dobValid;
        }
        function NameValidation(name){
            var nameValid = false;
            if(!name){
                return nameValid;
            }else{
                nameValid = true;
            }
            return nameValid;
        }
        function submitSignUpFormHandler(){
            if(!NameValidation($scope.initProfile.person.firstname)){
                $scope.initProfile.err = 'First Name is invalid';
                return;
            }
            if(!NameValidation($scope.initProfile.person.lastname)){
                $scope.initProfile.err = 'Last Name is invalid';
                return;
            }
            if(!dobValidation($scope.initProfile.person.dob)){
                $scope.initProfile.err = 'AGE RESTRICTED ABOVE 18 YEARS';
                return;
            }
            if(stringMatch($scope.initProfile.account.password,$scope.initProfile.account.reTypePassword)){
                $scope.initProfile.err = 'Password and confirmation are Not Match';
                return;
            }
            if(stringMatch($scope.initProfile.person.email,$scope.initProfile.person.reTypeEmail)){
                $scope.initProfile.err = 'Email and confirmation are Not Match';
                return;
            }

            accountService.signUp($scope.initProfile)
                .then(signUpRes,signUpRej);

            function signUpRes(res){
                if(res.data.success){
                    accountLoginHandler($scope.initProfile.account.username,$scope.initProfile.account.password);
                    window.location.href = '#/account/registration_success';
                }else if(res.data.errorMessage){
                    $scope.initProfile.err = res.data.errorMessage;
                }else{
                    $scope.initProfile.err = 'Please check the detail and try again.'
                }
            }
            function signUpRej(res){

            }
        }
        function stringMatch(newPassword,confirmPassword){
            var notMatch = true;
            if(newPassword != undefined && confirmPassword != undefined && newPassword == confirmPassword){
                notMatch = false;
            }
            return notMatch;
        }

        /*COMMON*/
        function getCountryPhoneCodeList() {
            return [
                {text: "Algeria (+213)", countryCode: "DZ", phoneCode: "213"},
                {text: "Andorra (+376)", countryCode: "AD", phoneCode: "376"},
                {text: "Angola (+244)", countryCode: "AO", phoneCode: "244"},
                {text: "Anguilla (+1264)", countryCode: "AI", phoneCode: "1264"},
                {text: "Antigua & Barbuda (+1268)", countryCode: "AG", phoneCode: "1268"},
                {text: "Argentina (+54)", countryCode: "AR", phoneCode: "54"},
                {text: "Armenia (+374)", countryCode: "AM", phoneCode: "374"},
                {text: "Aruba (+297)", countryCode: "AW", phoneCode: "297"},
                {text: "Australia (+61)", countryCode: "AU", phoneCode: "61"},
                {text: "Austria (+43)", countryCode: "AT", phoneCode: "43"},
                {text: "Azerbaijan (+994)", countryCode: "AZ", phoneCode: "994"},
                {text: "Bahamas (+1242)", countryCode: "BS", phoneCode: "1242"},
                {text: "Bahrain (+973)", countryCode: "BH", phoneCode: "973"},
                {text: "Bangladesh (+880)", countryCode: "BD", phoneCode: "880"},
                {text: "Barbados (+1246)", countryCode: "BB", phoneCode: "1246"},
                {text: "Belarus (+375)", countryCode: "BY", phoneCode: "375"},
                {text: "Belgium (+32)", countryCode: "BE", phoneCode: "32"},
                {text: "Belize (+501)", countryCode: "BZ", phoneCode: "501"},
                {text: "Benin (+229)", countryCode: "BJ", phoneCode: "229"},
                {text: "Bermuda (+1441)", countryCode: "BM", phoneCode: "1441"},
                {text: "Bhutan (+975)", countryCode: "BT", phoneCode: "975"},
                {text: "Bolivia (+591)", countryCode: "BO", phoneCode: "591"},
                {text: "Bosnia Herzegovina (+387)", countryCode: "BA", phoneCode: "387"},
                {text: "Botswana (+267)", countryCode: "BW", phoneCode: "267"},
                {text: "Brazil (+55)", countryCode: "BR", phoneCode: "55"},
                {text: "Brunei (+673)", countryCode: "BN", phoneCode: "673"},
                {text: "Bulgaria (+359)", countryCode: "BG", phoneCode: "359"},
                {text: "Burkina Faso (+226)", countryCode: "BF", phoneCode: "226"},
                {text: "Burundi (+257)", countryCode: "BI", phoneCode: "257"},
                {text: "Cambodia (+855)", countryCode: "KH", phoneCode: "855"},
                {text: "Cameroon (+237)", countryCode: "CM", phoneCode: "237"},
                {text: "Canada (+1)", countryCode: "CA", phoneCode: "1"},
                {text: "Cape Verde Islands (+238)", countryCode: "CV", phoneCode: "238"},
                {text: "Cayman Islands (+1345)", countryCode: "KY", phoneCode: "1345"},
                {text: "Central African Republic (+236)", countryCode: "CF", phoneCode: "236"},
                {text: "Chile (+56)", countryCode: "CL", phoneCode: "56"},
                {text: "China (+86)", countryCode: "CN", phoneCode: "86"},
                {text: "Colombia (+57)", countryCode: "CO", phoneCode: "57"},
                {text: "Comoros (+269)", countryCode: "KM", phoneCode: "269"},
                {text: "Congo (+242)", countryCode: "CG", phoneCode: "242"},
                {text: "Cook Islands (+682)", countryCode: "CK", phoneCode: "682"},
                {text: "Costa Rica (+506)", countryCode: "CR", phoneCode: "506"},
                {text: "Croatia (+385)", countryCode: "HR", phoneCode: "385"},
                {text: "Cuba (+53)", countryCode: "CU", phoneCode: "53"},
                {text: "Cyprus North (+90392)", countryCode: "CY", phoneCode: "90392"},
                {text: "Cyprus South (+357)", countryCode: "CY", phoneCode: "357"},
                {text: "Czech Republic (+42)", countryCode: "CZ", phoneCode: "42"},
                {text: "Denmark (+45)", countryCode: "DK", phoneCode: "45"},
                {text: "Djibouti (+253)", countryCode: "DJ", phoneCode: "253"},
                {text: "Dominica (+1809)", countryCode: "DM", phoneCode: "1809"},
                {text: "Dominican Republic (+1809)", countryCode: "DO", phoneCode: "1809"},
                {text: "Ecuador (+593)", countryCode: "EC", phoneCode: "593"},
                {text: "Egypt (+20)", countryCode: "EG", phoneCode: "20"},
                {text: "El Salvador (+503)", countryCode: "SV", phoneCode: "503"},
                {text: "Equatorial Guinea (+240)", countryCode: "GQ", phoneCode: "240"},
                {text: "Eritrea (+291)", countryCode: "ER", phoneCode: "291"},
                {text: "Estonia (+372)", countryCode: "EE", phoneCode: "372"},
                {text: "Ethiopia (+251)", countryCode: "ET", phoneCode: "251"},
                {text: "Falkland Islands (+500)", countryCode: "FK", phoneCode: "500"},
                {text: "Faroe Islands (+298)", countryCode: "FO", phoneCode: "298"},
                {text: "Fiji (+679)", countryCode: "FJ", phoneCode: "679"},
                {text: "Finland (+358)", countryCode: "FI", phoneCode: "358"},
                {text: "France (+33)", countryCode: "FR", phoneCode: "33"},
                {text: "French Guiana (+594)", countryCode: "GF", phoneCode: "594"},
                {text: "French Polynesia (+689)", countryCode: "PF", phoneCode: "689"},
                {text: "Gabon (+241)", countryCode: "GA", phoneCode: "241"},
                {text: "Gambia (+220)", countryCode: "GM", phoneCode: "220"},
                {text: "Georgia (+7880)", countryCode: "GE", phoneCode: "7880"},
                {text: "Germany (+49)", countryCode: "DE", phoneCode: "49"},
                {text: "Ghana (+233)", countryCode: "GH", phoneCode: "233"},
                {text: "Gibraltar (+350)", countryCode: "GI", phoneCode: "350"},
                {text: "Greece (+30)", countryCode: "GR", phoneCode: "30"},
                {text: "Greenland (+299)", countryCode: "GL", phoneCode: "299"},
                {text: "Grenada (+1473)", countryCode: "GD", phoneCode: "1473"},
                {text: "Guadeloupe (+590)", countryCode: "GP", phoneCode: "590"},
                {text: "Guam (+671)", countryCode: "GU", phoneCode: "671"},
                {text: "Guatemala (+502)", countryCode: "GT", phoneCode: "502"},
                {text: "Guinea (+224)", countryCode: "GN", phoneCode: "224"},
                {text: "Guinea - Bissau (+245)", countryCode: "GW", phoneCode: "245"},
                {text: "Guyana (+592)", countryCode: "GY", phoneCode: "592"},
                {text: "Haiti (+509)", countryCode: "HT", phoneCode: "509"},
                {text: "Honduras (+504)", countryCode: "HN", phoneCode: "504"},
                {text: "Hong Kong (+852)", countryCode: "HK", phoneCode: "852"},
                {text: "Hungary (+36)", countryCode: "HU", phoneCode: "36"},
                {text: "Iceland (+354)", countryCode: "IS", phoneCode: "354"},
                {text: "India (+91)", countryCode: "IN", phoneCode: "91"},
                {text: "Indonesia (+62)", countryCode: "ID", phoneCode: "62"},
                {text: "Iran (+98)", countryCode: "IR", phoneCode: "98"},
                {text: "Iraq (+964)", countryCode: "IQ", phoneCode: "964"},
                {text: "Ireland (+353)", countryCode: "IE", phoneCode: "353"},
                {text: "Israel (+972)", countryCode: "IL", phoneCode: "972"},
                {text: "Italy (+39)", countryCode: "IT", phoneCode: "39"},
                {text: "Jamaica (+1876)", countryCode: "JM", phoneCode: "1876"},
                {text: "Japan (+81)", countryCode: "JP", phoneCode: "81"},
                {text: "Jordan (+962)", countryCode: "JO", phoneCode: "962"},
                {text: "Kazakhstan (+7)", countryCode: "KZ", phoneCode: "7"},
                {text: "Kenya (+254)", countryCode: "KE", phoneCode: "254"},
                {text: "Kiribati (+686)", countryCode: "KI", phoneCode: "686"},
                {text: "Korea North (+850)", countryCode: "KP", phoneCode: "850"},
                {text: "Korea South (+82)", countryCode: "KR", phoneCode: "82"},
                {text: "Kuwait (+965)", countryCode: "KW", phoneCode: "965"},
                {text: "Kyrgyzstan (+996)", countryCode: "KG", phoneCode: "996"},
                {text: "Laos (+856)", countryCode: "LA", phoneCode: "856"},
                {text: "Latvia (+371)", countryCode: "LV", phoneCode: "371"},
                {text: "Lebanon (+961)", countryCode: "LB", phoneCode: "961"},
                {text: "Lesotho (+266)", countryCode: "LS", phoneCode: "266"},
                {text: "Liberia (+231)", countryCode: "LR", phoneCode: "231"},
                {text: "Libya (+218)", countryCode: "LY", phoneCode: "218"},
                {text: "Liechtenstein (+417)", countryCode: "LI", phoneCode: "417"},
                {text: "Lithuania (+370)", countryCode: "LT", phoneCode: "370"},
                {text: "Luxembourg (+352)", countryCode: "LU", phoneCode: "352"},
                {text: "Macao (+853)", countryCode: "MO", phoneCode: "853"},
                {text: "Macedonia (+389)", countryCode: "MK", phoneCode: "389"},
                {text: "Madagascar (+261)", countryCode: "MG", phoneCode: "261"},
                {text: "Malawi (+265)", countryCode: "MW", phoneCode: "265"},
                {text: "Malaysia (+60)", countryCode: "MY", phoneCode: "60"},
                {text: "Maldives (+960)", countryCode: "MV", phoneCode: "960"},
                {text: "Mali (+223)", countryCode: "ML", phoneCode: "223"},
                {text: "Malta (+356)", countryCode: "MT", phoneCode: "356"},
                {text: "Marshall Islands (+692)", countryCode: "MH", phoneCode: "692"},
                {text: "Martinique (+596)", countryCode: "MQ", phoneCode: "596"},
                {text: "Mauritania (+222)", countryCode: "MR", phoneCode: "222"},
                {text: "Mayotte (+269)", countryCode: "YT", phoneCode: "269"},
                {text: "Mexico (+52)", countryCode: "MX", phoneCode: "52"},
                {text: "Micronesia (+691)", countryCode: "FM", phoneCode: "691"},
                {text: "Moldova (+373)", countryCode: "MD", phoneCode: "373"},
                {text: "Monaco (+377)", countryCode: "MC", phoneCode: "377"},
                {text: "Mongolia (+976)", countryCode: "MN", phoneCode: "976"},
                {text: "Montserrat (+1664)", countryCode: "MS", phoneCode: "1664"},
                {text: "Morocco (+212)", countryCode: "MA", phoneCode: "212"},
                {text: "Mozambique (+258)", countryCode: "MZ", phoneCode: "258"},
                {text: "Myanmar (+95)", countryCode: "MN", phoneCode: "95"},
                {text: "Namibia (+264)", countryCode: "NA", phoneCode: "264"},
                {text: "Nauru (+674)", countryCode: "NR", phoneCode: "674"},
                {text: "Nepal (+977)", countryCode: "NP", phoneCode: "977"},
                {text: "Netherlands (+31)", countryCode: "NL", phoneCode: "31"},
                {text: "New Caledonia (+687)", countryCode: "NC", phoneCode: "687"},
                {text: "New Zealand (+64)", countryCode: "NZ", phoneCode: "64"},
                {text: "Nicaragua (+505)", countryCode: "NI", phoneCode: "505"},
                {text: "Niger (+227)", countryCode: "NE", phoneCode: "227"},
                {text: "Nigeria (+234)", countryCode: "NG", phoneCode: "234"},
                {text: "Niue (+683)", countryCode: "NU", phoneCode: "683"},
                {text: "Norfolk Islands (+672)", countryCode: "NF", phoneCode: "672"},
                {text: "Northern Marianas (+670)", countryCode: "NP", phoneCode: "670"},
                {text: "Norway (+47)", countryCode: "NO", phoneCode: "47"},
                {text: "Oman (+968)", countryCode: "OM", phoneCode: "968"},
                {text: "Palau (+680)", countryCode: "PW", phoneCode: "680"},
                {text: "Panama (+507)", countryCode: "PA", phoneCode: "507"},
                {text: "Papua New Guinea (+675)", countryCode: "PG", phoneCode: "675"},
                {text: "Paraguay (+595)", countryCode: "PY", phoneCode: "595"},
                {text: "Peru (+51)", countryCode: "PE", phoneCode: "51"},
                {text: "Philippines (+63)", countryCode: "PH", phoneCode: "63"},
                {text: "Poland (+48)", countryCode: "PL", phoneCode: "48"},
                {text: "Portugal (+351)", countryCode: "PT", phoneCode: "351"},
                {text: "Puerto Rico (+1787)", countryCode: "PR", phoneCode: "1787"},
                {text: "Qatar (+974)", countryCode: "QA", phoneCode: "974"},
                {text: "Reunion (+262)", countryCode: "RE", phoneCode: "262"},
                {text: "Romania (+40)", countryCode: "RO", phoneCode: "40"},
                {text: "Russia (+7)", countryCode: "RU", phoneCode: "7"},
                {text: "Rwanda (+250)", countryCode: "RW", phoneCode: "250"},
                {text: "San Marino (+378)", countryCode: "SM", phoneCode: "378"},
                {text: "Sao Tome &amp; Principe (+239)", countryCode: "ST", phoneCode: "239"},
                {text: "Saudi Arabia (+966)", countryCode: "SA", phoneCode: "966"},
                {text: "Senegal (+221)", countryCode: "SN", phoneCode: "221"},
                {text: "Serbia (+381)", countryCode: "CS", phoneCode: "381"},
                {text: "Seychelles (+248)", countryCode: "SC", phoneCode: "248"},
                {text: "Sierra Leone (+232)", countryCode: "SL", phoneCode: "232"},
                {text: "Singapore (+65)", countryCode: "SG", phoneCode: "65"},
                {text: "Slovak Republic (+421)", countryCode: "SK", phoneCode: "421"},
                {text: "Slovenia (+386)", countryCode: "SI", phoneCode: "386"},
                {text: "Solomon Islands (+677)", countryCode: "SB", phoneCode: "677"},
                {text: "Somalia (+252)", countryCode: "SO", phoneCode: "252"},
                {text: "South Africa (+27)", countryCode: "ZA", phoneCode: "27"},
                {text: "Spain (+34)", countryCode: "ES", phoneCode: "34"},
                {text: "Sri Lanka (+94)", countryCode: "LK", phoneCode: "94"},
                {text: "St. Helena (+290)", countryCode: "SH", phoneCode: "290"},
                {text: "St. Kitts (+1869)", countryCode: "KN", phoneCode: "1869"},
                {text: "St. Lucia (+1758)", countryCode: "SC", phoneCode: "1758"},
                {text: "Sudan (+249)", countryCode: "SD", phoneCode: "249"},
                {text: "Suriname (+597)", countryCode: "SR", phoneCode: "597"},
                {text: "Swaziland (+268)", countryCode: "SZ", phoneCode: "268"},
                {text: "Sweden (+46)", countryCode: "SE", phoneCode: "46"},
                {text: "Switzerland (+41)", countryCode: "CH", phoneCode: "41"},
                {text: "Syria (+963)", countryCode: "SI", phoneCode: "963"},
                {text: "Taiwan (+886)", countryCode: "TW", phoneCode: "886"},
                {text: "Tajikstan (+7)", countryCode: "TJ", phoneCode: "7"},
                {text: "Thailand (+66)", countryCode: "TH", phoneCode: "66"},
                {text: "Togo (+228)", countryCode: "TG", phoneCode: "228"},
                {text: "Tonga (+676)", countryCode: "TO", phoneCode: "676"},
                {text: "Trinidad &amp; Tobago (+1863)", countryCode: "TT", phoneCode: "1868"},
                {text: "Tunisia (+216)", countryCode: "TN", phoneCode: "216"},
                {text: "Turkey (+90)", countryCode: "TR", phoneCode: "90"},
                {text: "Turkmenistan (+7)", countryCode: "TM", phoneCode: "7"},
                {text: "Turkmenistan (+993)", countryCode: "TM", phoneCode: "993"},
                {text: "Turks & Caicos Islands (+1649)", countryCode: "TC", phoneCode: "1649"},
                {text: "Tuvalu (+688)", countryCode: "TV", phoneCode: "688"},
                {text: "Uganda (+256)", countryCode: "UG", phoneCode: "256"},
                {text: "UK (+44)", countryCode: "GB", phoneCode: "44"},
                {text: "Ukraine (+380)", countryCode: "UA", phoneCode: "380"},
                {text: "United Arab Emirates (+971)", countryCode: "AE", phoneCode: "971"},
                {text: "Uruguay (+598)", countryCode: "UY", phoneCode: "598"},
                {text: "USA (+1)", countryCode: "US", phoneCode: "1"},
                {text: "Uzbekistan (+7)", countryCode: "UZ", phoneCode: "7"},
                {text: "Vanuatu (+678)", countryCode: "VU", phoneCode: "678"},
                {text: "Vatican City (+379)", countryCode: "VA", phoneCode: "379"},
                {text: "Venezuela (+58)", countryCode: "VE", phoneCode: "58"},
                {text: "Vietnam (+84)", countryCode: "VN", phoneCode: "84"},
                {text: "Virgin Islands - British (+84)", countryCode: "VG", phoneCode: "84"},
                {text: "Virgin Islands - US (+84)", countryCode: "VI", phoneCode: "84"},
                {text: "Wallis &amp; Futuna (+681)", countryCode: "WF", phoneCode: "681"},
                {text: "Yemen (North) (+969)", countryCode: "YE", phoneCode: "969"},
                {text: "Yemen (South) (+967)", countryCode: "YE", phoneCode: "967"},
                {text: "Zambia (+260)", countryCode: "ZM", phoneCode: "260"},
                {text: "Zimbabwe (+263)", countryCode: "ZW", phoneCode: "263"}
            ];
        }
        function getCountryList() {
            return [
                {text: "Afghanistan"},
                {text: "Akrotiri"},
                {text: "Albania"},
                {text: "Algeria"},
                {text: "American Samoa"},
                {text: "Andorra"},
                {text: "Angola"},
                {text: "Anguilla"},
                {text: "Antarctica"},
                {text: "Antigua and Barbuda"},
                {text: "Argentina"},
                {text: "Armenia"},
                {text: "Aruba"},
                {text: "Ashmore and Cartier Islands"},
                {text: "Australia"},
                {text: "Austria"},
                {text: "Azerbaijan"},
                {text: "The Bahamas"},
                {text: "Bahrain"},
                {text: "Bangladesh"},
                {text: "Barbados"},
                {text: "Bassas da India"},
                {text: "Belarus"},
                {text: "Belgium"},
                {text: "Belize"},
                {text: "Benin"},
                {text: "Bermuda"},
                {text: "Bhutan"},
                {text: "Bolivia"},
                {text: "Bosnia and Herzegovina"},
                {text: "Botswana"},
                {text: "Bouvet Island"},
                {text: "Brazil"},
                {text: "British Indian Ocean Territory"},
                {text: "British Virgin Islands"},
                {text: "Brunei"},
                {text: "Bulgaria"},
                {text: "Burkina Faso"},
                {text: "Burma"},
                {text: "Burundi"},
                {text: "Cambodia"},
                {text: "Cameroon"},
                {text: "Canada"},
                {text: "Cape Verde"},
                {text: "Cayman Islands"},
                {text: "Central African Republic"},
                {text: "Chad"},
                {text: "Chile"},
                {text: "Christmas Island"},
                {text: "Clipperton Island"},
                {text: "Cocos (Keeling) Islands"},
                {text: "Colombia"},
                {text: "Comoros"},
                {text: "Congo"},
                {text: "Congo"},
                {text: "Cook Islands"},
                {text: "Coral Sea Islands"},
                {text: "Costa Rica"},
                {text: "Cote d'Ivoire"},
                {text: "Croatia"},
                {text: "Cuba"},
                {text: "Cyprus"},
                {text: "Czech Republic"},
                {text: "Denmark"},
                {text: "Dhekelia"},
                {text: "Djibouti"},
                {text: "Dominica"},
                {text: "Dominican Republic"},
                {text: "Ecuador"},
                {text: "Egypt"},
                {text: "El Salvador"},
                {text: "Equatorial Guinea"},
                {text: "Eritrea"},
                {text: "Estonia"},
                {text: "Ethiopia"},
                {text: "Europa Island"},
                {text: "Falkland Islands (Islas Malvinas)"},
                {text: "Faroe Islands"},
                {text: "Fiji"},
                {text: "Finland"},
                {text: "France"},
                {text: "French Guiana"},
                {text: "French Polynesia"},
                {text: "French Southern and Antarctic Lands"},
                {text: "Gabon"},
                {text: "Gambia"},
                {text: "Gaza Strip"},
                {text: "Georgia"},
                {text: "Ghana"},
                {text: "Gibraltar"},
                {text: "Glorioso Islands"},
                {text: "Greece"},
                {text: "Greenland"},
                {text: "Grenada"},
                {text: "Guadeloupe"},
                {text: "Guam"},
                {text: "Guatemala"},
                {text: "Guernsey"},
                {text: "Guinea"},
                {text: "Guinea-Bissau"},
                {text: "Guyana"},
                {text: "Haiti"},
                {text: "Heard Island and McDonald Islands"},
                {text: "Holy See (Vatican City)"},
                {text: "Honduras"},
                {text: "Hong Kong"},
                {text: "Hungary"},
                {text: "Iceland"},
                {text: "India"},
                {text: "Indonesia"},
                {text: "Iran"},
                {text: "Iraq"},
                {text: "Ireland"},
                {text: "Isle of Man"},
                {text: "Israel"},
                {text: "Italy"},
                {text: "Jamaica"},
                {text: "Jan Mayen"},
                {text: "Japan"},
                {text: "Jersey"},
                {text: "Jordan"},
                {text: "Juan de Nova Island"},
                {text: "Kazakhstan"},
                {text: "Kenya"},
                {text: "Kiribati"},
                {text: "North Korea"},
                {text: "South Korea"},
                {text: "Kuwait"},
                {text: "Kyrgyzstan"},
                {text: "Laos"},
                {text: "Latvia"},
                {text: "Lebanon"},
                {text: "Lesotho"},
                {text: "Liberia"},
                {text: "Libya"},
                {text: "Liechtenstein"},
                {text: "Lithuania"},
                {text: "Luxembourg"},
                {text: "Macau"},
                {text: "Macedonia"},
                {text: "Madagascar"},
                {text: "Malawi"},
                {text: "Malaysia"},
                {text: "Maldives"},
                {text: "Mali"},
                {text: "Malta"},
                {text: "Marshall Islands"},
                {text: "Martinique"},
                {text: "Mauritania"},
                {text: "Mauritius"},
                {text: "Mayotte"},
                {text: "Mexico"},
                {text: "Micronesia"},
                {text: "Federated States of"},
                {text: "Moldova"},
                {text: "Monaco"},
                {text: "Mongolia"},
                {text: "Montserrat"},
                {text: "Morocco"},
                {text: "Mozambique"},
                {text: "Namibia"},
                {text: "Nauru"},
                {text: "Navassa Island"},
                {text: "Nepal"},
                {text: "Netherlands"},
                {text: "Netherlands Antilles"},
                {text: "New Caledonia"},
                {text: "New Zealand"},
                {text: "Nicaragua"},
                {text: "Niger"},
                {text: "Nigeria"},
                {text: "Niue"},
                {text: "Norfolk Island"},
                {text: "Northern Mariana Islands"},
                {text: "Norway"},
                {text: "Oman"},
                {text: "Pakistan"},
                {text: "Palau"},
                {text: "Panama"},
                {text: "Papua New Guinea"},
                {text: "Paracel Islands"},
                {text: "Paraguay"},
                {text: "Peru"},
                {text: "Philippines"},
                {text: "Pitcairn Islands"},
                {text: "Poland"},
                {text: "Portugal"},
                {text: "Puerto Rico"},
                {text: "Qatar"},
                {text: "Reunion"},
                {text: "Romania"},
                {text: "Rwanda"},
                {text: "Saint Helena"},
                {text: "Saint Kitts and Nevis"},
                {text: "Saint Lucia"},
                {text: "Saint Pierre and Miquelon"},
                {text: "Saint Vincent and the Grenadines"},
                {text: "Samoa"},
                {text: "San Marino"},
                {text: "Sao Tome and Principe"},
                {text: "Saudi Arabia"},
                {text: "Senegal"},
                {text: "Serbia and Montenegro"},
                {text: "Seychelles"},
                {text: "Sierra Leone"},
                {text: "Singapore"},
                {text: "Slovakia"},
                {text: "Slovenia"},
                {text: "Solomon Islands"},
                {text: "Somalia"},
                {text: "South Africa"},
                {text: "South Georgia and the South Sandwich Islands"},
                {text: "Spain"},
                {text: "Spratly Islands"},
                {text: "Sri Lanka"},
                {text: "Sudan"},
                {text: "Suriname"},
                {text: "Svalbard"},
                {text: "Swaziland"},
                {text: "Sweden"},
                {text: "Switzerland"},
                {text: "Syria"},
                {text: "Taiwan"},
                {text: "Tajikistan"},
                {text: "Tanzania"},
                {text: "Thailand"},
                {text: "Timor-Leste"},
                {text: "Togo"},
                {text: "Tokelau"},
                {text: "Tonga"},
                {text: "Trinidad and Tobago"},
                {text: "Tromelin Island"},
                {text: "Tunisia"},
                {text: "Turkey"},
                {text: "Turkmenistan"},
                {text: "Turks and Caicos Islands"},
                {text: "Tuvalu"},
                {text: "Uganda"},
                {text: "Ukraine"},
                {text: "United Arab Emirates"},
                {text: "United Kingdom"},
                {text: "Uruguay"},
                {text: "Uzbekistan"},
                {text: "Vanuatu"},
                {text: "Venezuela"},
                {text: "Vietnam"},
                {text: "Virgin Islands"},
                {text: "Wake Island"},
                {text: "Wallis and Futuna"},
                {text: "West Bank"},
                {text: "Western Sahara"},
                {text: "Yemen"},
                {text: "Zambia"},
                {text: "Zimbabwe"}
            ];
        }
        function getStateList(){
            return [
                {text: "New South Wales"},
                {text: "Victoria"},
                {text: "Queensland"},
                {text: "Western Australia"},
                {text: "South Australia"},
                {text: "Tasmania"},
                {text: "Australian Capital Territory"},
                {text: "Northern Territory"},
                {text: "Norfolk Island"},
                {text: "Christmas Island"},
                {text: "Australian Antarctic Territory"},
                {text: "Jervis Bay Territory"},
                {text: "Cocos (Keeling) Islands"},
                {text: "Coral Sea Islands"},
                {text: "Ashmore and Cartier Islands"},
                {text: "Heard Island and McDonald Islands"}
            ];

        }
        function getYearOptions() {
            var yearOptions = [];
            for(var i  = 1900; i < $scope.yearThreshold; i ++){
                yearOptions.push({text: i + "", value: i});
            }
            return yearOptions;
        }
        function getMonthOptions() {
            return [
                {text: "January", value: 0},
                {text: "February", value: 1},
                {text: "March", value: 2},
                {text: "April", value: 3},
                {text: "May", value: 4},
                {text: "June", value: 5},
                {text: "July", value: 6},
                {text: "August", value: 7},
                {text: "September", value: 8},
                {text: "October", value: 9},
                {text: "November", value: 10},
                {text: "December", value: 11}
            ];
        }
        function getDayOptions() {
            var dayOptions = [];
            for(var i  = 1; i < 32; i ++){
                dayOptions.push({text: i + "", value: i});
            }
            return dayOptions;
        }
        function pageChanged(){
            $scope.param.limitFrom = ($scope.pagination.currentPage - 1) * 10;
            transactionHandler();
        }



    }

    function bankCtrl($rootScope,$scope,accountService,$stateParams,util){
        $scope.inValidText = '';
        $scope.errMessage = '';
        $scope.accountInfo = $rootScope.accountInfo;
        $scope.initWithdrawInfo = initWithdrawInfo();
        $scope.getInValidText = getInValidText;
        $scope.creditCardDeposit = creditCardDeposit;
        $scope.withdrawHandler = withdrawHandler;
        $scope.amountVaild = false;
        $scope.amountVaildation = amountVaildation;
        $scope.token = undefined;
        $scope.statusDto = undefined;
        $scope.result = undefined;
        /*$scope.receipt = undefined;
        $scope.withdrawAmount = undefined;*/

        var userAccount = $scope.accountInfo;
        if(userAccount == undefined){
            return;
        }else{
            depositConfirm();
        }


        /*Deposit Confirm*/
        function depositConfirm() {
            if($stateParams.token){
                $scope.token = $stateParams.token;
            }
            if($scope.token && $stateParams.gateway == "EWay"){
                ewayDepositConfirmHandler($scope.token);
            }
            if($scope.token && $stateParams.gateway == "poli"){
                poliDepositConfirmHandler($scope.token);
            }
        }
        function ewayDepositConfirmHandler(token){
            accountService.ewayDepositConfirmation(token)
                .then(depositConfirmationRes,depositConfirmationRej);

            function depositConfirmationRes(res){
                if(!res.data.success){
                    return;
                }
                if(res.data.accountFundTransferDto){
                    $scope.result = res.data.accountFundTransferDto;
                }
                if(res.data.accountTransactionStatusDto){
                    $scope.statusDto = res.data.accountTransactionStatusDto;
                }
            }
            function depositConfirmationRej(res) {

            }
        }
        function poliDepositConfirmHandler(token){
            accountService.poliDepositConfirmation(token)
                .then(poliDepositConfirmationRes,poliDepositConfirmationRej);

            function poliDepositConfirmationRes(res){
                if(!res.data.success){
                    return;
                }
                if(res.data.poliPaymentDetailDto) {
                    $scope.result = res.data.poliPaymentDetailDto;
                }
                if(res.data.poliStatusDto) {
                    $scope.statusDto = res.data.poliStatusDto;
                }
            }
            function poliDepositConfirmationRej(res){

            }
        }

        /*Deposit*/
        function creditCardDepostHandler(amount,type){
            accountService.deposit(amount,type)
                .then(depositRes,depositRej);

            function depositRes(res){
                if(!res) {
                    return;
                }
                if(res.data.errorType){
                    $scope.errMessage = res.data.errorMessage;
                }else if(res.data.paymentOutputDto.errorMessage){
                    $scope.errMessage = res.data.paymentOutputDto.errorMessage;
                }else{
                    window.location.href = res.data.paymentOutputDto.navigateURL;
                }
            }
            function depositRej(res){

            }
        }
        function creditCardDeposit(amount,type){
            if(!$scope.amountVaild){
                return;
            }
            getAccountHandler(amount,type);
        }
        function getAccountHandler(amount,type){
            accountService.getAccount()
                .then(getAccountRes,getAccountRej);

            function getAccountRes(res){
                if(res.data.success){
                    $scope.getAccountSuccessful = res.data.success;
                    var fee = res.data.accountHolderDto.wallet.ccDepositBasisPointFee;
                    if(fee != 0){
                        //pop up eway transfer fee
                        creditCardDepostHandler(amount,type);
                    }else{
                        creditCardDepostHandler(amount,type);
                    }
                }else{
                    $scope.getAccountSuccessful = res.data.success;
                    $scope.depositError = res.data.errorMessage;
                }
            }
            function getAccountRej(res){

            }
        }
        function amountVaildation(amount){
            var valid = false;
            if (amount != "") {
                if(amount < 50){
                    valid = false;
                    return valid;
                }
                var regExp = new RegExp(/^\d{0,10}?$/);
                valid = regExp.test(amount);
            }
            return valid;
        }
        function getInValidText(amount){
            $scope.amountVaild = false;
            if(amount < 20){
                $scope.inValidText = 'Amount should be more than the minimum of $20';
            }else if(!amountVaildation(amount)){
                $scope.inValidText = 'Only full amount with no decimals is accepted: eg.100'
            }else{
                $scope.amountVaild = true;
            }
        }

        /*WITHDRAW MONEY*/
        function withdrawHandler(){
            accountService.withdraw($scope.initWithdrawInfo)
                .then(withdrawRes,withdrawRej);

            function withdrawRes(res){
                if(res.data.withdrawalOutputDto.transferId == 0){
                    $scope.initWithdrawInfo.err = res.data.withdrawalOutputDto.errors;
                }else{
                    $scope.initWithdrawInfo.err = 'SUCCESS';
                    window.location.href = '#/account/withdrawSuccess';
                    $rootScope.receipt = res.data.withdrawalOutputDto.transferId;
                    $rootScope.withdrawAmount = $scope.initWithdrawInfo.withdrawAmount;

                }
            }
            function withdrawRej(res){

            }
        }
        function initWithdrawInfo(){

            var withdrawInfo = {
                bankName: '',
                accountHolderName: '',
                bsb: '',
                accountNumber: '',
                IBANNumber: '',
                SWIFTCode: '',
                mobile: '',
                countryCode: '',
                areaCode: '',
                homePhone: '',
                availableFund: '',
                withdrawAmount: 0,
                password: '',
                err : ''
            }
            if($scope.accountInfo.accountHolderDto){
                withdrawInfo.availableFund = util.setAmount($scope.accountInfo.accountHolderDto.wallet.availableFunds).setSportAmountWithDollarSign();
            }
            if($scope.accountInfo.accountHolderDto){
                if($scope.accountInfo.accountHolderDto.bank){
                    withdrawInfo.bankName = $scope.accountInfo.accountHolderDto.bank.bank;
                    withdrawInfo.accountHolderName = $scope.accountInfo.accountHolderDto.bank.accountName;
                    withdrawInfo.bsb = $scope.accountInfo.accountHolderDto.bank.bsb;
                    withdrawInfo.accountNumber = $scope.accountInfo.accountHolderDto.bank.accountNumber;
                }
            }
            if($scope.accountInfo.accountHolderDto){
                withdrawInfo.mobile = $scope.accountInfo.accountHolderDto.details.mobileNumber;
                withdrawInfo.countryCode = $scope.accountInfo.accountHolderDto.details.countryCode;
                withdrawInfo.areaCode = $scope.accountInfo.accountHolderDto.details.areaCode;
                withdrawInfo.homePhone = $scope.accountInfo.accountHolderDto.details.phoneNumber;
            }

            return withdrawInfo;
        }


    }

    function profileCtrl($rootScope,$scope,accountService){
        $scope.accountInfo = $rootScope.accountInfo;
        $scope.editAccountInfo = $rootScope.accountInfo;
        $scope.userInfo = $rootScope.accountInfo;
        $scope.yearThreshold = new Date().getFullYear();
        $scope.initProfile = initProfile();
        $scope.jumpToTransactionPage = jumpToTransactionPage;
        $scope.resetEditForm = resetEditForm;
        $scope.submitEditProfileFormHandler = submitEditProfileFormHandler;
        $scope.setEditProfileSelectedcountryCode = setEditProfileSelectedcountryCode;
        $scope.setEditProfileSelectedCountry = setEditProfileSelectedCountry;
        $scope.setEditProlfileSelectedState = setEditProlfileSelectedState;
        $scope.initEditProfile = initEditProfile();
        $scope.user = user();
        $scope.getNewPassword = getNewPassword;
        $scope.isCorrectPasswordHandler = isCorrectPasswordHandler;
        $scope.stringMatch = stringMatch;
        isLogin();

        /* validation */
        function isLogin(){
            if(!$rootScope.accountInfo.isLogin){
                window.location.href = '#/';
            }
        }

        /*CHANGE PASSWORD*/
        function changePasswordHandler(){
            accountService.changePassword($scope.user.username,$scope.user.password,$scope.user.changePasswordDto)
                .then(changePasswordRes,changePasswordRej);

            function changePasswordRes(res){
                if(!res){
                    return;
                }
                window.history.back();
            }
            function changePasswordRej(res){

            }
        }
        function user(){
            return {
                username : '',
                password : '',
                changePasswordDto : {
                    username : '',
                    password : '',
                    existingPassword : ''
                }
            };
        }
        function getNewPassword(newPassword,oldPassword){
            $scope.user.username = $scope.userInfo.username;
            $scope.user.password = newPassword;
            $scope.user.changePasswordDto.password = newPassword;
            $scope.user.changePasswordDto.username = $scope.userInfo.username;
            $scope.user.changePasswordDto.existingPassword = oldPassword;
            changePasswordHandler();
        }
        function isCorrectPasswordHandler(password){
            accountService.isCorrectPassword(password)
                .then(isCorrectPasswordRes,isCorrectPasswordRej);

            function isCorrectPasswordRes(res){
                $scope.disablePwdStatus = res.data.success;

            }
            function isCorrectPasswordRej(res){

            }
        }
        function stringMatch(newPassword,confirmPassword){
            var notMatch = true;
            if(newPassword != undefined && confirmPassword != undefined && newPassword == confirmPassword){
                notMatch = false;
            }
            return notMatch;
        }
        function jumpToTransactionPage(){
            window.location.href = '#/account';
        }

        /*EDIT PROFILE*/
        function initProfile(){
            var profile = {
                person : {
                    firstname : '',
                    lastname : '',
                    dob : {
                        daySelected : {text:'Day',value:-1},
                        dayOptions : getDayOptions(),
                        monthSelected : {text:'Month',value:-1},
                        monthOptions : getMonthOptions(),
                        yearSelected : {text:'Year',value:-1},
                        yearOptions : getYearOptions(),
                        value: ''
                    },
                    email : '',
                    reTypeEmail : ''
                },
                phone : {
                    mobile : '',
                    countryCode : {
                        selected : {text:"Australia (+61)",countryCode:"AU",phoneCode:"61"},
                        options: getCountryPhoneCodeList()
                    },
                    areaCode: '',
                    landLine:''
                },
                address : {
                    addressCountry : {
                        selected : {text:"Australia"},
                        options : getCountryList()
                    },
                    unitNo: '',
                    streetNo: '',
                    street: '',
                    suburb: '',
                    postcode: '',
                    state:{
                        selected : {text:"New South Wales"},
                        options : getStateList()
                    }
                },
                account : {
                    accountNo : '',
                    username : '',
                    password : '',
                    reTypePassword : ''
                },
                err:''
            };
            return profile;
        }
        function initEditProfile(){
            var editProfileInfo = initProfile();
            if($scope.accountInfo.accountHolderDto) {
                editProfileInfo.person.firstname = $scope.accountInfo.accountHolderDto.details.firstName;
                editProfileInfo.person.lastname = $scope.accountInfo.accountHolderDto.details.lastName;
                editProfileInfo.person.dob = $scope.accountInfo.accountHolderDto.details.dob;
                editProfileInfo.person.email = $scope.accountInfo.accountHolderDto.details.emailAddress;
                editProfileInfo.person.reTypeEmail = $scope.accountInfo.accountHolderDto.details.emailAddress;
                editProfileInfo.phone.mobile = $scope.accountInfo.accountHolderDto.details.mobileNumber;
                editProfileInfo.phone.countryCode.selected.text = $scope.accountInfo.accountHolderDto.details.countryCode;
                editProfileInfo.phone.areaCode = $scope.accountInfo.accountHolderDto.details.areaCode;
                editProfileInfo.phone.landLine = $scope.accountInfo.accountHolderDto.details.phoneNumber;
                editProfileInfo.address.addressCountry.selected.text = $scope.accountInfo.accountHolderDto.details.addressCountry;
                editProfileInfo.address.unitNo = $scope.accountInfo.accountHolderDto.details.addressFlatNumber;
                editProfileInfo.address.streetNo = $scope.accountInfo.accountHolderDto.details.addressStreetNumber;
                editProfileInfo.address.street = $scope.accountInfo.accountHolderDto.details.addressStreet;
                editProfileInfo.address.suburb = $scope.accountInfo.accountHolderDto.details.addressSuburb;
                editProfileInfo.address.postcode = $scope.accountInfo.accountHolderDto.details.addressPostcode;
                editProfileInfo.address.state.selected.text = $scope.accountInfo.accountHolderDto.details.addressState;

            }else{
                editProfileInfo.person.dob = '';
            }

            return editProfileInfo;
        }
        function resetEditForm(){
            $scope.initEditProfile.person.firstname = $scope.accountInfo.accountHolderDto.details.firstName;
            $scope.initEditProfile.person.lastname = $scope.accountInfo.accountHolderDto.details.lastName;
            $scope.initEditProfile.person.dob = $scope.accountInfo.accountHolderDto.details.dob;
            $scope.initEditProfile.person.email = $scope.accountInfo.accountHolderDto.details.emailAddress;
            $scope.initEditProfile.person.reTypeEmail = $scope.accountInfo.accountHolderDto.details.emailAddress;
            $scope.initEditProfile.phone.mobile = $scope.accountInfo.accountHolderDto.details.mobileNumber;
            $scope.initEditProfile.phone.countryCode.selected.text = $scope.accountInfo.accountHolderDto.details.countryCode;
            $scope.initEditProfile.phone.areaCode = $scope.accountInfo.accountHolderDto.details.areaCode;
            $scope.initEditProfile.phone.landLine = $scope.accountInfo.accountHolderDto.details.phoneNumber;
            $scope.initEditProfile.address.addressCountry.selected.text = $scope.accountInfo.accountHolderDto.details.addressCountry;
            $scope.initEditProfile.address.unitNo = $scope.accountInfo.accountHolderDto.details.addressFlatNumber;
            $scope.initEditProfile.address.streetNo = $scope.accountInfo.accountHolderDto.details.addressStreetNumber;
            $scope.initEditProfile.address.street = $scope.accountInfo.accountHolderDto.details.addressStreet;
            $scope.initEditProfile.address.suburb = $scope.accountInfo.accountHolderDto.details.addressSuburb;
            $scope.initEditProfile.address.postcode = $scope.accountInfo.accountHolderDto.details.addressPostcode;
            $scope.initEditProfile.address.state.selected.text = $scope.accountInfo.accountHolderDto.details.addressState;
        }
        function submitEditProfileFormHandler(){
            if(stringMatch($scope.initEditProfile.person.email,$scope.initEditProfile.person.reTypeEmail)){
                $scope.initEditProfile.err = 'Email and confirmation are Not Match';
                return;
            }

            accountService.updateAccount($scope.initEditProfile)
                .then(updateAccountRes,updateAccountRej);

            function updateAccountRes(res){
                if(res.data.success){
                    $scope.accountInfo.accountHolderDto.details.firstName = $scope.initEditProfile.person.firstname;
                    $scope.accountInfo.accountHolderDto.details.lastName = $scope.initEditProfile.person.lastname;
                    $scope.accountInfo.accountHolderDto.details.dob = $scope.initEditProfile.person.dob;
                    $scope.accountInfo.accountHolderDto.details.emailAddress = $scope.initEditProfile.person.email;
                    $scope.accountInfo.accountHolderDto.details.mobileNumber = $scope.initEditProfile.phone.mobile;
                    $scope.accountInfo.accountHolderDto.details.countryCode = $scope.initEditProfile.phone.countryCode.selected.text;
                    $scope.accountInfo.accountHolderDto.details.areaCode = $scope.initEditProfile.phone.areaCode;
                    $scope.accountInfo.accountHolderDto.details.phoneNumber = $scope.initEditProfile.phone.landLine;
                    $scope.accountInfo.accountHolderDto.details.addressCountry = $scope.initEditProfile.address.addressCountry.selected.text;
                    $scope.accountInfo.accountHolderDto.details.addressFlatNumber = $scope.initEditProfile.address.unitNo;
                    $scope.accountInfo.accountHolderDto.details.addressStreetNumber = $scope.initEditProfile.address.streetNo;
                    $scope.accountInfo.accountHolderDto.details.addressStreet = $scope.initEditProfile.address.street;
                    $scope.accountInfo.accountHolderDto.details.addressSuburb = $scope.initEditProfile.address.suburb;
                    $scope.accountInfo.accountHolderDto.details.addressPostcode = $scope.initEditProfile.address.postcode;
                    $scope.accountInfo.accountHolderDto.details.addressState = $scope.initEditProfile.address.state.selected.text;

                    window.location.href = '#/account/viewProfile';
                }
            }
            function updateAccountRej(res){

            }
        }
        function setEditProfileSelectedcountryCode(option){
            $scope.initEditProfile.phone.countryCode.selected.text = option.text;
        }
        function setEditProfileSelectedCountry(option){
            $scope.initEditProfile.address.addressCountry.selected.text = option.text;
        }
        function setEditProlfileSelectedState(option){
            $scope.initEditProfile.address.state.selected.text = option.text;
        }

        /*COMMON*/
        function getCountryPhoneCodeList() {
            return [
                {text: "Algeria (+213)", countryCode: "DZ", phoneCode: "213"},
                {text: "Andorra (+376)", countryCode: "AD", phoneCode: "376"},
                {text: "Angola (+244)", countryCode: "AO", phoneCode: "244"},
                {text: "Anguilla (+1264)", countryCode: "AI", phoneCode: "1264"},
                {text: "Antigua & Barbuda (+1268)", countryCode: "AG", phoneCode: "1268"},
                {text: "Argentina (+54)", countryCode: "AR", phoneCode: "54"},
                {text: "Armenia (+374)", countryCode: "AM", phoneCode: "374"},
                {text: "Aruba (+297)", countryCode: "AW", phoneCode: "297"},
                {text: "Australia (+61)", countryCode: "AU", phoneCode: "61"},
                {text: "Austria (+43)", countryCode: "AT", phoneCode: "43"},
                {text: "Azerbaijan (+994)", countryCode: "AZ", phoneCode: "994"},
                {text: "Bahamas (+1242)", countryCode: "BS", phoneCode: "1242"},
                {text: "Bahrain (+973)", countryCode: "BH", phoneCode: "973"},
                {text: "Bangladesh (+880)", countryCode: "BD", phoneCode: "880"},
                {text: "Barbados (+1246)", countryCode: "BB", phoneCode: "1246"},
                {text: "Belarus (+375)", countryCode: "BY", phoneCode: "375"},
                {text: "Belgium (+32)", countryCode: "BE", phoneCode: "32"},
                {text: "Belize (+501)", countryCode: "BZ", phoneCode: "501"},
                {text: "Benin (+229)", countryCode: "BJ", phoneCode: "229"},
                {text: "Bermuda (+1441)", countryCode: "BM", phoneCode: "1441"},
                {text: "Bhutan (+975)", countryCode: "BT", phoneCode: "975"},
                {text: "Bolivia (+591)", countryCode: "BO", phoneCode: "591"},
                {text: "Bosnia Herzegovina (+387)", countryCode: "BA", phoneCode: "387"},
                {text: "Botswana (+267)", countryCode: "BW", phoneCode: "267"},
                {text: "Brazil (+55)", countryCode: "BR", phoneCode: "55"},
                {text: "Brunei (+673)", countryCode: "BN", phoneCode: "673"},
                {text: "Bulgaria (+359)", countryCode: "BG", phoneCode: "359"},
                {text: "Burkina Faso (+226)", countryCode: "BF", phoneCode: "226"},
                {text: "Burundi (+257)", countryCode: "BI", phoneCode: "257"},
                {text: "Cambodia (+855)", countryCode: "KH", phoneCode: "855"},
                {text: "Cameroon (+237)", countryCode: "CM", phoneCode: "237"},
                {text: "Canada (+1)", countryCode: "CA", phoneCode: "1"},
                {text: "Cape Verde Islands (+238)", countryCode: "CV", phoneCode: "238"},
                {text: "Cayman Islands (+1345)", countryCode: "KY", phoneCode: "1345"},
                {text: "Central African Republic (+236)", countryCode: "CF", phoneCode: "236"},
                {text: "Chile (+56)", countryCode: "CL", phoneCode: "56"},
                {text: "China (+86)", countryCode: "CN", phoneCode: "86"},
                {text: "Colombia (+57)", countryCode: "CO", phoneCode: "57"},
                {text: "Comoros (+269)", countryCode: "KM", phoneCode: "269"},
                {text: "Congo (+242)", countryCode: "CG", phoneCode: "242"},
                {text: "Cook Islands (+682)", countryCode: "CK", phoneCode: "682"},
                {text: "Costa Rica (+506)", countryCode: "CR", phoneCode: "506"},
                {text: "Croatia (+385)", countryCode: "HR", phoneCode: "385"},
                {text: "Cuba (+53)", countryCode: "CU", phoneCode: "53"},
                {text: "Cyprus North (+90392)", countryCode: "CY", phoneCode: "90392"},
                {text: "Cyprus South (+357)", countryCode: "CY", phoneCode: "357"},
                {text: "Czech Republic (+42)", countryCode: "CZ", phoneCode: "42"},
                {text: "Denmark (+45)", countryCode: "DK", phoneCode: "45"},
                {text: "Djibouti (+253)", countryCode: "DJ", phoneCode: "253"},
                {text: "Dominica (+1809)", countryCode: "DM", phoneCode: "1809"},
                {text: "Dominican Republic (+1809)", countryCode: "DO", phoneCode: "1809"},
                {text: "Ecuador (+593)", countryCode: "EC", phoneCode: "593"},
                {text: "Egypt (+20)", countryCode: "EG", phoneCode: "20"},
                {text: "El Salvador (+503)", countryCode: "SV", phoneCode: "503"},
                {text: "Equatorial Guinea (+240)", countryCode: "GQ", phoneCode: "240"},
                {text: "Eritrea (+291)", countryCode: "ER", phoneCode: "291"},
                {text: "Estonia (+372)", countryCode: "EE", phoneCode: "372"},
                {text: "Ethiopia (+251)", countryCode: "ET", phoneCode: "251"},
                {text: "Falkland Islands (+500)", countryCode: "FK", phoneCode: "500"},
                {text: "Faroe Islands (+298)", countryCode: "FO", phoneCode: "298"},
                {text: "Fiji (+679)", countryCode: "FJ", phoneCode: "679"},
                {text: "Finland (+358)", countryCode: "FI", phoneCode: "358"},
                {text: "France (+33)", countryCode: "FR", phoneCode: "33"},
                {text: "French Guiana (+594)", countryCode: "GF", phoneCode: "594"},
                {text: "French Polynesia (+689)", countryCode: "PF", phoneCode: "689"},
                {text: "Gabon (+241)", countryCode: "GA", phoneCode: "241"},
                {text: "Gambia (+220)", countryCode: "GM", phoneCode: "220"},
                {text: "Georgia (+7880)", countryCode: "GE", phoneCode: "7880"},
                {text: "Germany (+49)", countryCode: "DE", phoneCode: "49"},
                {text: "Ghana (+233)", countryCode: "GH", phoneCode: "233"},
                {text: "Gibraltar (+350)", countryCode: "GI", phoneCode: "350"},
                {text: "Greece (+30)", countryCode: "GR", phoneCode: "30"},
                {text: "Greenland (+299)", countryCode: "GL", phoneCode: "299"},
                {text: "Grenada (+1473)", countryCode: "GD", phoneCode: "1473"},
                {text: "Guadeloupe (+590)", countryCode: "GP", phoneCode: "590"},
                {text: "Guam (+671)", countryCode: "GU", phoneCode: "671"},
                {text: "Guatemala (+502)", countryCode: "GT", phoneCode: "502"},
                {text: "Guinea (+224)", countryCode: "GN", phoneCode: "224"},
                {text: "Guinea - Bissau (+245)", countryCode: "GW", phoneCode: "245"},
                {text: "Guyana (+592)", countryCode: "GY", phoneCode: "592"},
                {text: "Haiti (+509)", countryCode: "HT", phoneCode: "509"},
                {text: "Honduras (+504)", countryCode: "HN", phoneCode: "504"},
                {text: "Hong Kong (+852)", countryCode: "HK", phoneCode: "852"},
                {text: "Hungary (+36)", countryCode: "HU", phoneCode: "36"},
                {text: "Iceland (+354)", countryCode: "IS", phoneCode: "354"},
                {text: "India (+91)", countryCode: "IN", phoneCode: "91"},
                {text: "Indonesia (+62)", countryCode: "ID", phoneCode: "62"},
                {text: "Iran (+98)", countryCode: "IR", phoneCode: "98"},
                {text: "Iraq (+964)", countryCode: "IQ", phoneCode: "964"},
                {text: "Ireland (+353)", countryCode: "IE", phoneCode: "353"},
                {text: "Israel (+972)", countryCode: "IL", phoneCode: "972"},
                {text: "Italy (+39)", countryCode: "IT", phoneCode: "39"},
                {text: "Jamaica (+1876)", countryCode: "JM", phoneCode: "1876"},
                {text: "Japan (+81)", countryCode: "JP", phoneCode: "81"},
                {text: "Jordan (+962)", countryCode: "JO", phoneCode: "962"},
                {text: "Kazakhstan (+7)", countryCode: "KZ", phoneCode: "7"},
                {text: "Kenya (+254)", countryCode: "KE", phoneCode: "254"},
                {text: "Kiribati (+686)", countryCode: "KI", phoneCode: "686"},
                {text: "Korea North (+850)", countryCode: "KP", phoneCode: "850"},
                {text: "Korea South (+82)", countryCode: "KR", phoneCode: "82"},
                {text: "Kuwait (+965)", countryCode: "KW", phoneCode: "965"},
                {text: "Kyrgyzstan (+996)", countryCode: "KG", phoneCode: "996"},
                {text: "Laos (+856)", countryCode: "LA", phoneCode: "856"},
                {text: "Latvia (+371)", countryCode: "LV", phoneCode: "371"},
                {text: "Lebanon (+961)", countryCode: "LB", phoneCode: "961"},
                {text: "Lesotho (+266)", countryCode: "LS", phoneCode: "266"},
                {text: "Liberia (+231)", countryCode: "LR", phoneCode: "231"},
                {text: "Libya (+218)", countryCode: "LY", phoneCode: "218"},
                {text: "Liechtenstein (+417)", countryCode: "LI", phoneCode: "417"},
                {text: "Lithuania (+370)", countryCode: "LT", phoneCode: "370"},
                {text: "Luxembourg (+352)", countryCode: "LU", phoneCode: "352"},
                {text: "Macao (+853)", countryCode: "MO", phoneCode: "853"},
                {text: "Macedonia (+389)", countryCode: "MK", phoneCode: "389"},
                {text: "Madagascar (+261)", countryCode: "MG", phoneCode: "261"},
                {text: "Malawi (+265)", countryCode: "MW", phoneCode: "265"},
                {text: "Malaysia (+60)", countryCode: "MY", phoneCode: "60"},
                {text: "Maldives (+960)", countryCode: "MV", phoneCode: "960"},
                {text: "Mali (+223)", countryCode: "ML", phoneCode: "223"},
                {text: "Malta (+356)", countryCode: "MT", phoneCode: "356"},
                {text: "Marshall Islands (+692)", countryCode: "MH", phoneCode: "692"},
                {text: "Martinique (+596)", countryCode: "MQ", phoneCode: "596"},
                {text: "Mauritania (+222)", countryCode: "MR", phoneCode: "222"},
                {text: "Mayotte (+269)", countryCode: "YT", phoneCode: "269"},
                {text: "Mexico (+52)", countryCode: "MX", phoneCode: "52"},
                {text: "Micronesia (+691)", countryCode: "FM", phoneCode: "691"},
                {text: "Moldova (+373)", countryCode: "MD", phoneCode: "373"},
                {text: "Monaco (+377)", countryCode: "MC", phoneCode: "377"},
                {text: "Mongolia (+976)", countryCode: "MN", phoneCode: "976"},
                {text: "Montserrat (+1664)", countryCode: "MS", phoneCode: "1664"},
                {text: "Morocco (+212)", countryCode: "MA", phoneCode: "212"},
                {text: "Mozambique (+258)", countryCode: "MZ", phoneCode: "258"},
                {text: "Myanmar (+95)", countryCode: "MN", phoneCode: "95"},
                {text: "Namibia (+264)", countryCode: "NA", phoneCode: "264"},
                {text: "Nauru (+674)", countryCode: "NR", phoneCode: "674"},
                {text: "Nepal (+977)", countryCode: "NP", phoneCode: "977"},
                {text: "Netherlands (+31)", countryCode: "NL", phoneCode: "31"},
                {text: "New Caledonia (+687)", countryCode: "NC", phoneCode: "687"},
                {text: "New Zealand (+64)", countryCode: "NZ", phoneCode: "64"},
                {text: "Nicaragua (+505)", countryCode: "NI", phoneCode: "505"},
                {text: "Niger (+227)", countryCode: "NE", phoneCode: "227"},
                {text: "Nigeria (+234)", countryCode: "NG", phoneCode: "234"},
                {text: "Niue (+683)", countryCode: "NU", phoneCode: "683"},
                {text: "Norfolk Islands (+672)", countryCode: "NF", phoneCode: "672"},
                {text: "Northern Marianas (+670)", countryCode: "NP", phoneCode: "670"},
                {text: "Norway (+47)", countryCode: "NO", phoneCode: "47"},
                {text: "Oman (+968)", countryCode: "OM", phoneCode: "968"},
                {text: "Palau (+680)", countryCode: "PW", phoneCode: "680"},
                {text: "Panama (+507)", countryCode: "PA", phoneCode: "507"},
                {text: "Papua New Guinea (+675)", countryCode: "PG", phoneCode: "675"},
                {text: "Paraguay (+595)", countryCode: "PY", phoneCode: "595"},
                {text: "Peru (+51)", countryCode: "PE", phoneCode: "51"},
                {text: "Philippines (+63)", countryCode: "PH", phoneCode: "63"},
                {text: "Poland (+48)", countryCode: "PL", phoneCode: "48"},
                {text: "Portugal (+351)", countryCode: "PT", phoneCode: "351"},
                {text: "Puerto Rico (+1787)", countryCode: "PR", phoneCode: "1787"},
                {text: "Qatar (+974)", countryCode: "QA", phoneCode: "974"},
                {text: "Reunion (+262)", countryCode: "RE", phoneCode: "262"},
                {text: "Romania (+40)", countryCode: "RO", phoneCode: "40"},
                {text: "Russia (+7)", countryCode: "RU", phoneCode: "7"},
                {text: "Rwanda (+250)", countryCode: "RW", phoneCode: "250"},
                {text: "San Marino (+378)", countryCode: "SM", phoneCode: "378"},
                {text: "Sao Tome &amp; Principe (+239)", countryCode: "ST", phoneCode: "239"},
                {text: "Saudi Arabia (+966)", countryCode: "SA", phoneCode: "966"},
                {text: "Senegal (+221)", countryCode: "SN", phoneCode: "221"},
                {text: "Serbia (+381)", countryCode: "CS", phoneCode: "381"},
                {text: "Seychelles (+248)", countryCode: "SC", phoneCode: "248"},
                {text: "Sierra Leone (+232)", countryCode: "SL", phoneCode: "232"},
                {text: "Singapore (+65)", countryCode: "SG", phoneCode: "65"},
                {text: "Slovak Republic (+421)", countryCode: "SK", phoneCode: "421"},
                {text: "Slovenia (+386)", countryCode: "SI", phoneCode: "386"},
                {text: "Solomon Islands (+677)", countryCode: "SB", phoneCode: "677"},
                {text: "Somalia (+252)", countryCode: "SO", phoneCode: "252"},
                {text: "South Africa (+27)", countryCode: "ZA", phoneCode: "27"},
                {text: "Spain (+34)", countryCode: "ES", phoneCode: "34"},
                {text: "Sri Lanka (+94)", countryCode: "LK", phoneCode: "94"},
                {text: "St. Helena (+290)", countryCode: "SH", phoneCode: "290"},
                {text: "St. Kitts (+1869)", countryCode: "KN", phoneCode: "1869"},
                {text: "St. Lucia (+1758)", countryCode: "SC", phoneCode: "1758"},
                {text: "Sudan (+249)", countryCode: "SD", phoneCode: "249"},
                {text: "Suriname (+597)", countryCode: "SR", phoneCode: "597"},
                {text: "Swaziland (+268)", countryCode: "SZ", phoneCode: "268"},
                {text: "Sweden (+46)", countryCode: "SE", phoneCode: "46"},
                {text: "Switzerland (+41)", countryCode: "CH", phoneCode: "41"},
                {text: "Syria (+963)", countryCode: "SI", phoneCode: "963"},
                {text: "Taiwan (+886)", countryCode: "TW", phoneCode: "886"},
                {text: "Tajikstan (+7)", countryCode: "TJ", phoneCode: "7"},
                {text: "Thailand (+66)", countryCode: "TH", phoneCode: "66"},
                {text: "Togo (+228)", countryCode: "TG", phoneCode: "228"},
                {text: "Tonga (+676)", countryCode: "TO", phoneCode: "676"},
                {text: "Trinidad &amp; Tobago (+1863)", countryCode: "TT", phoneCode: "1868"},
                {text: "Tunisia (+216)", countryCode: "TN", phoneCode: "216"},
                {text: "Turkey (+90)", countryCode: "TR", phoneCode: "90"},
                {text: "Turkmenistan (+7)", countryCode: "TM", phoneCode: "7"},
                {text: "Turkmenistan (+993)", countryCode: "TM", phoneCode: "993"},
                {text: "Turks & Caicos Islands (+1649)", countryCode: "TC", phoneCode: "1649"},
                {text: "Tuvalu (+688)", countryCode: "TV", phoneCode: "688"},
                {text: "Uganda (+256)", countryCode: "UG", phoneCode: "256"},
                {text: "UK (+44)", countryCode: "GB", phoneCode: "44"},
                {text: "Ukraine (+380)", countryCode: "UA", phoneCode: "380"},
                {text: "United Arab Emirates (+971)", countryCode: "AE", phoneCode: "971"},
                {text: "Uruguay (+598)", countryCode: "UY", phoneCode: "598"},
                {text: "USA (+1)", countryCode: "US", phoneCode: "1"},
                {text: "Uzbekistan (+7)", countryCode: "UZ", phoneCode: "7"},
                {text: "Vanuatu (+678)", countryCode: "VU", phoneCode: "678"},
                {text: "Vatican City (+379)", countryCode: "VA", phoneCode: "379"},
                {text: "Venezuela (+58)", countryCode: "VE", phoneCode: "58"},
                {text: "Vietnam (+84)", countryCode: "VN", phoneCode: "84"},
                {text: "Virgin Islands - British (+84)", countryCode: "VG", phoneCode: "84"},
                {text: "Virgin Islands - US (+84)", countryCode: "VI", phoneCode: "84"},
                {text: "Wallis &amp; Futuna (+681)", countryCode: "WF", phoneCode: "681"},
                {text: "Yemen (North) (+969)", countryCode: "YE", phoneCode: "969"},
                {text: "Yemen (South) (+967)", countryCode: "YE", phoneCode: "967"},
                {text: "Zambia (+260)", countryCode: "ZM", phoneCode: "260"},
                {text: "Zimbabwe (+263)", countryCode: "ZW", phoneCode: "263"}
            ];
        }
        function getCountryList() {
            return [
                {text: "Afghanistan"},
                {text: "Akrotiri"},
                {text: "Albania"},
                {text: "Algeria"},
                {text: "American Samoa"},
                {text: "Andorra"},
                {text: "Angola"},
                {text: "Anguilla"},
                {text: "Antarctica"},
                {text: "Antigua and Barbuda"},
                {text: "Argentina"},
                {text: "Armenia"},
                {text: "Aruba"},
                {text: "Ashmore and Cartier Islands"},
                {text: "Australia"},
                {text: "Austria"},
                {text: "Azerbaijan"},
                {text: "The Bahamas"},
                {text: "Bahrain"},
                {text: "Bangladesh"},
                {text: "Barbados"},
                {text: "Bassas da India"},
                {text: "Belarus"},
                {text: "Belgium"},
                {text: "Belize"},
                {text: "Benin"},
                {text: "Bermuda"},
                {text: "Bhutan"},
                {text: "Bolivia"},
                {text: "Bosnia and Herzegovina"},
                {text: "Botswana"},
                {text: "Bouvet Island"},
                {text: "Brazil"},
                {text: "British Indian Ocean Territory"},
                {text: "British Virgin Islands"},
                {text: "Brunei"},
                {text: "Bulgaria"},
                {text: "Burkina Faso"},
                {text: "Burma"},
                {text: "Burundi"},
                {text: "Cambodia"},
                {text: "Cameroon"},
                {text: "Canada"},
                {text: "Cape Verde"},
                {text: "Cayman Islands"},
                {text: "Central African Republic"},
                {text: "Chad"},
                {text: "Chile"},
                {text: "Christmas Island"},
                {text: "Clipperton Island"},
                {text: "Cocos (Keeling) Islands"},
                {text: "Colombia"},
                {text: "Comoros"},
                {text: "Congo"},
                {text: "Congo"},
                {text: "Cook Islands"},
                {text: "Coral Sea Islands"},
                {text: "Costa Rica"},
                {text: "Cote d'Ivoire"},
                {text: "Croatia"},
                {text: "Cuba"},
                {text: "Cyprus"},
                {text: "Czech Republic"},
                {text: "Denmark"},
                {text: "Dhekelia"},
                {text: "Djibouti"},
                {text: "Dominica"},
                {text: "Dominican Republic"},
                {text: "Ecuador"},
                {text: "Egypt"},
                {text: "El Salvador"},
                {text: "Equatorial Guinea"},
                {text: "Eritrea"},
                {text: "Estonia"},
                {text: "Ethiopia"},
                {text: "Europa Island"},
                {text: "Falkland Islands (Islas Malvinas)"},
                {text: "Faroe Islands"},
                {text: "Fiji"},
                {text: "Finland"},
                {text: "France"},
                {text: "French Guiana"},
                {text: "French Polynesia"},
                {text: "French Southern and Antarctic Lands"},
                {text: "Gabon"},
                {text: "Gambia"},
                {text: "Gaza Strip"},
                {text: "Georgia"},
                {text: "Ghana"},
                {text: "Gibraltar"},
                {text: "Glorioso Islands"},
                {text: "Greece"},
                {text: "Greenland"},
                {text: "Grenada"},
                {text: "Guadeloupe"},
                {text: "Guam"},
                {text: "Guatemala"},
                {text: "Guernsey"},
                {text: "Guinea"},
                {text: "Guinea-Bissau"},
                {text: "Guyana"},
                {text: "Haiti"},
                {text: "Heard Island and McDonald Islands"},
                {text: "Holy See (Vatican City)"},
                {text: "Honduras"},
                {text: "Hong Kong"},
                {text: "Hungary"},
                {text: "Iceland"},
                {text: "India"},
                {text: "Indonesia"},
                {text: "Iran"},
                {text: "Iraq"},
                {text: "Ireland"},
                {text: "Isle of Man"},
                {text: "Israel"},
                {text: "Italy"},
                {text: "Jamaica"},
                {text: "Jan Mayen"},
                {text: "Japan"},
                {text: "Jersey"},
                {text: "Jordan"},
                {text: "Juan de Nova Island"},
                {text: "Kazakhstan"},
                {text: "Kenya"},
                {text: "Kiribati"},
                {text: "North Korea"},
                {text: "South Korea"},
                {text: "Kuwait"},
                {text: "Kyrgyzstan"},
                {text: "Laos"},
                {text: "Latvia"},
                {text: "Lebanon"},
                {text: "Lesotho"},
                {text: "Liberia"},
                {text: "Libya"},
                {text: "Liechtenstein"},
                {text: "Lithuania"},
                {text: "Luxembourg"},
                {text: "Macau"},
                {text: "Macedonia"},
                {text: "Madagascar"},
                {text: "Malawi"},
                {text: "Malaysia"},
                {text: "Maldives"},
                {text: "Mali"},
                {text: "Malta"},
                {text: "Marshall Islands"},
                {text: "Martinique"},
                {text: "Mauritania"},
                {text: "Mauritius"},
                {text: "Mayotte"},
                {text: "Mexico"},
                {text: "Micronesia"},
                {text: "Federated States of"},
                {text: "Moldova"},
                {text: "Monaco"},
                {text: "Mongolia"},
                {text: "Montserrat"},
                {text: "Morocco"},
                {text: "Mozambique"},
                {text: "Namibia"},
                {text: "Nauru"},
                {text: "Navassa Island"},
                {text: "Nepal"},
                {text: "Netherlands"},
                {text: "Netherlands Antilles"},
                {text: "New Caledonia"},
                {text: "New Zealand"},
                {text: "Nicaragua"},
                {text: "Niger"},
                {text: "Nigeria"},
                {text: "Niue"},
                {text: "Norfolk Island"},
                {text: "Northern Mariana Islands"},
                {text: "Norway"},
                {text: "Oman"},
                {text: "Pakistan"},
                {text: "Palau"},
                {text: "Panama"},
                {text: "Papua New Guinea"},
                {text: "Paracel Islands"},
                {text: "Paraguay"},
                {text: "Peru"},
                {text: "Philippines"},
                {text: "Pitcairn Islands"},
                {text: "Poland"},
                {text: "Portugal"},
                {text: "Puerto Rico"},
                {text: "Qatar"},
                {text: "Reunion"},
                {text: "Romania"},
                {text: "Rwanda"},
                {text: "Saint Helena"},
                {text: "Saint Kitts and Nevis"},
                {text: "Saint Lucia"},
                {text: "Saint Pierre and Miquelon"},
                {text: "Saint Vincent and the Grenadines"},
                {text: "Samoa"},
                {text: "San Marino"},
                {text: "Sao Tome and Principe"},
                {text: "Saudi Arabia"},
                {text: "Senegal"},
                {text: "Serbia and Montenegro"},
                {text: "Seychelles"},
                {text: "Sierra Leone"},
                {text: "Singapore"},
                {text: "Slovakia"},
                {text: "Slovenia"},
                {text: "Solomon Islands"},
                {text: "Somalia"},
                {text: "South Africa"},
                {text: "South Georgia and the South Sandwich Islands"},
                {text: "Spain"},
                {text: "Spratly Islands"},
                {text: "Sri Lanka"},
                {text: "Sudan"},
                {text: "Suriname"},
                {text: "Svalbard"},
                {text: "Swaziland"},
                {text: "Sweden"},
                {text: "Switzerland"},
                {text: "Syria"},
                {text: "Taiwan"},
                {text: "Tajikistan"},
                {text: "Tanzania"},
                {text: "Thailand"},
                {text: "Timor-Leste"},
                {text: "Togo"},
                {text: "Tokelau"},
                {text: "Tonga"},
                {text: "Trinidad and Tobago"},
                {text: "Tromelin Island"},
                {text: "Tunisia"},
                {text: "Turkey"},
                {text: "Turkmenistan"},
                {text: "Turks and Caicos Islands"},
                {text: "Tuvalu"},
                {text: "Uganda"},
                {text: "Ukraine"},
                {text: "United Arab Emirates"},
                {text: "United Kingdom"},
                {text: "Uruguay"},
                {text: "Uzbekistan"},
                {text: "Vanuatu"},
                {text: "Venezuela"},
                {text: "Vietnam"},
                {text: "Virgin Islands"},
                {text: "Wake Island"},
                {text: "Wallis and Futuna"},
                {text: "West Bank"},
                {text: "Western Sahara"},
                {text: "Yemen"},
                {text: "Zambia"},
                {text: "Zimbabwe"}
            ];
        }
        function getStateList(){
            return [
                {text: "New South Wales"},
                {text: "Victoria"},
                {text: "Queensland"},
                {text: "Western Australia"},
                {text: "South Australia"},
                {text: "Tasmania"},
                {text: "Australian Capital Territory"},
                {text: "Northern Territory"},
                {text: "Norfolk Island"},
                {text: "Christmas Island"},
                {text: "Australian Antarctic Territory"},
                {text: "Jervis Bay Territory"},
                {text: "Cocos (Keeling) Islands"},
                {text: "Coral Sea Islands"},
                {text: "Ashmore and Cartier Islands"},
                {text: "Heard Island and McDonald Islands"}
            ];

        }
        function getYearOptions() {
            var yearOptions = [];
            for(var i  = 1900; i < $scope.yearThreshold; i ++){
                yearOptions.push({text: i + "", value: i});
            }
            return yearOptions;
        }
        function getMonthOptions() {
            return [
                {text: "January", value: 0},
                {text: "February", value: 1},
                {text: "March", value: 2},
                {text: "April", value: 3},
                {text: "May", value: 4},
                {text: "June", value: 5},
                {text: "July", value: 6},
                {text: "August", value: 7},
                {text: "September", value: 8},
                {text: "October", value: 9},
                {text: "November", value: 10},
                {text: "December", value: 11}
            ];
        }
        function getDayOptions() {
            var dayOptions = [];
            for(var i  = 1; i < 32; i ++){
                dayOptions.push({text: i + "", value: i});
            }
            return dayOptions;
        }

        $rootScope.$watch('accountInfo',function(nv,ov){
            $scope.initEditProfile = initEditProfile();

            if(nv){
              if(!$rootScope.accountInfo.isLogin){
                  window.location.href = '#/';
              }
            }
        }, true);

    }

    function forgotPasswordCtrl($rootScope,$scope,accountService,$stateParams){
        $scope.goBackToHomePage = goBackToHomePage;
        $scope.emailVerify = emailVerify;
        $scope.getPassword = getPassword;
        $scope.PasswordDto = {
            username : '',
            email : ''
        };
        $scope.resetPassword = resetPassword;
        $scope.jumpToHomePage = jumpToHomePage;
        $scope.stringMatch = stringMatch;
        $scope.password = '';

        function goBackToHomePage(){
            window.location.href = '#/';
        }
        function emailVerify(email){
            var isMatch = false;
            if(!email){
                return isMatch;
            }
            var EMAIL_REGX = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i;
            if(email.match(EMAIL_REGX)){
                isMatch = true;
            }
            return isMatch;
        }
        function getPassword(){
            accountService.getPasswordByEmail($scope.passwordDto.username,$scope.passwordDto.email)
                .then(getPasswordRes,getPasswordRej);

            function getPasswordRes(res){
                if(res.data.success){
                    alert('Successful, please check your email box.');
                    window.location.href = '#/';
                }else{
                    alert('The Username and Email address do not match your record.');
                }
            }
            function getPasswordRej(rej){

            }
        }
        function resetPassword(){
            var username = $stateParams.d;
            var token = $stateParams.t;
            var pwd = $scope.password;

            accountService.resetPassword(username,pwd,token)
                .then(resetPasswordRes,resetPasswordRej);

            function resetPasswordRes(res){
                if(res.data.success){
                    alert('Successful, please login with new password.');
                    window.location.href = '#/'
                }else{
                    alert(res.data);
                }
            }
            function resetPasswordRej(res){

            }
        }
        function jumpToHomePage(){
            window.location.href = '#/';
        }
        function stringMatch(newPassword,confirmPassword){
            var notMatch = true;
            if(newPassword != undefined && confirmPassword != undefined && newPassword == confirmPassword){
                notMatch = false;
            }
            return notMatch;
        }


    }

})();
