(function(){
    'use strict';

    angular
        .module("pinnacleBetApp")
        .factory("util", util);

    util.$inject =['$filter','EVN'];

    function util($filter, EVN){
        var service = {
            sidebarView                 : sidebarView,
            scrollTop                   : scrollTop,
            log                         : log,
            sportPricePrio              : sportPricePrio,
            getTime                     : getTime,
            hashCode                    : hashCode,
            getSportPriceDSP            : getSportPriceDSP,
            setAmount                   : setAmount,
            setOdds                     : setOdds,
            isNumber                    : isNumber,
            getOdds                     : getOdds,
            flatten                     : flatten,
            floorToFixed                : floorToFixed,
            timeToStartText             : timeToStartText,
            minutesToRaceStart          : minutesToRaceStart,
            formatDateString            : formatDateString,
            formatDateStringForTxTable  : formatDateStringForTxTable,
            parseStartTime              : parseStartTime,
            isEmpty                     : isEmpty,
            oddsFromPrice               : oddsFromPrice,
            getRaceOddsObj              : getRaceOddsObj,
            removeDup                   : removeDup,
            isTopsport                  : isTopsport
        }

        return service;

        function sidebarView(type, rootScope){
            switch(type){
                case 'HOME':
                    rootScope.sidebarStatus.status = 'HOME';
                    rootScope.sidebarStatus.liveBetting = true;
                    rootScope.sidebarStatus.quickLink = true;
                    rootScope.sidebarStatus.sportBar = true;
                    rootScope.sidebarStatus.multisBar = false;
                    rootScope.sidebarStatus.raceBar = true;
                    rootScope.sidebarStatus.myAccount = false;
                    break;
                case 'SPORT':
                    rootScope.sidebarStatus.status = 'SPORT';
                    rootScope.sidebarStatus.liveBetting = false;
                    rootScope.sidebarStatus.quickLink = true;
                    rootScope.sidebarStatus.sportBar = true;
                    rootScope.sidebarStatus.multisBar = false;
                    rootScope.sidebarStatus.raceBar = false;
                    rootScope.sidebarStatus.myAccount = false;
                    break;
                case 'RACING':
                    rootScope.sidebarStatus.status = 'RACING';
                    rootScope.sidebarStatus.liveBetting = false;
                    rootScope.sidebarStatus.quickLink = true;
                    rootScope.sidebarStatus.sportBar = false;
                    rootScope.sidebarStatus.multisBar = false;
                    rootScope.sidebarStatus.raceBar = true;
                    rootScope.sidebarStatus.myAccount = false;
                    break;
                case 'MULTI':
                    rootScope.sidebarStatus.status = 'MULTI';
                    rootScope.sidebarStatus.liveBetting = false;
                    rootScope.sidebarStatus.quickLink = false;
                    rootScope.sidebarStatus.sportBar = false;
                    rootScope.sidebarStatus.multisBar = true;
                    rootScope.sidebarStatus.raceBar = false;
                    rootScope.sidebarStatus.myAccount = false;
                    break;
                case 'ACCOUNT':
                    rootScope.sidebarStatus.status = 'ACCOUNT';
                    rootScope.sidebarStatus.liveBetting = false;
                    rootScope.sidebarStatus.quickLink = true;
                    rootScope.sidebarStatus.sportBar = false;
                    rootScope.sidebarStatus.multisBar = false;
                    rootScope.sidebarStatus.raceBar = true;
                    rootScope.sidebarStatus.myAccount = true;
                    break;
            }
        }

        function scrollTop(){
            $(window).scrollTop(0);
        }

        function log(msg){
            if(EVN.logConf === 'dev'){
                console.log(msg);
            }
        }

        function sportPricePrio(priceObj){
            if(priceObj){
                if (priceObj.proposition.key.toLowerCase() === 'draw'){
                    return 1;
                }else{
                    return 0;
                }

            }
        }

        /*
        *  @name       getTime
        *  @return     MMSS, YYMMDD, .....
        */
        function getTime(sec){
            var DateObj = new Date(sec);
            var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            var week = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
            var timeServ = {
                yymmdd : getYYMMDD(),
                mmdd : getMMDD(),
                ddmmyy : getDDYYMM(),
                hhmmss : gethhmmss(),
                mmss : getMMSS(),
                msec : getMSec,
                getPopularTime : getPopularTime()
            }

            return timeServ;

            function getMMSS(){
                var minutes = DateObj.getMinutes() < 10 ? '0'+DateObj.getMinutes() : DateObj.getMinutes();
                var hours = DateObj.getHours() < 10 ? '0'+DateObj.getHours() : +DateObj.getHours();
                return hours + ':' + minutes;
            }
            function gethhmmss(){
                var seconds = DateObj.getSeconds() < 10 ? '0'+DateObj.getSeconds() : DateObj.getSeconds();
                var minutes = DateObj.getMinutes() < 10 ? '0'+DateObj.getMinutes() : DateObj.getMinutes();
                var hours = DateObj.getHours() < 10 ? '0'+DateObj.getHours() : +DateObj.getHours();
                return hours + ':' + minutes + ':' + seconds;
            }
            function getMMDD(){
                return month[DateObj.getMonth()] + ' '+DateObj.getDate();
            }

            function getYYMMDD(){
                return DateObj.getDate()+'/'+ month[DateObj.getMonth()] +'/'+DateObj.getFullYear();
            }
            function getDDYYMM(){
                return DateObj.getDate()+'-'+ month[DateObj.getMonth()] +'-'+DateObj.getFullYear();
            }
            function getMSec() {
                return (new Date()).getTime();
            }
            function getPopularTime(mm){
                var hours = DateObj.getHours();
                var minutes = DateObj.getMinutes();
                var ampm = hours >= 12 ? 'PM' : 'AM';
                var weekDay = week[DateObj.getDay()];
                var timeZone = ' UTC'+( DateObj.getTimezoneOffset() <=0 ? ' + ': ' - ') + DateObj.getTimezoneOffset()/ -60;
                var date = weekDay +'  '+ DateObj.getDate() + '  ' + month[(DateObj.getMonth())] + timeZone;
                hours = hours % 12;
                hours = hours ? hours : 12; // the hour '0' should be '12'
                minutes = minutes < 10 ? '0'+minutes : minutes;
                var time = date + '  |' + hours + ':' + minutes + ' ' + ampm + '|';

                return time;
            }
        }

        /*
        *  @name       getSportPriceDSP
        *  @return     1.234
        */
        function getSportPriceDSP(price){
            if(!isNaN(price) && angular.isNumber(price)){
                return (price/1000).toFixed(3);
            }

        }

        /*
        *  @name       hashCode
        *  @return     integer
        */
        function hashCode(str){
            var hash = 5381,
              i    = str.length

            while(i)
            hash = (hash * 33) ^ str.charCodeAt(--i)

            /* JavaScript does bitwise operations (like XOR, above) on 32-bit signed
            * integers. Since we want the results to be always positive, convert the
            * signed int to an unsigned by doing an unsigned bitshift. */
            return hash >>> 0;
        }

        /*
        *  @name       inputValid
        *  @return     Boolean
        */
        function isNumber(input){
            return typeof input === 'number' && !isNaN(input)
                ? true
                : false;
        }


        /*
         *  @name       setAmount
         *  @return     add amount value
         */
        function setAmount(amount) {
            var service = {
                setRaceAmountWithDollarSign : setRaceAmountWithDollarSign,
                setSportAmountWithDollarSign : setSportAmountWithDollarSign,
                amount : getAmount(),
                getFixed : getFixed(),
                toCents : toCents(),
                toDollars : toDollars(),

            };
            return service;

            function setSportAmountWithDollarSign(){
                return '$' + convertToTwoDecimal(amount);
            }

            function setRaceAmountWithDollarSign(){
                return '$' + convertToTwoDecimal(amount);
            }

            function getFixed(){
                return angular.isNumber(amount) && !isNaN(amount)
                    ? parseFloat(amount.toFixed(2))
                    : 0;
            }

            function getAmount(){
                return angular.isNumber(amount) && !isNaN(amount)
                    ? amount
                    : 0;
            }

            function toCents(){
                return angular.isNumber(amount) && !isNaN(amount)
                    ? Math.round(amount * EVN.MONEY_PRECISION)
                    : 0;
            }
            function toDollars(){
                return angular.isNumber(amount) && !isNaN(amount)
                    ? parseFloat(parseFloat(amount / EVN.MONEY_PRECISION).toFixed(EVN.FIX_DECIMAL))
                    : 0;
            }

            function convertToTwoDecimal(amount) {
                return angular.isNumber(amount) && !isNaN(amount)
                    ? (amount / EVN.MONEY_PRECISION).toFixed(EVN.FIX_DECIMAL)
                    : 0;
            }

        }

        /*
         *  @name       setOdds
         *  @return     odds value
         */
        function setOdds(odds,type){

            if(type == 'SPORTS' || type == 'MULTIS'){
                if( !angular.isNumber(odds) ){
                    return 'TBD';
                }else if(parseFloat(odds) <= EVN.SPORT_ODDS_PRECISION){
                    return 'TBD';
                }else{
                    return setSportsOdds(odds);
                }
            }

            if(type == 'RACE'){
                if( !angular.isNumber(odds) ){
                    return 'TBD';
                }else if(parseFloat(odds) <= EVN.RACE_ODDS_PRECISION){
                    return 'TBD';
                }else{
                    return setRaceOdds(odds);
                }
            }

            function setSportsOdds(odds) {
                var oddValue = parseFloat(odds);
                var accOdds = fixDecimalPlaces((oddValue/EVN.SPORT_ODDS_PRECISION),3);
                return '$' + accOdds;

            }

            function setRaceOdds(odds) {
                var oddValue = parseFloat(odds);
                var accOdds = fixDecimalPlaces((oddValue/EVN.RACE_ODDS_PRECISION),2);
                return '$' + accOdds;
            }

            function fixDecimalPlaces(number, decimalPlaces) {
                number = parseFloat(number);
                if (!decimalPlaces)
                    decimalPlaces = 2;

                var d = Math.pow(10, decimalPlaces);
                var roundedValue = Math.floor(number * d + 0.0000001) / d;
                var resultFixed = roundedValue.toFixed(decimalPlaces);
                return resultFixed;
            }

        }

        function getOdds(odds,type){
            var service = {
                toInteger   : toInteger(),
                toDecimal   : toDecimal(),
                toDecimalDSP: toDecimalDSP()
            }
            return service;

            function toInteger(){
                switch(type){
                    case 'RACING':
                        return toIntegerhelper(odds, EVN.RACE_ODDS_PRECISION);
                        break;
                    case 'SPORT' :
                        return toIntegerhelper(odds, EVN.SPORT_ODDS_PRECISION);
                        break;
                    default:
                        return 0;
                }
            }
            function toDecimal(){
                switch(type){
                    case 'RACING':
                        return toDecimalhelper(odds, EVN.RACE_ODDS_PRECISION, 2);
                        break;
                    case 'SPORT' :
                        return toDecimalhelper(odds, EVN.SPORT_ODDS_PRECISION, 3);
                        break;
                    default:
                        return 0;
                }
            }
            function toDecimalDSP(){
                switch(type){
                    case 'RACING':
                        return toDecimalDSPhelper(odds, EVN.RACE_ODDS_PRECISION, 2);
                        break;
                    case 'SPORT' :
                        return toDecimalDSPhelper(odds, EVN.SPORT_ODDS_PRECISION, 3);
                        break;
                    default:
                        return 0;
                }
            }
            function toIntegerhelper(odds, num){
                return Math.round(odds * num);
            }
            function toDecimalhelper(odds, num, fracNum){
                return parseFloat((odds/num).toFixed(fracNum));
            }
            function toDecimalDSPhelper(odds, num, fracNum){
                return (odds/num).toFixed(fracNum);
            }
        }

        function flatten(arr){
            return arr.reduce(function (flat, toFlatten) {
                return flat.concat(Array.isArray(toFlatten) ? flatten(toFlatten) : toFlatten);
            }, []);
        }

        function floorToFixed(number , decimalPlaces){
            number = parseFloat(number);
            !decimalPlaces ? decimalPlaces = 2 : '';
            var d = Math.pow(10, decimalPlaces);
            var roundedValue = Math.floor(number * d + 0.0000001) / d;
            var resultFixed = roundedValue.toFixed(decimalPlaces);
            return parseFloat(resultFixed);
        }

        function getRaceOddsObj(entrant, pool){
            var oddsObj = pool.entrantOdds.filter(function(obj){
                return obj.firstEntrant.id === entrant.id;
            });

            if(oddsObj instanceof Array && oddsObj.length > 0){
                return oddsObj[0];
            }else{
                return {};
            }
        }

        function timeToStartText (eventStartTimeStamp) {
        	//14Hr 6Min
        	var currentTime = this.getTime().msec();
        	var  NOW = "< 1";
        	var suffixMin = "Min";
        	var suffixHour = "H";
        	// var suffixHr = "Hr";
        	var ONE_HOUR = 60*60*EVN.ONEMILLISECOND;
        	var ONE_MINUTE = 60 * EVN.ONEMILLISECOND;
        	var inMinute = 0;
        	if( eventStartTimeStamp < currentTime + EVN.ONE_HOUR) {
                inMinute = Math.floor((eventStartTimeStamp - currentTime) / EVN.ONE_MINUTE);
                if (inMinute == 0) {
                    return NOW + suffixMin;
                } else {
                    return inMinute + suffixMin;
                }
        	} else {
                var tmpDate = new Date(eventStartTimeStamp);
                var hours = tmpDate.getHours();
                var minutes = tmpDate.getMinutes();
                var ampm = hours >= 12 ? 'pm' : 'am';
//                hours = hours % 12;
                hours = hours ? hours : 12; // the hour '0' should be '12'
                minutes = minutes < 10 ? '0'+minutes : minutes;
                return hours + ':' + minutes ;
        	}
        }

        /**
         * Get string value of the rmaining time left to race start.
         */
        function minutesToRaceStart (eventStartTimeStamp) {
        	var ONE_MINUTE = 60 * EVN.ONEMILLISECOND;

        	var currentTime = this.getTime().msec();
        	var remainingMin = (eventStartTimeStamp - currentTime) / 1000;


        	return parseInt(remainingMin / ONE_MINUTE);
        }

        function formatDateTime (format, date) {
            return  $filter('date')(new Date(date), format);
        }

       function formatDateString(fullDate) {
           if (fullDate) {
               var format = EVN.dateTimeDisplayformat;
               var rv = formatDateTime(format, new Date(fullDate));
               return rv;
           } else {
               return "";
           }
       }

       function formatDateStringForTxTable (fullDate) {
           if (fullDate) {
               var format = EVN.dateTimeDisplayformatTxTable;
               var rv = formatDateTime(format, new Date(fullDate));
               return rv;
           } else {
               return "";
           }
       }

       function removeDup(arr){
           var seen = {};

           return arr.filter(function(elem){
               if(!seen.hasOwnProperty(elem)){
                   seen[elem] = true;
                   return true;
               }else{
                   return false;
               }
           });
       }

       /* racing */

       function isTopsport(pool){
           return pool.bookmaker.id == "TopSport" && (pool.poolTypeId == "P" || pool.poolTypeId == "W");
       }

       this.parseStartTime = function(mm) {
           var date = new Date(mm);
           var ret = "";
           if (date.getHours() > 12) {
               ret += date.getHours() - 12;
           }
           else {
               ret += date.getHours();
           }
           if (ret.length == 1) {
               ret = "0" + ret;
           };
           ret += ":" + (date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes());
           if (date.getHours() > 11) {
               ret += " PM";
           }
           else {
               ret += " AM";
           }
           return ret;
       };

       this.raceMap = {
               'Duet' : 'exotic',
               'Exacta' : 'exotic',
               'Quinella' : 'exotic',
               'Trifecta' : 'exotic',
               'First Four' : 'exotic',
               'Quadrella' : 'multileg',
               'Big 6' : 'multileg',
               'Treble' : 'multileg',
               'Running Double' : 'multileg'
           }

       this.topSportPriority = {
           win : 1,
           handicap : 2,
           over_under : 3,
           draw : 4
       }

       this.digitNumberValid = function(amount) {
           if (!amount) {return};
           amount = amount.toString();
           return sportsInputValid(amount);
       }

       this.sportsInputValid = function(amount) {
           if (amount.indexOf('.') != amount.lastIndexOf('.')) {
               return false;
           };

           amount = parseFloat(amount);

           if (isNaN(amount)) {
               return false;
           };

           if (amount <= 0) {
               return false;
           };

           return true;
       }

       this.racingInputValid = function(amount) {
           if (amount.indexOf('.') != amount.lastIndexOf('.')) {
               return false;
           };

           amount = parseFloat(amount);

           if (isNaN(amount)) {
               return false;
           };
           if (amount < 1) {
               return false;
           };
           if (amount % 1 != 0) {
               return false;
           };

           return true;
       }

       this.isNumber = function(amount){
           if(!amount){
               return false;
           }
           if(isNaN(amount) ){
              return false;
           }

           return true;
       }

       function oddsFromPrice(price, oddsPrecision, trailings) {
           if (!trailings) {
               trailings = 3;
           };
           price = price / oddsPrecision;
           var ret = price.toFixed(Math.max(trailings,
                   (price.toString().split('.')[1] || []).length));
           return ret;
       }

       this.convertToOriginPrice = function(price, oddsPrecision){
           if(!isNaN(price)){
               var number = Math.round(parseFloat(price) * oddsPrecision);
               return number;
           }else{
               return 0;
           }

       }

       this.toFixed2Floor = function(num, k, n){
           //num : base number
           //k: rounding digital
           //n: tofixed
           var base = Math.pow(10, n);
           var roundingNum = num.toFixed(k);

           return (Math.floor(base * roundingNum)/base).toFixed(n);
       }

       this.toFixedOdds = function(num){
           /*
           *  odds : 4321 -> 4320,   odds : 4325 -> 4330
           */
           return parseFloat( Math.round(num / 10)  * 10 )
       }

       this.getAmount = function(amount){
           return parseFloat( Math.floor(parseFloat(amount) * MONEY_PRECISION) );
       }


       this.getCurrentTime = function() {
           var clientTime = new Date().getTime();
           return clientTime - SERVER_TIME_DIFF;
       }

       function parseStartTime (mm) {
           var date = new Date(mm);
           var ret = "";
           if (date.getHours() > 12) {
               ret += date.getHours() - 12;
           }
           else {
               ret += date.getHours();
           }
           if (ret.length == 1) {
               ret = "0" + ret;
           };
           ret += ":" + (date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes());
           /*if (date.getHours() > 11) {
               ret += " PM";
           }
           else {
               ret += " AM";
           }*/
           return ret;
       };

       this.calculateTimeToStartText = function(eventStartTimeStamp) {
           //14Hr 6Min
           var currentTime = getCurrentTime();
           var  NOW = "< 1";
           var suffixMin = "Min";
           var suffixHour = "H";
           // var suffixHr = "Hr";
           var ONE_HOUR = 60*60*ONEMILLISECOND;
           var ONE_MINUTE = 60 * ONEMILLISECOND;
           var inMinute = 0;
           if( eventStartTimeStamp < currentTime + ONE_HOUR) {
               inMinute = Math.floor((eventStartTimeStamp - currentTime) / ONE_MINUTE);
               if (inMinute == 0) {
                   return NOW + suffixMin;
               } else {
                   return inMinute + suffixMin;
               }
           } else {
               var tmpDate = new Date(eventStartTimeStamp);
               var hours = tmpDate.getHours();
               var minutes = tmpDate.getMinutes();
               var ampm = hours >= 12 ? 'pm' : 'am';
//        	        hours = hours % 12;
               hours = hours ? hours : 12; // the hour '0' should be '12'
               minutes = minutes < 10 ? '0'+minutes : minutes;
               return hours + ':' + minutes ;
           }
       }

       this.popularSportTime = function(mm){
           var d = new Date(mm);
           var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
           var hours = d.getHours();
           var minutes = d.getMinutes();
           var ampm = hours >= 12 ? 'pm' : 'am';
           hours = hours % 12;
           hours = hours ? hours : 12; // the hour '0' should be '12'
           minutes = minutes < 10 ? '0'+minutes : minutes;
           var time = hours + ':' + minutes + ' ' + ampm;
           var date = d.getDate() + '/' + (d.getMonth()+1) + '/' + d.getFullYear();
           return date + "<br> " + time;
       }

       this.popularTime = function(mm){
           var d = new Date(mm);
           var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
           var hours = d.getHours();
           var minutes = d.getMinutes();
           var ampm = hours >= 12 ? 'pm' : 'am';
           var date = d.getDate() + '/' + month[(d.getMonth())] + '/' + d.getFullYear();
           hours = hours % 12;
           hours = hours ? hours : 12; // the hour '0' should be '12'
           minutes = minutes < 10 ? '0'+minutes : minutes;
           var time = date + '  ' + hours + ':' + minutes + ' ' + ampm;

           return time;
       }

       // Convert the milliseconds to time format for a race. eg.5Min, <1Min
       this.displayTimeLeft = function(time) {
           var startTime = calculateTimeToStartText(time);
           if(startTime.match(/Min/g))
               startTime = startTime.replace('Min', '') + ' m';
           return startTime;
       }

       this.getStatusClass = function(time) {
           var startTime = calculateTimeToStartText(time);
           var statusClass = 'Open';
           if(startTime.match(/Min/g)) {
               var startValue = startTime.replace('Min', '');
               if(startValue <= 5 || startValue.match(/</g)) {
                   statusClass = "blink_1s";
               }
               else if(startValue > 5 && startValue <= 30) {
                   statusClass = "lessThan30";
               }
           }
           return statusClass;
       }

       /**
        * used for money
        * @param number
        * @param decimalPlaces
        * @returns {Number}
        */
       this.fixDecimalPlaces = function(number, decimalPlaces) {
           number = parseFloat(number);
           if (!decimalPlaces)
               decimalPlaces = 2;
           var d = Math.pow(10, decimalPlaces);
           var roundedValue = Math.floor(number * d + 0.0000001) / d;
           var resultFixed = roundedValue.toFixed(decimalPlaces);
           return resultFixed ;
       }

       this.convertToTwoDecimal = function(amount) {
           return this.fixDecimalPlaces((amount / EVN.MONEY_PRECISION), FIX_DECIMAL);
       }

       this.setAmount = function(amount) {
           return '$' + convertToTwoDecimal(amount);
       }

       this.setRaceOdds = function(odds){
           var oddValue = parseFloat(odds);
           var accOdds = this.fixDecimalPlaces((oddValue/EVN.RACE_ODDS_PRECISION),2);
           return '$' + accOdds;
       }

       this.setSportsOdds = function(odds){
           var oddValue = parseFloat(odds);
           var accOdds = this.fixDecimalPlaces((oddValue/EVN.ODDS_PRECISION),3);
           return '$' + accOdds;
       }

//    	this.setOdds = function(odds, type){
//
//    	    if(type == 'sport' || type == 'multi'){
//    	        if( !isNumber(odds) ){
//    	            return 'TBD';
//    	        }else if(parseFloat(odds) <= ODDS_PRECISION){
//    	            return 'TBD';
//    	        }else{
//    	            return setSportsOdds(odds);
//    	        }
//    	    }
//
//    	    if(type == 'race'){
//    	        if( !isNumber(odds) ){
//    	            return 'TBD';
//    	        }else if(parseFloat(odds) <= RACE_ODDS_PRECISION){
//    	            return 'TBD';
//    	        }else{
//    	            return setRaceOdds(odds);
//    	        }
//    	    }
//    	}

       this.whichType = function(type){
           if(type.toLowerCase() == 'harness' ||
              type.toLowerCase() == 'thoroughbred' ||
              type.toLowerCase() == 'greyhound' ||
              type.toLowerCase() == 'g' ||
              type.toLowerCase() == 't' ||
              type.toLowerCase() == 'r' ){
               return 'race';
           }else{
               return 'sport';
           }
       }

       this.betReceiptContent = function(sportType, parentEventName, wrapped, bookmakerPrice, isConfirmed) {
           var content = {};
           var wager = wrapped.wager;

           content.mainEvent = parentEventName;
           content.sportType = sportType;
           var eventString = wager.name;
           if(eventString.indexOf('@#@#') > -1) {
               eventString = eventString.substring(0, eventString.indexOf('@#@#'));
           }
           var teams = eventString.split(' vs ');
           content.team1 = teams[0].trim();
           content.team2 = teams[1].trim();

           var periodString = bookmakerPrice.periodName;
           if(periodString.indexOf(' ') > -1) {
               //periodStringParts = periodString.split(' ');
               periodString = periodStringParts[0][0] + ' ' + periodStringParts[1][0] + ' ';
               if(periodString == '2 H ') {
                   periodString = '2nd Half ';
               }
           }
           else {
               periodString = '';
           }
           // current event, chosen team, and other info, depending on the market
           switch(wager.marketType) {
               case "WIN" : {
                   receiptContent4Win(bookmakerPrice, wrapped, content, periodString, isConfirmed);
                   break;
               }
               case "HANDICAP" : {
                   receiptContent4Handicap(bookmakerPrice, wrapped, content, periodString, isConfirmed);
                   break;
               }
               case "OVER_UNDER" : {
                   receiptContent4OverUnder(bookmakerPrice, wrapped, content, periodString, isConfirmed);
                   break;
               }
               case "TEAM_TOTALS" : {
                   receiptContent4TeamTotal(bookmakerPrice, wrapped, content, periodString, isConfirmed);
                   break;
               }
               default : break;
           }

           if (isConfirmed) {
               var winAmount = (parseFloat(wager.odds) * parseFloat(wager.enteredvalue));
               if(wrapped.splittedBets && wrapped.splittedBets == true){
                   content.betAmount = "Betting $" + fixDecimalPlaces(wager.enteredvalue, FIX_DECIMAL)
                       + "@" + oddsFromPrice(wager.odds, ODDS_PRECISION)
                       + " returns a maximum of $" + fixDecimalPlaces((winAmount / ODDS_PRECISION), FIX_DECIMAL);
               }else{
                   content.betAmount = "Betting $" + fixDecimalPlaces(wager.enteredvalue, FIX_DECIMAL)
                       + "@" + oddsFromPrice(wager.odds, ODDS_PRECISION)
                       + " returns $" + fixDecimalPlaces((winAmount / ODDS_PRECISION), FIX_DECIMAL);
               }
           }
           else {
               content.betAmount = "";
           }
           return content;
       }

       this.receiptContent4Win = function(bookmakerPrice, wrapped, content, period, isConfirmed) {
           var wager = wrapped.wager;
           // event, chosen team and market
           content.event = wager.name;
           content.teamMarket = getChosenTeamName(wager.propositionName);
           content.periodBetType = period + 'Money Line';
           content.priceDisplay = '@' + oddsFromPrice(wager.odds, ODDS_PRECISION);
           if(isConfirmed && wager.enteredvalue > 0)
               content.priceDisplay = '$' + wager.enteredvalue + content.priceDisplay;
       }

       this.receiptContent4Handicap = function(bookmakerPrice, wrapped, content, period, isConfirmed) {
           var wager = wrapped.wager;
           var chosenTeam = getChosenTeamName(wager.propositionName);
           var odds = oddsFromPrice(wager.odds, ODDS_PRECISION);

           // event, chosen team and market
           content.event = wager.name;
           content.teamMarket = chosenTeam;
           content.periodBetType = period + 'Handicap: ' + bookmakerPrice.propositionValue;
           content.priceDisplay = '@' + oddsFromPrice(wager.odds, ODDS_PRECISION);
           if(isConfirmed && wager.enteredvalue > 0)
               content.priceDisplay = '$' + wager.enteredvalue + content.priceDisplay;
           content.others = otherInfoHandicapBySport(content.sportType, bookmakerPrice.displayName, wrapped, isConfirmed);
       }

       this.receiptContent4OverUnder = function(bookmakerPrice, wrapped, content, period, isConfirmed) {
           var wager = wrapped.wager;
           // event, chosen team and market
           content.event = wager.name;
           content.teamMarket = "";
           content.periodBetType = period + 'Total: ' + bookmakerPrice.propositionValue;
           var overUnderString = "Over";
           if(bookmakerPrice.propositionName.indexOf('Under') > -1) {
               overUnderString = "Under";
           }
           content.overUnder = overUnderString + ' ';
           content.priceDisplay = '@' + oddsFromPrice(wager.odds, ODDS_PRECISION);
           if(isConfirmed && wager.enteredvalue > 0)
               content.priceDisplay = '$' + wager.enteredvalue + content.priceDisplay;
           content.others = otherInfoOverUnderBySport(content.sportType, content.overUnder + bookmakerPrice.displayName, wrapped, isConfirmed);
       }

       this.receiptContent4TeamTotal = function(bookmakerPrice, wrapped, content, period, isConfirmed) {
           var wager = wrapped.wager;
           // event, chosen team and market
           var eventProposition = wager.name.split('@#@#');
           // content.event = eventProposition[0].replace("vs", "/");
           content.event = eventProposition[0];
           content.teamMarket = eventProposition[1];
           content.periodBetType = period + 'Team Total: ' + bookmakerPrice.propositionValue;
           var overUnderString = "Over";
           if(bookmakerPrice.propositionName.indexOf('Under') > -1) {
               overUnderString = "Under";
           }
           content.overUnder = overUnderString + ' ';
           content.priceDisplay = '@' + oddsFromPrice(wager.odds, ODDS_PRECISION);
           if(isConfirmed && wager.enteredvalue > 0)
               content.priceDisplay = '$' + wager.enteredvalue + content.priceDisplay;
           content.others = otherInfoOverUnderBySport(content.sportType, content.overUnder + bookmakerPrice.displayName, wrapped, isConfirmed)
       }

       this.getHTMLFromSelection = function(selection) {
           try {
               var html = '';
               var selectionJson = JSON.parse(selection);
               if(selectionJson.teamName && selectionJson.teamName != 'null') {
                   html += selectionJson.teamName + '<br/>';
               }
               var typeNameMap = {
                   WIN: 'Money Line',
                   HANDICAP: 'Handicap',
                   OVER_UNDER: 'Total',
                   TEAM_TOTALS: 'Team Total'
               }
               var period = "";
               if(selectionJson.period && selectionJson.period.trim().indexOf(' ') > -1) {
                   var periods = selectionJson.period.trim().split(' ');
                   period = periods[0][0] + ' ' + periods[1][0] + ' ';
               }
               if(period == '2 H ') {
                   period = '2nd Half ';
               }
               html += period + typeNameMap[selectionJson.type];
               if(selectionJson.type != 'WIN')
                   html += ': ' + selectionJson.propositionValue;
               html += '<br/>';
               var displayProp = getDisplayProposition(selectionJson.propositionValue);
               if(selectionJson.type == 'OVER_UNDER' || selectionJson.type == 'TEAM_TOTALS') {
                   if(selectionJson.propositionName.indexOf('Over') > -1) {
                       html += 'Over ';
                       displayProp = 'Over ' + displayProp;
                   }
                   else {
                       html += 'Under ';
                       displayProp = 'Under ' + displayProp;
                   }
               }
               html += setAmount(selectionJson.betValue) + '@' + oddsFromPrice(selectionJson.odds, ODDS_PRECISION);
               var others = getOtherInfoForSports(selectionJson.type, selectionJson.sportType, displayProp, convertToTwoDecimal(selectionJson.betValue), selectionJson.teamName, oddsFromPrice(selectionJson.odds, ODDS_PRECISION));
               if(others && others.length > 0)
                   html += '<br/>' + others;
               return html;
           } catch (e) {
               return selection;
           }
       }

       this.otherInfoHandicapBySport = function(sport, priceDisplay, wrapped, isConfirmed) {
           var wager = wrapped.wager;
           var chosenTeam = getChosenTeamName(wager.propositionName);
           var odds = oddsFromPrice(wager.odds, ODDS_PRECISION);
           var others = "";

           switch(sport){
               case "SOCCER" : {
                   // append "Note: This is two bets" into display, if any
                   if(priceDisplay.indexOf(" and ") > -1) {
                       var dividedAmount = "Half amount";
                       if (isConfirmed && wager.enteredvalue > 0)
                           dividedAmount = "$" + fixDecimalPlaces(wager.enteredvalue/2, FIX_DECIMAL);

                       var _1stHandicapDisplay = "";

                       var dividedHandicaps = priceDisplay.split(" and ");
                       if(dividedHandicaps[0] == "DNB"){
                           _1stHandicapDisplay = "to win (refund if a draw)";
                       }else{
                           _1stHandicapDisplay = dividedHandicaps[0];
                       }

                       /*others = "Note: This is two bets - ("
                           + "$" + dividedAmount + " " + chosenTeam + " " + _1stHandicapDisplay
                           + " and " + "$" + dividedAmount + " " + chosenTeam + " " + dividedHandicaps[1]
                           + "@" + odds + ")";*/

                       others = "Note: This is two bets" + "<br/>"
                           + " - " + dividedAmount + " on " + chosenTeam + " " + _1stHandicapDisplay + "<br/>"
                           + " - " + dividedAmount + " on " + chosenTeam + " " + dividedHandicaps[1] + "@" + odds;

                       wrapped.splittedBets = true;
                   }
                   break;
               }
               case "TENNIS" : {
                   // append "Note: If the total games for each player ..."
                   if(priceDisplay.indexOf("DNB") > -1){
                       others = "Note: If the total games for each player is a tie then bet is refunded";
                   }
                   break;
               }
               default : break;
           }

           return others;
       }

       this.otherInfoOverUnderBySport = function(sport, priceDisplay, wrapped, isConfirmed) {
           var wager = wrapped.wager;
           var dividedAmount = "Half amount";
           if (isConfirmed && wager.enteredvalue > 0)
               dividedAmount = "$" + fixDecimalPlaces(wager.enteredvalue/2, FIX_DECIMAL);
           var others = "";

           var isInt = function (n) {
               if(!isNaN(n) && parseInt(n) == parseFloat(n)){
                   return true;
               }else{
                   return false;
               }
           };

           switch(sport){
               case "SOCCER" : {
                   if(priceDisplay.indexOf(" and ") > -1){
                       var left = priceDisplay.split(" and ")[0];
                       var type = left.split(" ")[0];
                       var leftValue = left.split(" ")[1];
                       var rightValue = priceDisplay.split(" and ")[1];

                       others = "Note: This is two bets" + "<br/>"
                           + " - " + dividedAmount + " on " + left + " goals";

                       // determine the number of refund
                       if(isInt(leftValue)){
                           others += " (" + leftValue + " goals is a refund)";
                       }
                       others += "<br/>";
                       others += " - " + dividedAmount + " on " + type + " " + rightValue + " goals";
                       if(isInt(rightValue)){
                           others += " (" + rightValue + " goals is a refund)";
                       }
                       others += "<br/>";
                   }else{
                       var typeValue = priceDisplay.split(" ");
                       if(typeValue && typeValue.length > 1 && isInt(typeValue[1])){
                           others += "Note: If " + typeValue[1] + " goals are scored, bet will be refunded";
                       }
                   }
                   break;
               }
               default : break;
           }

           return others;
       }

       this.getOtherInfoForSports = function(betType, sportID, propositionDisplay, betAmount, chosenTeam, odds) {
           if(sportID == 'SOCCER') {
               if(betType == 'HANDICAP') {
                   if(propositionDisplay.indexOf(" and ") > -1) {
                       var	dividedAmount = "$" + fixDecimalPlaces(betAmount/2, FIX_DECIMAL);
                       var _1stHandicapDisplay = "";
                       var dividedHandicaps = propositionDisplay.split(" and ");
                       if(dividedHandicaps[0] == "DNB"){
                           _1stHandicapDisplay = "to win (refund if a draw)";
                       }else{
                           _1stHandicapDisplay = dividedHandicaps[0];
                       }
                       /*others = "Note: This is two bets - ("
                           + "$" + dividedAmount + " " + chosenTeam + " " + _1stHandicapDisplay
                           + " and " + "$" + dividedAmount + " " + chosenTeam + " " + dividedHandicaps[1]
                           + "@" + odds + ")";*/
                       return "Note: This is two bets" + "<br/>"
                           + " - " + dividedAmount + " on " + chosenTeam + " " + _1stHandicapDisplay + "<br/>"
                           + " - " + dividedAmount + " on " + chosenTeam + " " + dividedHandicaps[1] + "@" + odds;

                   }
               }
               else if(betType == 'OVER_UNDER' || betType == 'TEAM_TOTALS') {
                   var isInt = function (n) {
                       if(!isNaN(n) && parseInt(n) == parseFloat(n)){
                           return true;
                       }else{
                           return false;
                       }
                   };
                   if(propositionDisplay.indexOf(" and ") > -1){
                       var left = propositionDisplay.split(" and ")[0];
                       var type = left.split(" ")[0];
                       var leftValue = left.split(" ")[1];
                       var rightValue = propositionDisplay.split(" and ")[1];
                       var	dividedAmount = "$" + fixDecimalPlaces(betAmount/2, FIX_DECIMAL);
                       var others = "Note: This is two bets" + "<br/>"
                           + " - " + dividedAmount + " on " + left + " goals";

                       // determine the number of refund
                       if(isInt(leftValue)){
                           others += " (" + leftValue + " goals is a refund)";
                       }
                       others += "<br/>";
                       others += " - " + dividedAmount + " on " + type + " " + rightValue + " goals";
                       if(isInt(rightValue)){
                           others += " (" + rightValue + " goals is a refund)";
                       }
                       others += "<br/>";
                       return others;
                   }else{
                       var typeValue = propositionDisplay.split(" ");
                       if(typeValue && typeValue.length > 1 && isInt(typeValue[1])){
                           return "Note: If " + typeValue[1] + " goals are scored, bet will be refunded";
                       }
                   }
               }
           }
           else if (sportID == "TENNIS") {
               if(betType == 'HANDICAP' && propositionDisplay.indexOf("DNB") > -1) {
                   return "Note: If the total games for each player is a tie then bet is refunded";
               }
           }
           return undefined;
       }

       this.getChosenTeamName = function(propositionName) {
           if(propositionName.indexOf(" -") > -1){
               return propositionName.substring(0, propositionName.indexOf(" -"));
           }else if(propositionName.indexOf(" (") > -1){
               //Exclude the case for like tennis Roger Federer (+1.5 Sets)
               if(propositionName.indexOf(" (+") > -1 || propositionName.indexOf(" (-") > -1)
                   return propositionName;
               return propositionName.substring(0, propositionName.indexOf(" ("));
           }else if(propositionName.indexOf(" +") > -1){
               return propositionName.substring(0, propositionName.indexOf(" +"));
           }else if(propositionName.indexOf(" 0", propositionName.length - 2) !== -1){
               return propositionName.substring(0, propositionName.length - 2);
           }else{
               return propositionName;
           }
       }

       this.getDisplayProposition = function(propositionValue) {
           var slipDisplayName = "";
           if(!propositionValue || propositionValue.length == 0)
               return slipDisplayName;
           switch (propositionValue) {
               case "0.25":
                   slipDisplayName = "DNB and +0.5";
                   break;
               case "0.75":
                   slipDisplayName = "0.5 and +1";
                   break;
               case "0":
                   slipDisplayName = "DNB";
                   break;
               case "-0.75":
                   slipDisplayName = "-0.5 and -1";
                   break;
               case "-0.25":
                   slipDisplayName = "DNB and -0.5";
                   break;
               default:
                   slipDisplayName = propositionValue;
                   if (propositionValue.indexOf(".") > -1) {
                       var prop = propositionValue.split(".");
                       if (prop[1] == "25") {
                           slipDisplayName = prop[0] + " and " + prop[0] + ".5";
                       } else if (prop[1] == "75" && prop[0].indexOf("-") == 0) {
                           slipDisplayName = prop[0] + ".5 and " + (parseInt(prop[0]) - 1);
                       } else if (prop[1] == "75") {
                           slipDisplayName = prop[0] + ".5 and " + (parseInt(prop[0]) + 1);
                       }
                   }
                   break;
           }
           return slipDisplayName;
       }

       this.getTotesColor = function(poolTypeId, bookmakerId) {
           var desc = getTotesDesc(poolTypeId, bookmakerId);
           if(desc == FIXED_ODDS){
               return {color:"#FF6600"};
           } if(desc == TOTES_STAB){
               return {color:"maroon"};
           } if(desc == TOP_FLUC_TEXT || desc == BEST_START_PRICE_TEXT){
               return {color:"#FF00FF"};
           } if(desc == BEST_3_TOTES_TEXT){
               return {color:"purple"};
           } if(desc == TOTES){
               return {color:"blue"};
           } else {
               return {color:"black"};
           }
       }
       /*(1) TOTES
       (2) FIXED ODDS
       (3) TOTES EXOTICS
       (4) BEST TOTE ((BEST OF 3 TOTES))
       (5) TOP FLUCTUATIONS  BEST TOTE + SP*/
       this.getTotesDesc = function(poolTypeId, bookmakerId) {
           if(poolTypeId == FIXED_WIN_ID || poolTypeId == FIXED_PLACE_ID){
               if(bookmakerId == TOPSPORT){
                   return FIXED_ODDS;
               } else {
                   return TOTES_STAB;
               }
           } else if(poolTypeId == TOP_FLUC_ID) {
               return TOP_FLUC_TEXT;
           }else if(poolTypeId == BEST_START_PRICE_ID) {
               return BEST_START_PRICE_TEXT;
           } else if(poolTypeId == BEST_3_TOTES_ID) {
               return BEST_3_TOTES_TEXT;
           }else if(poolTypeId == QUINELLA_ID || poolTypeId == EXACTA_ID || poolTypeId ==
               TRIFECTA_ID || poolTypeId == FIRST_FOUR_ID || poolTypeId == QUINELLA_ID
               || poolTypeId == QUADRELLA_ID
               || poolTypeId == DAILY_DOUBLE_ID) {
               return TOTES;
           } else {
               return "";
           }
       }
       this.isTotePool = function(poolType){
           if( poolType == 'BT3SP' ||poolType == 'BT1W' ||poolType == 'BT1P' ||poolType == 'WT5' ||poolType == 'PT5' ||poolType == 'BT3W'||poolType == 'BT3P'){
               return 'TOTE';
           }
       }

       this.getPoolTypeDesc = function(poolTypeId, bookmakerId) {
           if (poolTypeId == FIXED_WIN_ID || poolTypeId == FIXED_PLACE_ID) {
               if (bookmakerId == TOPSPORT) {
                   if (poolTypeId == FIXED_WIN_ID) {
                       return FIXED_ODDS + " " + FIXED_WIN_TEXT;
                   } else if (poolTypeId == FIXED_PLACE_ID) {
                       return FIXED_ODDS + " " + FIXED_PLACE_TEXT;
                   }
               } else {
                   if (poolTypeId == FIXED_WIN_ID) {
                       return TOTES_STAB + " " + FIXED_WIN_TEXT;
                   } else if (poolTypeId == FIXED_PLACE_ID) {
                       return TOTES_STAB + " " + FIXED_PLACE_TEXT;
                   }
               }
           } else if (poolTypeId == TOP_FLUC_ID) {
               return TOP_FLUC_TEXT;
           } else if (poolTypeId == BEST_START_PRICE_ID) {
               return BEST_START_PRICE_TEXT;
           } else if (poolTypeId == BEST_3_TOTES_ID) {
               return BEST_3_TOTES_TEXT + " " + FIXED_WIN_TEXT;
           } else if (poolTypeId == QUINELLA_ID) {
               return TOTES + " " + QUINELLA_TEXT;
           } else if (poolTypeId == EXACTA_ID) {
               return TOTES + " " + EXACTA_TEXT;
           } else if (poolTypeId == TRIFECTA_ID) {
               return TOTES + " " + TRIFECTA_TEXT;
           } else if (poolTypeId == FIRST_FOUR_ID) {
               return TOTES + " " + FIRST_FOUR_TEXT;
           } else if (poolTypeId == QUADRELLA_ID) {
               return TOTES + " " + QUADRELLA_TEXT;
           } else if (poolTypeId == DAILY_DOUBLE_ID) {
               return TOTES + " " + DAILY_DOUBLE_TEXT;
           } else {
               return "";
           }
       }

       this.getWagerDescText = function(accountRaceWagerdto) {
           var desc = getPoolTypeDesc(accountRaceWagerdto.poolTypeId, accountRaceWagerdto.bookmakerId);
           if (accountRaceWagerdto.flexi == true) {
               desc += ", Flexi " + accountRaceWagerdto.amountPerPermutation + "%";
           }
           return desc;
       }

       this.getConstructedDescSuperTab = function(bookmakerId) {
           var desc = "";
           if (bookmakerId == TOBRACING) {
               desc += "Supertab Div";
           }
           return desc;
       }

       this.getNewDesc = function(description){
           var matches=null;
           if ((description.indexOf('refunded') != -1)||
                   (description.indexOf('Refunded') != -1) ||
                   (description.indexOf('accepted') != -1)||
                   (description.indexOf('Accepted') != -1))
                   {
                        matches = description.match(/\[[^\]]*?(?:accepted|refunded)[^\]]*?\]/i);
                   }
           if (matches!=null){
           return matches[0];
           }
           else
               {
               return '';
               }
       }

       this.getPoolTypeSelected = function(wagerContent) {
           if (wagerContent.product) {
               return wagerContent.product;
           } else {
               return getTotesDesc(wagerContent.pool.poolTypeId, wagerContent.pool.bookmaker.id);
           }
           return "";
       }

       this.getBaseballDescription = function(briefDescription, pitcher1MustStart, pitcher2MustStart) {
           if(briefDescription.indexOf(' (') == -1)
               return briefDescription;
           var teams = briefDescription.split(' vs ');
           var team1 = teams[0];
           var team2 = teams[1];
           if(pitcher1MustStart) {
               team1 = team1.replace(/\(/g, '[').replace(/\)/g, ' must start]');
           }
           else {
               team1 = team1.replace(/\(.*\)/g, '');
           }
           if(pitcher2MustStart) {
               team2 = team2.replace(/\(/g, '[').replace(/\)/g, ' must start]');
           }
           else {
               team2 = team2.replace(/\(.*\)/g, '');
           }
           return team1 + ' vs ' + team2;
       }

       this.scrollTop = function() {
           $(".snap-content").scrollTop(0);
       };

       this.safelog = function() {
           if (debug_mode) {
               if (typeof console !== "undefined" && typeof console.log !== "undefined") {
                   var c = "console.log(";
                   for (var i = 0; i < arguments.length; i++) {
                       // var arg = arguments[i];
                       if (i != 0) {
                           c += ",";
                       };
                       c += "arguments[" + i + "]";
                   };
                   c += ");";
                   eval(c);
               };
           };
       }

       function isEmpty (obj) {
           for(var prop in obj) {
               if(obj.hasOwnProperty(prop))
                   return false;
           }

           return true;
       }


       this.triColRow = function(arr){
           var res = [];
           var arrlen = arr.length;
           if(arr instanceof Array){
               if(arr[0] && (arr[0].marketType.value.toLowerCase() == 'handicap' || arr[0].marketType.value.toLowerCase() == 'over_under') ){
                   for(var i = 0; arr.length > 0 ; i=i+2){
                       res.push(arr.splice(0,2));
                   }
               }else{
                   for(var i = 0; arr.length > 0 ; i=i+3){
                       res.push(arr.splice(0,3));
                   }
               }

           }

           return res;

       }


       this.getTheDate = function(mm) {
           var date = new Date(mm);
           var weekday = [];
           weekday[0] = "Sun";
           weekday[1] = "Mon";
           weekday[2] = "Tue";
           weekday[3] = "Wedy";
           weekday[4] = "Thu";
           weekday[5] = "Fri";
           weekday[6] = "Sat";

           var month = ['Jan','Feb','Mar','Apr','May','June','July','Aug','Sept','Oct','Nov','Dec']

           var dateVal = weekday[date.getDay()]+"  "+date.getDate()+'/'+month[date.getMonth()]+'/'+date.getFullYear();
           var datestd = date.getDate()+'/'+month[date.getMonth()]+'/'+date.getFullYear();
           var timeVal = '';
           timeVal += date.getHours() < 10? '0'+date.getHours(): date.getHours();
           timeVal += ":";
           timeVal += date.getMinutes()< 10 ? '0'+date.getMinutes() : date.getMinutes();

           return {
               Date: dateVal,
               Time:  timeVal,
               datestd: datestd,
               mm: date.getTime()
           }
       }

       this.mobile = function() {
           document.getElementById("mobile").href= window.location.protocol + "//" + window.location.host.replace('www.', 'm.');
       }

       this.stringStartsWith = function(string, prefix) {
           if(typeof string === 'string' ){
               return string.slice(0, prefix.length) == prefix;
           }else{
               return false;
           }
       }


    }


})();
