(function(){
    'use strict';

    angular
        .module('pinnacleBetApp')
        .directive('inputValid',inputValid)
        .directive('betSlip', betSlip)
        .directive('betSlipPanel', betSlipPanel);


    betSlipPanel.$inject = ['$rootScope','betslipService','util', 'accountService', 'messagingService'];
    betSlip.$inject = ['$rootScope','betslipService','util'];

    function betSlipPanel($rootScope, betslipService, util, accountService, messagingService) {
        var directive = {
            restrict: 'A',
            templateUrl: 'components/betslip/betslippanel.html',
            scope : {
                slips : '='
            },
            link: linkFunc,
            controller: ctrlFunc

        }

        return directive;

        function ctrlFunc($scope, $element){
            $scope.pendingSlips = [];
            $scope.accountInfo = {
                username : '',
                password : '',
                msg: ''
            }
            $scope.bonus = {
                bonus_amount: 0,
                bonus_wallet: undefined,
                voucher: undefined
            };
            $scope.setAmount = util.setAmount;
            $scope.clearSlips = betslipService.clearSlips;
            $scope.clearPenging = clearPenging;
            $scope.slipsPanel = new SlipsPanel();
            $scope.multiSlips = [];
            $scope.multiPenddingSlips = [];
            $scope.editBtn = editBtn;
            $scope.placeBetBtn = placeBetBtn;
            $scope.confirmBtn = confirmBtn;
            $scope.doneBtn = doneBtn;
            $scope.isResolved = isResolved;
            $scope.toggleSlips = toggleSlips;
            $scope.multibetName = multibetName;
            $scope.reSubmit = reSubmit;
            $scope.sportFilter = sportFilter;
            $scope.sportMultiFilter = sportMultiFilter;
            $scope.raceFilter = raceFilter;
            $scope.exoticFilter = exoticFilter;
            $scope.multiLegsFilter = multiLegsFilter;

            //switches
            $scope.openBetslipTab = openBetslipTab;
            $scope.openPendingTab = openPendingTab;
            $scope.openLogin = openLogin;
            $scope.closeLogin = closeLogin;
            $scope.login = login;
            $scope.closeBetslip = closeBetslip;
            var betslipLoginElem = $('#betslip .betslip-login');
            var betslipErrorElem = $('#betslip .betslip-error');

            function closeBetslip(){
                betslipService.closeBetslip();
            }
            function toggleSlips(){
                $scope.slipsPanel.isBetslip
                    ? $scope.slipsPanel.isBetslip = false
                    : $scope.slipsPanel.isBetslip = true;
            }
            function openBetslipTab(){
                $scope.slipsPanel.isBetslip = true;
            }
            function openPendingTab(){
                $scope.slipsPanel.isBetslip = false;
            }
            function openLogin(){
                $scope.accountInfo.msg = '';
                betslipLoginElem.fadeIn(500, function(){});
            }
            function closeLogin(){
                betslipLoginElem.fadeOut(500, function(){
                    // $scope.slipsPanel.swthLogin = false;
                });
            }
            function login(){
                accountService.login($scope.accountInfo.username, $scope.accountInfo.password, $rootScope)
                    .then(function(res){
                        if($rootScope.accountInfo.isLogin){
                            closeLogin();
                        }else{
                            $scope.accountInfo.msg = $rootScope.accountInfo.errorMessage;
                        }
                    });
            }
            function setPanelError(msg){
                $scope.slipsPanel.msg = msg;
                betslipErrorElem.fadeIn();

                setTimeout(function(){
                    betslipErrorElem.fadeOut();
                    $scope.slipsPanel.msg = '';
                },5000);
            }

            function slipValid(slip){
                // input greater than $0
                if(util.isNumber(slip.info.amount) && slip.info.amount > 0){
                    // input minimumRisk
                    if(!!slip.data.minimumRisk && slip.data.minimumRisk.match(/\d*\.?\d*/)){
                        slip.info.amount >= parseFloat(slip.data.minimumRisk)
                            ? slip.info.isConfirm = true
                            : slip.info.isConfirm = false;
                    }else{
                        slip.info.isConfirm = true;
                    }
                }else{
                    slip.info.isConfirm = false;
                }

                //EXOTICS flexi pecentage has to be >1%
                if(slip.info.isFlex){
                    (slip.info.amount / slip.data.combinations) > 0.01
                        ? slip.info.isConfirm = true
                        : slip.info.isConfirm = false;
                }

                //bet maximum
                if(slip.info.betMax){
                    if(!!slip.data.maximumRisk && slip.data.maximumRisk.match(/\d*\.?\d*/)){
                        slip.info.amount = parseInt(slip.data.maximumRisk);
                    }
                }

                // mark it as pending
                if(slip.info.wagerId || slip.info.code){
                    slip.info.isPending = true;
                }


            }

            function SlipsPanel(){
                this.swthLogin = false;
                this.isBetslip = true;
                this.isAval = false;
                this.isConfirm = false;
                this.isDone = false;
                this.isMulti = true;
                this.totalBets = 0;
                this.TotalStake = 0;
                this.estRtn = 0;
                this.multibets = 0;
                this.multiStake = 0;
                this.multiestRtn = 0;
                this.msg = '';
            }

            function initSlipPanel(){
                $scope.slipsPanel.totalBets = 0;
                $scope.slipsPanel.TotalStake = 0;
                $scope.slipsPanel.estRtn = 0;
            }
            function initBonusPanel(){
                $scope.bonus.bonus_amount = 0;
            }
            function initMultiSlipPanel(){
                $scope.slipsPanel.multibets = 0;
                $scope.slipsPanel.multiStake = 0;
                $scope.slipsPanel.multiestRtn = 0;
            }
            function initSlips(){
                $scope.slips.forEach(function(slip){
                    slip.info.isConfirm = false;
                    slip.info.isInputAval = true;
                    slip.info.isHide = false;
                });
            }
            function getSlipsPanel(){
                // for betslip panel
                $scope.slipsPanel.isAval = $scope.slipsPanel.totalBets + $scope.slipsPanel.multibets > 0 ? true : false;
                $scope.slipsPanel.isMulti = isMultiAval();
            }
            function getSlipStake(slipsPanel, slip){
                slipsPanel.totalBets += slip.info.isConfirm ? 1 : 0;
                slipsPanel.TotalStake += !slip.info.isFlex && util.isNumber(slip.data.combinations)
                    ? util.setAmount(slip.info.amount).amount * slip.data.combinations
                    : util.setAmount(slip.info.amount).amount;
                slipsPanel.estRtn += util.setAmount(slip.info.amount).amount * slip.info.odds / (slip.info.type === 'RACING'? 100 : 1000);
            }
            function getMultiStake(slipsPanel, slip){
                slipsPanel.multibets += slip.info.isConfirm ? slip.info.multiRef.length : 0;
                slipsPanel.multiStake += util.setAmount(slip.info.amount).amount * slip.info.multiRef.length;
                slipsPanel.multiestRtn += util.setAmount(slip.info.amount).amount * slip.info.odds ;
            }
            function getBonusStake(slip){
                $scope.bonus.bonus_amount += slip.info.bonus.value
                    ? util.setAmount(slip.info.amount).amount
                    : 0;
            }
            function getMulti(){
                var multibets = $scope.slips.filter(function(slip){return slip.info.isMulti;});
                var multiSlips = [];
                for(var i = 1; i < multibets.length ; i++){
                    if(i > 1 && multibets.length >5){
                        break;
                    }
                    var combiArr = betslipService.getCombination(multibets.length, i);
                    var odds = betslipService.getCombinationOdds(combiArr, multibets);
                    var multiSlip = betslipService.multipAsm(combiArr, odds, '');
                    multiSlips.push(multiSlip);
                }
                $scope.multiSlips = multiSlips;
            }
            function clearPenging(){
                $scope.pendingSlips = [];
            }

            /*
            *  @ description Filters
            */
            function sportFilter(slip){
                return slip.info.type === 'SPORT' && !slip.info.isMulti;
            }
            function sportMultiFilter(slip){
                return slip.info.type === 'SPORT' && slip.info.isMulti;
            }
            function raceFilter(slip){
                return slip.info.type === 'RACING';
            }
            function exoticFilter(slip){
                return slip.info.type === 'EXOTICS' && slip.data.pool.poolTypeId !== 'QD';
            }
            function multiLegsFilter(slip){
                return slip.info.type === 'EXOTICS' && slip.data.pool.poolTypeId === 'QD';
            }


            /*
            *  @ description  submitting section
            */
            function submitHandler(){
                var slipsReq = {
                    sportReqs : [],
                    raceReqs : {
                        fixedOddsWagerRequests: [],
                        toteWagerRequests: []
                    }
                }
                var multipendingsReq = {
                    multiReqs : []
                }
                betslipService.multiRequestAssembler($scope.multiSlips,$scope.multiPenddingSlips, multipendingsReq);
                betslipService.sportRaceRequestAssembler($scope.slips, slipsReq);
                betslipService.betslipsSubmit(slipsReq, $scope.slips);
                betslipService.betslipsSubmit(multipendingsReq, $scope.multiPenddingSlips);

            }
            function reSubmit(slip){
                if(slip){
                    var slipsReq = {
                        sportReqs : [],
                        raceReqs : {
                            fixedOddsWagerRequests: [],
                            toteWagerRequests: []
                        },
                        multiReqs : []
                    }
                    slip.info.wagerId = '';
                    slip.info.msg = '';
                    slip.info.code = '';
                    betslipService.sportRaceRequestAssembler([slip], slipsReq);
                    if(slipLocationCheck($scope.pendingSlips, slip)){
                        betslipService.betslipsSubmit(slipsReq, $scope.pendingSlips);
                    }else{
                        betslipService.betslipsSubmit(slipsReq, $scope.slips);
                    }
                }

                function slipLocationCheck(array, slip) {
                    for(var i = 0; i < array.length; i++){
                        if(array[i].info.referenceId === slip.info.referenceId){
                            return true;
                        }
                    }
                    return false;
                }
            }
            function msgUpdateSlips(slips,data){
                slips.forEach(function (wager) {
                    if (wager.info.wagerId == data.id) {
                        betslipService.wagerHandler(wager, data);
                    }
                });
                slips.forEach(function (wager) {
                    if (wager.info.referenceId == data.referenceId) {
                        wager.info.wagerId = data.id;
                        betslipService.wagerHandler(wager, data);
                    }
                });
            }

            function editBtn(){
                if($scope.slipsPanel.isConfirm){
                    initSlips();
                    changeStatus(false, false, false);
                }
            }
            function placeBetBtn(){

                if(!$scope.slipsPanel.isAval){
                    return;
                }

                // login validation
                if(!accountService.isLogin($rootScope)){
                    openLogin();
                    return;
                }

                // Available fund validation
                if( ($scope.slipsPanel.TotalStake - $scope.bonus.bonus_amount) > util.setAmount($rootScope.accountInfo.accountHolderDto.wallet.availableFunds).toDollars ){
                    setPanelError('Insufficient Funds');
                    return;
                }

                // Bonus available fund validation
                if($scope.bonus.bonus_wallet){
                    if($scope.bonus.bonus_amount > util.setAmount($scope.bonus.bonus_wallet.availableFund).toDollars ){
                        setPanelError('Bonus Insufficient Funds');
                        return;
                    }
                }

                // successful
                changeStatus(false, true, false);
                markUnfittedSlips(); // remove dubious slips e.g amount = ('', not number)

            }
            function confirmBtn(){
                markPendingSlips();
                submitHandler();
                // initSlips();
                changeStatus(false, false, true);
            }
            function doneBtn(){
                betslipService.toPendingSlips($scope.slips, $scope.multiPenddingSlips, $scope.pendingSlips);
                initSlips();
                changeStatus(false, false, false);
            }
            function markUnfittedSlips(){
                $scope.slips.forEach(function(slip){
                    if(slip.info.isConfirm){
                        slip.info.isHide = false;
                    }else{
                        slip.info.isHide = true;
                    }

                });
            }
            function isMultiAval(){
                var isValid = true;
                var multis = [];
                var racingDuplicates = {};
                var sportDuplicates = {};

                // extract multi bet
                multis = $scope.slips.filter(function(slip){
                    return slip.info.isMulti;
                });

                multis.forEach(function(slip){
                    //same race number
                    if(slip.info.type == 'RACING' && slip.data.raceInfo){
                        if(racingDuplicates.hasOwnProperty(slip.data.raceInfo.id)){
                            isValid = false;
                        }else{
                            racingDuplicates[slip.data.raceInfo.id] = true;
                        }
                    }

                    //same sport number
                    if(slip.info.type == 'SPORT' && slip.data.event){
                        if(sportDuplicates.hasOwnProperty(slip.data.event.key)){
                            isValid = false;
                        }else{
                            sportDuplicates[slip.data.event.key] = true;
                        }
                    }
                });

                //no more than 7 multibets
                if(multis.length < 2 || multis.length > 7){
                    isValid = false;
                }

                return isValid;

            }
            function markPendingSlips(){
                $scope.slips.forEach(function(slip){
                    slip.info.isInputAval = false;
                });
            }
            function changeStatus(isAval, isConfirm, isDone){
                $scope.slipsPanel.isAval = isAval; //
                $scope.slipsPanel.isConfirm = isConfirm; //
                $scope.slipsPanel.isDone = isDone; //
            }
            function isResolved(slip){
                if (slip) {
                    return slip.info.code == "W" || slip.info.code == "L" || slip.info.code == "F" || slip.info.code == "CN";
                } else {
                    return false;
                }
            }
            function pendingSlipsUpdate(){
                $scope.pendingSlips.forEach(function(slip){
                    betslipService.betslipUpdate(slip);
                });
            }
            function multibetName(index){
                switch(index){
                    case 0 :
                        return "Doubles";
                    case 1 :
                        return "Trebles";
                    case 2 :
                        return "Four_Picks";
                    default :
                        return "Any_"+ (index+ 1)+ "_combinations";
                }
            }

            /*
            *  @ description  bonus section
            */
            // bonus initialisaztion
            function bonusInit(){
                if($rootScope.accountInfo.accountHolderDto.bonusBets && $rootScope.accountInfo.accountHolderDto.bonusBets.length > 0){
                    //bonus restrictions
                    $scope.bonus.restriction = $rootScope.accountInfo.accountHolderDto.bonusBets[0].restriction || 'ALL';

                    // bonus voucher
                    $scope.bonus.voucher = $rootScope.accountInfo.accountHolderDto.bonusBets[0].statusId == 'AVAILABLE'? $rootScope.accountInfo.accountHolderDto.bonusBets[0] : undefined;
                    $scope.bonus.bonus_wallet = $rootScope.accountInfo.accountHolderDto.bonusBets[0].statusId == 'WALLET_IN_USE'? $rootScope.accountInfo.accountHolderDto.bonusBets[0] : undefined;
                }else{
                    $scope.bonus.voucher = undefined;
                    $scope.bonus.bonus_wallet = undefined;
                }
            }
            function bonusValid(){
                //restriction
                $scope.slips.forEach(function(slip){
                    slip.info.bonus.restriction = $scope.bonus.restriction !== 'ALL' && slip.info.type === 'SPORT'? true : false;
                });

                //voucher, bonus_wallet
                if($scope.bonus.voucher){
                    var selected = [];
                    var notSelected = [];
                    $scope.slips.forEach(function(slip){
                        slip.info.bonus.value
                            ? selected.push(slip)
                            : notSelected.push(slip);
                    });

                    if(selected.length > 0){
                        $scope.slips.forEach(function(slip){
                            if(slip.info.bonus.value){
                                slip.info.amount = util.setAmount($scope.bonus.voucher.availableFund).toDollars;
                                slip.info.bonus.id = $scope.bonus.voucher.id;
                            }else{
                                slip.info.bonus.status = 'notAvl';
                            }
                        });
                    }else{
                        $scope.slips.forEach(function(slip){
                            slip.info.bonus.status = 'enable';
                        });
                    }

                }else if($scope.bonus.bonus_wallet){
                    $scope.slips.forEach(function(slip){
                        slip.info.bonus.id = $scope.bonus.bonus_wallet.id;
                    });
                }else{
                    // not bonus account
                    $scope.slips.forEach(function(slip){
                        slip.info.bonus.status = 'notAvl';
                    });
                }

            }
            function bonusRestriction(slip){
                if(slip.info.type === 'SPORT' && $scope.bonus.restriction !== 'ALL'){
                    return false;
                }
                return true;
            }


            $scope.$watch('slips', function(nv, ov){
                //increasing bets
                if(angular.isArray(nv) && angular.isArray(ov) && nv.length > ov.length){
                    openBetslipTab();
                    doneBtn();
                }

                // init slippanel status, prepare for ...
                initSlipPanel();
                initBonusPanel();
                for(var i = 0 ; i < $scope.slips.length ; i++){
                    // slip is available for placing a bet
                    slipValid($scope.slips[i]);

                    // Bonus stake
                    getBonusStake($scope.slips[i]);

                    // Total (e.g tatalstake, tatalbets, estimateReturn)
                    getSlipStake($scope.slipsPanel, $scope.slips[i]);
                }

                //Bonus validation
                bonusValid();

                // During conforming, back to initial status
                if($scope.slipsPanel.isConfirm){
                    if(nv.length !== ov.length){
                        initSlips();
                        changeStatus(false, false, false);
                    }
                }


                //caculate multis
                if(!($scope.slipsPanel.isConfirm || $scope.slipsPanel.isDone)){
                    if(nv.length !== ov.length){
                        getMulti();
                    }
                }

                getSlipsPanel();

            },true);

            $scope.$watch('multiSlips',function(nv, ov){
                // init slippanel status, prepare for ...
                initMultiSlipPanel();
                for(var i = 0 ; i < $scope.multiSlips.length ; i++){
                    // slip is available for placing a bet
                    slipValid($scope.multiSlips[i]);

                    // Total (e.g tatalstake, tatalbets, estimateReturn)
                    getMultiStake($scope.slipsPanel, $scope.multiSlips[i]);
                }

                getSlipsPanel();
            }, true);

            $scope.$watch('slipsPanel',function(nv, ov){
            }, true);

            $rootScope.$watch('accountInfo',function(nv, ov){
                // empty pengingslips when logout and messaging
                if(accountService.isLogin($rootScope)){
                    subWagerEvent();
                    bonusInit();
                }else{
                    $scope.pendingSlips = [];
                }

            }, true);

            $scope.$on('$destroy', function(){
                if(!!ajaxBetslipInt){
                    clearInterval(ajaxBetslipInt);
                }

                // destroyed messaging
                unsubWagerEvent();
            });

            // var ajaxBetslipInt = setInterval(function(){
            //     pendingSlipsUpdate();
            // }, 5000);



            /*
            *  Message section
            */
            function subWagerEvent() {
                if ($rootScope.accountInfo.accountHolderDto) {
                    messagingService.subscribe('WagerEvent', $rootScope.accountInfo.accountHolderDto.wallet.id, function (data) {
                        util.log("betslip messaging work ");
                        util.log(data);
                        if (data) {
                            msgUpdateSlips($scope.slips, data);
                            msgUpdateSlips($scope.multiPenddingSlips, data);
                            msgUpdateSlips($scope.pendingSlips, data);
                        }

                        // updating scope
                        if (!$scope.$root.$$phase) {
                            $scope.$apply();
                        }
                    });

                    //Bonus wager
                    if($rootScope.accountInfo.accountHolderDto.bonusBets.length > 0){
                        messagingService.subscribe('WagerEvent', $rootScope.accountInfo.accountHolderDto.bonusBets[0].walletId, function (data) {
                            util.log("betslip bonus messaging work ");
                            util.log(data);
                            if (data) {
                                msgUpdateSlips($scope.slips, data);
                                msgUpdateSlips($scope.multiPenddingSlips, data);
                                msgUpdateSlips($scope.pendingSlips, data);
                            }

                            // updating scope
                            if (!$scope.$root.$$phase) {
                                $scope.$apply();
                            }
                        });
                    }

                }
            }
            function unsubWagerEvent() {
                if ($rootScope.accountInfo.accountHolderDto) {
                    messagingService.unsubscribe('WagerEvent', $rootScope.accountInfo.accountHolderDto.wallet.id);

                    if($rootScope.accountInfo.accountHolderDto.bonusBets.length > 0){
                        messagingService.unsubscribe('WagerEvent', $rootScope.accountInfo.accountHolderDto.bonusBets[0].walletId);
                    }
                }
            }
        }

        function linkFunc(scope, element){}
    }

    function betSlip($rootScope, betslipService, util){
        var directive = {
            restrict: 'A',
            templateUrl: 'components/betslip/betslip.html',
            scope : {
                slip : '=',
                slipsPanel : '=',
                editBtn : '&',
                reSubmit : '&'
            },
            link: linkFunc,
            controller: ctrlFunc

        }

        return directive;

        function ctrlFunc($scope, $element){
            $scope.getSportPriceDSP = util.getSportPriceDSP;
            $scope.removeSlip = betslipService.removeSlip;
            $scope.oddsFromPrice = util.oddsFromPrice;
            $scope.betslipUpdate = betslipService.betslipUpdate;
            $scope.getPoolName = betslipService.getPoolName;
            $scope.isTopsport = betslipService.isTopsport;

        }

        function linkFunc(scope, element){}
    }

    function inputValid(){
        var directive = {
            restrict: 'A',
            require: 'ngModel', // require:  '^form',
            link: linkFunc
        }

        return directive;

        function linkFunc(scope, element, attrs, ctrl) {
            element.bind('keyup', function (event) {

            });

        }

    }


})();
