(function(){
    'use strict';

    angular
        .module('pinnacleBetApp')
        .service('betslipService', betslipService);

    betslipService.$inject = ['$rootScope', '$http', 'util','EVN'];

    function betslipService($rootScope,$http,util,EVN){
        this.openBetslip = openBetslip;
        this.closeBetslip = closeBetslip;
        this.addSlip = addSlip;
        this.betslipAsm = betslipAsm;
        this.removeSlip = removeSlip;
        this.clearSlips = clearSlips;
        this.sportReqAsm = sportReqAsm;
        this.raceReqAsm = raceReqAsm;
        this.multiReqAsm = multiReqAsm;
        this.multipAsm = multipAsm;
        this.sportWager = sportWager;
        this.raceWager = raceWager;
        this.multiWager = multiWager;
        this.multiRequestAssembler = multiRequestAssembler;
        this.sportRaceRequestAssembler = sportRaceRequestAssembler;
        this.toPendingSlips = toPendingSlips;
        this.betslipsSubmit = betslipsSubmit;
        this.sportWagerStatus = sportWagerStatus;
        this.wagerIdHandler = wagerIdHandler;
        this.wagerHandler = wagerHandler;
        this.betslipUpdate = betslipUpdate;
        this.getCombination = getCombination;
        this.getCombinationOdds = getCombinationOdds;
        this.getMultiRef = getMultiRef;
        this.isTopsport = isTopsport;
        this.priceRefresher = priceRefresher;
        this.getPoolName = getPoolName;

        /* general functions*/
        function openBetslip(){
            if(!$("#wrapper").hasClass("toggled")){
                $("#wrapper").addClass("toggled");
            }
        }
        function closeBetslip(){
            if($("#wrapper").hasClass("toggled")){
                $("#wrapper").removeClass("toggled");
            }
        }
        function betslipValidation(slip){
            var isValid = false;
            //Duplicated race, sport , multi eventListeners
            isValid = $rootScope.slips.every(function(origin){
                return !slip.oddsId || origin.info.oddsId !== slip.oddsId;
            });

            return isValid;
        }
        function addSlip(slip){
            if(betslipValidation(slip)){
                var betslip = betslipAsm(slip);
                openBetslip();
                priceRefresher(betslip);
                $rootScope.slips.unshift(betslip);
            }
        }
        function removeSlip(slip){
            for(var i = 0; i< $rootScope.slips.length ; i++){
                if($rootScope.slips[i].info.referenceId === slip.info.referenceId){
                    $rootScope.slips.splice(i,1);
                }
            }
        }
        function betslipAsm(slip){
            return {
                data : slip.data,
                info : {
                    referenceId : getRefId(),
                    wagerId: '',
                    type : slip.type,
                    odds : slip.odds,
                    oddsId : slip.oddsId,
                    amount : '',
                    isMulti : slip.isMulti,
                    isHide : false,
                    isConfirm : false,
                    isPending : false,
                    isInputAval : true,
                    isFlex : hassFlexi(slip) ? true : false,
                    betMax : false,
                    bonus : {
                        id : '',
                        status : 'enable', // enable , disable, notAvl
                        value : false,
                        restriction : false
                    },
                    msg : '',
                    code: ''
                }
            }
        }
        function multipAsm(multiRef, odds, amount){
            return {
                data : {},
                info : {
                    referenceId : getRefId(),
                    wagerId: '',
                    type : 'MULTI',
                    odds : odds,
                    amount : amount,
                    isMulti : true,
                    isHide : false,
                    isConfirm : false,
                    isPending : false,
                    isInputAval : true,
                    msg : '',
                    code: '',

                    //only for multibet
                    multiRef  : multiRef,
                    multiRefSlips: []
                }
            }
        }
        function getRefId(){
            var str = Math.random().toString();
            return str.substring(2, str.length);
        }
        function clearSlips(){
            $rootScope.slips = [];
        }
        function priceRefresher(slip){
            if(EVN.priceRefresherUrl){
                if (slip.info.type == 'SPORT') {
                    if(slip.data.bookmaker == "Pinnacle"){
                        return pinnaclePriceRefresher(slip.data.id).then(function(res){
                            if(res){
                                slip.info.odds =  res.price ?  res.price.price: slip.info.odds;
                                slip.data.maximumRisk = res.price ?  res.price.maximumRisk: slip.data.maximumRisk;
                                slip.data.minimumRisk = res.price ?  res.price.minimumRisk: slip.data.minimumRisk;
                            }

                        });
                    }

                    if(slip.data.bookmaker == "TopSport"){
                        return topsportpriceRefresher(slip.data.event.key).then(function(res){});

                    }

                }

                if (slip.info.type == 'RACING'){
                    return racePriceRefresher(slip.data.raceInfo.id).then(function(res){});

                }
            }

            return undefined;
        }

        /* submitting */
        function sportReqAsm(slip){
            var r = {
                priceId: slip.data.id,
                amount: util.setAmount(slip.info.amount).toCents,
                fixedOdds: slip.data.price,
                additionalDesc: 'no need',
                pitcher1MustStart: slip.info.pitcher1Checked ? true : false,
                pitcher2MustStart: slip.info.pitcher2Checked ? true : false,
                referenceId: slip.info.referenceId
            };

            // bonus bet
            if(slip.info.bonus.value){
                r.bonusBetId = slip.info.bonus.id;
            }

            return r ;
        }
        function raceReqAsm(slip){
            if(isTopsport(slip)){
                var r = {
                    referenceId: slip.info.referenceId,
                    amount: util.setAmount(slip.info.amount).toCents,
                    oddsId : getRaceOddsObj(slip.data.entrant, slip.data.pool).id,
                    fixedOdds :slip.info.odds
                };

                // bonus bet
                if(slip.info.bonus.value){
                    r.bonusBetId = slip.info.bonus.id;
                }

                return r;
            }else{
                if(isExotics(slip)){
                    var r = {
                        referenceId: slip.info.referenceId,
                        poolId: slip.data.pool.id,
                        amount: slip.info.isFlex ? util.setAmount(slip.info.amount).toCents : util.setAmount(slip.info.amount).toCents * slip.data.combinations,
                        amountPerPermutation: slip.info.isFlex ? util.setAmount(slip.info.amount/ slip.data.combinations).toCents : util.setAmount(slip.info.amount).toCents,
                        isFlexi: slip.info.isFlex,
                    }
                    for(var i = 0 ; i < slip.data.selections.length; i++){
                        r['rugNumber' + (i + 1)] = slip.data.selections[i].toString().replace(/,/g ,'+');
                    }
                    return r;
                }else {
                    var r = {
                        referenceId: slip.info.referenceId,
                        poolId: slip.data.pool.id,
                        amount: util.setAmount(slip.info.amount).toCents,
                        amountPerPermutation: util.setAmount(slip.info.amount).toCents,
                        isFlexi: slip.info.isFlex,
                        rugNumber1 : slip.data.entrant.rugNumber
                    }
                    return r;
                }

            }

        }
        function multiRaceReqAsm(slip){
            var r = {
                amount : 0,
                amountPerPermutation : 0,
                bookmakerId : slip.data.pool.bookmaker.id,
                description : 'boom clap',
                fixedOdds : slip.info.odds,
                isFlexi : slip.info.isFlex,
                poolTypeId : slip.data.pool.poolTypeId,
                raceId : slip.data.raceInfo.id,
                raceOddsId : getRaceOddsObj(slip.data.entrant, slip.data.pool).id,
                referenceId : slip.info.referenceId,
                rugNumber1 : slip.data.entrant.rugNumber
            }
            return r;
        }
        function multiReqAsm(slip,arr){
            var refs = getMultiRefReq(arr);
            return {
                sportWagers : refs.sportRef,
                raceEventWagerRequests : refs.raceRef,
                amount : util.setAmount(slip.info.amount).toCents,
                fixedOdds : slip.info.odds,
                referenceId : slip.info.referenceId
            }

            function getMultiRefReq(arr){
                var sportRef = [];
                var raceRef = [];
                arr.forEach(function(idx){
                    if($rootScope.slips[idx].info.type === 'SPORT'){
                        sportRef.push(sportReqAsm($rootScope.slips[idx]));
                    }
                    if($rootScope.slips[idx].info.type === 'RACING'){
                        raceRef.push(multiRaceReqAsm($rootScope.slips[idx]));
                    }
                });

                return {
                    sportRef : sportRef,
                    raceRef : raceRef
                }

            };

        }
        function multiRequestAssembler(multiSlips, pendingSlips, slipsReq){
            //Assemble multi slips
            for(var i = 0; i < multiSlips.length ; i++){
                if(!multiSlips[i].info.isConfirm){
                    continue;
                }

                // multibet
                if(i === 0){
                    var arr = util.flatten(multiSlips[i].info.multiRef);
                    var multiSlip = multipAsm(arr, multiSlips[i].info.odds, multiSlips[i].info.amount);
                    multiSlip.info.multiRefSlips = getMultiRef(arr);
                    slipsReq.multiReqs.push(multiReqAsm(multiSlip, arr));
                    pendingSlips.push(multiSlip);

                }

                // Doubles, tribles, fourpicks, bigsix, .....
                if(i > 0){
                    multiSlips[i].info.multiRef.forEach(function(arr){
                        var multiSlip = multipAsm(arr, multiSlips[i].info.odds, multiSlips[i].info.amount);
                        multiSlip.info.multiRefSlips = getMultiRef(arr);
                        slipsReq.multiReqs.push(multiReqAsm(multiSlip, arr));
                        pendingSlips.push(multiSlip);

                    });
                }
            }
        }
        function sportRaceRequestAssembler(slips, slipsReq){
            //Assemble sport and racing
            for(var i = 0 ; i < slips.length; i++){
                if(slips[i].info.isConfirm){
                    if(slips[i].info.type === 'SPORT'){
                        slipsReq.sportReqs.push(sportReqAsm(slips[i]));
                    }
                    if(slips[i].info.type === 'RACING' || slips[i].info.type === 'EXOTICS'){
                        isTopsport(slips[i])
                        ? slipsReq.raceReqs.fixedOddsWagerRequests.push(raceReqAsm(slips[i]))
                        : slipsReq.raceReqs.toteWagerRequests.push(raceReqAsm(slips[i]));
                    }

                }
            }
        }
        function toPendingSlips(slips, multiPenddingSlips, pendingSlips){
            //migrate sport and racing to pending
            var i = 0;
            while(i < slips.length){
                if(slips[i].info.isPending){
                    pendingSlips.push(slips[i]);
                    slips.splice(i,1);
                    continue;
                }
                i++;
            }

            //migrate multi to pending
            i = 0;
            while(i < multiPenddingSlips.length){
                pendingSlips.push(multiPenddingSlips[i]);
                multiPenddingSlips.splice(i,1);
            }
        }
        function betslipsSubmit(slipsReq, pendingSlips){
            if (slipsReq.sportReqs && slipsReq.sportReqs.length > 0) {
                sportWager(slipsReq.sportReqs)
                    .then(sportWagerRes, sportWagerRej);

            }

            if (slipsReq.raceReqs && (slipsReq.raceReqs.fixedOddsWagerRequests.length > 0 || slipsReq.raceReqs.toteWagerRequests.length > 0)) {
                raceWager(slipsReq.raceReqs)
                    .then(raceWagerRes, raceWagerRej);
            }

            if (slipsReq.multiReqs && slipsReq.multiReqs.length > 0) {
                multiWager(slipsReq.multiReqs)
                    .then(multiWagerRes, multiWagerRej);
            }

            function sportWagerRes(res){
                if (res.data.success) { // expected or normal case
                    wagerIdHandler(res.data.responses, pendingSlips);
                }else{ // all failed case
                    wagerIdHandler(slipsReq.sportReqs, pendingSlips, res.data);
                }
            }
            function sportWagerRej(){}
            function raceWagerRes(res){
                if (res.data.success) { // expected or normal case
                    wagerIdHandler(res.data.responses, pendingSlips);
                }else{ // all failed case
                    wagerIdHandler(slipsReq.raceReqs.fixedOddsWagerRequests, pendingSlips, res.data);
                    wagerIdHandler(slipsReq.raceReqs.toteWagerRequests, pendingSlips, res.data);
                }
            }
            function raceWagerRej(){}
            function multiWagerRes(res){
                if (!res.data) {}
                if (res.data.success) { // expected or normal case
                    wagerIdHandler(res.data.responses, pendingSlips);
                }else{ // all failed case
                    wagerIdHandler(slipsReq.multiReqs, pendingSlips, res.data);
                }

            }
            function multiWagerRej(){}
        }
        function sportWager(requests){
            return $http.post(EVN.apiDataUrl + '/data/wager/submitSportWagers', {
                sportWagers: requests
            });
        }
        function sportWagerStatus(wagerId){
            return $http.get(EVN.apiDataUrl + '/data/wager/sportWagerStatus/' + wagerId);
        }
        function multiWager(requests){
            return $http.post(EVN.apiDataUrl + '/data/wager/submitMultibetWagers', {
                multibetWagers: requests
            });
        }
        function betslipUpdate(slip){
            if(slip && slip.info.wagerId){
                if(slip.info.type === 'SPORT'){
                    sportWagerStatus(slip.info.wagerId)
                        .then(sportWagerStatusRes,sportWagerStatusRej);
                }
                if(slip.info.type === 'RACING'){
                    raceWagerStatus(slip.info.wagerId)
                        .then(raceWagerStatusRes,raceWagerStatusRej);
                }
                if(slip.info.type === 'EXOTICS'){
                    raceWagerStatus(slip.info.wagerId)
                        .then(raceWagerStatusRes,raceWagerStatusRej);
                }
                if(slip.info.type === 'MULTI'){
                    sportWagerStatus(slip.info.wagerId)
                        .then(sportWagerStatusRes,sportWagerStatusRej);
                }

            }


            function sportWagerStatusRes(res){
                if(!res){
                    return;
                }
                wagerHandler(slip, res.data.sportWager);
            }
            function sportWagerStatusRej(res){}
            function raceWagerStatusRes(res){
                if(!res){
                    return;
                }
                wagerHandler(slip, res.data.wager);
            }
            function raceWagerStatusRej(res){}
        }


        /* wager resolved handler */
        function wagerIdHandler(resSlips, slips, res){
            resSlips.forEach(function (resSlip) {
                slips.forEach(function (slip) {
                    if (slip.info.referenceId === resSlip.referenceId) {
                        if(resSlip.wagerId){
                            slip.info.wagerId = resSlip.wagerId;
                            slip.info.code = 'N';
                            slip.info.msg = 'Submitting';
                        }else{
                            failedHandler(slip, resSlip, res);
                        }
                    }
                });
            });
        }
        function wagerHandler(slip, resSlip){
            if(!slip && !resSlip){
                return;
            }

            switch (resSlip.status.key) {
                /* New Slip */
                case 'N': // new betslip
                case 'SIP': //Pending Acceptance
                    break;


                    /* Resolved */
                case 'S': //Bet Accepted
                    acceptedHandler(slip, resSlip);
                    break;
                case 'W': // Won
                    successHandler(slip, resSlip);
                    break;
                case 'L': // Lost
                case 'CN': //cancelled
                    break;


                    /* Failed  */
                case 'E': // Error confirmation in progress
                case 'F': // Failed
                    failedHandler(slip, resSlip);
                    break;

                    /* network Error */
                case 'NE':
                    break;

                default: // unrecognised synonym
                    break;

            }

        }
        function failedHandler(slip, resSlip, res){
            if(!resSlip){
                // wager failed for some reason ( e.g  no wagerId  or no response ...)
                slip.info.code = 'F';
                slip.info.msg = 'Failed';
            }else{
                // wager failed with proper error message (e.g  odds change, partially accepted, system error)
                if(resSlip.status){
                    slip.info.code = resSlip.status.key;
                    slip.info.msg = resSlip.status.value;
                }else{
                    slip.info.code = 'F';
                    slip.info.msg = res?res.errorMessage:'Failed';
                }

                if (resSlip.errorMsg || resSlip.errorMessage) {
                    slip.info.msg = resSlip.errorMsg || resSlip.errorMessage;
                    if (slip.info.msg.indexOf('Odds changed') != -1) {
                        // odds change
                        oddsChangeHandler(slip, resSlip);
                    }
                }
            }
        }
        function acceptedHandler(slip, resSlip){
            slip.info.code = resSlip.status.key;
            slip.info.msg = resSlip.status.value;
            if(slip.info.amount > util.setAmount(resSlip.amount).toDollars){
                partialHandler(slip, resSlip);
            }
        }
        function successHandler(slip, resSlip){
            slip.info.code = resSlip.status.key;
            slip.info.msg = resSlip.status.value;
        }
        function oddsChangeHandler(slip, resSlip){
            var newOdds = slip.info.msg.replace(/^(.*)New odds:/g, '').trim();
            if (!isNaN(newOdds)) {
                slip.info.oldOdds = slip.info.odds;
                slip.info.odds = parseFloat(newOdds);
                if(parseFloat(newOdds) === slip.info.oldOdds){
                    priceRefresher(slip);
                }
            }
        }
        function partialHandler(slip, resSlip){
            slip.info.code = 'PS';
            slip.info.msg  = 'Partially accepted bet : $'+ util.setAmount(resSlip.amount).toDollars;
            slip.info.msg += ' Refunded: $'+ (slip.info.amount - util.setAmount(resSlip.amount).toDollars).toFixed(2);
        }

        /* multi functions helper */
        function getMultiRef(arr){
            if(angular.isArray(arr) && arr.length > 0 ){
                var res = [];
                arr.forEach(function(idx){
                    res.push(angular.copy($rootScope.slips[idx]));
                });
                return res;
            }else{
                return [];
            }
        }
        function getCombination(n, k) {
            var listSize = 0;
            var combinationsCount = 0;
            var list = [];
            var set = [];

            //generate list
            for (var i = 0; i < n; i++) {
                list.push(i);
            }

            //get combination
            listSize = list.length,
            combinationsCount = (1 << listSize);
            for (var i = 1; i < combinationsCount; i++) {
                var combination = [];
                for (var j = 0; j < listSize; j++) {
                    if ((i & (1 << j))) {
                        combination.push(list[j]);
                    }
                }
                if (combination.length == k) {
                    set.push(combination);
                }
            }

            if(k === 1){ // only for winning all the combination
                set = util.flatten(set);

                return [set];
            }else{
                return set;
            }

        }
        function getCombinationOdds(coordinates, eventsArr) {
            var sum = 0;
            var isTBD = false;
            for(var i = 0; i < coordinates.length ; i++){
                var oddsMulti = 1;
                for(var j = 0; j < coordinates[i].length ; j++){
                    var slip = eventsArr[coordinates[i][j]];
                    if(isOddsVal(slip.info.odds)){
                        oddsMulti *= getSlipOdds(slip);
                    }else{
                        return 'TBD';
                    }
                }

                sum += oddsMulti;
            }

            return util.floorToFixed(sum, 3);
        }
        function isOddsVal(odds){
            if(!isNaN(odds) && angular.isNumber(odds)){
                return true;
            }else{
                return false;
            }
        }
        function getSlipOdds(slip){
            switch(slip.info.type){
                case 'RACING':
                    return slip.info.odds/EVN.RACE_ODDS_PRECISION;
                    break;
                case 'SPORT':
                    return slip.info.odds/EVN.SPORT_ODDS_PRECISION;
                    break;
                default:
                    return 0;
            }

        }

        /* racing */
        function hassFlexi(slip){
            return slip.type === 'EXOTICS'
                && (slip.data.pool.poolTypeId == 'T'
                || slip.data.pool.poolTypeId == 'FF'
                || slip.data.pool.poolTypeId == 'QD');

        }
        function isExotics(slip){
            return slip.info.type === "EXOTICS";
        }
        function isTopsport(slip){
            return slip.data.pool.bookmaker.id == "TopSport" && (slip.data.pool.poolTypeId == "P" || slip.data.pool.poolTypeId == "W");
        }
        function getPoolName(poolTypeId, bookmaker) {
            switch(poolTypeId){
                case 'W' :
                    if(bookmaker !== 'TopSport'){
                        return {
                                name : 'TOTE - WIN',
                                abbr : 'WIN'
                            }
                    }else{
                        return {
                                name : 'FIXED ODDS - WIN',
                                abbr : 'WIN'
                            }
                    }
                case 'P':
                    if(bookmaker !== 'TopSport'){
                        return {
                            name : 'TOTE - PLACE',
                            abbr : 'PLACE'
                        }
                    }else{
                        return {
                            name : 'FIXED ODDS - PLACE',
                            abbr : 'PLACE'
                        }
                    }
                case 'BT1W':
                    return {
                        name : 'MID TOTE WIN',
                        abbr : 'WIN'
                    }
                case 'BT1P':
                    return {
                        name : 'MID TOTE PLACE',
                        abbr : 'PLACE'
                    }
                case 'BT3W':
                    return {
                        name : 'BEST TOTE WIN',
                        abbr : 'WIN'
                    }
                case 'BT3P':
                    return {
                        name : 'BEST TOTE PLACE',
                        abbr : 'PLACE'
                    }
                case 'BT3SP':
                    return {
                        name : 'BEST TOTE + SP',
                        abbr : 'WIN'
                    }
                case 'TF':
                    return {
                        name : 'TOP FLUC',
                        abbr : 'WIN'
                    }
                case 'WT5':
                    return {
                        name : 'TOTE - WIN',
                        abbr : 'WIN'
                    }
                case 'PT5':
                    return {
                        name : 'TOTE - PLACE',
                        abbr : 'PLACE'
                    }
                case 'Q':
                    return {
                        name : 'Quinella',
                        abbr : 'Q'
                    }
                case 'E':
                    return {
                        name : 'Exacta',
                        abbr : 'E'
                    }
                case 'T':
                    return {
                        name : 'Trifecta',
                        abbr : 'T'
                    }
                case 'QD':
                    return {
                        name : 'Quadrella',
                        abbr : 'QD'
                    }
                case 'FF':
                    return {
                        name : 'First Four',
                        abbr : 'FF'
                    }
            }

        }
        function getRaceOddsObj(entrant, pool){
            return util.getRaceOddsObj(entrant, pool);
        }
        function raceWager(requests){
            return $http.post(EVN.apiDataUrl + '/data/wager/race', {
                toteWagerRequests: requests.toteWagerRequests,
                fixedOddsWagerRequests: requests.fixedOddsWagerRequests
            });
        }
        function raceWagerStatus(wagerId){
            return $http.get(EVN.apiDataUrl + '/data/wager/race/' + wagerId);
        }
        function racePriceRefresher(raceId){
            return $http.put(EVN.priceRefresherUrl + '/data/race/'+raceId).then(function (res) {
                return res.data;
            });
        }

        /* Sport */
        function pinnaclePriceRefresher(priceId){
            return $http.put(EVN.priceRefresherUrl + '/data/sport/price/pinnacle/'+priceId).then(function (res) {
                return res.data;
            });
        }
        function topsportpriceRefresher(eventId){
            return $http.put(EVN.priceRefresherUrl  + '/data/sport/event/topsport/'+eventId).then(function (res) {
                return res.data;
            });
        }



    }

})();
