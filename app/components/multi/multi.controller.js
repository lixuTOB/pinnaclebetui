(function(){
    'use strict';

    angular
        .module('pinnacleBetApp')
        .controller('multiCtrl', multiCtrl);

    multiCtrl.$inject = ['$scope','$rootScope','$stateParams','multiService','util','$http','betslipService'];

    function multiCtrl($scope,$rootScope, $stateParams, multiService, util, $http, betslipService){
        $scope.hashCode = util.hashCode;
        $scope.leagueId = $stateParams.leagueId;
        $scope.marketId = $stateParams.marketId;
        $scope.eventId = $stateParams.eventId;
        $scope.duration = $stateParams.duration;
        $scope.sportDurt = [];
        $scope.sportAll = {};
        $scope.events = {
            sportEvents  : [],
            marketTypes  : [],
            marketSw     : true,
            sportType    : '',
            isReady      : true
        };
        $scope.topMarkets = {};
        $scope.eventMarket = {};
        $scope.getSportPriceDSP = util.getSportPriceDSP;
        $scope.multiEventMarket = multiEventMarket;
        $scope.multiEventMarketSwitch = multiEventMarketSwitch;
        $scope.addSlip = addSlip;
        $scope.getDurt = sportDurtHandler;
        $scope.getMarket = sportMarketHandler;
        $scope.getTime = util.getTime;

        util.sidebarView('MULTI',$rootScope);
        mainHandler();

        function mainHandler(){
            util.scrollTop();
            if($scope.eventId){
                sportMarketHandler($scope.eventId);
            }
            if($scope.leagueId){
                sportLeagueHandler($scope.leagueId,$scope.marketId);
            }
            if($scope.duration){
                sportDurtHandler($scope.duration);
            }
        }
        function sportDurtHandler(duration){
            multiService.multiDuration('ALL', duration)
                .then(multiDurtRes, multiDurtRej);
            /*
            *   @resolve   sportDurtRes
            *   @reject    sportDurtRej
            */
            function multiDurtRes(res){
                if(!res.data.sportEvents){
                    return ;
                }
                $scope.sportDurt = res.data.sportEvents;
            }
            function multiDurtRej(res){}
        }
        function sportLeagueHandler(leagueId, marketId){
            if(marketId){
                multiService.getMultiAMarkets(leagueId, marketId)
                    .then(sportEventRes, sportEventRej);
            }else{
                multiService.getMultiAllEvents(leagueId)
                    .then(sportEventRes, sportEventRej);
            }

            /*
            *   @resolve   sportEventRes
            *   @reject    sportEventRej
            */
            function sportEventRes(res){
                if(!res.data.sportEvents || !res.data.topMarkets){
                    return;
                }


                $scope.events.sportEvents  = res.data.sportEvents;
                $scope.events.marketTypes  = res.data.marketTypes;
                $scope.events.sportType    = res.data.id;
                $scope.events.isReady      = true;
                $scope.topMarkets          = res.data.topMarkets;

            }
            function sportEventRej(res){}
        }
        function sportMarketHandler(eventId){
            multiService.getMultiAllMarkets(eventId)
                .then(sportMarketRes, sportMarketRej);
            /*
            *   @resolve   sportDurtRes
            *   @reject    sportDurtRej
            */
            function sportMarketRes(res){
                if(!res.data.markets || !res.data.event){
                    return;
                }
                $scope.eventMarket = {
                    markets : res.data.markets,
                    event   : res.data.event,
                    isReady : true
                };

            }
            function sportMarketRej(res){}
        }

        function addSlip(price){
            var slip = {
                data : price,
                type : 'SPORT',
                odds : price.price,
                oddsId : price.id,
                isMulti: true
            };
            betslipService.addSlip(slip);
        }
        function multiEventMarket(marketId){
            $scope.marketId = marketId;
            if($scope.leagueId){
                sportLeagueHandler($scope.leagueId,$scope.marketId);
            }
        }
        function multiEventMarketSwitch(){
            $('#multi-detail .multi-markets-panel .multi-collapsable').toggle('slow');
            $scope.events.marketSw ? $scope.events.marketSw = false : $scope.events.marketSw = true;
        }


    }

})();
