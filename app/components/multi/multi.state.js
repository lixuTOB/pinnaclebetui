(function() {
    'use strict';

    angular
        .module('pinnacleBetApp')
        .config(multiConfig);

    multiConfig.$inject = ['$stateProvider'];

    function multiConfig($stateProvider) {
        $stateProvider
            .state('multi',{
                url : "/multi?duration",
                views : {
                    'content' : {
                        templateUrl: "components/multi/multi.html",
                        controller: "multiCtrl"
                    }
                }
            })
            .state('multileagueDetail', {
                url : "/multileagueDetail?leagueId&marketId",
                views : {
                    'content' : {
                        templateUrl: "components/multi/multiLeague.html",
                        controller: "multiCtrl"
                    }
                }
            })
            .state('multiDetail', {
                url : "/multiDetail?eventId",
                views : {
                    'content' : {
                        templateUrl: "components/multi/multiDetail.html",
                        controller: "multiCtrl"
                    }
                }
            });
    }

})();
