(function(){
    'use strict';

    angular
        .module('pinnacleBetApp')
        .factory('multiService', multiService);

    multiService.$inject = ['util','$http','EVN'];

    function multiService(util, $http, EVN){
        var service = {
            multiDuration       : multiDuration,
            getMultiAllEvents   : getMultiAllEvents,
            getMultiAMarkets    : getMultiAMarkets,
            getMultiAllMarkets  : getMultiAllMarkets
        }
        return service;

        function multiDuration(sportType, duration){
            return $http.get(EVN.apiDataUrl + '/data/sport/topSport/'+sportType+'/sportEvents/duration/'+duration);
        }
        function getMultiAllEvents(leagueId){
            return $http.get(EVN.apiDataUrl + '/data/sport/topMarkets/'+leagueId);
        }
        function getMultiAMarkets(leagueId, marketId){
            return $http.get(EVN.apiDataUrl + '/data/sport/topMarkets/'+leagueId+'/'+marketId);
        }
        function getMultiAllMarkets(eventId){
            return $http.get(EVN.apiDataUrl + '/data/sport/markets/' + eventId);
        }

    }
})();
