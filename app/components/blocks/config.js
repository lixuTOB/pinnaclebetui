(function(){
    'use strict';

    angular
        .module("pinnacleBetApp")
        .config(httpConfig)
        .constant("EVN", EVNConfig())
        .run(globleConfig);

    httpConfig.$inject = ['$urlRouterProvider', '$httpProvider'];
    globleConfig.$inject = ['$rootScope'];

    function httpConfig($urlRouterProvider,$httpProvider) {
        $urlRouterProvider.otherwise('/');

        // cross origin with credentials
        $httpProvider.defaults.withCredentials = true;


        // nav-bar-fixed
        (function(){
            $(document).ready(function(){
                $(window).scroll(function(){
                    if(parseInt($(document).scrollTop()) > 150){
                        $('div#nav-bar-fixed').addClass('sticky');
                    }else{
                        $('div#nav-bar-fixed').removeClass('sticky');
                    }

                });
            });
        })();
    }

    function globleConfig($rootScope){
        $rootScope.accountInfo = new accountInfo();
        $rootScope.slips = [];
        $rootScope.sidebarStatus = new sidebarStatus();
		$rootScope.navbarStatus = new navbarStatus();

        function accountInfo(){
            this.username;
            this.password;
            this.accountHolderDto;
            this.isLogin;
            this.errorMessage;
        }
        function sidebarStatus(){
            this.status = 'HOME';
            this.liveBetting = true;
            this.quickLink = true;
            this.sportBar =true;
            this.multisBar =true;
            this.raceBar = false;
            this.myAccount = false;
        }
		function navbarStatus(){
			this.sportTab = true;
			this.raceTab = true;
			this.multisTab = true;
			this.myAccountTab = false;
			this.liveTab = false;
		}

    }

    function EVNConfig(){
        return {
            logConf : 'dev',
            apiDataUrl : 'https://uatwww.bettortest.com',
            // apiDataUrl : 'https://www-v1.pinnaclebet.com.au',
            // apiDataUrl : 'http://localhost:8080/webclienttv',
            priceRefresherUrl : 'https://price.bettortest.com/',
            // priceRefresherUrl : 'http://localhost:8080/pricerefreshernv/',
      			mobileUrl : 'https://uatm.bettortest.com',
            racingUpdateInterval: 10000,
            pusherConf: {
          	    key : "108b4e84e9486e475d04",
          	    // key : "33f84a5f3f43001d0eb4",
          	    options : {
          	        encrypted : true
          	    }
          	},
            racing_enabled: true,
            sports_enabled: true,
            pinnacle_sports_enabled: true,
            debug_mode: true,

            //New Message channels
            AccountWalletEvent: 'AccountWalletEvent',
            WagerEvent: 'WagerEvent',
            RaceUpdateEvent: 'RaceUpdateEvent',
            RacePoolUpdateEvent: 'RacePoolUpdateEvent',
            RaceEntrantUpdateEvent: 'RaceEntrantUpdateEvent',
            SportEventUpdateEvent: 'SportEventUpdateEvent',

            //profession view
            pro_ser_addr: 'https://devprof.bettortest.com/#/',

            //server_addr: "wss://localhost",
            server_addr: "wss://devmessage.bettortest.com",

            account_wager_event_server_addr: "https://uatwww.bettortest.com:52001",
            account_race_wager_event_server_addr: "https://uatwww.bettortest.com:52002",
            account_wallet_event_server_addr: "https://uatwww.bettortest.com:52003",
            sport_market_list_update_event_server_addr: "https://uatwww.bettortest.com:51901",
            sport_bookmaker_market_update_event_server_addr: "https://uatwww.bettortest.com:51902",
            sport_update_event_server_addr: "https://uatwww.bettortest.com:51903",
            race_tobracing_exotics_server_addr: "https://uatwww.bettortest.com:51801",
        	race_tobracing_multileg_server_addr: "https://uatwww.bettortest.com:51802",
        	race_tobracing_place_server_addr: "https://uatwww.bettortest.com:51803",
        	race_tobracing_win_server_addr: "https://uatwww.bettortest.com:51804",
        	race_topsport_exotics_server_addr: "https://uatwww.bettortest.com:51805",
        	race_topsport_place_server_addr: "https://uatwww.bettortest.com:51806",
        	race_topsport_win_server_addr: "https://uatwww.bettortest.com:51807",
        	race_update_event_server_addr: "https://uatwww.bettortest.com:51808",
        	meeting_update_event_server_addr: "https://uatwww.bettortest.com:51809",

        	BETSLIP_MAX_UPDATE_TIME: 10, // betslip wager status
	        BETSLIP_MAX_UPDATE_INTERVAL: 5000, // betslip wager status
	        SERVER_TIME_DIFF: 0,
	        websiteCssMainFrameWidth: 992,
	        BOOKMAKERNAME: "TopSport",
	        BET_SUBMIT: "Submitting Bets...",
	        color_update_interval: 30000,
	        color_test_interval: 1000,
	        panel_refersh_interval: 60000,
	        WS_RECONN_INTERVAL: 5000000,
	        bsbLength: 6,
	        passwordMinLength: 8,
	        validMobileNumberMin: 9,
	        validMobileNumberMax: 14,
	        MIN_AMOUNT_BET: 1, // the minimum amount of a bet
	        MIN_INCREMENT_BET: 0.5, // the minimum increment of a bet
	        FIX_DECIMAL: 2, // the fixed decimal
	        ODDS_PRECISION: 1000, // change 2 digit into 3
	        ODDS_PRECISION_SPORT: 100,
	        MIN_ODDS: 1, // minimum odds
	        SINGLE_SLIP: 1,
	        MONEY_PRECISION: 100,
	        ONEMILLISECOND: 1000,
            SPORT_ODDS_PRECISION: 1000,
	        RACE_ODDS_PRECISION: 100,
	        dateTimeDisplayformat: 'hh:ii:ss a dd-MM-yy', // date & time display
	        dateTimeformat: 'hh:ii a dd-MM-yy',
	        dateTimeDisplayformatTxTable: 'dd-mm-yy hh:ii:ss ', // date & time display
	        cookie_session_time: 1440,
	        trasactionPagingLimit: 20,
	        dollarSign: "$",
	        cookie_name_availableFunds: 'availableFunds',
	        cookie_name_username: 'username',
	        cookie_name_user_loggedin: 'userLogged',
	        pleaseLogInErrorMsg: "Please login",
	        alphabetRequiredError: " use only letters",
	        alphaNumericError: " should be more than 8 characters (alphanumeric)",
	        numericValError: " should be a number",
	        firstNameRequired: "First name required",
	        lastNameRequired: "Last name required",
	        mobileNumberRequiredError: "Mobile number required",
	        mobileNumberInvalidError: "Mobile number must be atleast 10 digits",
	        emailRequiredError: "Email required",
	        emailInvalidError: "Email address invalid",
	        emailsDoesMatch: "Emails do not match",
	        emailAlreadyinUse: "Email already in use",
	        passwordRequired: "Password required",
	        usernameRequired: "Username required",
	        usernameAlreadyUsed: "Username already in use",
	        requiredErrorMsg: " required",
	        amountValError: " eg.100",
	        bsbValError: "Please enter a 6 digit BSB number",
	        suburbRequiredError: "Suburb required",
	        streetNumberRequiredError: "Street number required",
	        streetNameRequiredError: "Street name required",
	        postCodeRequiredError: "Postcode required",
	        stateRequired: "State required",
	        dobRequired: "Select date of birth",
	        passwordMismatch: "Passwords don't match",
	        oldPasswordWrongMsg: "Old password was incorrect",
	        amountMinError: " should be more than the minimum of $ 50" ,
	        amountMaxPoliError: "Please enter a deposit amount less than $ 59" ,
	        msgbalance: "Your account balance is insufficient to proceed.",
	        msgMinimum: "Each amount must be greater than or equal to $1.00",
	        msgIncrements: "Each amount must be in increments of $0.50, e.g. $1.00, $1.50, $2.00",
	        FLEXI_MIN_AMOUNT: 0.1,
	        FLEXI_MIN_ERROR: "For the flexi bet, the minimum amount for each combination must be greater than $0.10",
	        TOTAL_AMOUNT_ERROR: "The total amount must be greater than $1.00, and it must be in increments of $1.00, e.g. $1.00, $2.00, $3.00, etc.",
	        TOTAL_COMBINATIONS_ERROR: "Please ensure there is at least one valid selection and submit again.",
	        reminding: "Please correct your input",
	        BET_CONFIRM_FAILED: "This bet is failed to process !",
	        BETS_FAILED: "All bets are failed to process !",
	        BETS_ERRORMSG: "Fail to process. Please try it later!",
	        ALL_SPORT_KEYWORD: "All",
	        sport_closing_interval: 0,
	        FIXED_WIN_ID: "W",
	        FIXED_WIN_TEXT: "Win",
	        FIXED_PLACE_ID: "P",
	        FIXED_PLACE_TEXT: "Place",
	        BEST_3_TOTES_ID: "BT3W",
	        BEST_3_TOTES_TEXT: "Best Tote",
	        MID_TOTES_WIN_ID: "BT1W",
	        MID_TOTES_WIN_TEXT: "Mid Tote",
	        MID_TOTES_PLACE_ID: "BT1P",
	        MID_TOTES_PLACE_TEXT: "Mid Tote",
	        TOTES_FIVEPCT_WIN_ID: "WT5",
	        TOTES_FIVEPCT_WIN_TEXT: "TOTE",
	        TOTES_FIVEPCT_PLACE_ID: "PT5",
	        TOTES_FIVEPCT_PLACE_TEXT: "TOTE",
	        TOP_FLUC_ID: "TF",
	        TOP_FLUC_TEXT: "Top Fluc",
	        BEST_START_PRICE_ID: "BT3SP",
	        BEST_START_PRICE_TEXT: "Best Tote + SP",
	        QUINELLA_ID: "Q",
	        QUINELLA_TEXT: "Quinella",
	        EXACTA_ID: "E",
	        EXACTA_TEXT: "Exacta",
	        TRIFECTA_ID: "T",
	        TRIFECTA_TEXT: "Trifecta",
	        FIRST_FOUR_ID: "FF",
	        FIRST_FOUR_TEXT: "First Four",
	        QUADRELLA_ID: "QD",
	        QUADRELLA_TEXT: "Quadrella",
	        DAILY_DOUBLE_ID: "DD",
	        DAILY_DOUBLE_TEXT: "Daily Double",
	        TOPSPORT: "TopSport",
	        NSW_TOTE: "N",
	        UNITAB_TOTE: "U",
	        STAB_TOTE: "V",
	        FIXED_ODDS: "Fixed Odds",
	        TOTES_STAB: "Tote",
	        TOTES: "Tote Exotics",
	        NO_THIRD_DIVIDEND: "NTD",
	        NO_SECOND_DIVIDEND: "NSD",
	        NO_DIVIDEND: "1.00",
	        TOBRACING: "TobRacing",
	        AJAX_TIMEOUT: 30000,
	        NO_EVENTS_AVAILABLE_MSG: "No events available"

        }
    }

})();
