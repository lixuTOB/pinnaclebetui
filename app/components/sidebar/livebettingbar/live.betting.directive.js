/* globals $ */
(function() {
    'use strict';

    angular
        .module('pinnacleBetApp')
        .directive('liveBettingBar', LiveBettingBar);
    
    LiveBettingBar.$inject = ['sidebarService'];
    
    function LiveBettingBar (sidebarService) {
        var directive = {
            restrict: 'E',
            templateUrl: 'components/sidebar/livebettingbar/livebetting.html',
            link: linkFunc
        };

        return directive;

        /* private helper methods*/

        function linkFunc(scope, iElement) {
            scope.liveEvents = [];
            scope.quickLinks = [];
            scope.sportTypes = [];

            
            sidebarService.liveEvents().then(function(res){
                if(res && res.data){
                    scope.liveEvents = res.data.events;
                }
            });
            sidebarService.quickLinks().then(function(res){
                if(res && res.data){
                    scope.quickLinks = res.data.types;            
                }
            });
            sidebarService.sportTypes().then(function(res){
                if(res && res.data){
                    scope.sportTypes = res.data.typeAndLeagueMap;    
                }
            });
        }
    }
})();
