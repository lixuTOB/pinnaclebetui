(function(){
    'use strict';

    angular
        .module("pinnacleBetApp")
        .factory("sidebarService", sidebar);

    sidebar.$inject = ['$http', 'EVN'];

    function sidebar($http, EVN){
        var service = {
            liveEvents: liveEvents,
            quickLinks: quickLinks,
            sportTypes: sportTypes,
            allRaces  : allRaces,
            multisSportType : multisSportType,
            sportLeagueList : sportLeagueList
        }

        return service;

        function liveEvents(){
            return $http.get(EVN.apiDataUrl + '/data/pntsport/liveevents');
        }
        function quickLinks(){
            return $http.get(EVN.apiDataUrl + '/data/pntsport/quickLink');
        }
        function sportTypes(){
            return $http.get(EVN.apiDataUrl + '/data/pntsport/sportEventTypesAndLeagues');
        }
        function allRaces(){
            return $http.get(EVN.apiDataUrl + '/data/racing/allraces');
        }
        function multisSportType(){
            return $http.get(EVN.apiDataUrl + '/data/sport/sportEventTypes');
        }
        function sportLeagueList(sportType){
            return $http.get(EVN.apiDataUrl + '/data/sport/sportEventTypes/' + sportType +'/sportEventsList');
        }

    }

})();
