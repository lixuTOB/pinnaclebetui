/* globals $ */
(function() {
    'use strict';

    angular
        .module('pinnacleBetApp')
        .directive('sideBar', sideBar);

    sideBar.$inject = ['EVN', 'util', '$interval'];

    function sideBar ( EVN, util, $interval) {
        var directive = {
            restrict: 'E',
            scope : {
                sidebarStatus : '='
            },
            templateUrl: 'components/sidebar/sidebar.html',
            link: linkFunc
        };

        return directive;

        function linkFunc($scope, el, attrs){

        }
    }
})();
