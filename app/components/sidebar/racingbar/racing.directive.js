/* globals $ */
(function() {
    'use strict';

    angular
        .module('pinnacleBetApp')
        .directive('racingBar', racingBar);

    racingBar.$inject = ['sidebarService', 'EVN', 'util', '$interval'];

    function racingBar (sidebarService, EVN, util, $interval) {
        var directive = {
            restrict: 'E',
            templateUrl: 'components/sidebar/racingbar/racing.html',
            link: linkFunc
        };

        return directive;

        function linkFunc($scope, el, attrs){
            $scope.filterVenue = filterVenue;
            $scope.selectedVenue = 'All Venues';
            $scope.typeSwitch = typeSwitch;
            $scope.filterType = filterType;
            $scope.activeFilter = {
                R: true,
                G: true,
                T: true
            };
            var eventsLastUpdate = util.getTime().msec();
            var typeFilterButtonGroup = el.find('.type_filter_buttonGroup');
            var veneuFilter = el.find('.selVenue');
            var allRacesContainer = el.find('#next-to-race');
            var activeFilter = {
                R: true,
                G: true,
                T: true
            };
            var selectedVenue = 'All Venues';
            var venueList = [];
            var payingRaces = [];
            var nonePayingRaces = [];


            init();

            function init(){
                updateAllRaces(true);


            }
            /* Get next to jump races */
            function updateAllRaces(now){
            	// stop timer
            	if (angular.isDefined($scope.timer)) {
                	$interval.cancel($scope.timer);
                }
            	var currentTime = util.getTime().msec();
                var dt = currentTime - eventsLastUpdate;

                if (dt > EVN.racingUpdateInterval || now) {
                	sidebarService.allRaces().then(function (res) {
                		var data = res.data;
                        if (data && data.races) {
                            setRaces(data.races);
                        }
                    });
                }
                // start timer
                $scope.timer = $interval(updateAllRaces,
            		EVN.racingUpdateInterval);
            }

            function setRaces(races){
            	payingRaces = [];
                nonePayingRaces = [];
                $scope.allRaces = []; // re-populate our list

                for (var i = 0; i < races.length; i++) {
                    if( races[i].status == 'Open'){
                        var tempRace = races[i];
                        var raceToAppend = {
                            id: tempRace.id,
                            number: tempRace.raceNumber,
                            venue: tempRace.venue,
                            startTime: tempRace.startTime,
                            status: tempRace.status,
                            results: tempRace.results,
                            startTimeDisplay: getTextDisplay(tempRace),
                            statusClass: getNTJStatusClass(tempRace),
                            type: tempRace.type,
                            url: "/racing/meeting/" + tempRace.meetingId + "/" + tempRace.id,
                            typeClass: getRaceTypeClass(tempRace.type),
                            venueDisplay: 'R' + tempRace.raceNumber + ', ' + tempRace.venue,
                            meetingId: tempRace.meetingId
                        }
                        appendRace(raceToAppend);
                        venueList.push(raceToAppend.venue);
                    }
                }
                $scope.venueList = util.removeDup(venueList);
            }

            function getRaceTypeClass(type) {
            	return (type === 'R' ? 'horse' : (type === 'G' ? 'hound' : (type === 'T' ? 'carriage' : '')));
            }




            function updateRace(race, matchedElement) {
                var matchedRaceIndex = undefined;
                for (var i = 0; i < nonePayingRaces.length; i++) {
                    if (nonePayingRaces[i].id == race.id) {
                        matchedRaceIndex = i;
                        break;
                    }
                }
                if (matchedRaceIndex) {
                    var matchedRace = nonePayingRaces[matchedRaceIndex];
                    var statusChanges = matchedRace.status == "Open" && race.status != "Open";
                    if (race.status == 'Paying') {
                        payingRaces.push(matchedRace);
                        nonePayingRaces.splice(matchedRaceIndex, 1);
                    }
                    matchedRace.results = race.results;
                    matchedRace.startTimeDisplay = getTextDisplay(race);
                    matchedRace.status = race.status;
                    matchedRace.statusClass = getNTJStatusClass(race);
                    matchedElement.find('.ntj_time > a').text(matchedRace.startTimeDisplay);
                    matchedElement.find('.ntj_time > a').attr('class', matchedRace.statusClass);
                }
            }

            function filterVenue(race){
                if($scope.selectedVenue == 'All Venues'){
                    return true;
                }else if($scope.selectedVenue == race.venue){
                    return true;
                }else{
                    return false;
                }
            }

            function filterType(race){
                if($scope.activeFilter[race.type]){
                    return true;
                }
            }
            function typeSwitch(type){
                $scope.activeFilter[type]
                    ?  $scope.activeFilter[type] = false
                    :  $scope.activeFilter[type] = true;
            }

            function appendRace(race) {
            	race.displayCss = "true";
                if (!activeFilter[race.type] || !venueMatch(race.venue)) {
                    race.displayCss = "false";
                }
                $scope.allRaces.push(race);
                if (race.status == "Paying") {
                    payingRaces.push(race);
                } else {
                    nonePayingRaces.push(race);
                }
            }

            function venueMatch(venue) {
                return selectedVenue == 'All Venues' || selectedVenue == venue;
            }

            function getTextDisplay(tempRace) {
                var textDisplay = '';
                /* For Open races
                 * If jump in more than an hour, diplay 'Open'.
                 * If jump in an hour, diplay the remaining time to jump.
                 * If jump in 30 mins, diplay the remaining time to jump with class 'Closing30'.
                 * If jump in 5 mins, diplay the remaining time to jump with class 'Closing5'.
                 */
                if (tempRace.status == 'Open') {
                    var startTime = util.timeToStartText(tempRace.startTime);
                    if (startTime.match(/Min/g)) {
                        var startValue = startTime.replace('Min', '');
                        // if(startValue <= 5 || startValue.match(/</g)) {
                        // 	tempRace.statusClass = "Closing5";
                        // }
                        // else if(startValue > 5 && startValue <= 30) {
                        // 	tempRace.statusClass = "Closing30";
                        // }
                        startTime = startValue + ' m';
                    }
                    textDisplay = startTime;
                }
                /* For Paying races, try to display the first four result. */
                else if (tempRace.status == 'Paying') {
                    if (tempRace.results == "")
                        textDisplay = "Paying";
                    else
                        textDisplay = tempRace.results;
                }
                /* For Abandoned races, display 'Aband.' */
                else if (tempRace.status == 'Abandoned') {
                    textDisplay = 'Aband.';
                }
                /* For other cases, display the race status */
                else {
                    textDisplay = tempRace.status;
                }
                return textDisplay;
            }

            function getNTJStatusClass(race) {
                if (race.status == 'Open') {
                    var startTime = util.timeToStartText(race.startTime);
                    var statusClass = 'Open';
                    if (startTime.match(/Min/g)) {
                        var startValue = startTime.replace('Min', '');
                        if (startValue <= 3 || startValue.match(/</g)) {
                            statusClass = "blink_1s";
                        }
                    }
                    return statusClass;
                } else {
                    return race.status;
                }
            }

            $scope.$on("$destroy", function () {
                typeFilterButtonGroup.find('.type_filter_button').off('click');
                veneuFilter.find('li').off('click');
                // clearTimeout(updateStartTimeIntervalID);
                // clearTimeout(allRacesIntervalID);
                veneuFilter.find('.dropdown-menu').children().remove();
            });


        }
    }
})();
