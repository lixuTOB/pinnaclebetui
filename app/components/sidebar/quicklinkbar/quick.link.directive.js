(function() {
    'use strict';

    angular
        .module('pinnacleBetApp')
        .directive('quickLinkBar', quickLinkBar);

    quickLinkBar.$inject = ['sidebarService'];

    function quickLinkBar (sidebarService) {
        var directive = {
            restrict: 'E',
            templateUrl: 'components/sidebar/quicklinkbar/quickLink.html',
            link: linkFunc
        };

        return directive;

        /* private helper methods*/

        function linkFunc(scope, iElement) {
            scope.nameMap = nameMap();
            scope.quickLinks = [];
            sidebarService.quickLinks().then(function(res){
                if(res && res.data){
                    scope.quickLinks = res.data.types;
                }
            });

            function nameMap(){
                return {
                    'National Rugby League'         : 'NRL Matches',
                    'Aussie Rules Football (AFL)'   : 'AFL Matches',
                    'England - Premier League'      : 'EPL Matches',
                    'Super 18 Rugby'                : 'Super 18 Matches',
                    'NBA'                           : 'NBA Matches',
                    'NCAA'                          : 'NCAA Matches',
                    'UEFA - Champions League'       : 'UEFA Champions League',
                    'Australia - A League'          : 'A-League Matches',
                    'NFL'                           : 'NFL Matches',
                    'NHL OT Included Alternates'    : 'NHL Matches',
                    'MLB'                           : 'MLB Matches'
                }
            }
        }
    }
})();
