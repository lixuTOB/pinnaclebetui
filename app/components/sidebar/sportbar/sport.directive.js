(function() {
    'use strict';

    angular
        .module('pinnacleBetApp')
        .directive('sportBar', sportBar);

    sportBar.$inject = ['sidebarService'];

    function sportBar (sidebarService) {
        var directive = {
            restrict: 'E',
            templateUrl: 'components/sidebar/sportbar/sportbar.html',
            link: linkFunc
        };

        return directive;

        /* private helper methods*/

        function linkFunc(scope, iElement) {
            scope.sportTypes = {};
            sidebarService.sportTypes().then(function(res){
                if(res && res.data){
                    scope.sportTypes.leagues = res.data.typeAndLeagueMap;
                    scope.sportTypes.types = res.data.types;
                }
            });
        }
    }
})();
