(function() {
    'use strict';

    angular
        .module('pinnacleBetApp')
        .directive('multisBar', multisBar);

    multisBar.$inject = ['sidebarService'];

    function multisBar (sidebarService) {
        var directive = {
            restrict: 'E',
            templateUrl: 'components/sidebar/multisbar/multisbar.html',
            link: linkFunc
        };

        return directive;

        /* private helper methods*/

        function linkFunc(scope, iElement) {
            scope.map = {};
            scope.multisSportType = {};
            sidebarService.multisSportType().then(function(res){
                if(res && res.data){
                    scope.multisSportType.types = res.data.types;
                    for(var i = 0 ; i <scope.multisSportType.types.length; i++){
                        scope.map[scope.multisSportType.types[i].id] = '';
                    }
                }
            });

            scope.getLeagueList = getLeagueList;
            function getLeagueList(selectedSportType){
                scope.multisSportLeague = {};
                sidebarService.sportLeagueList(selectedSportType).then(function(res){
                    if(res && res.data){
                        scope.multisSportLeague.leagues = res.data.sportParentEvents;
                        scope.map[selectedSportType] = scope.multisSportLeague.leagues;

                    }
                });
            }



        }


    }
})();
