(function(){
    'use strict';

    angular
        .module('pinnacleBetApp')
        .factory('headerService', headerService);

    headerService.$inject = ['$http','EVN'];

    function headerService($http,EVN){
        var service = {
            accountLogin : accountLogin,
            bonusTransfer : bonusTransfer
        }
        return service;

        function accountLogin(username,password){
            return $http.post(EVN.apiDataUrl + '/data/security/signIn',{
                username : username,
                password : password
            })
        }

        function bonusTransfer(bonusWalletId){
            return $http.post(EVN.apiDataUrl + '/data/banking/transfers/bonuswallet',{
                bonusWalletId : bonusWalletId
            })
        }
    }

})();
