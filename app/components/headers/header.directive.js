(function(){
    'use strict';

    angular
        .module('pinnacleBetApp')
        .directive('mastHead', mastHead)
        .directive('navBarFixed', navBarFixed)
        .directive('topBar', topBar)
        .directive('navBarPanel', navBarPanel);

    mastHead.$inject = ['$rootScope','$compile','headerService','accountService','messagingService','util'];
    navBarFixed.$inject = ['$rootScope','$compile','headerService','util'];
    topBar.$inject = ['$rootScope','$compile','headerService'];
    navBarPanel.$inject = ['$rootScope','$compile','headerService'];

    function mastHead($rootScope,$compile,headerService,accountService,messagingService,util){


        var directive = {
            restrict: 'A',
            templateUrl: 'components/headers/masthead/masthead.html',
            link: linkFunc
        };

        return directive;



        /* private helper methods*/

        function linkFunc(scope, element) {
            scope.accountInfo = $rootScope.accountInfo;
            scope.manulLogin = manulLogin;
            scope.setAmount = util.setAmount;
            scope.getTime = util.getTime;
            scope.logout = logout;
            scope.amountShow = amountShow;
            scope.amountHide = amountHide;
            scope.bonusSwitch = bonusSwitch;
            scope.bonusTransfer = bonusTransfer;
            scope.bonusDone = bonusDone;
            scope.bonus = {
                msg : '',
                voucher : undefined,
                voucher_switch : false,
                bonus_wallet : undefined,
                bonus_wallet_switch : false
            };
            autoLogin();

            function bonusDone(){
                scope.bonus.bonus_wallet = undefined;
                scope.bonus.bonus_wallet_switch = false;
                scope.bonus.voucher = undefined;
                scope.bonus.voucher_switch = false;
            }
            function bonusSwitch(){
                scope.bonus.voucher_switch ? scope.bonus.voucher_switch = false :   scope.bonus.voucher_switch = true;
                scope.bonus.bonus_wallet_switch ? scope.bonus.bonus_wallet_switch = false : scope.bonus.bonus_wallet_switch = true;
            }
            function bonusInit(accountInfo){
                if(accountInfo.accountHolderDto && accountInfo.accountHolderDto.bonusBets && accountInfo.accountHolderDto.bonusBets.length > 0){
                    scope.bonus.voucher = accountInfo.accountHolderDto.bonusBets[0].statusId == 'AVAILABLE'? accountInfo.accountHolderDto.bonusBets[0] : undefined;
                    scope.bonus.bonus_wallet =
                        accountInfo.accountHolderDto.bonusBets[0].statusId == 'WALLET_IN_USE' ||
                        accountInfo.accountHolderDto.bonusBets[0].statusId == 'TRANSFER_PA'
                            ? accountInfo.accountHolderDto.bonusBets[0] : undefined;
                }else{
                    scope.bonus.voucher = undefined;
                    scope.bonus.bonus_wallet = undefined;
                }
            }
            function bonusTransfer(walletId){
                if(walletId){
                    headerService.bonusTransfer(walletId).then(function(res){
                        if(res.data){
                            // failed case
                            if(res.data.errorMessage){
                                scope.bonus.msg = res.data.errorMessage;
                                $('#masthead .bonus-section .bonus-panel .msg').show();
                                setTimeout(function(){
                                    $('#masthead .bonus-section .bonus-panel .msg').fadeOut("slow", function(){
                                        scope.bonus.msg = '';
                                    });
                                }, 5000);
                            }

                            // successful vase
                            if(res.data.bonusBet){
                                $rootScope.accountInfo.accountHolderDto.bonusBets[0] = res.data.bonusBet;
                                scope.bonus.bonus_wallet = res.data.bonusBet;
                            }

                        }
                        // updating scope
                        if (!scope.$root.$$phase) {
                            scope.$apply();
                        }
                    });
                }
            }
            function autoLogin(){
                if(window.localStorage.username != "undefined" && window.localStorage.password != "undefined" &&
                    window.localStorage.username && window.localStorage.password){
                    scope.accountInfo.username = window.localStorage.username;
                    scope.accountInfo.password = window.localStorage.password;
                    login(window.localStorage.username, window.localStorage.password);
                }
            }

            function manulLogin(){
                login(scope.accountInfo.username,scope.accountInfo.password);
            }

            function login(username,password){
                accountService.login(username, password, $rootScope);
            }
            function logout(){
                accountService.logout($rootScope);

            }
            function removeLocalStorage(){
                window.localStorage.setItem("username","undefined");
                window.localStorage.setItem("password","undefined");
            }

            function amountShow(){
                scope.ifShow = true;
            }

            function amountHide(){
                scope.ifShow = false;
            }

            scope.$watch('accountInfo', function(nv, ov){
                if(nv){
                    if(nv.errorMessage){
                        $('#masthead .login-msg').show();
                        setTimeout(function(){
                            $('#masthead .login-msg').fadeOut("slow", function(){
                                scope.accountInfo.errorMessage = '';
                            });
                        }, 5000);
                    }else{
                        $('#masthead .login-msg').hide();
                    }

                    //account message
                    subscribeAccount();

                    //get Bonus
                    bonusInit(nv);
                }
            }, true);

            scope.$on('$destroy', function(){
                unSubscribeAccount();
            });

            /* messageing section */
            function subscribeAccount() {
                if($rootScope.accountInfo.accountHolderDto){
                    if($rootScope.accountInfo.accountHolderDto.wallet){
                        messagingService.subscribe('AccountWalletEvent', $rootScope.accountInfo.accountHolderDto.wallet.id, function(data){
                            $rootScope.accountInfo.accountHolderDto.wallet.balance = data.balance;
                            $rootScope.accountInfo.accountHolderDto.wallet.availableFunds = data.availableFunds;
                            $rootScope.accountInfo.accountHolderDto.wallet.openbets = data.openbets;
                            $rootScope.accountInfo.accountHolderDto.wallet.transactionId = data.transactionId;
                            scope.accountInfo = $rootScope.accountInfo;
                            if(!scope.$root.$$phase){
                                scope.$apply();
                            }
                        });
                    }
                    //bonue account
                    if($rootScope.accountInfo.accountHolderDto.bonusBets && $rootScope.accountInfo.accountHolderDto.bonusBets.length > 0){
                        messagingService.subscribe('AccountWalletEvent', $rootScope.accountInfo.accountHolderDto.bonusBets[0].id, function(data){
                            $rootScope.accountInfo.accountHolderDto.bonusBets[0] = data;
                            scope.accountInfo = $rootScope.accountInfo;
                            if(!scope.$root.$$phase){
                                scope.$apply();
                            }
                        });
                    }

                }

            }
            function unSubscribeAccount() {
                if($rootScope.accountInfo.accountHolderDto){
                    if($rootScope.accountInfo.accountHolderDto.wallet){
                        messagingService.unsubscribe('AccountWalletEvent', $rootScope.accountInfo.accountHolderDto.wallet.id);
                    }
                    if($rootscope.accountInfo.accountHolderDto.bonusBets.length > 0){
                        messagingService.unsubscribe('AccountWalletEvent', $rootScope.accountInfo.accountHolderDto.bonusBets[0].id);
                    }
                }
            }

        }

    }
    function navBarFixed($rootScope,$compile,headerService,util){
        var directive = {
            restrict: 'A',
            templateUrl: 'components/headers/navbarfixed/navbarfixed.html',
            link: linkFunc
        };

        return directive;

        /* private helper methods*/

        function linkFunc(scope, element) {
            scope.slips = $rootScope.slips;
            scope.accountInfo = $rootScope.accountInfo;
            scope.setAmount = util.setAmount;
            scope.betslip = betslip;

            function betslip() {
                $("#wrapper").toggleClass("toggled");
            }
        }
    }
    function topBar($rootScope,$compile,headerService){

        var directive = {
            restrict: 'A',
            templateUrl: 'components/headers/topbar/topbar.html',
            link: linkFunc
        };

        return directive;

        /* private helper methods*/

        function linkFunc(scope, element) {
            scope.localTime = localTime();
            setInterval(function(){
                scope.localTime = localTime();
            }, 60000);
            function localTime(){
                var d = new Date();
                var timeZone = d.toString().split("GMT")[1].split(" (")[1].split(")")[0];
                var withoutSecond = d.toString().split("GMT")[0].split(":")[0] + ':' + d.toString().split("GMT")[0].split(":")[1];
                var fulltime = withoutSecond + ' ' + timeZone;
                return fulltime;
            }
        }

    }
    function navBarPanel($rootScope,$compile,headerService){
        var directive = {
            restrict: 'A',
            templateUrl: 'components/headers/navbarpanel/navbarpanel.html',
            link: linkFunc
        };

        return directive;

        /* private helper methods*/

        function linkFunc(scope, element) {
            scope.slips = $rootScope.slips;
            scope.betslip = betslip;
            scope.activeTab = 'home';
            scope.activeTabFun =activeTabFun;

            function activeTabFun(activeTab){
                scope.activeTab = activeTab;
            }

            function betslip() {
                $("#wrapper").toggleClass("toggled");
            }
        }
    }
})();
