(function () {
	'use strict';

    angular
        .module('pinnacleBetApp')
        .factory('mainService', mainService);

    mainService.$inject = ['$window', '$location', '$stateParams', '$timeout','$sce', 'EVN'];

    function mainService($window, $location, $stateParams, $timeout, $sce, EVN)
    {
        var modalCount = 0;
        var self = this;

        var activeTab = 0;
        var isRacingOpen = false;
        var isSportsOpen = false;
        var racing_menu_position = '';
        var sports_menu_position = '';
        var isBetSlipPaneDisplay = false;
        var isPinnacleBetSlipPaneDisplay = false;
        var pinnacle_sports_menu_enabled = false;
        var sports_menu_enabled = false;
        var racing_menu_enabled = false;
        var next_sports_enabled = false;
        var next_to_jump_enabled = false;
        var isHttpLoading = false;
        var userAccount = undefined;
        var loginInfo = {
    		username: undefined,
    		password: undefined
        };

        var PRACopyright = '';

        this.modalOn = function () {
            modalCount++;
            if (modalCount > 0) {
                isHttpLoading = true;
            };
        };

        this.modalOff = function () {
            modalCount--;
            if (modalCount <= 0) {
                var tempID = $timeout(function () {
                    if (modalCount <= 0) {
                        isHttpLoading = false;
                    };
                    $timeout.cancel(tempID);
                }, 50);
            };
        };

        this.sessiontimeout = function (path) {
            userAccount = undefined;
            loginInfo.username = undefined;
            loginInfo.password = undefined;
            if (path)
                $location.path(path);
        }

        this.handleResponse = function (res, isSilence) {
            if (res.errorCode && res.errorCode == 1) {
                userAccount = undefined;
                loginInfo.username = undefined;
                loginInfo.password = undefined;
                $location.path('/');
                if (isSilence) {
                } else {
                    self.notify('Please login again');
                }
                return false;
            } else if (res.errorMessage && res.errorMessage.trim() != "") {
                if (isSilence) {
                } else {
                    self.notify(res.errorMessage, 'ERROR');
                }
                return false;
            } else if (res.success) {
                return true;
            } else {
                return false;
            }
        }

        this.redirectTo = function (path) {
            $location.path(path);
        }

        this.getClientBrowser = function () {
            var ua = $window.navigator.userAgent,
                tem,
                M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
            if (/trident/i.test(M[1])) {
                tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
                return 'IE ' + (tem[1] || '');
            }
            if (M[1] === 'Chrome') {
                tem = ua.match(/\bOPR\/(\d+)/)
                if (tem != null) return 'Opera ' + tem[1];
            }
            M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
            if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);
            return M.join(' ');
        }

        /* Check if it is a mobile device.
         * Use different meetings table template for responsive design.
         * Mobile device (window width less or equal than 992px) - meetingList-mobile.html template.
         * Desktop device (window width larger than 992px) - meetingList-desktop.html template.
         */


        this.initViewConfig = function (path) {
            switch (path) {
                case "home":
                    activeTab = 0;
                    isRacingOpen = true;
                    isSportsOpen = true;
                    racing_menu_position = "left";
                    if (!racing_enabled)
                        sports_menu_position = "left";
                    else
                        sports_menu_position = "right";
                    isBetSlipPaneDisplay = true;
                    isPinnacleBetSlipPaneDisplay = false;
                    pinnacle_sports_menu_enabled = false;
                    sports_menu_enabled = true;
                    racing_menu_enabled = true;
                    next_sports_enabled = true;
                    next_to_jump_enabled = true;
                    break;
                case "pinnacle_sports_home":
                    if (!pinnacle_sports_enabled)
                        $location.path('/');
                    activeTab = 4;
                    isRacingOpen = true;
                    isSportsOpen = true;
                    racing_menu_position = "right";
                    sports_menu_position = "left";
                    isBetSlipPaneDisplay = true;
                    isPinnacleBetSlipPaneDisplay = true;
                    pinnacle_sports_menu_enabled = true;
                    sports_menu_enabled = false;
                    racing_menu_enabled = true;
                    next_sports_enabled = true;
                    next_to_jump_enabled = true;

                    break;
                case "pinnacle_sports_details":
                    if (!sports_enabled)
                        $location.path('/');
                    activeTab = 4;
                    isRacingOpen = false;
                    isSportsOpen = true;
                    racing_menu_position = "right";
                    sports_menu_position = "left";
                    isBetSlipPaneDisplay = true;
                    isPinnacleBetSlipPaneDisplay = true;
                    pinnacle_sports_menu_enabled = true;
                    sports_menu_enabled = false;
                    racing_menu_enabled = true;
                    next_sports_enabled = true;
                    next_to_jump_enabled = true;
                    break;
                case "sports_normal":
                    if (!sports_enabled)
                        $location.path('/');
                    activeTab = 1;
                    isRacingOpen = true;
                    isSportsOpen = true;
                    racing_menu_position = "right";
                    sports_menu_position = "left";
                    isBetSlipPaneDisplay = true;
                    isPinnacleBetSlipPaneDisplay = false;
                    pinnacle_sports_menu_enabled = false;
                    sports_menu_enabled = true;
                    racing_menu_enabled = true;
                    next_sports_enabled = true;
                    next_to_jump_enabled = true;
                    break;
                case "racing_home":
                    if (!racing_enabled)
                        $location.path('/');
                    activeTab = 2;
                    isRacingOpen = true;
                    isSportsOpen = true;
                    racing_menu_position = "left";
                    sports_menu_position = "right";
                    isBetSlipPaneDisplay = true;
                    isPinnacleBetSlipPaneDisplay = false;
                    pinnacle_sports_menu_enabled = false;
                    sports_menu_enabled = true;
                    racing_menu_enabled = true;
                    next_sports_enabled = true;
                    next_to_jump_enabled = true;
                    break;
                case "racing_details":
                    if (!racing_enabled)
                        $location.path('/');
                    activeTab = 2;
                    isRacingOpen = true;
                    isSportsOpen = false;
                    racing_menu_position = "left";
                    sports_menu_position = "right";
                    isBetSlipPaneDisplay = true;
                    isPinnacleBetSlipPaneDisplay = false;
                    pinnacle_sports_menu_enabled = false;
                    sports_menu_enabled = true;
                    racing_menu_enabled = true;
                    next_sports_enabled = true;
                    next_to_jump_enabled = true;
                    break;
                case "myaccount":
                    activeTab = 3;
                    isRacingOpen = false;
                    isSportsOpen = false;
                    racing_menu_position = "none";
                    sports_menu_position = "none";
                    isBetSlipPaneDisplay = false;
                    isPinnacleBetSlipPaneDisplay = false;
                    pinnacle_sports_menu_enabled = false;
                    sports_menu_enabled = false;
                    racing_menu_enabled = false;
                    next_sports_enabled = true;
                    next_to_jump_enabled = true;
                    break;
                case "signup":
                    activeTab = 3;
                    isRacingOpen = true;
                    isSportsOpen = true;
                    racing_menu_position = "none";
                    sports_menu_position = "none";
                    isBetSlipPaneDisplay = false;
                    isPinnacleBetSlipPaneDisplay = false;
                    pinnacle_sports_menu_enabled = false;
                    sports_menu_enabled = true;
                    racing_menu_enabled = true;
                    next_sports_enabled = true;
                    next_to_jump_enabled = true;
                    break;
                default:
                    break;
            }
        }

        this.getUserAccountDto = function () {
            return userAccount;
        }

        this.setUserAccountDto = function (userDto) {
            userAccount = getDtoJson(userDto);
        }

        this.isUserLogged = function () {
            return userAccount != undefined;
        };

        this.notify = function (message, type) {
            message = $sce.trustAsHtml(message);
            var duration = 3000;
            var template = EVN.apiDatUrl + '/static/angular-templates/notification-templates/notify.html';
            var styleClass = "alert-info";
            if (type == "ERROR")
                styleClass = "alert-danger";
            else if (type == "WARNING")
                styleClass = "alert-warning";
            else if (type == "SUCCESS")
                styleClass = "alert-success";
        }

        //Tempo fix for warning message for 2nd half bets, should be merged with notify or a generic model
        this.warning2ndHBets = function (message) {
            message = $sce.trustAsHtml(message);
            var duration = 15000;
            var template = EVN.apiDatUrl + '/static/angular-templates/notification-templates/warning2H.html';
            var styleClass = "alert-warning";
        }

        function getDtoJson(userDto) {
            var returnDto = {
                username: userDto.id,
                firstname: userDto.details.firstName,
                lastname: userDto.details.lastName,
                dob: userDto.details.dob,
                email: userDto.details.emailAddress,
                mobile: userDto.details.mobileNumber,
                countrycode: userDto.details.countryCode,
                areacode: userDto.details.areaCode,
                phone: userDto.details.phoneNumber,
                unitno: userDto.details.addressFlatNumber,
                streetno: userDto.details.addressStreetNumber,
                street: userDto.details.addressStreet,
                suburb: userDto.details.addressSuburb,
                postcode: userDto.details.addressPostcode,
                state: userDto.details.addressState,
                restrictions: userDto.restrictionTypes,
                country: userDto.details.addressCountry,
                accountno: userDto.details.accountNumber,

                walletid: userDto.wallet.id,
                balance: userDto.wallet.balance,
                availablefunds: userDto.wallet.availableFunds,
                openbets: userDto.wallet.openbets,
                withdrawalLockedAmount: userDto.wallet.withdrawalLockedAmount,
                ccDepositBasisPointFee: userDto.wallet.ccDepositBasisPointFee,
                kycStatus: userDto.wallet.kycStatus,
                ccDepositTotalAmount: userDto.wallet.ccDepositTotalAmount,
                transactionId: ''
            }
            if (userDto.bank) {
                returnDto.bank = userDto.bank.bank;
                returnDto.accountname = userDto.bank.accountName;
                returnDto.bsbnumber = userDto.bank.bsb;
                returnDto.accountnumber = userDto.bank.accountNumber;
                if (userDto.bank.ibanNumber)
                    returnDto.iban = userDto.bank.ibanNumber;
                if (userDto.bank.swiftCode)
                    returnDto.swiftcode = userDto.bank.swiftCode;
            }
            return returnDto;
        }

        return this;
	}

})();
