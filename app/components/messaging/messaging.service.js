(function(){
    'use strict';

    angular
        .module('pinnacleBetApp')
        .service('messagingService', messagingService);

    messagingService.$inject = ['$http', 'EVN'];

    function messagingService($http, EVN) {
        this.subscribe = subscribe;
        this.unsubscribe = unsubscribe;
        this.unsubscribeChannel = unsubscribeChannel;
        this.getLut = getLut;
        this.getEventNames = getEventNames;
        this.getChannelNames = getChannelNames;
        this.getAddrFromPool = getAddrFromPool;

        var channelInfos = {};
        var pusher = new Pusher(EVN.pusherConf.key, EVN.pusherConf.options);
        function subscribe(channelName, event, handler) {
            if (channelName == null || event == null || handler == null) {
                return;
            }

            var channelInfo = channelInfos[channelName];

            if (channelInfo == null || channelInfo.channel == null) {
                channelInfo = {};
                channelInfo.events = {};
                channelInfo.channel = pusher.subscribe(channelName);
                channelInfos[channelName] = channelInfo;
            }
            var channel = channelInfo.channel;

            if(!channelInfo.events[event]){
                channel.bind(event, function (data) {
                    channelInfo.events[event] = {
                        lut: new Date().getTime()
                    };
                    handler(data);
                });
                channelInfo.events[event] = {
                    lut: 0
                };
            }
        };
        function unsubscribe(channelName, event) {
            if (channelName == null || event == null) {
                return;
            }
            var channelInfo = channelInfos[channelName];
            if (channelInfo == null || channelInfo.channel == null) {
                return;
            }
            channelInfo.channel.unbind(event);
            delete channelInfo.events[event];
        };
        function unsubscribeChannel(channelName) {
            if (channelName == null) {
                return;
            }
            var channelInfo = channelInfos[channelName];
            if (channelInfo == null || channelInfo.channel == null) {
                return;
            }
            pusher.unsubscribe(channelName);
            delete channelInfos[channelName];
        }
        function getLut(channelName, event) {
            if (channelName == null || event == null) {
                return;
            }
            var channelInfo = channelInfos[channelName];
            if (channelInfo == null) {
                return;
            }
            if (!channelInfo.events.hasOwnProperty(event)) {
                return;
            }
            return channelInfo.events[event].lut || 0;
        };
        function getChannelNames() {
            return Object.keys(channelInfos);
        };
        function getAddrFromPool(arg) {};
        function getEventNames(channelName) {
            var channelInfo = channelInfos[channelName];
            if (channelInfo == null) {
                return [];
            }
            return Object.keys(channelInfo.events);
        };

    }

})();
