(function(){
    'use strict';

    angular
        .module('pinnacleBetApp')
        .directive('liveslip', liveslip)


    liveslip.$inject = ['$rootScope','liveslipService','util','betslipService'];

    function liveslip($rootScope, liveslipService, util,betslipService){
        var directive = {
            restrict: 'E',
            templateUrl: 'components/liveslip/liveslip.html',
            scope : {
                price : '='
            },
            link: linkFunc,
            controller: ctrlFunc

        }

        return directive;

        function ctrlFunc($scope, $element){

        }

        function linkFunc(scope, element){
            scope.oddsFromPrice = util.oddsFromPrice;
            scope.slip = {};
            scope.$watch('price',function(nv,ov){
                if(nv){
                    scope.slip = addLiveSlip(nv);
                }
            });
            function addLiveSlip(price){
                if(price && price.id){
                    var slip = {
                        data : price,
                        type : 'SPORT',
                        odds : price.price,
                        oddsId : price.id,
                        isMulti: false
                    };
                    var betslip = betslipService.betslipAsm(slip);
                    return betslip;
                }
            }
        }
    }


})();
