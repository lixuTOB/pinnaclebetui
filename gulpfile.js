var gulp        = require('gulp');
var webserver   = require('gulp-webserver');
var concat      = require('gulp-concat');
var jshint      = require('gulp-jshint');
var uglify      = require('gulp-uglify');
var size        = require('gulp-size');
var uglifycss   = require('gulp-uglifycss');
var imagemin    = require('gulp-imagemin');
var htmlmin     = require('gulp-htmlmin');
var runSequence = require('run-sequence');
var ngAnnotate  = require('gulp-ng-annotate');
var usemin      = require('gulp-usemin');
var htmlreplace = require('gulp-html-replace');
var inject      = require('gulp-inject');
var browserify  = require('gulp-browserify');
var livereload  = require('gulp-livereload');
var rename      = require('gulp-rename');
var notify      = require('gulp-notify');
var server      = require('tiny-lr')();
var exec        = require('gulp-exec');







var config = {
    vendor : {
        scripts : [
            "app/bower_components/jquery/dist/jquery.js",
            "app/bower_components/bootstrap/dist/js/bootstrap.js",
            "app/bower_components/angular/angular.js",
            "app/bower_components/angular-bootstrap/ui-bootstrap-tpls.js",
            "app/bower_components/angular-ui-router/release/angular-ui-router.js"
        ],
        styles : [
            "app/bower_components/bootstrap/dist/css/bootstrap.min.css"
        ]
    },
    app : {
        scripts : [],
        styles : []
    }
}

gulp.task('appjs',function(){
    return gulp.src('app/components/**/*.js')
    //   .pipe(jshint('.jshintrc'))
      .pipe(jshint.reporter('default'))
      .pipe(concat('app.js'))
      .pipe(rename({ suffix: '.min' }))
      .pipe(uglify({
          mangle : false
      }))
      .pipe(livereload(server))
      .pipe(gulp.dest('dist/'))
      .pipe(notify({ message: 'Scripts task complete' }));

});

gulp.task('appcss', function(){
    return gulp.src('app/styles/**/*.css')
        .pipe(concat('app.css'))
        .pipe(uglifycss())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('dist/'));
});

gulp.task('vendorjs',function(){
    return gulp.src(config.vendor.scripts)
        .pipe(concat('vendor.js'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('dist/'));

});

gulp.task('vendorcss', function(){
    return gulp.src(config.vendor.styles)
        .pipe(concat('vendor.css'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('dist/'));
});


gulp.task('html-injection',function(){
    return gulp.src('app/index.html')
        .pipe(notify({ message: 'html task start' }))
        .pipe(htmlreplace({
            'appjs': 'app.min.js',
            'appcss': 'app.min.css'
            // 'vendorjs': 'vendor.min.js',
            // 'vendorcss': 'vendor.min.css',
        }))
        .pipe(gulp.dest('dist/'))
        .pipe(notify({ message: 'html task complete' }));
});

gulp.task('html',function(){
    return gulp.src('app/components/**/*.html')
        .pipe(gulp.dest('dist/components'))
});

gulp.task('prod-server', function() {
  gulp.src('./dist')
    .pipe(webserver({
      port: 3000,
      livereload: false,
      directoryListing: false,
      open: true
    }));
});


gulp.task('webserver', function() {
  gulp.src('./app')
    .pipe(webserver({
      port: 3000,
      livereload: false,
      directoryListing: false,
      open: true
    }));
});


gulp.task('set-to-tomcat', function(){
    var options = {
        continueOnError: false,
        pipeStdout: false,
        customTemplationThing: "copy to the tomcat"
    }

    gulp.src('.')
        .pipe(exec('cp -r /Users/tob/Desktop/li/pinnaclebet/pinnaclebetUI/app/components /Users/tob/Desktop/li/workspace/pinnaclebet.com.au/webclienttv/src/main/webapp/', options))
        .pipe(exec('cp -r /Users/tob/Desktop/li/pinnaclebet/pinnaclebetUI/app/styles /Users/tob/Desktop/li/workspace/pinnaclebet.com.au/webclienttv/src/main/webapp/', options))
        .pipe(exec('cp /Users/tob/Desktop/li/pinnaclebet/pinnaclebetUI/app/index.html /Users/tob/Desktop/li/workspace/pinnaclebet.com.au/webclienttv/src/main/webapp/WEB-INF/view/newlayout.jsp', options));

});

gulp.task('get-from-tomcat', function(){
    var options = {
        continueOnError: false,
        pipeStdout: false,
        customTemplationThing: "copy from the tomcat"
    }

    gulp.src('.')
        .pipe(exec('cp -r /Users/tob/Desktop/li/workspace/pinnaclebet.com.au/webclienttv/src/main/webapp/components /Users/tob/Desktop/li/pinnaclebet/pinnaclebetUI/app/' , options))
        .pipe(exec('cp -r /Users/tob/Desktop/li/workspace/pinnaclebet.com.au/webclienttv/src/main/webapp/styles /Users/tob/Desktop/li/pinnaclebet/pinnaclebetUI/app/ ', options))
        .pipe(exec('cp /Users/tob/Desktop/li/workspace/pinnaclebet.com.au/webclienttv/src/main/webapp/WEB-INF/view/newlayout.jsp /Users/tob/Desktop/li/pinnaclebet/pinnaclebetUI/app/index.html ', options));

});


gulp.task('prod', function(){
    runSequence('appjs','appcss','vendorjs','vendorcss','html','html-injection','prod-server');
});



gulp.task('dev', function(){
    runSequence('webserver');

});


gulp.task('default', function() {
    runSequence('scripts','styles','webserver');
});
